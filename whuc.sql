-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 01:05 PM
-- Server version: 5.6.43
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whuc`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration-user`
--

CREATE TABLE `registration-user` (
  `reg_id` int(11) NOT NULL,
  `participate_in` text,
  `participate_as` text,
  `company_name` text,
  `contact_name` text NOT NULL,
  `job_title` text,
  `office_telephone` int(11) DEFAULT NULL,
  `mobile_no` int(11) DEFAULT NULL,
  `email` text,
  `gender` tinyint(1) DEFAULT NULL COMMENT '1->Male, 2->Female',
  `dob` date DEFAULT NULL,
  `company_business_nature` text,
  `profile_img` text,
  `address_line1` text,
  `address_line2` text,
  `city` text,
  `state` text,
  `postal_code` int(11) DEFAULT NULL,
  `country` text,
  `nationality` text,
  `passport_no` varchar(150) DEFAULT NULL,
  `hajj_pilgrims_annually` int(11) DEFAULT NULL,
  `hajj_umrah_organisers_assocciation_membership` tinyint(1) DEFAULT NULL,
  `british_hajj_umrah_council_membership` tinyint(1) DEFAULT NULL,
  `national_pilgrimage_organisers_association` tinyint(1) DEFAULT NULL,
  `payment_option` text,
  `umrah_pilgrims_annually` int(11) DEFAULT NULL,
  `hajj_people_membership` tinyint(1) DEFAULT NULL,
  `social_id_fb` text,
  `social_id_twitter` text,
  `social_id_instagram` text,
  `flag` tinyint(1) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration-user`
--

INSERT INTO `registration-user` (`reg_id`, `participate_in`, `participate_as`, `company_name`, `contact_name`, `job_title`, `office_telephone`, `mobile_no`, `email`, `gender`, `dob`, `company_business_nature`, `profile_img`, `address_line1`, `address_line2`, `city`, `state`, `postal_code`, `country`, `nationality`, `passport_no`, `hajj_pilgrims_annually`, `hajj_umrah_organisers_assocciation_membership`, `british_hajj_umrah_council_membership`, `national_pilgrimage_organisers_association`, `payment_option`, `umrah_pilgrims_annually`, `hajj_people_membership`, `social_id_fb`, `social_id_twitter`, `social_id_instagram`, `flag`, `add_date`, `ip`, `status`) VALUES
(1, 'London', 'Visitor', 'wrw', 'weqes', 'tyryfh', 353553, 75675758, 'ewsd@gmail.com', 1, '1993-12-03', 'dfdf', '', 'ete', 'wrsfdg', 'iuk', 'etry', 5346536, 'tetet', 'rtryry', 'e54tet6', 12, NULL, NULL, NULL, NULL, 12, NULL, '', '', '', 1, '2019-08-23 23:34:04', '127.0.0.1', 1),
(2, 'London', 'Visitor', 'wrw', 'weqes', 'tyryfh', 3465757, 75675758, 'pramodinidas100@gmail.com', 1, '1993-12-09', 'dfdf', 'rayen-lazghab-g02tbKeI4f4-unsplash.jpg', 'ete', 'wrsfdg', 'iuk', 'etry', 5346536, 'tetet', 'rtryry', 'e54tet6', 12, 1, 1, NULL, 'Bank Transfer', 12, 1, 'qwqed', 'qeads', 'ewrwt', 1, '2019-08-24 00:04:57', '127.0.0.1', 1),
(3, 'Paris', 'Sponsor', 'wrw', 'wrw', 'etet', 3465757, 75675758, 'pramodinidas100@gmail.com', 1, '1994-12-09', 'dfdf', 'photo-1511723991310-0e859b96b2e2.jpg', 'weews', 'et', 'rewtr', 'etry', 5346536, 'tetet', 'rtryry', 'e54tet6', 24, 1, 2, 1, 'Bank Transfer', 12, 1, 'qwqed', 'qeads', 'ewrwt', 1, '2019-08-24 00:06:54', '127.0.0.1', 1),
(4, 'Paris', 'Exhibitor', 'test', 'weqes', 'tyryfh', 3465757, 75675758, 'pramodinidas100@gmail.com', 1, '1998-12-09', 'refdf', 'beaded-1630493_1280.jpg', 'weews', 'et', 'iuk', 'etry', 5346536, 'tetet', 'ewrwfs', '2324re65657', 12, 1, 2, NULL, NULL, 23, NULL, '', '', '', 4, '2019-08-24 00:14:27', '127.0.0.1', 1),
(5, 'Paris', 'Exhibitor', 'wrw', 'pd', 'resd', 2324325, 12131, 'pramodinidas100@gmail.com', 2, '1996-12-09', 'dfdf', 'matt-artz-353227.jpg', 'ete', 'wrsfdg', 'rewtr', 'etry', 5346536, 'tetet', 'indian', '2324re65657', 12, 1, 2, 1, 'Credit Card', 12, 1, '', '', '', 2, '2019-08-23 18:52:39', '157.41.161.37', 1),
(6, 'Paris', 'Visitor', 'test', 'wrw', 'tyryfh', 3465757, 75675758, 'pramodinidas100@gmail.com', 1, '1993-12-09', 'refdf', 'dejan-zakic-5BD-HzHr_LI-unsplash.jpg', 'weews', 'et', 'iuk', 'etry', 5346536, 'tetet', 'rtryry', 'e54tet6', 12, 1, 1, 1, 'Except from Payment', 12, 1, '', '', '', 4, '2019-08-23 18:56:07', '157.41.161.37', 1),
(7, 'London', 'Visitor', 'wrw', 'eerfsf', 'etet', 353553, 353545, 'pramodinidas100@gmail.com', 1, '1290-12-03', 'refdf', 'beaded-1630493_1280.jpg', 'ete', 'et', 'rewtr', 'etry', 5346536, 'ewrsfs', 'rtryry', '2324re65657', 0, NULL, NULL, NULL, NULL, 0, NULL, '', '', '', 1, '2019-08-24 11:51:47', '157.41.29.43', 1),
(8, 'London', 'Sponsor', 'test', 'wrw', 'resd', 353553, 353545, 'we@gmail.com', 1, '2013-12-09', 'dfdf', 'christin-hume-NYLLXAiUEhw-unsplash.jpg', 'ete', 'et', 'iuk', 'oilk', 5346536, 'tetet', 'rtryry', 'e54tet6', 0, NULL, NULL, NULL, NULL, 0, NULL, '', '', '', 1, '2019-08-24 11:54:44', '157.41.29.43', 1),
(9, 'London', 'Exhibitor', 'Sachin More', 'Sachin More', 'consult', 2147483647, 2147483647, 'sachinmore16@gmail.com', 1, '1970-01-01', 'Technology', '', 'Shree Ravechi Apt, B304, Plot No 14/15, Sector 20,', 'Koparkhairane', 'Navi Mumbai', 'Maharashtra', 400709, 'India', 'Indian', 'B8741855221', 0, NULL, NULL, NULL, NULL, 0, NULL, '', '', '', 1, '2019-08-24 19:07:57', '49.32.13.182', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(111) NOT NULL,
  `fname` varchar(222) DEFAULT NULL,
  `lname` varchar(222) DEFAULT NULL,
  `agency_name` varchar(222) DEFAULT NULL,
  `city` varchar(222) DEFAULT NULL,
  `address` text,
  `country` varchar(222) DEFAULT NULL,
  `phone` varchar(222) DEFAULT NULL,
  `email` varchar(222) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1-> active 2-> inactive',
  `user_type` tinyint(1) DEFAULT '3' COMMENT '1->supper admin 2-> admin 3->SSTA ',
  `is_questionnaire` tinyint(4) DEFAULT '0' COMMENT '1->if questionnaire done,0->if questionnaire not done',
  `is_solution` int(11) NOT NULL DEFAULT '0' COMMENT '0->Pending,1->Approved,2->Cancel,3->Request Sent to admin',
  `admin_view` int(222) DEFAULT '0' COMMENT 'id_admin-> view 0-> not view by admin',
  `add_date` datetime NOT NULL,
  `ip` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `fname`, `lname`, `agency_name`, `city`, `address`, `country`, `phone`, `email`, `password`, `status`, `user_type`, `is_questionnaire`, `is_solution`, `admin_view`, `add_date`, `ip`) VALUES
(1, 'ssta super', 'admin', NULL, 'bbsr', 'bbsr', 'india', NULL, 'sstasuperadmin@amadeus.ae', 'ssta@pass', 1, 1, NULL, 0, 0, '2016-11-10 15:19:23', '192.168.1.26'),
(2, 'ssta', 'admin', NULL, NULL, 'asdasd', 'india', '7873387489', 'sstaadmin@amadeus.ae', 'ssta@pass', 1, 2, NULL, 0, 0, '2016-11-14 07:42:19', '127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration-user`
--
ALTER TABLE `registration-user`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration-user`
--
ALTER TABLE `registration-user`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
