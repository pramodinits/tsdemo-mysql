-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 12:34 PM
-- Server version: 5.6.43
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilift_update`
--

-- --------------------------------------------------------

--
-- Table structure for table `breakdown`
--

CREATE TABLE `breakdown` (
  `id_breakdown` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `breakdown`
--

INSERT INTO `breakdown` (`id_breakdown`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'breakdown 1', 'test breakdown', 1, '2018-08-30 20:36:56'),
(2, 'breakdown 2', 'test 2 breakdown', 1, '2018-08-30 20:37:18'),
(3, 'Breakdown 3', 'Test break', 1, '2018-08-30 21:17:25'),
(4, 'Forklift Batteries Breakdown', 'forklift', 1, '2018-08-31 12:54:59'),
(5, 'Forklift Battery Chargers', 'Test Breakdown 4', 1, '2018-09-07 19:25:59'),
(6, 'test breakdown', 'check', 1, '2019-01-02 14:07:15'),
(7, 'Test breakdown2', 'Test', 1, '2019-01-02 14:08:35');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id_city` int(222) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `status` tinyint(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'Durban', 'Durban', 1, '2018-12-19 11:20:58'),
(2, 'Johannesburg', 'Johannesburg', 1, '2018-12-19 14:16:56'),
(3, 'Cape Town', 'Cape Town', 1, '2018-12-31 08:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id_item` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id_item`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'product 1', 'test product', 1, '2018-08-30 20:31:15'),
(2, 'Product2', 'test description', 1, '2018-08-30 20:33:14'),
(3, 'Product 3', 'Test product', 1, '2018-08-30 21:18:25'),
(4, 'Forklifts', 'f', 1, '2018-08-31 12:52:55'),
(5, 'test pr', 'test test test', 1, '2018-09-07 16:35:37'),
(6, 'Product 4', 'Test a new Product', 1, '2018-09-07 19:31:02'),
(7, 'Forklift Battery Chargers', 'FBC', 1, '2018-09-27 20:10:32'),
(8, 'product 5', 'test description', 1, '2019-01-02 14:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(111) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `id_city` int(111) DEFAULT NULL,
  `sendtype` int(2) DEFAULT NULL,
  `sendflg` smallint(2) DEFAULT NULL,
  `sendto` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `description`, `id_city`, `sendtype`, `sendflg`, `sendto`, `status`, `add_date`) VALUES
(1, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 3, 1, 88, 1, '2019-01-02 11:04:18'),
(2, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 3, 18, 1, 1, '2019-01-02 11:04:18'),
(3, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 11:05:53'),
(4, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 11:05:53'),
(5, 'Registration', 'You have registered successfully.', 3, 1, 1, 90, 1, '2019-01-02 12:37:20'),
(6, 'raka Registration', 'You have received a sign up request name :raka, Kindly approve.', 3, 1, 16, 1, 1, '2019-01-02 12:37:20'),
(7, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 12:45:50'),
(8, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 12:45:50'),
(9, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 12:52:52'),
(10, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 12:52:52'),
(11, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 12:54:44'),
(12, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 12:54:44'),
(13, 'Breakdown request closed', 'Breakdown service request closed.', 2, 3, 1, 47, 1, '2019-01-02 13:02:05'),
(14, 'Breakdown request closed', ' Breakdown service request closed.', 2, 3, 18, 1, 1, '2019-01-02 13:02:05'),
(15, 'Breakdown request closed', 'Breakdown service request closed', 2, 3, 8, 27, 1, '2019-01-02 13:02:05'),
(16, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 13:03:21'),
(17, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 13:03:21'),
(18, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-02 13:04:01'),
(19, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-02 13:04:01'),
(20, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-02 13:04:01'),
(21, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-02 13:20:57'),
(22, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-02 13:20:57'),
(23, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-02 13:20:57'),
(24, 'Breakdown request accepted', 'Your breakdown service request assigned to kisanunder sub.', 3, 3, 1, 88, 1, '2019-01-02 13:24:28'),
(25, 'Breakdown request assigned', ' Breakdown service request assigned to kisanunder sub.', 3, 3, 18, 1, 1, '2019-01-02 13:24:28'),
(26, 'Breakdown assigned', 'New breakdown service assigned', 3, 3, 8, 87, 1, '2019-01-02 13:24:28'),
(27, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-02 13:44:11'),
(28, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-02 13:44:11'),
(29, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-02 13:44:11'),
(30, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-02 14:15:44'),
(31, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-02 14:15:44'),
(32, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 87, 1, '2019-01-02 14:15:44'),
(33, 'Service Closed', 'Your service closed', 1, 2, 1, 48, 1, '2019-01-02 14:16:22'),
(34, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-02 14:16:22'),
(35, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-02 14:16:22'),
(36, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-02 14:16:56'),
(37, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-02 14:16:56'),
(38, 'Service Closed', 'Service Closed', 1, 2, 8, 75, 1, '2019-01-02 14:16:56'),
(39, 'Breakdown request closed', 'Breakdown service request closed.', 3, 3, 1, 88, 1, '2019-01-02 14:25:34'),
(40, 'Breakdown request closed', ' Breakdown service request closed.', 3, 3, 18, 1, 1, '2019-01-02 14:25:34'),
(41, 'Breakdown request closed', 'Breakdown service request closed', 3, 3, 8, 87, 1, '2019-01-02 14:25:34'),
(42, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-02 14:26:31'),
(43, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-02 14:26:31'),
(44, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-02 14:26:31'),
(45, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-02 14:27:20'),
(46, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-02 14:27:20'),
(47, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-02 14:27:20'),
(48, 'Breakdown request accepted', 'Breakdown service request assigned to Demo Executive.', 2, 3, 1, 48, 1, '2019-01-02 14:28:16'),
(49, 'Breakdown request assigned', ' Breakdown service request assigned to Demo Executive.', 2, 3, 18, 1, 1, '2019-01-02 14:28:16'),
(50, 'Breakdown request assigned', 'Breakdown service request assigned', 2, 3, 8, 58, 1, '2019-01-02 14:28:16'),
(51, 'Breakdown request closed', 'Breakdown service request closed.', 3, 3, 1, 88, 1, '2019-01-02 14:29:50'),
(52, 'Breakdown request closed', ' Breakdown service request closed.', 3, 3, 18, 1, 1, '2019-01-02 14:29:50'),
(53, 'Breakdown request closed', 'Breakdown service request closed', 3, 3, 8, 87, 1, '2019-01-02 14:29:50'),
(54, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-02 14:30:11'),
(55, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-02 14:30:11'),
(56, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-02 14:30:11'),
(57, 'New Service Request', 'You have posted a new Service Request', 3, 2, 1, 88, 1, '2019-01-02 15:33:53'),
(58, ' New Service Request', 'raj posted a new service request.', 3, 2, 18, 1, 1, '2019-01-02 15:33:53'),
(59, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 3, 1, 88, 1, '2019-01-02 15:36:13'),
(60, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 3, 18, 1, 1, '2019-01-02 15:36:13'),
(61, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-02 15:40:34'),
(62, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-02 15:40:34'),
(63, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-02 15:40:34'),
(64, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 1, 3, 1, 48, 1, '2019-01-02 15:55:03'),
(65, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 1, 3, 18, 1, 1, '2019-01-02 15:55:03'),
(66, 'Breakdown request assigned', 'Breakdown service request assigned', 1, 3, 8, 27, 1, '2019-01-02 15:55:03'),
(67, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 48, 1, '2019-01-02 16:00:20'),
(68, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-02 16:00:20'),
(69, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 27, 1, '2019-01-02 16:00:20'),
(70, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-05 08:25:30'),
(71, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-05 08:25:30'),
(72, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-05 08:25:30'),
(73, 'Registration', 'You have registered successfully.', 2, 1, 1, 92, 1, '2019-01-05 08:52:35'),
(74, 'chitta Registration', 'You have received a sign up request name :chitta, Kindly approve.', 2, 1, 16, 1, 1, '2019-01-05 08:52:35'),
(75, 'New Service Request', 'You have posted a new Service Request', 2, 2, 1, 92, 1, '2019-01-05 09:01:57'),
(76, ' New Service Request', 'chitta posted a new service request.', 2, 2, 18, 1, 1, '2019-01-05 09:01:57'),
(77, 'Service request accepted', 'Your service request assigned to JB.', 2, 2, 1, 92, 1, '2019-01-05 09:07:25'),
(78, 'Service request assigned', ' Service request assigned to JB.', 2, 2, 18, 1, 1, '2019-01-05 09:07:25'),
(79, 'Service assigned', 'New service assigned', 2, 2, 8, 91, 1, '2019-01-05 09:07:25'),
(80, 'Service Closed', 'Your service closed', 2, 2, 1, 92, 1, '2019-01-05 09:09:22'),
(81, 'Service Closed', ' Service closed', 2, 2, 18, 1, 1, '2019-01-05 09:09:22'),
(82, 'Service Closed', 'Service Closed', 2, 2, 8, 91, 1, '2019-01-05 09:09:22'),
(83, 'Breakdown Request', 'You have posted a new Breakdown Request', 2, 3, 1, 92, 1, '2019-01-05 09:12:23'),
(84, 'Breakdown Request', 'chitta posted a new breakdown request.', 2, 3, 18, 1, 1, '2019-01-05 09:12:23'),
(85, 'Breakdown request accepted', 'Your breakdown service request assigned to JB.', 2, 3, 1, 92, 1, '2019-01-05 09:12:43'),
(86, 'Breakdown request assigned', ' Breakdown service request assigned to JB.', 2, 3, 18, 1, 1, '2019-01-05 09:12:43'),
(87, 'Breakdown assigned', 'New breakdown service assigned', 2, 3, 8, 91, 1, '2019-01-05 09:12:43'),
(88, 'Breakdown Request Closed', 'Your breakdown request closed', 2, 3, 1, 92, 1, '2019-01-05 09:13:01'),
(89, 'Breakdown Request Closed', ' Breakdown request closed', 2, 3, 18, 1, 1, '2019-01-05 09:13:01'),
(90, 'Breakdown Request Closed', 'Breakdown request closed', 2, 3, 8, 91, 1, '2019-01-05 09:13:01'),
(91, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-07 08:51:53'),
(92, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-07 08:51:53'),
(93, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-07 08:51:53'),
(94, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-08 08:02:11'),
(95, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-08 08:02:11'),
(96, 'Quote Request', 'You have posted a new quote request', 1, 4, 1, 47, 1, '2019-01-11 08:56:27'),
(97, 'Quote Request', 'kisan posted a new quote request.', 1, 4, 20, 1, 1, '2019-01-11 08:56:27'),
(98, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 09:00:01'),
(99, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 09:00:01'),
(100, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 09:01:56'),
(101, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 09:01:56'),
(102, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 09:04:59'),
(103, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 09:04:59'),
(104, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 09:07:10'),
(105, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 09:07:10'),
(106, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 09:07:45'),
(107, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 09:07:45'),
(108, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-11 09:10:52'),
(109, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-11 09:10:52'),
(110, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-11 09:10:52'),
(111, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-11 09:38:44'),
(112, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-11 09:38:44'),
(113, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-11 09:38:44'),
(114, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-11 09:40:34'),
(115, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-11 09:40:34'),
(116, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-11 09:40:34'),
(117, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-11 09:41:58'),
(118, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-11 09:41:58'),
(119, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-11 09:41:58'),
(120, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-11 09:46:29'),
(121, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-11 09:46:29'),
(122, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-11 09:46:29'),
(123, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 09:52:25'),
(124, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 09:52:25'),
(125, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 09:53:14'),
(126, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 09:53:14'),
(127, 'Breakdown request accepted', 'Your breakdown service request assigned to service ex 3.', 1, 3, 1, 49, 1, '2019-01-11 09:56:39'),
(128, 'Breakdown request assigned', ' Breakdown service request assigned to service ex 3.', 1, 3, 18, 1, 1, '2019-01-11 09:56:39'),
(129, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 77, 1, '2019-01-11 09:56:39'),
(130, 'Breakdown request accepted', 'Your breakdown service request assigned to service ex 1.', 1, 3, 1, 47, 1, '2019-01-11 09:57:44'),
(131, 'Breakdown request assigned', ' Breakdown service request assigned to service ex 1.', 1, 3, 18, 1, 1, '2019-01-11 09:57:44'),
(132, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 75, 1, '2019-01-11 09:57:44'),
(133, 'Breakdown request accepted', 'Your breakdown service request assigned to newservice.', 1, 3, 1, 47, 1, '2019-01-11 10:03:31'),
(134, 'Breakdown request assigned', ' Breakdown service request assigned to newservice.', 1, 3, 18, 1, 1, '2019-01-11 10:03:31'),
(135, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 93, 1, '2019-01-11 10:03:31'),
(136, 'Breakdown request accepted', 'Your breakdown service request assigned to newservice.', 1, 3, 1, 47, 1, '2019-01-11 10:04:36'),
(137, 'Breakdown request assigned', ' Breakdown service request assigned to newservice.', 1, 3, 18, 1, 1, '2019-01-11 10:04:36'),
(138, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 93, 1, '2019-01-11 10:04:36'),
(139, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 10:05:53'),
(140, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 10:05:53'),
(141, 'Breakdown request accepted', 'Your breakdown service request assigned to newservice.', 1, 3, 1, 47, 1, '2019-01-11 10:06:17'),
(142, 'Breakdown request assigned', ' Breakdown service request assigned to newservice.', 1, 3, 18, 1, 1, '2019-01-11 10:06:17'),
(143, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 93, 1, '2019-01-11 10:06:17'),
(144, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-11 10:19:04'),
(145, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-11 10:19:04'),
(146, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 93, 1, '2019-01-11 10:19:04'),
(147, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-11 10:21:39'),
(148, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-11 10:21:39'),
(149, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-11 10:21:39'),
(150, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 12:23:40'),
(151, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 12:23:40'),
(152, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 12:24:25'),
(153, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 12:24:25'),
(154, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 12:25:03'),
(155, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 12:25:03'),
(156, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 12:25:34'),
(157, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 12:25:34'),
(158, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 12:26:27'),
(159, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 12:26:27'),
(160, 'New Service Request', 'You have posted a new Service Request', 1, 2, 1, 47, 1, '2019-01-11 12:28:07'),
(161, ' New Service Request', 'kisan posted a new service request.', 1, 2, 18, 1, 1, '2019-01-11 12:28:07'),
(162, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 12:29:59'),
(163, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 12:29:59'),
(164, 'Breakdown Request', 'You have posted a new Breakdown Request', 1, 3, 1, 47, 1, '2019-01-11 12:30:40'),
(165, 'Breakdown Request', 'kisan posted a new breakdown request.', 1, 3, 18, 1, 1, '2019-01-11 12:30:40'),
(166, 'Quote Request', 'You have posted a new quote request', 2, 4, 1, 92, 1, '2019-01-12 13:10:45'),
(167, 'Quote Request', 'chitta posted a new quote request.', 2, 4, 20, 1, 1, '2019-01-12 13:10:45'),
(168, 'Breakdown Request', 'You have posted a new Breakdown Request', 2, 3, 1, 92, 1, '2019-01-12 13:11:13'),
(169, 'Breakdown Request', 'chitta posted a new breakdown request.', 2, 3, 18, 1, 1, '2019-01-12 13:11:13'),
(170, 'New Service Request', 'You have posted a new Service Request', 2, 2, 1, 92, 1, '2019-01-12 13:11:39'),
(171, ' New Service Request', 'chitta posted a new service request.', 2, 2, 18, 1, 1, '2019-01-12 13:11:39'),
(172, 'Service request accepted', 'Your service request assigned to JB.', 2, 2, 1, 92, 1, '2019-01-12 13:13:47'),
(173, 'Service request assigned', ' Service request assigned to JB.', 2, 2, 18, 1, 1, '2019-01-12 13:13:47'),
(174, 'Service assigned', 'New service assigned', 2, 2, 8, 91, 1, '2019-01-12 13:13:47'),
(175, 'Service Closed', 'Your service closed', 2, 2, 1, 92, 1, '2019-01-12 13:14:26'),
(176, 'Service Closed', ' Service closed', 2, 2, 18, 1, 1, '2019-01-12 13:14:26'),
(177, 'Service Closed', 'Service Closed', 2, 2, 8, 91, 1, '2019-01-12 13:14:26'),
(178, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 13:13:46'),
(179, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 13:13:46'),
(180, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 97, 1, '2019-01-15 13:13:46'),
(181, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 15:55:09'),
(182, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 15:55:09'),
(183, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 97, 1, '2019-01-15 15:55:09'),
(184, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 16:05:40'),
(185, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 16:05:40'),
(186, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 97, 1, '2019-01-15 16:05:40'),
(187, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 16:06:33'),
(188, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 16:06:33'),
(189, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 97, 1, '2019-01-15 16:06:33'),
(190, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 16:08:31'),
(191, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 16:08:31'),
(192, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 75, 1, '2019-01-15 16:08:31'),
(193, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-15 16:12:18'),
(194, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-15 16:12:18'),
(195, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 93, 1, '2019-01-15 16:12:18'),
(196, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 08:11:04'),
(197, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 08:11:04'),
(198, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 08:11:04'),
(199, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:24:04'),
(200, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:24:04'),
(201, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:24:04'),
(202, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:25:20'),
(203, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:25:20'),
(204, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:25:20'),
(205, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:27:19'),
(206, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:27:19'),
(207, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:27:19'),
(208, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:27:47'),
(209, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:27:47'),
(210, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:27:47'),
(211, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:29:13'),
(212, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:29:13'),
(213, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:29:13'),
(214, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:29:35'),
(215, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:29:35'),
(216, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:29:35'),
(217, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:31:31'),
(218, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:31:31'),
(219, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:31:31'),
(220, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:32:25'),
(221, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:32:25'),
(222, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:32:25'),
(223, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:32:47'),
(224, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:32:47'),
(225, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:32:47'),
(226, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:32:59'),
(227, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:32:59'),
(228, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:32:59'),
(229, 'Service request accepted', 'Your service request assigned to kisanunder sub.', 3, 2, 1, 88, 1, '2019-01-16 09:34:14'),
(230, 'Service request assigned', ' Service request assigned to kisanunder sub.', 3, 2, 18, 1, 1, '2019-01-16 09:34:14'),
(231, 'Service assigned', 'New service assigned', 3, 2, 8, 87, 1, '2019-01-16 09:34:14'),
(232, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-16 09:34:32'),
(233, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-16 09:34:32'),
(234, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-16 09:34:32'),
(235, 'Breakdown request accepted', 'Breakdown service request assigned to kisanunder sub.', 3, 3, 1, 88, 1, '2019-01-16 09:35:14'),
(236, 'Breakdown request assigned', ' Breakdown service request assigned to kisanunder sub.', 3, 3, 18, 1, 1, '2019-01-16 09:35:14'),
(237, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 3, 8, 87, 1, '2019-01-16 09:35:14'),
(238, 'Breakdown request closed', 'Breakdown service request closed.', 3, 3, 1, 88, 1, '2019-01-16 09:35:27'),
(239, 'Breakdown request closed', ' Breakdown service request closed.', 3, 3, 18, 1, 1, '2019-01-16 09:35:27'),
(240, 'Breakdown request closed', 'Breakdown service request closed', 3, 3, 8, 87, 1, '2019-01-16 09:35:27'),
(241, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 1, 3, 1, 47, 1, '2019-01-16 09:41:08'),
(242, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 1, 3, 18, 1, 1, '2019-01-16 09:41:08'),
(243, 'Breakdown request assigned', 'Breakdown service request assigned', 1, 3, 8, 27, 1, '2019-01-16 09:41:08'),
(244, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-16 09:41:37'),
(245, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-16 09:41:37'),
(246, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 27, 1, '2019-01-16 09:41:37'),
(247, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 1, 3, 1, 47, 1, '2019-01-16 09:42:17'),
(248, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 1, 3, 18, 1, 1, '2019-01-16 09:42:17'),
(249, 'Breakdown request assigned', 'Breakdown service request assigned', 1, 3, 8, 27, 1, '2019-01-16 09:42:17'),
(250, 'Breakdown request closed', 'Breakdown service request closed.', 1, 3, 1, 47, 1, '2019-01-16 09:42:31'),
(251, 'Breakdown request closed', ' Breakdown service request closed.', 1, 3, 18, 1, 1, '2019-01-16 09:42:31'),
(252, 'Breakdown request closed', 'Breakdown service request closed', 1, 3, 8, 27, 1, '2019-01-16 09:42:31'),
(253, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 09:44:55'),
(254, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 09:44:55'),
(255, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 09:44:55'),
(256, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 09:45:15'),
(257, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 09:45:15'),
(258, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 09:45:15'),
(259, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 12:41:34'),
(260, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 12:41:34'),
(261, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 12:41:34'),
(262, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 12:42:02'),
(263, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 12:42:02'),
(264, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 12:42:02'),
(265, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 12:43:02'),
(266, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 12:43:02'),
(267, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 12:43:02'),
(268, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 15:49:10'),
(269, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 15:49:10'),
(270, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 15:49:10'),
(271, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 15:52:54'),
(272, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 15:52:54'),
(273, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 15:52:54'),
(274, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-16 15:53:59'),
(275, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-16 15:53:59'),
(276, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-16 15:53:59'),
(277, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 15:54:30'),
(278, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 15:54:30'),
(279, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 15:54:30'),
(280, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-16 15:57:02'),
(281, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-16 15:57:02'),
(282, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-16 15:57:02'),
(283, 'Service Closed', 'Your service closed', 3, 2, 1, 88, 1, '2019-01-16 15:57:36'),
(284, 'Service Closed', ' Service closed', 3, 2, 18, 1, 1, '2019-01-16 15:57:36'),
(285, 'Service Closed', 'Service Closed', 3, 2, 8, 87, 1, '2019-01-16 15:57:36'),
(286, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-16 15:59:14'),
(287, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-16 15:59:14'),
(288, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-16 15:59:14'),
(289, 'Service request accepted', 'Your service request assigned to JB.', 2, 2, 1, 47, 1, '2019-01-17 09:38:26'),
(290, 'Service request assigned', ' Service request assigned to JB.', 2, 2, 18, 1, 1, '2019-01-17 09:38:26'),
(291, 'Service assigned', 'New service assigned', 2, 2, 8, 91, 1, '2019-01-17 09:38:26'),
(292, 'Breakdown request closed', 'Breakdown service request closed.', 2, 3, 1, 48, 1, '2019-01-17 09:43:47'),
(293, 'Breakdown request closed', ' Breakdown service request closed.', 2, 3, 18, 1, 1, '2019-01-17 09:43:47'),
(294, 'Breakdown request closed', 'Breakdown service request closed', 2, 3, 8, 58, 1, '2019-01-17 09:43:47'),
(295, 'Breakdown Request Closed', 'Your breakdown request closed', 2, 3, 1, 48, 1, '2019-01-17 09:45:27'),
(296, 'Breakdown Request Closed', ' Breakdown request closed', 2, 3, 18, 1, 1, '2019-01-17 09:45:27'),
(297, 'Breakdown Request Closed', 'Breakdown request closed', 2, 3, 8, 58, 1, '2019-01-17 09:45:27'),
(298, 'Breakdown request accepted', 'Breakdown service request assigned to JB.', 2, 3, 1, 92, 1, '2019-01-17 09:46:26'),
(299, 'Breakdown request assigned', ' Breakdown service request assigned to JB.', 2, 3, 18, 1, 1, '2019-01-17 09:46:26'),
(300, 'Breakdown request assigned', 'Breakdown service request assigned', 2, 3, 8, 91, 1, '2019-01-17 09:46:26'),
(301, 'Breakdown Request Closed', 'Your breakdown request closed', 2, 3, 1, 92, 1, '2019-01-18 16:34:58'),
(302, 'Breakdown Request Closed', ' Breakdown request closed', 2, 3, 18, 1, 1, '2019-01-18 16:34:58'),
(303, 'Breakdown Request Closed', 'Breakdown request closed', 2, 3, 8, 91, 1, '2019-01-18 16:34:58'),
(304, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-18 16:36:11'),
(305, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-18 16:36:11'),
(306, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-18 16:36:11'),
(307, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-18 16:36:20'),
(308, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-18 16:36:20'),
(309, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-18 16:36:20'),
(310, 'Quote Request', 'You have posted a new quote request', 2, 4, 1, 92, 1, '2019-01-19 07:15:03'),
(311, 'Quote Request', 'chitta posted a new quote request.', 2, 4, 20, 1, 1, '2019-01-19 07:15:03'),
(312, 'New Service Request', 'You have posted a new Service Request', 2, 2, 1, 92, 1, '2019-01-19 07:19:29'),
(313, ' New Service Request', 'chitta posted a new service request.', 2, 2, 18, 1, 1, '2019-01-19 07:19:29'),
(314, 'Service request accepted', 'Your service request assigned to JB.', 2, 2, 1, 92, 1, '2019-01-19 07:21:48'),
(315, 'Service request assigned', ' Service request assigned to JB.', 2, 2, 18, 1, 1, '2019-01-19 07:21:48'),
(316, 'Service assigned', 'New service assigned', 2, 2, 8, 91, 1, '2019-01-19 07:21:48'),
(317, 'Service Closed', 'Your service closed', 2, 2, 1, 92, 1, '2019-01-19 07:31:04'),
(318, 'Service Closed', ' Service closed', 2, 2, 18, 1, 1, '2019-01-19 07:31:04'),
(319, 'Service Closed', 'Service Closed', 2, 2, 8, 91, 1, '2019-01-19 07:31:04'),
(320, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-19 08:37:04'),
(321, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-19 08:37:04'),
(322, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-19 08:37:04'),
(323, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-19 08:42:47'),
(324, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-19 08:42:47'),
(325, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-19 08:42:47'),
(326, 'Service request accepted', 'Your service request assigned to Namita Das.', 1, 2, 1, 47, 1, '2019-01-19 08:43:59'),
(327, 'Service request assigned', ' Service request assigned to Namita Das.', 1, 2, 18, 1, 1, '2019-01-19 08:43:59'),
(328, 'Service assigned', 'New service assigned', 1, 2, 8, 27, 1, '2019-01-19 08:43:59'),
(329, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-19 08:45:03'),
(330, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-19 08:45:03'),
(331, 'Service Closed', 'Service Closed', 1, 2, 8, 27, 1, '2019-01-19 08:45:03'),
(332, 'Service request accepted', 'Your service request assigned to Bichitra.', 1, 2, 1, 47, 1, '2019-01-19 08:46:54'),
(333, 'Service request assigned', ' Service request assigned to Bichitra.', 1, 2, 18, 1, 1, '2019-01-19 08:46:54'),
(334, 'Service assigned', 'New service assigned', 1, 2, 8, 50, 1, '2019-01-19 08:46:54'),
(335, 'Service Closed', 'Your service closed', 1, 2, 1, 47, 1, '2019-01-19 08:47:53'),
(336, 'Service Closed', ' Service closed', 1, 2, 18, 1, 1, '2019-01-19 08:47:53'),
(337, 'Service Closed', 'Service Closed', 1, 2, 8, 50, 1, '2019-01-19 08:47:53'),
(338, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita Das.', 1, 3, 1, 47, 1, '2019-01-19 08:49:29'),
(339, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 1, 3, 18, 1, 1, '2019-01-19 08:49:29'),
(340, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 27, 1, '2019-01-19 08:49:29'),
(341, 'Breakdown Request Closed', 'Your breakdown request closed', 1, 3, 1, 47, 1, '2019-01-19 08:50:40'),
(342, 'Breakdown Request Closed', ' Breakdown request closed', 1, 3, 18, 1, 1, '2019-01-19 08:50:40'),
(343, 'Breakdown Request Closed', 'Breakdown request closed', 1, 3, 8, 27, 1, '2019-01-19 08:50:40'),
(344, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita Das.', 1, 3, 1, 47, 1, '2019-01-19 08:51:33'),
(345, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 1, 3, 18, 1, 1, '2019-01-19 08:51:33'),
(346, 'Breakdown assigned', 'New breakdown service assigned', 1, 3, 8, 27, 1, '2019-01-19 08:51:33'),
(347, 'Breakdown Request Closed', 'Your breakdown request closed', 1, 3, 1, 47, 1, '2019-01-19 08:52:21'),
(348, 'Breakdown Request Closed', ' Breakdown request closed', 1, 3, 18, 1, 1, '2019-01-19 08:52:21'),
(349, 'Breakdown Request Closed', 'Breakdown request closed', 1, 3, 8, 27, 1, '2019-01-19 08:52:21'),
(350, 'Breakdown request accepted', 'Your breakdown service request assigned to service ex 4.', 2, 3, 1, 47, 1, '2019-01-19 08:53:31'),
(351, 'Breakdown request assigned', ' Breakdown service request assigned to service ex 4.', 2, 3, 18, 1, 1, '2019-01-19 08:53:31'),
(352, 'Breakdown assigned', 'New breakdown service assigned', 2, 3, 8, 78, 1, '2019-01-19 08:53:31'),
(353, 'Breakdown request accepted', 'Your breakdown service request assigned to kisanunder sub.', 3, 3, 1, 88, 1, '2019-01-19 08:54:45'),
(354, 'Breakdown request assigned', ' Breakdown service request assigned to kisanunder sub.', 3, 3, 18, 1, 1, '2019-01-19 08:54:45'),
(355, 'Breakdown assigned', 'New breakdown service assigned', 3, 3, 8, 87, 1, '2019-01-19 08:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(12) NOT NULL,
  `name` varchar(222) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `image` varchar(222) DEFAULT NULL,
  `description` text,
  `links` varchar(222) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `name`, `start_date`, `end_date`, `image`, `description`, `links`, `status`, `add_date`, `added_by`) VALUES
(1, 'New Image', '2018-08-01 00:00:00', '2018-09-30 00:00:00', '1_9gms8hhf6i37c0fohdcvle.jpg', '', 'www.google.com', 2, '2018-08-30 20:44:20', 1),
(2, 'New Test', '2018-08-01 00:00:00', '2018-08-28 00:00:00', NULL, 'Test', 'www.google.com', 2, '2018-08-30 20:45:35', 1),
(3, 'TEST2', '2018-08-29 00:00:00', '2018-09-12 00:00:00', '3_apple.jpg', '', 'https://www.google.com/', 1, '2018-08-30 20:47:46', 1),
(4, 'Free Download', '2018-09-05 00:00:00', '2018-09-05 00:00:00', '4_Chrysanthemum.jpg', 'Super Deals. ... Snapdeal Offers of the Day: UP TO 80% OFF on Electronics, Mobiles, Fashion & more. ... Snapdeal Online Shopping Offers & Discounts Today.', 'google.com', 2, '2018-09-05 14:04:34', 1),
(5, 'Discount in Batteryasas', '2018-09-14 00:00:00', '2018-09-29 00:00:00', '5_asian_paint.jpeg', NULL, 'http://www.orizengroup.com/products/batteries/industrial-batteries/', 1, '2018-09-14 16:08:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id_service` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id_service`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'Rental of Forklifts', 'RF', 1, '2018-08-30 20:35:50'),
(2, 'Preventative Maintenance Program', 'PMP', 1, '2018-08-30 20:36:20'),
(3, 'Repairs & Maintenance Services', 'Test service 3', 1, '2018-08-30 21:16:55'),
(4, 'Electric Forklift', 'EF', 1, '2018-09-07 19:24:38'),
(5, 'Family Information', 'Family Information', 1, '2018-10-08 19:46:15'),
(6, 'Breakdown 3', 'Test', 1, '2018-10-08 20:00:14'),
(7, 'Forklift Battery', 'FBC', 1, '2018-10-09 17:01:31'),
(8, 'test service', 'test test', 1, '2019-01-02 14:06:20');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id_state` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1->Active,2->In-active',
  `add_date` datetime NOT NULL,
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_breakdown`
--

CREATE TABLE `sub_breakdown` (
  `id_breakdown` int(11) DEFAULT NULL,
  `name` varchar(222) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_breakdown`
--

INSERT INTO `sub_breakdown` (`id_breakdown`, `name`, `type`, `status`) VALUES
(5, 'TEST', 1, 1),
(5, 'fdhjfdyjh', 1, 1),
(7, 'Test 1', 1, 1),
(7, 'Test2', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_quote`
--

CREATE TABLE `sub_quote` (
  `id_quote` int(122) DEFAULT NULL,
  `name` varchar(222) DEFAULT NULL,
  `type` smallint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_quote`
--

INSERT INTO `sub_quote` (`id_quote`, `name`, `type`, `status`) VALUES
(7, 'TEST   TEST', 1, 1),
(6, 'chjfgjh', 1, 1),
(6, 'Product 4', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_service`
--

CREATE TABLE `sub_service` (
  `id_service` int(11) DEFAULT NULL,
  `name` text,
  `type` int(2) DEFAULT '0' COMMENT '1->Radio 2->Text',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_service`
--

INSERT INTO `sub_service` (`id_service`, `name`, `type`, `status`) VALUES
(1, 'TEST', 1, 1),
(1, 'TEST2', 2, 1),
(5, 'Total Member', 2, 1),
(5, 'Male', 2, 1),
(5, 'Female', 2, 1),
(5, 'LPG Connection', 1, 1),
(5, 'Electricity', 1, 1),
(7, 'Voltage or Number of Cells', 2, 1),
(7, 'Amp hour', 2, 1),
(7, 'Cell type', 2, 1),
(7, 'Water Filling System', 1, 1),
(7, 'Battery Tank', 1, 1),
(7, 'dfgdfhgfgeryhery', 2, 1),
(6, 'Sub1', 2, 1),
(6, 'Sub2', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `track_breakdown_request`
--

CREATE TABLE `track_breakdown_request` (
  `id` int(11) NOT NULL,
  `id_breakdown_request` int(11) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `note` text,
  `added_by` int(12) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_breakdown_request`
--

INSERT INTO `track_breakdown_request` (`id`, `id_breakdown_request`, `id_city`, `status`, `note`, `added_by`, `add_date`, `end_date`) VALUES
(1, 1, NULL, 1, 'test for new progress', 1, '2018-08-30 21:19:50', NULL),
(2, 1, NULL, 1, 'check properly', 2, '2018-08-30 21:25:15', NULL),
(3, 1, NULL, 1, 'ok good job', 1, '2018-08-30 21:25:50', NULL),
(4, 1, NULL, 1, ' Service Closed', 1, '2018-08-30 21:26:18', NULL),
(5, 2, NULL, 1, 'test', 1, '2018-08-30 21:31:30', NULL),
(6, 2, NULL, 1, ' Service Closed', 1, '2018-08-30 21:32:36', NULL),
(7, 8, NULL, 1, 'test', 1, '2018-09-04 17:39:28', NULL),
(8, 8, NULL, 1, 'work in progress', 1, '2018-09-04 17:40:03', NULL),
(9, 8, NULL, 1, 'testtestt', 27, '2018-09-04 17:40:20', NULL),
(10, 8, NULL, 1, 'service closed', 1, '2018-09-04 17:41:03', NULL),
(11, 5, NULL, 1, 'drtdtdtdtgdtdt', 1, '2018-09-04 17:42:19', NULL),
(12, 5, NULL, 1, 'work in progress', 1, '2018-09-04 17:42:33', NULL),
(13, 6, NULL, 1, 'xvvgv', 1, '2018-09-04 18:22:24', NULL),
(14, 6, NULL, 1, ' Service Closed', 1, '2018-09-04 18:23:32', NULL),
(15, 10, NULL, 1, 'ghgfhg', 1, '2018-09-04 18:25:31', NULL),
(16, 10, NULL, 1, 'closed status', 1, '2018-09-04 18:26:22', NULL),
(17, 12, NULL, 1, 'hfghfgh', 2, '2018-09-04 19:57:49', NULL),
(18, 12, NULL, 1, 'service closed', 27, '2018-09-04 19:58:13', NULL),
(19, 13, NULL, 1, 'ytguygyujgh', 2, '2018-09-04 20:01:30', NULL),
(20, 13, NULL, 1, 'service closed', 2, '2018-09-04 20:01:43', NULL),
(21, 14, NULL, 1, 'fghfghfh', 2, '2018-09-04 20:06:03', NULL),
(22, 14, NULL, 1, 'service closed', 27, '2018-09-04 20:06:23', NULL),
(23, 15, NULL, 1, 'hfghfhg', 2, '2018-09-04 20:09:51', NULL),
(24, 16, NULL, 1, 'jio bichitra', 2, '2018-09-05 14:34:27', NULL),
(25, 16, NULL, 1, '88888', 1, '2018-09-05 14:39:38', NULL),
(26, 17, NULL, 1, '4745', 1, '2018-09-05 14:46:53', NULL),
(27, 17, NULL, 1, 'close', 1, '2018-09-05 15:28:06', NULL),
(28, 17, NULL, 1, 'closed', 1, '2018-09-05 15:29:53', NULL),
(29, 19, NULL, 1, 'check it', 1, '2018-09-05 15:34:17', NULL),
(30, 19, NULL, 1, 'tezf', 1, '2018-09-05 15:38:02', NULL),
(31, 4, NULL, 1, 'wip', 2, '2018-09-05 15:39:34', NULL),
(32, 19, NULL, 1, ' Service Closed', 2, '2018-09-05 15:44:01', NULL),
(33, 23, NULL, 1, 'revert', 1, '2018-09-11 20:02:29', NULL),
(34, 23, NULL, 1, 'vinay will arrive on Thursday around 2pm', 27, '2018-09-11 20:06:57', NULL),
(35, 22, NULL, 1, 'tt', 1, '2018-09-14 15:52:42', NULL),
(36, 21, NULL, 1, 'Test', 1, '2018-09-15 15:22:06', NULL),
(37, 3, NULL, 1, 'test', 1, '2018-09-15 15:22:18', NULL),
(38, 23, NULL, 1, ' Service Closed', 1, '2018-09-15 15:22:44', NULL),
(39, 24, NULL, 1, 'xugdyidugd', 1, '2018-09-15 15:29:00', NULL),
(40, 25, NULL, 1, 'hpvojcououvuo', 1, '2018-09-15 15:33:53', NULL),
(41, 26, NULL, 1, 'xjguxic', 1, '2018-09-17 11:38:26', NULL),
(42, 11, NULL, 1, 'hdhdh', 1, '2018-09-17 11:45:34', NULL),
(43, 30, NULL, 1, 'xgkchk hkc', 1, '2018-09-17 16:59:10', NULL),
(44, 30, NULL, 1, 'frhfgreg', 1, '2018-09-17 17:03:22', NULL),
(45, 30, NULL, 1, ' Service Closed', 1, '2018-09-17 17:03:49', NULL),
(46, 26, NULL, 1, 'service closed', 1, '2018-09-17 17:05:27', NULL),
(47, 32, NULL, 1, 'fiicof', 2, '2018-09-17 17:15:58', NULL),
(48, 31, NULL, 1, 'cbjjgsjdd', 1, '2018-09-17 17:16:38', NULL),
(49, 32, NULL, 1, ' Service Closed', 1, '2018-09-17 17:17:45', NULL),
(50, 31, NULL, 1, ' Service Closed', 1, '2018-09-17 17:21:10', NULL),
(51, 33, NULL, 1, 'dgudgusfus', 2, '2018-09-17 17:56:06', NULL),
(52, 34, NULL, 1, 'Ttygggh', 1, '2018-09-17 17:57:49', NULL),
(53, 34, NULL, 1, ' Service Closed', 1, '2018-09-17 17:58:10', NULL),
(54, 33, NULL, 1, ' Service Closed', 2, '2018-09-17 17:58:37', NULL),
(55, 35, NULL, 1, 'cghhhjj', 2, '2018-09-17 18:52:31', NULL),
(56, 35, NULL, 1, ' Service Closed', 27, '2018-09-17 18:53:26', NULL),
(57, 25, NULL, 1, ' Service Closed', 27, '2018-09-17 18:54:23', NULL),
(58, 24, NULL, 1, ' Service Closed', 1, '2018-09-17 19:12:49', NULL),
(59, 22, NULL, 1, 'ok done ', 27, '2018-09-17 19:36:21', NULL),
(60, 37, NULL, 1, 'jfigjgigigigigiy', 2, '2018-09-17 19:43:19', NULL),
(61, 38, NULL, 1, 'chfugigjgjfug', 1, '2018-09-17 19:44:18', NULL),
(62, 36, NULL, 1, 'fufugiykyigigigi', 2, '2018-09-17 19:45:05', NULL),
(63, 36, NULL, 1, ' Service Closed', 27, '2018-09-17 19:45:49', NULL),
(64, 37, NULL, 1, ' Service Closed', 2, '2018-09-17 19:46:54', NULL),
(65, 38, NULL, 1, ' Service Closed', 1, '2018-09-17 19:47:32', NULL),
(66, 11, NULL, 1, ' Service Closed', 2, '2018-09-22 15:47:06', NULL),
(67, 44, NULL, 1, 'test', 1, '2018-10-12 15:32:00', NULL),
(68, 44, NULL, 1, 'Ok babay', 27, '2018-10-12 18:52:42', NULL),
(69, 49, NULL, 1, 'hbv', 1, '2019-01-02 08:42:59', NULL),
(70, 53, NULL, 1, 'test', 84, '2019-01-02 09:47:34', NULL),
(71, 44, NULL, 1, 'ok close', 72, '2019-01-02 13:02:06', NULL),
(72, 54, NULL, 1, '56', 1, '2019-01-02 13:24:28', NULL),
(73, 49, NULL, 1, 'mmk', 1, '2019-01-02 14:15:44', NULL),
(74, 53, NULL, 1, 'test test test test', 1, '2019-01-02 14:25:22', NULL),
(75, 53, NULL, 1, ' Service Closed', 1, '2019-01-02 14:25:34', NULL),
(76, 50, NULL, 1, 'check your email', 1, '2019-01-02 14:28:16', NULL),
(77, 54, NULL, 1, 'hhhbb', 84, '2019-01-02 14:29:35', NULL),
(78, 54, NULL, 1, ' Service Closed', 84, '2019-01-02 14:29:50', NULL),
(79, 51, NULL, 1, 'vhvhvh', 1, '2019-01-02 15:55:04', NULL),
(80, 51, NULL, 1, 'hhh', 27, '2019-01-02 15:59:50', NULL),
(81, 51, NULL, 1, ' Service Closed', 27, '2019-01-02 16:00:20', NULL),
(82, 56, NULL, 1, 'ddf', 81, '2019-01-05 09:12:44', NULL),
(83, 56, NULL, 1, 'dfd dfe3d', 81, '2019-01-05 09:12:54', NULL),
(84, 56, NULL, 1, 'cccc', 81, '2019-01-05 09:13:01', NULL),
(85, 58, NULL, 1, 'ggggggg', 1, '2019-01-11 09:43:46', NULL),
(86, 57, NULL, 1, 'tttttgggg', 1, '2019-01-11 09:46:07', NULL),
(87, 39, NULL, 1, 'TEST TEST ', 1, '2019-01-11 09:56:39', NULL),
(88, 47, NULL, 1, '75 ex 1 city id', 1, '2019-01-11 09:57:44', NULL),
(89, 59, NULL, 1, 'test', 1, '2019-01-11 10:00:36', NULL),
(90, 45, NULL, 1, 'Test TEST ', 1, '2019-01-11 10:03:31', NULL),
(91, 43, NULL, 1, 'TEst ', 1, '2019-01-11 10:04:36', NULL),
(92, 60, NULL, 1, 'test test', 1, '2019-01-11 10:06:17', NULL),
(93, 60, NULL, 1, ' Service Closed', 84, '2019-01-11 10:19:09', NULL),
(94, 59, NULL, 1, ' Service Closed', 84, '2019-01-15 13:13:51', NULL),
(95, 57, NULL, 1, ' Service Closed', 84, '2019-01-15 15:44:08', NULL),
(96, 57, NULL, 1, ' Service Closed', 84, '2019-01-15 15:44:29', NULL),
(97, 57, NULL, 1, ' Service Closed', 84, '2019-01-15 15:46:21', NULL),
(98, 59, NULL, 1, ' Service Closed', 84, '2019-01-15 15:55:15', NULL),
(99, 57, NULL, 1, ' Service Closed', 84, '2019-01-15 15:56:42', NULL),
(100, 57, NULL, 1, 'ckos', 84, '2019-01-15 15:57:10', NULL),
(101, 57, NULL, 1, ' Service Closed', 84, '2019-01-15 16:05:46', NULL),
(102, 58, NULL, 1, ' Service Closed', 84, '2019-01-15 16:06:05', NULL),
(103, 58, NULL, 1, ' Service Closed', 84, '2019-01-15 16:06:38', NULL),
(104, 47, NULL, 1, ' Service Closed', 84, '2019-01-15 16:08:35', NULL),
(105, 45, NULL, 1, ' Service Closed', 84, '2019-01-15 16:12:23', NULL),
(106, 55, NULL, 1, 'hifyifiyfyi', 1, '2019-01-16 09:35:14', NULL),
(107, 55, NULL, 1, ' Service Closed', 1, '2019-01-16 09:35:32', NULL),
(108, 64, NULL, 1, 'gufugug', 84, '2019-01-16 09:41:08', NULL),
(109, 64, NULL, 1, ' Service Closed', 84, '2019-01-16 09:41:42', NULL),
(110, 63, NULL, 1, 'tfufuguf', 84, '2019-01-16 09:42:17', NULL),
(111, 63, NULL, 1, ' Service Closed', 84, '2019-01-16 09:42:37', NULL),
(112, 50, NULL, 1, 'hj', 81, '2019-01-17 09:43:53', NULL),
(113, 50, NULL, 1, 'what about it', 81, '2019-01-17 09:45:27', NULL),
(114, 65, NULL, 1, 'checj', 81, '2019-01-17 09:46:26', NULL),
(115, 65, NULL, 1, 'hh', 81, '2019-01-17 09:47:11', NULL),
(116, 65, NULL, 1, 'Test Test ', 1, '2019-01-18 16:34:50', NULL),
(117, 65, NULL, 1, 'Test ', 1, '2019-01-18 16:34:58', NULL),
(118, 62, NULL, 1, 'hrthrth', 84, '2019-01-19 08:49:29', NULL),
(119, 62, NULL, 1, 'drtyrt', 84, '2019-01-19 08:49:34', NULL),
(120, 62, NULL, 1, 'hghh', 1, '2019-01-19 08:50:23', NULL),
(121, 62, NULL, 1, '0', 84, '2019-01-19 08:50:40', NULL),
(122, 61, NULL, 1, 'trtrtrtr', 84, '2019-01-19 08:51:33', NULL),
(123, 61, NULL, 1, 'gdgdgfgf', 1, '2019-01-19 08:51:56', NULL),
(124, 61, NULL, 1, '4343', 84, '2019-01-19 08:52:05', NULL),
(125, 61, NULL, 1, '0', 84, '2019-01-19 08:52:21', NULL),
(126, 48, NULL, 1, 'retrt', 1, '2019-01-19 08:53:31', NULL),
(127, 52, NULL, 1, '43243', 84, '2019-01-19 08:54:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `track_service_request`
--

CREATE TABLE `track_service_request` (
  `id` int(11) NOT NULL,
  `id_service_request` int(11) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `note` text,
  `added_by` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_service_request`
--

INSERT INTO `track_service_request` (`id`, `id_service_request`, `id_city`, `status`, `note`, `added_by`, `add_date`, `end_date`) VALUES
(1, 1, NULL, 1, 'test test', 1, '2018-08-30 20:49:40', NULL),
(2, 1, NULL, 1, 'Demo', 27, '2018-08-30 20:50:34', NULL),
(3, 1, NULL, 1, 'check', 2, '2018-08-30 20:51:44', NULL),
(4, 1, NULL, 1, 'work in progress', 1, '2018-08-30 20:52:02', NULL),
(5, 1, NULL, 1, ' Service Closed', 27, '2018-08-30 20:53:14', NULL),
(6, 2, NULL, 1, 'test', 1, '2018-08-30 21:30:17', NULL),
(7, 2, NULL, 1, 'check', 1, '2018-08-30 21:30:35', NULL),
(8, 2, NULL, 1, ' Service Closed', 1, '2018-08-30 21:30:41', NULL),
(9, 3, NULL, 1, 'check on pri', 1, '2018-09-01 12:06:40', NULL),
(10, 3, NULL, 1, 'checked & working fine', 27, '2018-09-01 12:08:43', NULL),
(11, 3, NULL, 1, 'confirmed from customer, it is working fine now', 27, '2018-09-01 12:09:14', NULL),
(12, 4, NULL, 1, 'yy', 2, '2018-09-01 16:30:03', NULL),
(13, 5, NULL, 1, 'CHECK IT', 1, '2018-09-04 15:10:40', NULL),
(14, 8, NULL, 1, 'test', 1, '2018-09-04 17:28:48', NULL),
(15, 8, NULL, 1, 'testtest', 27, '2018-09-04 17:29:28', NULL),
(16, 8, NULL, 1, ' Service Closed', 27, '2018-09-04 17:30:40', NULL),
(17, 7, NULL, 1, 'dfgdgh', 1, '2018-09-04 17:43:23', NULL),
(18, 7, NULL, 1, 'service closed', 1, '2018-09-04 17:43:51', NULL),
(19, 9, NULL, 1, 'test', 2, '2018-09-04 19:29:58', NULL),
(20, 9, NULL, 1, 'fgfgh', 27, '2018-09-04 19:31:20', NULL),
(21, 9, NULL, 1, 'service closed', 27, '2018-09-04 19:32:01', NULL),
(22, 10, NULL, 1, 'dfhfsdgf', 2, '2018-09-04 19:58:59', NULL),
(23, 10, NULL, 1, 'dfgsadgfgsd', 2, '2018-09-04 19:59:26', NULL),
(24, 11, NULL, 1, 'gyhfhgfhgf', 2, '2018-09-04 20:07:52', NULL),
(25, 11, NULL, 1, 'service closed', 2, '2018-09-04 20:08:06', NULL),
(26, 12, NULL, 1, 'vghfhg', 2, '2018-09-04 20:08:48', NULL),
(27, 4, NULL, 1, 'what about it check it & found some problem in it & need to', 1, '2018-09-05 13:20:15', NULL),
(28, 4, NULL, 1, 'what about it check it & found some problem in it & need to  what about it check it & found some problem in it & need to kjd kljkd kljkld ljkldj kljd ', 1, '2018-09-05 13:21:05', NULL),
(29, 13, NULL, 1, 'ts', 1, '2018-09-05 14:11:49', NULL),
(30, 14, NULL, 1, 'kkkkk', 1, '2018-09-05 14:13:16', NULL),
(31, 13, NULL, 1, 'hh', 1, '2018-09-05 14:13:42', NULL),
(32, 14, NULL, 1, '6yyy', 1, '2018-09-05 14:14:09', NULL),
(33, 15, NULL, 1, '222', 1, '2018-09-05 14:33:43', NULL),
(34, 16, NULL, 1, 'yyy', 1, '2018-09-05 14:37:49', NULL),
(35, 15, NULL, 1, '5555555555555555555', 1, '2018-09-05 14:39:08', NULL),
(36, 19, NULL, 1, 'test', 2, '2018-09-05 15:41:44', NULL),
(37, 19, NULL, 1, 'ii', 1, '2018-09-05 15:45:25', NULL),
(38, 12, NULL, 1, 'sggdj', 27, '2018-09-06 16:06:17', NULL),
(39, 16, NULL, 1, 'sggd', 27, '2018-09-06 16:06:36', NULL),
(40, 18, NULL, 1, 'dg', 2, '2018-09-11 16:55:04', NULL),
(41, 17, NULL, 1, 'test', 1, '2018-09-12 15:55:38', NULL),
(42, 21, NULL, 1, 'started', 1, '2018-09-12 16:00:50', NULL),
(43, 20, NULL, 1, 'gy', 1, '2018-09-12 16:13:58', NULL),
(44, 22, NULL, 1, 'xigixg uh xuodood', 2, '2018-09-15 15:31:24', NULL),
(45, 23, NULL, 1, 'test', 1, '2018-09-15 15:36:25', NULL),
(46, 23, NULL, 1, ' Service Closed', 1, '2018-09-15 15:36:50', NULL),
(47, 24, NULL, 1, 'xgnxgjxjx', 1, '2018-09-17 11:42:27', NULL),
(48, 25, NULL, 1, 'gusyidyid', 1, '2018-09-17 11:42:58', NULL),
(49, 6, NULL, 1, 'star', 1, '2018-09-17 12:08:59', NULL),
(50, 25, NULL, 1, ' Service Closed', 1, '2018-09-17 17:24:46', NULL),
(51, 29, NULL, 1, 'Fddhgug', 1, '2018-09-17 18:01:45', NULL),
(52, 32, NULL, 1, 'hkfulfchchk', 1, '2018-09-17 19:51:02', NULL),
(53, 31, NULL, 1, 'ec4vtvt', 2, '2018-09-17 19:51:53', NULL),
(54, 30, NULL, 1, 'efgg', 2, '2018-09-17 19:52:36', NULL),
(55, 30, NULL, 1, ' Service Closed', 27, '2018-09-17 19:53:02', NULL),
(56, 31, NULL, 1, ' Service Closed', 2, '2018-09-17 19:53:37', NULL),
(57, 32, NULL, 1, ' Service Closed', 1, '2018-09-17 19:54:24', NULL),
(58, 28, NULL, 1, 'hh', 1, '2018-09-18 12:55:03', NULL),
(59, 27, NULL, 1, 'tesf', 1, '2018-09-18 13:05:15', NULL),
(60, 33, NULL, 1, 'check it', 1, '2018-09-18 13:17:07', NULL),
(61, 35, NULL, 1, 'Check & Respond to customer', 1, '2018-09-27 20:30:40', NULL),
(62, 35, NULL, 1, ' Service Closed', 27, '2018-09-27 20:33:32', NULL),
(63, 29, NULL, 1, 'service closed', 1, '2018-10-03 15:16:35', NULL),
(64, 39, NULL, 1, 'test', 1, '2018-10-09 17:06:27', NULL),
(65, 39, NULL, 1, ' Service Closed', 2, '2018-10-11 12:07:39', NULL),
(66, 43, NULL, 1, 'TEST ', 1, '2018-10-12 17:29:43', NULL),
(67, 43, NULL, 1, 'TEST TEST ', 27, '2018-10-12 18:44:51', NULL),
(68, 42, NULL, 1, 'dfsdczcx', 1, '2018-10-29 12:39:47', NULL),
(69, 42, NULL, 1, 'closed status', 1, '2018-10-29 12:39:58', NULL),
(70, 44, NULL, 1, 'ghghgjh', 1, '2018-10-29 13:01:54', NULL),
(71, 44, NULL, 1, 'close', 1, '2018-10-29 13:04:22', NULL),
(72, 116, NULL, 1, 'Test TESt ', 1, '2018-12-28 16:40:11', NULL),
(73, 116, NULL, 1, 'test tets ', 1, '2018-12-28 16:41:34', NULL),
(74, 116, NULL, 1, 'done', 1, '2018-12-28 16:41:45', NULL),
(75, 94, NULL, 1, 'TEst TEST ', 70, '2018-12-31 08:10:33', NULL),
(76, 114, NULL, 1, 'Purusottama ', 70, '2018-12-31 08:10:59', NULL),
(77, 112, NULL, 1, 'test test test', 70, '2018-12-31 12:49:29', NULL),
(78, 110, NULL, 1, 'test check test', 1, '2018-12-31 12:52:55', NULL),
(79, 113, NULL, 1, 'test test check', 1, '2018-12-31 13:25:18', NULL),
(80, 106, NULL, 1, 'tets', 70, '2019-01-02 07:55:15', NULL),
(81, 115, NULL, 1, 'test', 1, '2019-01-02 09:08:45', NULL),
(82, 118, NULL, 1, 'test', 1, '2019-01-02 09:13:29', NULL),
(83, 108, NULL, 1, 'TEST TETS ', 1, '2019-01-02 09:44:58', NULL),
(84, 117, NULL, 1, 'test', 84, '2019-01-02 09:46:47', NULL),
(85, 123, NULL, 1, 'TETTET', 1, '2019-01-02 13:04:01', NULL),
(86, 122, NULL, 1, 'test test ', 1, '2019-01-02 13:20:57', NULL),
(87, 121, NULL, 1, 'rgfvdf', 84, '2019-01-02 13:44:11', NULL),
(88, 121, NULL, 1, 'test', 1, '2019-01-02 13:45:49', NULL),
(89, 114, NULL, 1, 'vhhjj', 1, '2019-01-02 14:15:57', NULL),
(90, 115, NULL, 1, 'vggh', 1, '2019-01-02 14:16:11', NULL),
(91, 115, NULL, 1, ' Service Closed', 1, '2019-01-02 14:16:22', NULL),
(92, 113, NULL, 1, ' Service Closed', 1, '2019-01-02 14:16:56', NULL),
(93, 117, NULL, 1, 'check check check', 1, '2019-01-02 14:26:10', NULL),
(94, 117, NULL, 1, ' Service Closed', 1, '2019-01-02 14:26:31', NULL),
(95, 111, NULL, 1, 'test test', 1, '2019-01-02 14:27:20', NULL),
(96, 118, NULL, 1, ' Service Closed', 84, '2019-01-02 14:30:11', NULL),
(97, 124, NULL, 1, 'test', 84, '2019-01-02 15:40:34', NULL),
(98, 124, NULL, 1, 'c', 1, '2019-01-05 08:25:30', NULL),
(99, 125, NULL, 1, 'df', 81, '2019-01-05 09:07:25', NULL),
(100, 125, NULL, 1, 'cls', 81, '2019-01-05 09:09:22', NULL),
(101, 120, NULL, 1, 'strat', 1, '2019-01-07 08:51:53', NULL),
(102, 129, NULL, 1, 'test properly', 1, '2019-01-11 09:10:52', NULL),
(103, 129, NULL, 1, 'check', 84, '2019-01-11 09:13:19', NULL),
(104, 129, NULL, 1, 'yes', 1, '2019-01-11 09:13:41', NULL),
(105, 129, NULL, 1, 'no', 1, '2019-01-11 09:14:10', NULL),
(106, 129, NULL, 1, 'ttt', 27, '2019-01-11 09:15:28', NULL),
(107, 129, NULL, 1, ' Service Closed', 27, '2019-01-11 09:38:49', NULL),
(108, 127, NULL, 1, 'test test', 1, '2019-01-11 09:40:34', NULL),
(109, 127, NULL, 1, 'yesy', 27, '2019-01-11 09:40:55', NULL),
(110, 127, NULL, 1, 'bbjjjj', 84, '2019-01-11 09:41:24', NULL),
(111, 127, NULL, 1, ' Service Closed', 27, '2019-01-11 09:42:02', NULL),
(112, 128, NULL, 1, 'dfgd', 1, '2019-01-11 09:46:29', NULL),
(113, 128, NULL, 1, ' Service Closed', 84, '2019-01-11 10:21:43', NULL),
(114, 135, NULL, 1, 'tttt', 81, '2019-01-12 13:13:47', NULL),
(115, 135, NULL, 1, 'tyty', 81, '2019-01-12 13:14:12', NULL),
(116, 135, NULL, 1, 'ccc', 81, '2019-01-12 13:14:26', NULL),
(117, 111, NULL, 1, ' Service Closed', 84, '2019-01-16 08:11:08', NULL),
(118, 134, NULL, 1, 'testing and the best way to reach', 84, '2019-01-16 09:24:04', NULL),
(119, 133, NULL, 1, 'testing and the best way to reach', 84, '2019-01-16 09:25:20', NULL),
(120, 134, NULL, 1, 'check', 84, '2019-01-16 09:26:49', NULL),
(121, 134, NULL, 1, ' Service Closed', 84, '2019-01-16 09:27:22', NULL),
(122, 133, NULL, 1, ' Service Closed', 84, '2019-01-16 09:27:50', NULL),
(123, 132, NULL, 1, 'gygty', 1, '2019-01-16 09:29:13', NULL),
(124, 132, NULL, 1, 'close', 1, '2019-01-16 09:29:35', NULL),
(125, 131, NULL, 1, 'bhjfhkhjkhjkh', 84, '2019-01-16 09:31:31', NULL),
(126, 131, NULL, 1, 'close', 84, '2019-01-16 09:32:25', NULL),
(127, 130, NULL, 1, '543256', 84, '2019-01-16 09:32:47', NULL),
(128, 130, NULL, 1, 'close', 84, '2019-01-16 09:32:59', NULL),
(129, 119, NULL, 1, 'ggu', 1, '2019-01-16 09:34:14', NULL),
(130, 119, NULL, 1, ' Service Closed', 1, '2019-01-16 09:34:36', NULL),
(131, 126, NULL, 1, 'vigug', 2, '2019-01-16 09:44:55', NULL),
(132, 126, NULL, 1, ' Service Closed', 2, '2019-01-16 09:45:19', NULL),
(133, 109, NULL, 1, 'test', 84, '2019-01-16 12:41:34', NULL),
(134, 109, NULL, 1, ' Service Closed', 84, '2019-01-16 12:42:06', NULL),
(135, 107, NULL, 1, 'yyhyhy', 1, '2019-01-16 12:43:02', NULL),
(136, 108, NULL, 1, 'dfhgh', 1, '2019-01-16 12:45:56', NULL),
(137, 107, NULL, 1, 'close', 1, '2019-01-16 12:46:28', NULL),
(138, 105, NULL, 1, 'ertgrgy', 1, '2019-01-16 15:49:10', NULL),
(139, 105, NULL, 1, 'close', 1, '2019-01-16 15:52:54', NULL),
(140, 103, NULL, 1, 'dtghr', 84, '2019-01-16 15:53:59', NULL),
(141, 103, NULL, 1, 'close', 84, '2019-01-16 15:54:30', NULL),
(142, 123, NULL, 1, 'close', 1, '2019-01-16 15:57:02', NULL),
(143, 122, NULL, 1, 'dfg', 1, '2019-01-16 15:57:36', NULL),
(144, 107, NULL, 1, 'close', 84, '2019-01-16 15:59:14', NULL),
(145, 104, NULL, 1, 'check', 1, '2019-01-17 09:38:26', NULL),
(146, 93, NULL, 1, 'sada', 84, '2019-01-18 16:36:11', NULL),
(147, 93, NULL, 1, 'sd', 84, '2019-01-18 16:36:13', NULL),
(148, 93, NULL, 1, '0', 84, '2019-01-18 16:36:20', NULL),
(149, 136, NULL, 1, 'please attend by tomorrow', 81, '2019-01-19 07:21:48', NULL),
(150, 136, NULL, 1, 'martin is on his way', 81, '2019-01-19 07:28:41', NULL),
(151, 136, NULL, 1, 'job done. are you happy', 81, '2019-01-19 07:31:04', NULL),
(152, 101, NULL, 1, 'Test Test Test', 1, '2019-01-19 08:37:04', NULL),
(153, 101, NULL, 1, 'check\n', 1, '2019-01-19 08:38:25', NULL),
(154, 101, NULL, 1, '0', 1, '2019-01-19 08:42:47', NULL),
(155, 99, NULL, 1, 'check', 84, '2019-01-19 08:43:59', NULL),
(156, 99, NULL, 1, 'check123', 84, '2019-01-19 08:44:10', NULL),
(157, 99, NULL, 1, 'test', 1, '2019-01-19 08:44:37', NULL),
(158, 99, NULL, 1, '0', 84, '2019-01-19 08:45:03', NULL),
(159, 97, NULL, 1, 'test test', 84, '2019-01-19 08:46:54', NULL),
(160, 97, NULL, 1, 'vbedhvh', 1, '2019-01-19 08:47:11', NULL),
(161, 97, NULL, 1, '0', 84, '2019-01-19 08:47:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(222) NOT NULL,
  `firstname` varchar(222) DEFAULT NULL,
  `lastname` varchar(222) DEFAULT NULL,
  `companyname` varchar(222) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `alternate_phone` varchar(50) DEFAULT NULL,
  `email` varchar(222) NOT NULL,
  `reg_no` varchar(222) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `address` text,
  `is_admin` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '1->Active,2->Inactive',
  `user_type` int(2) DEFAULT NULL COMMENT '1-&gt;Service manager 2-&gt;Sales manager 3-&gt;Service Executive 4-&gt;customer',
  `added_by` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `deviceId` varchar(100) DEFAULT NULL,
  `gcmId` text,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `firstname`, `lastname`, `companyname`, `phone`, `alternate_phone`, `email`, `reg_no`, `location`, `city`, `id_city`, `password`, `address`, `is_admin`, `status`, `user_type`, `added_by`, `add_date`, `deviceId`, `gcmId`, `ip`) VALUES
(1, 'iliftadmin', 'iliftadmin', '', '9861601016', '8280030338', 'info@iliftforklifts.co.za', NULL, NULL, NULL, NULL, '111111', 'BBsr', 1, 1, 7, NULL, '2018-07-17 14:06:07', 'f4a4437be57983ee', 'dTgjEoN6iNE:APA91bGhpklRP4Lcey_3mo7rVhQSuvxpfdmXHS4nnm1AUI8FmRrQ3zZ2NQjgt-fDx1WY-IjaQBUoYtgltebvxZmu0McY3MNZsW76lAE9puQCvWIkbSOEpWyKs--Bmosve654l8-SQ5sp', NULL),
(2, 'mahesh SM', NULL, '', '9439328268', '4545455556', 'servicemanager@ilift.com', NULL, NULL, 'khurdha', 1, '111111', 'bbsr', 0, 1, 1, 1, '2018-12-31 08:49:51', 'f4a4437be57983ee', '', NULL),
(22, 'surplus1', NULL, '', '4741255452', '4568566665', 'salesmanager@ilift.com', NULL, NULL, 'bbsr', 2, '111111', 'bbsr', 0, 1, 2, 1, '2019-01-03 09:06:40', '949349623e96d4ee', '', NULL),
(23, 'Orimark', NULL, '', '4554578455', '5656655644', 'career@orimarktechnologies.com', NULL, NULL, 'bbsr', 1, '123456', 'Sahid Nagar', 0, 1, 2, 1, '2019-01-02 12:02:13', NULL, NULL, NULL),
(25, 'test puru', NULL, '', '7878784561', '', 'testpuru@gmail.com', NULL, NULL, NULL, 1, '111111', 'khordha', 0, 2, 3, NULL, '2018-07-17 19:18:19', NULL, NULL, NULL),
(27, 'Namita Das', NULL, '', '3232323232', '', 'namitadas1@gmail.com', NULL, NULL, NULL, 1, '111111', '32323', 0, 1, 3, NULL, '2018-07-17 19:22:05', 'f4a4437be57983ee', '', NULL),
(46, 'New executive', NULL, NULL, '2346578324', '', 'patrakisan.923@gmail.com', NULL, NULL, NULL, 2, '111111', 'BBSR', 0, 1, 3, 1, '2019-01-11 08:51:14', NULL, NULL, NULL),
(47, 'kisan', NULL, 'kumar group', '4567891234', NULL, 'kisanpatra.92@gmail.com', '', 'jayadev bihar', 'Bhubaneswar', 1, '111111', NULL, 0, 1, 4, NULL, '2018-08-30 20:25:16', 'f4a4437be57983ee', '', NULL),
(48, 'raj', NULL, 'Raj industry', '4567891235', NULL, 'raj@gmail.com', '', 'Jayadeva', 'Bhubaneswar', 2, '111111', NULL, 0, 1, 4, NULL, '2018-08-30 20:26:55', 'f84b962ff09efab5', '', NULL),
(49, 'chitta', NULL, 'orimark', '9437302803', NULL, 'systemadmin@orimark.com', '', 'Sahid Nagar', 'bhubanesqar', 1, '111111', NULL, 0, 1, 4, NULL, '2018-08-31 15:27:09', '9d67c3bc1ef8568', '', NULL),
(50, 'Bichitra', NULL, NULL, '9437302805', NULL, 'bichitra@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 0, 1, 3, 2, '2018-09-01 16:13:01', '1154f36d5239aa9e', '', NULL),
(51, 'vivek', NULL, 'univate', '9438505144', NULL, 'vivek.das@univate.in', '', 'Saheed nagar', 'Bhubaneswar', 1, '1234', NULL, 0, 1, 4, NULL, '2018-09-04 14:55:06', '6e5ff888bf6ff565', '', NULL),
(52, 'demo new', NULL, 'test test', '1234567995', NULL, 'kis@gmail.com', '', 'vhhjk', 'fghjjj', 2, '111111', NULL, 0, 1, 4, NULL, '2018-09-04 17:58:18', NULL, NULL, NULL),
(53, 'ghh', NULL, 'cgh', '8569321470', NULL, 'kisan.patra@thoughtspheres.com', '', 'vgvb', 'ggbb', 1, '111111', NULL, 0, 1, 4, NULL, '2018-09-04 17:59:07', NULL, NULL, NULL),
(54, 'nisikantkar', NULL, 'Nisikant Kar', '9938989900', NULL, 'nisikantkar@orimark.com', '', 'Saheednagar', 'Bhubaneswar', 2, 'linku1', NULL, 0, 1, 4, NULL, '2018-09-05 11:55:08', NULL, NULL, NULL),
(55, 'nisikantkar', NULL, 'Univate', '9438181899', NULL, 'nisikant.kar@univate.in', '', 'bbsr', 'Bbsr', 1, '123456', NULL, 0, 1, 4, NULL, '2018-09-05 12:44:26', '547ec9b56adaa751', '', NULL),
(56, 'lalit', NULL, 'otpl', '9556786167', NULL, 'lalitrath24@gmail.com', '', 'bbsr', 'bbsr', 2, '1234', NULL, 0, 1, 4, NULL, '2018-09-05 14:31:44', '1154f36d5239aa9e', '', NULL),
(57, 'chitta', NULL, 'ori', '9437302804', NULL, 'chitta@orimarktechnologies.com', '', 'bbst', 'sahid', 1, '111111', NULL, 0, 1, 4, NULL, '2018-09-05 17:10:09', NULL, NULL, NULL),
(58, 'Demo Executive', NULL, NULL, '7474543757', NULL, 'exe@gmail.com', NULL, NULL, NULL, 2, '111111', NULL, 0, 2, 3, NULL, '2018-09-07 19:29:52', NULL, NULL, NULL),
(59, 'shalini.rath', NULL, 'Swarna', '8763413139', NULL, 'rathshalini@yahoo.in', '', 'India', 'Bhubaneswar', 1, 'intexP2009', NULL, 0, 1, 4, NULL, '2018-09-11 16:39:16', '602c5820236917be', '', NULL),
(60, 'S T', NULL, 'Sudhir', '9876543211', NULL, 'nisikant.kar@koolfeedback.com', '', 'bbsr', 'odisha', 2, '111111', NULL, 0, 1, 4, NULL, '2018-09-11 19:43:58', '9d67c3bc1ef8568', '', NULL),
(61, 'Ramesh', NULL, 'ISIS', '9876598765', NULL, 'rfff@hh.bb', '', 'test', 'tesf', 1, '12', NULL, 0, 1, 4, NULL, '2018-09-11 19:49:03', NULL, NULL, NULL),
(62, 'Vivek Ilift', NULL, 'Orizen', '9439008434', NULL, 'vibek@orizen.com', '', 'puri', 'puri', 2, '123456', NULL, 0, 1, 4, NULL, '2018-09-12 16:07:34', NULL, NULL, NULL),
(63, 'rajesh', NULL, 'Infotech systech', '9040092345', NULL, 'rakesh@gmail.com', '', 'sahid nagar', 'bbsr', 1, '111111', NULL, 0, 1, 4, NULL, '2018-09-15 12:49:42', NULL, NULL, NULL),
(64, 'vvek', NULL, 'Univate', '9938989901', NULL, 'vibek@gmail.com', '', 'Odisha', 'test', 2, '123456', NULL, 0, 1, 4, NULL, '2018-09-15 12:56:51', NULL, NULL, NULL),
(65, 'Vijay', NULL, 'Orizen', '0835084897', NULL, 'vijay@orizengroup.com', '', 'Jet Park', 'Johannesburg', 1, '123456', NULL, 0, 1, 4, NULL, '2018-09-27 20:07:00', '01133c1ad83ae435', '', NULL),
(66, 'inf', NULL, 'Infotech', '9437302809', NULL, 'infi@hh.ddd', '', 'tst', 'ygg', 2, '123', NULL, 0, 1, 4, NULL, '2018-10-04 11:35:19', NULL, NULL, NULL),
(67, 'dd', NULL, 'hhhf', '9437385256', NULL, 'ff@ddd.jjj', '', 'yhdye', 'dhye', 1, '123456789123456789123456789', NULL, 0, 1, 4, NULL, '2018-10-04 11:37:21', NULL, NULL, NULL),
(68, 'fufu', NULL, 'fyezfu', '8585586888', NULL, 'bbs@gmail.com', '', 'cbchf', 'hxhch', 2, '1111', NULL, 0, 1, 4, NULL, '2018-12-03 15:15:53', NULL, NULL, NULL),
(69, 'puru', NULL, NULL, '9861601711', NULL, 'sahoo.puru@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 2, 1, 7, NULL, '2018-12-19 15:55:56', NULL, NULL, NULL),
(70, 'Suparijita1', NULL, NULL, '7878787878', NULL, 'suparijita@ts.com', NULL, NULL, '', 2, '111111', NULL, 2, 1, 7, 1, '2018-12-29 08:14:52', 'a36627fbb5ea109b', '', NULL),
(71, 'puru', NULL, NULL, '1234567890', NULL, 'puru@ts.com', NULL, NULL, NULL, 2, 'password', NULL, 2, 1, 7, NULL, '2018-12-19 16:51:16', NULL, NULL, NULL),
(72, 'Suparijita1234', NULL, NULL, '8280030335', '45454555558', 'suparijita.mohanty@gmail.com', NULL, NULL, 'bbsr', 2, '111111', 'Jaidev bihar1', 0, 1, 1, 1, '2019-01-02 09:34:32', '949349623e96d4ee', 'eSUrG2aAG1s:APA91bHIrCBK-UsP5kWUkasuUJqB9tJCfho-Ga7rKHrJ-I66qUfXMrhyqbt8CutsYeB5JVkAPhGS9Z89xlkkCYER3mWN7qyJvzMJEJmMrmbL1zkl6Db5QjvpHwg1jITeGG1f12tNNblc', NULL),
(73, 'twst', NULL, 'ts', '8885707579', NULL, 'ttt@gmail.com', '', 'FSA ga', NULL, 1, '1111', NULL, 0, 3, 4, NULL, '2018-12-24 14:58:07', NULL, NULL, NULL),
(74, 'mahesh', NULL, 'tsss', '8270951636', NULL, 'mahesh@gmail.com', '', 'fgh', NULL, 2, '111111', NULL, 0, 1, 4, NULL, '2018-12-24 15:01:21', '1607a79c94f216c0', '', NULL),
(75, 'service ex 1', NULL, NULL, '7897897897', NULL, 'seviceex1@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 0, 1, 3, 1, '2019-01-11 09:56:03', 'f4a4437be57983ee', '', NULL),
(76, 'service ex2', NULL, NULL, '7417417417', NULL, 'serviceex2@gmail.com', NULL, NULL, NULL, 2, '222222', NULL, 0, 1, 3, NULL, '2018-12-27 15:17:10', NULL, NULL, NULL),
(77, 'service ex 3', NULL, NULL, '7537537537', NULL, 'serviceex3@gmail.com', NULL, NULL, NULL, 1, '222222', NULL, 0, 1, 3, NULL, '2018-12-27 15:19:48', NULL, NULL, NULL),
(78, 'service ex 4', NULL, NULL, '3213213213', NULL, 'serviceex4@gmail.com', NULL, NULL, NULL, 2, '111111', NULL, 0, 1, 3, 1, '2019-01-11 10:10:22', 'f4a4437be57983ee', '', NULL),
(79, 'service ex 5', NULL, NULL, '5656565656', NULL, 'serviceex5@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 0, 1, 3, 70, '2018-12-27 15:30:10', NULL, NULL, NULL),
(80, 'puru', NULL, NULL, '8976543456', NULL, 'puru@puru.com', NULL, NULL, NULL, 1, '111111', NULL, 0, 1, 0, 1, '2018-12-29 08:29:44', NULL, NULL, NULL),
(81, 'Lalit', NULL, NULL, '1123456789', NULL, 'lalitrath@orimark.com', NULL, NULL, NULL, 2, '111111', NULL, 2, 1, 7, 1, '2019-01-05 08:37:39', '9d67c3bc1ef8568', 'ecEWlPvknew:APA91bHdln34zMlMHI3y1SaDcg2OhHE3Wo4oSosxvEyThu0FVeYlIK4FDMfHr8JnuLvQzfCbfq4kU9JAmAoxnjJ--yNrYrMX1nbf4ow0RGUJD4lWMEjERks4xopJKac3IWAKSnDq-oEq', NULL),
(82, 'Tet', NULL, NULL, '1212121212', '111111', 'serviceex12@gmail.com', NULL, NULL, NULL, 1, '111111', '111111', 0, 1, 2, 1, '2018-12-29 08:38:05', NULL, NULL, NULL),
(83, 'happy singh', NULL, NULL, '1234567893', NULL, 'happy@ts.com', NULL, NULL, NULL, 1, '111111', NULL, 2, 1, 7, 1, '2018-12-31 08:01:53', NULL, NULL, NULL),
(84, 'kisan admin', NULL, NULL, '8956451223', NULL, 'patrakisan.92@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 2, 1, 7, 1, '2019-01-19 08:40:16', 'f4a4437be57983ee', '', NULL),
(85, 'kisan service manager', NULL, NULL, '8888123434', '', 'kisanservice@gmail.com', NULL, NULL, NULL, 3, '111111', 'bhubaneswar', 0, 1, 1, 1, '2019-01-03 09:06:29', NULL, NULL, NULL),
(86, 'kisan sales manager', NULL, NULL, '8998988998', '', 'kisansales@gmail.com', NULL, NULL, NULL, 3, '111111', 'bbsr bbsr', 0, 1, 2, 1, '2018-12-31 08:31:37', NULL, NULL, NULL),
(87, 'kisanunder sub', NULL, NULL, '7878946544', NULL, 'kisanserv@gmail.com', NULL, NULL, NULL, 3, '111111', NULL, 0, 1, 3, 1, '2018-12-31 09:17:31', NULL, NULL, NULL),
(88, 'raj', NULL, 'rajendra co ltd', '4567891236', NULL, 'raj123@gmail.com', '', 'orisha', '', 3, 'raj1234', NULL, 0, 1, 4, NULL, '2019-01-02 08:25:08', 'f4a4437be57983ee', '', NULL),
(89, 'hehe', NULL, 'ggzh', '6464904949', NULL, 'hwhwh@hwjw.jzj', '', 'hwhhw', '', 2, '11111', NULL, 0, 3, 4, NULL, '2019-01-02 08:33:36', NULL, NULL, NULL),
(90, 'raka', NULL, 'rakesh co ltd', '4567895689', NULL, 'raka@gmail.com', '', 'orisa', '', 3, '111111', NULL, 0, 3, 4, NULL, '2019-01-02 12:37:20', NULL, NULL, NULL),
(91, 'JB', NULL, NULL, '2233445566', NULL, 'jb@ga.com', NULL, NULL, NULL, 2, '111111', NULL, 0, 1, 3, 81, '2019-01-05 08:40:30', NULL, NULL, NULL),
(92, 'chitta', NULL, 'ori', '9437302808', NULL, 'cet@gmail.com', '', 'sub', '', 2, '111111', NULL, 0, 1, 4, NULL, '2019-01-05 08:52:35', '01133c1ad83ae435', 'fKhTZJwUXTA:APA91bFS-Idq7h-KMhsaZOk3Gehwg2BDcLkuIdUj7BIhZZ7pNmL8gzLSX-8SXemA0MAaieZ5m4H3_nYi7xhk3hTImGuTuFDv3qifq4AzlAntV3yQkLGBXmY2MnNcwguRrmUmBsIs0Jjk', NULL),
(93, 'newservice', NULL, NULL, '1111111111', NULL, 'newservice@gmail.com', NULL, NULL, NULL, 1, '111111', NULL, 0, 1, 3, 1, '2019-01-11 09:59:58', 'f4a4437be57983ee', '', NULL),
(94, 'Chitta', NULL, NULL, '1111511114', NULL, 'chitta@gmail.com', NULL, NULL, NULL, 3, '111111', NULL, 2, 1, 7, 1, '2019-01-17 14:35:58', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_breakdown_request`
--

CREATE TABLE `user_breakdown_request` (
  `id` int(11) NOT NULL,
  `id_breakdown` int(111) DEFAULT NULL,
  `id_user` int(111) DEFAULT NULL,
  `contact_person` varchar(222) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `product_identification` varchar(222) DEFAULT NULL,
  `expectedbreakdowndate` datetime DEFAULT NULL,
  `location` varchar(111) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(222) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '3',
  `add_date` datetime DEFAULT NULL,
  `assign_executive_id` int(11) DEFAULT NULL,
  `statusnote` text,
  `assigndate` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT '0.0',
  `rating_date` datetime DEFAULT NULL,
  `closeby` int(11) DEFAULT NULL,
  `closedate` datetime DEFAULT NULL,
  `invoice` smallint(1) DEFAULT '0',
  `signature` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_breakdown_request`
--

INSERT INTO `user_breakdown_request` (`id`, `id_breakdown`, `id_user`, `contact_person`, `contact_number`, `product_identification`, `expectedbreakdowndate`, `location`, `id_city`, `comments`, `subcategorydata`, `image`, `status`, `add_date`, `assign_executive_id`, `statusnote`, `assigndate`, `user_review`, `rating`, `rating_date`, `closeby`, `closedate`, `invoice`, `signature`) VALUES
(1, 1, 48, 'sanu', '74589631458', 'test as new product', NULL, 'Jayadeva', 1, 'test test test', NULL, NULL, 1, '2018-08-30 20:38:31', 27, 'test for new progress', '2018-08-30 21:19:50', 'test', 2.5, '2018-08-30 21:29:17', NULL, NULL, 0, NULL),
(2, 3, 48, 'test123', '8456797979', 'test', NULL, 'bbsr', 2, 'best', NULL, NULL, 1, '2018-08-30 21:27:53', 27, NULL, '2018-08-30 21:31:30', 'test', 3.5, '2018-08-30 21:33:26', NULL, NULL, 0, NULL),
(3, 2, 49, 'hhh', '666', 'hhj', NULL, 'ctc', 1, 'hh', NULL, NULL, 4, '2018-09-01 16:20:27', 50, NULL, '2018-09-15 15:22:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(4, 3, 51, 'vivek', '7978782185', 'forklift 1', NULL, 'saheednagar', 2, 'text', NULL, NULL, 4, '2018-09-04 14:57:16', 50, NULL, '2018-09-05 15:39:34', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(5, 4, 48, 'sam', '456789052', 'long product', NULL, 'jayadev bihar', 1, 'Test\nTest\nTest\nTest up\nTest\nTest\nTest\nTest', NULL, NULL, 1, '2018-09-04 16:59:38', 27, 'drtdtdtdtgdtdt', '2018-09-04 17:42:19', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(6, 1, 48, 'bikas', '7589669888', 'good', NULL, 'Jayadev Bihar', 2, 'Cfgv\nXgv\nCvvb\nBbb\nBb', NULL, NULL, 1, '2018-09-04 17:13:15', 27, NULL, '2018-09-04 18:22:24', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(7, 2, 48, 'ghh', '566', 'vv', NULL, 'ghh', 1, 'Vvv', NULL, NULL, 3, '2018-09-04 17:15:07', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(8, 2, 48, 'hhj', '66', 'gbj', NULL, 'ghj', 2, 'Ghh\nBhjj', NULL, NULL, 1, '2018-09-04 17:22:16', 27, 'test', '2018-09-04 17:39:28', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(9, 2, 48, 'ydyd', '6868686868683', 'hdhd', NULL, 'hdhd', 1, 'Hxhxh', NULL, NULL, 3, '2018-09-04 18:22:05', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(10, 3, 48, 'rth', '55788868', 'edfd', NULL, 'rdfgh', 2, 'Gzdhdhfuofh', NULL, NULL, 1, '2018-09-04 18:23:55', 27, 'ghgfhg', '2018-09-04 18:25:31', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(11, 3, 48, 'gghh', '556', 'gghh', NULL, 'ygg', 1, 'Vvvbbbbbb', NULL, NULL, 1, '2018-09-04 19:24:47', 27, NULL, '2018-09-17 11:45:34', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(12, 3, 48, 'gghh', '5563', 'vggg', NULL, 'ggh', 2, 'Vbbhhu', NULL, NULL, 1, '2018-09-04 19:57:22', 27, 'hfghfgh', '2018-09-04 19:57:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(13, 2, 48, 'yzhzh', '6868', 'habxhx', NULL, 'hdhdh', 1, 'Hdhxhjxjxhx', NULL, NULL, 1, '2018-09-04 20:01:17', 27, 'ytguygyujgh', '2018-09-04 20:01:30', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(14, 2, 48, 'tghh', '59666', 'ggh', NULL, 'fghh', 2, 'Cvbjk', NULL, NULL, 1, '2018-09-04 20:05:42', 27, 'fghfghfh', '2018-09-04 20:06:02', 'test test test', 3.5, '2018-09-07 16:40:34', NULL, NULL, 0, NULL),
(15, 2, 48, 'hh', '66', 'fggh', NULL, 'fggg', 1, 'Gh', NULL, NULL, 4, '2018-09-04 20:09:36', 27, 'hfghfhg', '2018-09-04 20:09:51', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(16, 3, 56, 'ggg', '9556786167', 'h', NULL, 'ff', 2, 'Abcd', NULL, NULL, 1, '2018-09-05 14:33:18', 50, NULL, '2018-09-05 14:34:27', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(17, 3, 56, 'gzgh', '5442608428', 'fd', NULL, 'cjf', 1, 'Dggf', NULL, NULL, 1, '2018-09-05 14:43:36', 50, '4745', '2018-09-05 14:46:53', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(18, 1, 56, 'fjhh', '2156274250', 'fg', NULL, 'fzg', 2, 'Tifg', NULL, NULL, 3, '2018-09-05 15:31:20', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(19, 3, 56, 'dugg', '5485284458', 'fhh', NULL, 'fdj', 1, 'Ch', NULL, NULL, 1, '2018-09-05 15:32:11', 50, 'check it', '2018-09-05 15:38:01', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(20, 1, 56, 'cc', '558', 'ff', NULL, 'vv', 2, 'Vvv', NULL, NULL, 3, '2018-09-05 15:36:39', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(21, 2, 56, 'vv', '555', 'gf', NULL, 'gg', 1, 'Vv', NULL, NULL, 4, '2018-09-05 15:40:38', 46, NULL, '2018-09-15 15:22:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(22, 4, 59, 'Xavier', '1231415280', 'rechargeable batteries', NULL, 'Bhubaneswar', 2, 'The batteries is slightly damaged', NULL, NULL, 1, '2018-09-11 16:53:07', 50, NULL, '2018-09-14 15:52:42', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(23, 4, 55, 'vijay', '0835084897', 'A24', NULL, 'Jet park', 1, 'Battery not lasting', NULL, NULL, 1, '2018-09-11 19:56:21', 27, NULL, '2018-09-11 20:02:29', 'Good and intime service', 5.0, '2018-09-15 15:23:19', NULL, NULL, 0, NULL),
(24, 2, 48, 'test tsst', '7567644435537', 'test demo', NULL, 'bbsr', 2, 'Zhfzfusyrs\nJfzyfsuts\nUfzy\nDidiydyi\nFihdiy', NULL, NULL, 1, '2018-09-15 15:27:55', 27, NULL, '2018-09-15 15:29:00', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(25, 3, 48, 'gusigdig', '86856737537357', 'kdihfih', NULL, 'djxigxuy', 1, 'Hzgjzugxud', NULL, NULL, 1, '2018-09-15 15:33:04', 27, NULL, '2018-09-15 15:33:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(26, 4, 48, 'hxhududh', '68668686898', 'judud', NULL, 'hxhux', 2, 'Gdgdgdg\nGgxgxgx\nHxhchc\nHdhdh\nHxhd', NULL, NULL, 1, '2018-09-15 16:49:11', 27, NULL, '2018-09-17 11:38:26', 'ttttt', 4.5, '2018-09-17 17:05:54', NULL, NULL, 0, NULL),
(27, 3, 47, 'hdyd', '8686', 'xfu', NULL, 'duf7', 1, 'Xhdufugif\nXhdufugif\nBxh', NULL, NULL, 3, '2018-09-17 12:09:58', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(28, 3, 47, 'ydhdu', '6565665', 'jdjjd', NULL, 'hdjdj', 2, 'Hdhd\nHdhd\nHdhd\nHdhd\nUdud', NULL, NULL, 3, '2018-09-17 12:13:51', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(29, 3, 47, 'ydhdu', '6565665', 'jdjjd', NULL, 'hdjdj', 1, 'Hdhd\nHdhd\nHdhd\nHdhd\nUdud', NULL, NULL, 3, '2018-09-17 12:14:50', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(30, 1, 48, 'hkdjgd', '556668', 'jgdd', NULL, 'kdgdy', 2, 'Zshfsd\nKvdc\n\nHkf', NULL, NULL, 1, '2018-09-17 16:58:31', 27, NULL, '2018-09-17 16:59:10', 'test', 2.5, '2018-09-17 17:04:34', NULL, NULL, 0, NULL),
(31, 2, 48, 'xbfjr', '9895', 'cjfi', NULL, 'bcncu', 1, 'Vxhdurjc\nGdhf', NULL, NULL, 1, '2018-09-17 17:00:12', 27, NULL, '2018-09-17 17:16:32', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(32, 3, 48, 'chf', '865', 'ydgidd', NULL, 'xhch', 2, 'Xhxhdhdhch\nDyd', NULL, NULL, 1, '2018-09-17 17:15:04', 27, NULL, '2018-09-17 17:15:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(33, 3, 48, 'hxhchchf', '98686595', 'fjfjfut', NULL, 'fhhhf', 1, 'Vxgxcgug', NULL, NULL, 1, '2018-09-17 17:55:39', 27, NULL, '2018-09-17 17:56:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(34, 2, 48, 'f', '868686', 'fkhfvjx', NULL, 'jxgj', 2, 'Xxgjxgjd\nGdfjguu\nVchchf\nGxhcf\nBchfhf cute', NULL, NULL, 1, '2018-09-17 17:57:25', 50, NULL, '2018-09-17 17:57:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(35, 2, 48, 'xgxycyf', '8656565', 'bchfhf', NULL, 'dhjgjg', 1, 'Gdyfuv\nHfjgu\nHfugi\nHvugk\nHfugi\nJhij', NULL, NULL, 1, '2018-09-17 18:50:40', 27, NULL, '2018-09-17 18:52:30', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(36, 2, 48, 'xhxhf', '8683', 'xgx', NULL, 'hcjg', 2, 'Hchcjhhi', NULL, NULL, 1, '2018-09-17 19:42:03', 27, NULL, '2018-09-17 19:45:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(37, 2, 48, 'ghhj', '5966', 'fghhh', NULL, 'ghhh', 1, 'Cbnn', NULL, NULL, 1, '2018-09-17 19:42:29', 27, NULL, '2018-09-17 19:43:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(38, 2, 48, 'fhi', '8666', 'tyy', NULL, 'rgh', 2, 'Xhjiig', NULL, NULL, 1, '2018-09-17 19:42:49', 27, NULL, '2018-09-17 19:44:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(39, 4, 49, 'hhs', '333', 'hh', NULL, 'hh', 1, 'Bbfb', NULL, NULL, 4, '2018-09-19 19:36:39', 77, 'TEST TEST ', '2019-01-11 09:56:39', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(40, 5, 48, 'rfy', '22699', 'er', NULL, 'rtt', 2, 'Fgyy', NULL, NULL, 3, '2018-10-10 16:42:56', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(41, 5, 47, 'titi', '868686868', 't66', NULL, 'rty', 1, 'Test', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '41_resquestbreakdown.jpg', 3, '2018-10-12 15:15:03', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(42, 4, 47, 'hdhys', '166464', 'nenej', NULL, 'jdjd', 2, 'Bdbfhf', '[]', '42_resquestbreakdown.jpg', 3, '2018-10-12 15:18:15', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(43, 5, 47, 'bb', '9895665', 'identify', NULL, 'sit', 1, 'Bdbbdhxhxhh', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-12 15:21:23', 93, 'TEst ', '2019-01-11 10:04:36', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(44, 5, 47, 'bahu3', '1234', 'identify1', NULL, 'location 2', 2, 'Tedt5', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '44_resquestbreakdown.jpg', 1, '2018-10-12 15:31:10', 27, NULL, '2018-10-12 15:32:00', NULL, 0.0, NULL, 72, '2019-01-02 13:02:05', 1, '44_breakdownsignature.jpg'),
(45, 5, 47, 'tsys', '5464', '5w6w', NULL, 'fsya', 1, 'Gsys', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-12 16:25:55', 93, 'Test TEST ', '2019-01-11 10:03:31', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(46, 4, 47, 'babu', '985985985', 'no', NULL, 'mark', 2, 'Test', '[]', '46_resquestbreakdown.jpg', 3, '2018-10-29 16:52:16', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(47, 4, 47, 'B', '7', 'b', NULL, 'B', 1, 'B', '[]', '47_resquestbreakdown.jpg', 1, '2018-10-29 18:14:14', 75, '75 ex 1 city id', '2019-01-11 09:57:44', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(48, 5, 47, 'yyyyyy', '33666', 'rrrrrr', NULL, 'ttttt', 2, 'Cccvvv', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '48_resquestbreakdown.jpg', 4, '2018-10-29 18:29:20', 78, 'retrt', '2019-01-19 08:53:31', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(49, 2, 47, 'cvbhhhjj', '896666669', 'dgbvhb', NULL, 'fffvghhhhhhh', 1, 'Cvhjjjj', '[]', '49_resquestbreakdown.jpg', 1, '2018-10-29 18:33:32', 87, 'hbv', '2019-01-02 08:42:59', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(50, 5, 48, 'yshs', '976464646', 'gshs', NULL, 'hshs', 2, 'Hsjs', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\"}, {\"id_breakdown\":\"5\",\"name\":\"fdhjfdyjh\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, '2018-12-01 11:08:42', 58, NULL, '2019-01-02 14:28:16', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(51, 5, 48, 'tayau', '97646497', 'gzy', NULL, 'gzzu', 1, 'Gsjsu', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"5\",\"name\":\"fdhjfdyjh\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, '2018-12-04 08:42:57', 27, NULL, '2019-01-02 15:55:03', NULL, 0.0, NULL, 27, '2019-01-02 16:00:20', 1, '51_breakdownsignature.jpg'),
(52, 1, 88, 'muna', '456', 'not mention', NULL, 'bbsr bbsr', 3, 'Yesterday today', '[]', '52_resquestbreakdown.jpg', 4, '2019-01-02 08:36:53', 87, '43243', '2019-01-19 08:54:45', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(53, 5, 88, 'jaga', '8989', 'target space', NULL, 'Bhutan', 3, 'Test description', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_breakdown\":\"5\",\"name\":\"fdhjfdyjh\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '53_resquestbreakdown.jpg', 1, '2019-01-02 08:40:17', 87, NULL, '2019-01-02 09:47:29', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(54, 2, 88, '123456', '568954', 'hhh', NULL, 'bbsr', 3, 'Teet', '[]', '54_resquestbreakdown.jpg', 1, '2019-01-02 11:04:18', 87, '56', '2019-01-02 13:24:28', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(55, 6, 88, 'bdhdh', '6565665656', 'hdhdh', NULL, 'hddhh', 3, 'Hdhdh', '[]', '55_resquestbreakdown.jpg', 1, '2019-01-02 15:36:13', 87, NULL, '2019-01-16 09:35:14', NULL, 0.0, NULL, NULL, NULL, 2, NULL),
(56, 5, 92, 'h hi ih', '556688556', 'h', NULL, 'yuu', 2, 'Vhh', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_breakdown\":\"5\",\"name\":\"fdhjfdyjh\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2019-01-05 09:12:23', 91, 'ddf', '2019-01-05 09:12:43', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(57, 1, 47, 'test', '456789', 'test', NULL, 'bbsr', 1, 'Check', '[]', '57_resquestbreakdown.jpg', 1, '2019-01-11 09:00:01', 0, 'tttttgggg', '2019-01-11 09:46:07', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(58, 5, 47, 'yard', '8888', 'check', NULL, 'bbsr', 1, 'You', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_breakdown\":\"5\",\"name\":\"fdhjfdyjh\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2019-01-11 09:01:56', 0, 'ggggggg', '2019-01-11 09:43:46', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(59, 1, 47, 'ndjdj', '65353', 'jdjdj', NULL, 'jdjjd', 1, 'Jdjdj', '[]', NULL, 1, '2019-01-11 09:52:25', 97, 'test', '2019-01-11 10:00:36', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(60, 1, 47, 'hdhd', '95656', 'jehdu', NULL, 'hdhud', 1, 'Bdhdh', '[]', NULL, 1, '2019-01-11 10:05:53', 93, 'test test', '2019-01-11 10:06:17', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(61, 1, 47, 'gug', '6838', 'uvu', NULL, 'ufu', 1, 'Hchf', '[]', '61_resquestbreakdown.jpg', 1, '2019-01-11 12:23:40', 27, 'trtrtrtr', '2019-01-19 08:51:33', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(62, 1, 47, 'vyv', '05058', 'hvyg uvu', NULL, 'bubu yvy', 1, 'G ycy', '[]', NULL, 1, '2019-01-11 12:25:34', 27, 'hrthrth', '2019-01-19 08:49:29', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(63, 1, 47, '3', '4', '1', NULL, '2', 1, '5', '[]', NULL, 1, '2019-01-11 12:29:59', 27, NULL, '2019-01-16 09:42:17', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(64, 1, 47, '8', '9', '6', NULL, '7', 1, '10', '[]', '64_resquestbreakdown.jpg', 1, '2019-01-11 12:30:40', 27, NULL, '2019-01-16 09:41:08', NULL, 0.0, NULL, NULL, NULL, 2, NULL),
(65, 7, 92, 'hhdhhd', '58848', 'hh', NULL, 'hdhs', 2, 'V vs vvs', '[{\"id_breakdown\":\"7\",\"name\":\"Test 1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"7\",\"name\":\"Test2\",\"type\":\"2\",\"status\":\"1\"}]', NULL, 1, '2019-01-12 13:11:13', 91, NULL, '2019-01-17 09:46:26', NULL, 0.0, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id_user_log` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `events` varchar(222) DEFAULT NULL,
  `log_status` varchar(222) DEFAULT NULL,
  `operation_mode` tinyint(4) NOT NULL COMMENT '1->Through Web,2->Through App',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id_user_log`, `id_user`, `events`, `log_status`, `operation_mode`, `add_date`, `ip`) VALUES
(1, 1, 'Log In', 'Success', 1, '2018-08-30 20:10:57', '182.73.48.34'),
(2, 2, 'Log In', 'Success', 1, '2018-08-30 20:11:42', '182.73.48.34'),
(3, 2, 'Logged out', 'Logout', 2, '2018-08-30 21:08:10', '182.73.48.34'),
(4, 1, 'Service Added', 'Success', 1, '2018-08-30 21:16:55', '182.73.48.34'),
(5, 1, 'Breakdown Added', 'Success', 1, '2018-08-30 21:17:25', '182.73.48.34'),
(6, 1, 'Item Added', 'Success', 1, '2018-08-30 21:18:25', '182.73.48.34'),
(7, 1, 'Log In', 'Success', 1, '2018-08-31 11:18:40', '182.73.48.34'),
(8, 1, 'Log In', 'Success', 1, '2018-08-31 12:46:25', '103.39.243.254'),
(9, 1, 'Logged out', 'Logout', 2, '2018-08-31 12:49:20', '103.39.243.254'),
(10, 1, 'Log In', 'Success', 1, '2018-08-31 12:49:44', '103.39.243.254'),
(11, 1, 'Item Added', 'Success', 1, '2018-08-31 12:52:55', '103.39.243.254'),
(12, 1, 'Breakdown Added', 'Success', 1, '2018-08-31 12:54:59', '103.39.243.254'),
(13, 1, 'Logged out', 'Logout', 2, '2018-08-31 13:25:54', '182.73.48.34'),
(14, 27, 'Log In', 'Success', 1, '2018-08-31 13:26:04', '182.73.48.34'),
(15, 1, 'Log In', 'Success', 1, '2018-08-31 16:00:38', '103.39.243.254'),
(16, 27, 'Logged out', 'Logout', 2, '2018-08-31 16:27:57', '182.73.48.34'),
(17, 1, 'Log In', 'Success', 1, '2018-08-31 16:28:04', '182.73.48.34'),
(18, 1, 'Logged out', 'Logout', 2, '2018-08-31 16:57:54', '182.73.48.34'),
(19, 27, 'Log In', 'Success', 1, '2018-08-31 16:58:06', '182.73.48.34'),
(20, 1, 'Log In', 'Success', 1, '2018-09-01 11:41:08', '103.39.243.8'),
(21, 2, 'Log In', 'Success', 1, '2018-09-01 11:59:48', '103.39.243.8'),
(22, 2, 'Logged out', 'Logout', 2, '2018-09-01 11:59:58', '103.39.243.8'),
(23, 27, 'Log In', 'Success', 1, '2018-09-01 12:03:05', '103.39.243.8'),
(24, 27, 'Logged out', 'Logout', 2, '2018-09-01 12:12:37', '103.39.243.8'),
(25, 2, 'Log In', 'Success', 1, '2018-09-01 12:12:52', '103.39.243.8'),
(26, 1, 'Log In', 'Success', 1, '2018-09-01 16:10:42', '103.213.27.0'),
(27, 1, 'Log In', 'Success', 1, '2018-09-04 10:45:29', '182.73.48.34'),
(28, 1, 'Logged out', 'Logout', 2, '2018-09-04 10:45:38', '182.73.48.34'),
(29, 1, 'Log In', 'Success', 1, '2018-09-04 10:47:10', '182.73.48.34'),
(30, 1, 'Log In', 'Success', 1, '2018-09-04 12:01:51', '182.73.48.34'),
(31, 1, 'Logged out', 'Logout', 2, '2018-09-04 13:08:22', '182.73.48.34'),
(32, 1, 'Log In', 'Success', 1, '2018-09-04 13:31:53', '103.39.243.8'),
(33, 1, 'Logged out', 'Logout', 2, '2018-09-04 13:34:38', '103.39.243.8'),
(34, 1, 'Log In', 'Success', 1, '2018-09-04 14:53:26', '103.39.243.8'),
(35, 1, 'Log In', 'Success', 1, '2018-09-04 16:47:00', '182.73.48.34'),
(36, 1, 'User Updated', 'Success', 1, '2018-09-04 16:51:34', '182.73.48.34'),
(37, 1, 'Logged out', 'Logout', 2, '2018-09-04 17:50:36', '182.73.48.34'),
(38, 27, 'Log In', 'Success', 1, '2018-09-04 17:50:43', '182.73.48.34'),
(39, 27, 'Logged out', 'Logout', 2, '2018-09-04 17:54:56', '182.73.48.34'),
(40, 1, 'Log In', 'Success', 1, '2018-09-04 17:55:02', '182.73.48.34'),
(41, 1, 'Log In', 'Success', 1, '2018-09-04 18:25:17', '182.73.48.34'),
(42, 1, 'Logged out', 'Logout', 2, '2018-09-04 19:18:07', '182.73.48.34'),
(43, 2, 'Log In', 'Success', 1, '2018-09-04 19:18:27', '182.73.48.34'),
(44, 2, 'Logged out', 'Logout', 2, '2018-09-04 19:20:36', '182.73.48.34'),
(45, 22, 'Log In', 'Success', 1, '2018-09-04 19:20:51', '182.73.48.34'),
(46, 22, 'Logged out', 'Logout', 2, '2018-09-04 19:21:09', '182.73.48.34'),
(47, 22, 'Log In', 'Success', 1, '2018-09-04 19:21:26', '182.73.48.34'),
(48, 22, 'Logged out', 'Logout', 2, '2018-09-04 19:22:43', '182.73.48.34'),
(49, 2, 'Log In', 'Success', 1, '2018-09-04 19:23:08', '182.73.48.34'),
(50, 27, 'Log In', 'Success', 1, '2018-09-04 19:29:05', '182.73.48.34'),
(51, 1, 'Log In', 'Success', 1, '2018-09-05 11:53:29', '182.73.48.34'),
(52, 1, 'Breakdown Updated', 'Success', 1, '2018-09-05 11:55:25', '182.73.48.34'),
(53, 1, 'Logged out', 'Logout', 2, '2018-09-05 12:03:56', '182.73.48.34'),
(54, 27, 'Log In', 'Success', 1, '2018-09-05 12:04:37', '182.73.48.34'),
(55, 27, 'Logged out', 'Logout', 2, '2018-09-05 12:06:20', '182.73.48.34'),
(56, 2, 'Log In', 'Success', 1, '2018-09-05 12:06:27', '182.73.48.34'),
(57, 2, 'Logged out', 'Logout', 2, '2018-09-05 12:08:39', '182.73.48.34'),
(58, 22, 'Log In', 'Success', 1, '2018-09-05 12:08:52', '182.73.48.34'),
(59, 22, 'Logged out', 'Logout', 2, '2018-09-05 12:09:42', '182.73.48.34'),
(60, 1, 'Log In', 'Success', 1, '2018-09-05 13:17:46', '103.213.25.37'),
(61, 1, 'Logged out', 'Logout', 2, '2018-09-05 13:33:11', '103.213.25.37'),
(62, 2, 'Log In', 'Success', 1, '2018-09-05 13:33:32', '103.213.25.37'),
(63, 2, 'Logged out', 'Logout', 2, '2018-09-05 13:49:21', '103.213.25.37'),
(64, 1, 'Log In', 'Success', 1, '2018-09-05 13:49:27', '103.213.25.37'),
(65, 1, 'User Updated', 'Success', 1, '2018-09-05 13:49:53', '103.213.25.37'),
(66, 1, 'Log In', 'Success', 1, '2018-09-05 17:53:50', '182.73.48.34'),
(67, 1, 'Log In', 'Success', 1, '2018-09-07 11:59:36', '182.73.48.34'),
(68, 1, 'Log In', 'Success', 1, '2018-09-07 12:10:01', '103.213.25.235'),
(69, 1, 'Log In', 'Success', 1, '2018-09-07 16:23:19', '182.73.48.34'),
(70, 1, 'Log In', 'Success', 1, '2018-09-07 19:04:23', '182.73.48.34'),
(71, 1, 'Logged out', 'Logout', 2, '2018-09-07 19:10:01', '182.73.48.34'),
(72, 1, 'Log In', 'Success', 1, '2018-09-07 19:10:14', '182.73.48.34'),
(73, 1, 'Log In', 'Success', 1, '2018-09-07 19:17:48', '182.73.48.34'),
(74, 1, 'Service Added', 'Success', 1, '2018-09-07 19:24:38', '182.73.48.34'),
(75, 1, 'Breakdown Added', 'Success', 1, '2018-09-07 19:25:59', '182.73.48.34'),
(76, 1, 'User Added', 'Success', 1, '2018-09-07 19:29:52', '182.73.48.34'),
(77, 1, 'Item Added', 'Success', 1, '2018-09-07 19:31:02', '182.73.48.34'),
(78, 1, 'Log In', 'Success', 1, '2018-09-08 11:47:52', '103.56.221.32'),
(79, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:49:42', '103.56.221.32'),
(80, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:50:10', '103.56.221.32'),
(81, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:50:35', '103.56.221.32'),
(82, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:53:26', '103.56.221.32'),
(83, 1, 'Breakdown Updated', 'Success', 1, '2018-09-08 11:53:58', '103.56.221.32'),
(84, 1, 'Log In', 'Success', 1, '2018-09-08 14:40:49', '103.56.221.32'),
(85, 1, 'Log In', 'Success', 1, '2018-09-11 11:38:18', '182.73.48.34'),
(86, 1, 'Breakdown Updated', 'Success', 1, '2018-09-11 12:32:43', '182.73.48.34'),
(87, 1, 'Log In', 'Success', 1, '2018-09-11 15:41:54', '103.213.24.205'),
(88, 1, 'Logged out', 'Logout', 2, '2018-09-11 15:46:03', '103.213.24.205'),
(89, 1, 'Log In', 'Success', 1, '2018-09-11 15:52:07', '103.213.24.205'),
(90, 1, 'User Updated', 'Success', 1, '2018-09-11 15:53:04', '103.213.24.205'),
(91, 1, 'Log In', 'Success', 1, '2018-09-11 16:02:03', '182.73.48.34'),
(92, 1, 'Log In', 'Success', 1, '2018-09-11 16:02:25', '182.73.48.34'),
(93, 1, 'Logged out', 'Logout', 2, '2018-09-11 16:35:24', '103.213.24.205'),
(94, 27, 'Log In', 'Success', 1, '2018-09-11 16:35:38', '103.213.24.205'),
(95, 27, 'Logged out', 'Logout', 2, '2018-09-11 16:35:45', '103.213.24.205'),
(96, 1, 'Log In', 'Success', 1, '2018-09-11 16:35:51', '103.213.24.205'),
(97, 1, 'User Updated', 'Success', 1, '2018-09-11 16:36:11', '103.213.24.205'),
(98, 1, 'Log In', 'Success', 1, '2018-09-11 20:09:40', '103.56.222.187'),
(99, 1, 'User Updated', 'Success', 1, '2018-09-11 20:11:49', '103.56.222.187'),
(100, 1, 'Log In', 'Success', 1, '2018-09-14 11:24:18', '182.73.48.34'),
(101, 1, 'Log In', 'Success', 1, '2018-09-14 15:58:12', '103.213.24.131'),
(102, 1, 'User Updated', 'Success', 1, '2018-09-14 15:59:45', '103.213.24.131'),
(103, 1, 'Logged out', 'Logout', 2, '2018-09-14 15:59:55', '103.213.24.131'),
(104, 1, 'Log In', 'Success', 1, '2018-09-14 16:06:48', '103.213.24.131'),
(105, 1, 'Log In', 'Success', 1, '2018-09-15 15:25:44', '103.213.27.182'),
(106, 1, 'Log In', 'Success', 1, '2018-09-15 15:26:13', '45.251.37.24'),
(107, 1, 'Logged out', 'Logout', 2, '2018-09-15 15:31:13', '45.251.37.24'),
(108, 1, 'Log In', 'Success', 1, '2018-09-15 15:46:40', '45.251.37.24'),
(109, 1, 'Logged out', 'Logout', 2, '2018-09-15 16:00:33', '103.213.27.182'),
(110, 1, 'Log In', 'Success', 1, '2018-09-15 16:16:32', '45.251.37.24'),
(111, 1, 'Logged out', 'Logout', 2, '2018-09-15 16:17:18', '45.251.37.24'),
(112, 27, 'Log In', 'Success', 1, '2018-09-15 16:18:54', '45.251.37.24'),
(113, 27, 'Logged out', 'Logout', 2, '2018-09-15 16:19:42', '45.251.37.24'),
(114, 1, 'Log In', 'Success', 1, '2018-09-17 11:28:29', '45.251.37.186'),
(115, 1, 'Log In', 'Success', 1, '2018-09-18 11:44:29', '182.73.48.34'),
(116, 1, 'Log In', 'Success', 1, '2018-09-18 12:55:33', '103.213.25.20'),
(117, 1, 'Log In', 'Success', 1, '2018-09-19 12:29:49', '182.73.48.34'),
(118, 1, 'Log In', 'Success', 1, '2018-09-19 19:05:39', '103.56.221.76'),
(119, 1, 'Log In', 'Success', 1, '2018-09-20 17:42:27', '182.73.48.34'),
(120, 1, 'Log In', 'Success', 1, '2018-09-21 12:05:05', '182.73.48.34'),
(121, 1, 'Log In', 'Success', 1, '2018-09-21 15:24:39', '182.73.48.34'),
(122, 1, 'Log In', 'Success', 1, '2018-09-27 19:51:12', '103.56.220.104'),
(123, 1, 'Item Added', 'Success', 1, '2018-09-27 20:10:32', '103.56.220.104'),
(124, 1, 'Log In', 'Success', 1, '2018-10-01 19:01:13', '182.73.48.34'),
(125, 1, 'Log In', 'Success', 1, '2018-10-01 19:21:38', '182.73.48.34'),
(126, 1, 'Log In', 'Success', 1, '2018-10-03 15:16:09', '182.73.48.34'),
(127, 1, 'Logged out', 'Logout', 2, '2018-10-03 15:17:22', '182.73.48.34'),
(128, 1, 'Log In', 'Success', 1, '2018-10-04 19:10:47', '103.56.223.4'),
(129, 1, 'Log In', 'Success', 1, '2018-10-08 11:43:01', '182.73.48.34'),
(130, 1, 'Log In', 'Success', 1, '2018-10-08 17:26:42', '182.73.48.34'),
(131, 1, 'Logged out', 'Logout', 2, '2018-10-08 19:11:04', '182.73.48.34'),
(132, 1, 'Log In', 'Success', 1, '2018-10-08 19:41:54', '182.73.48.34'),
(133, 1, 'Log In', 'Success', 1, '2018-10-08 19:45:00', '182.73.48.34'),
(134, 1, 'Service Updated', 'Success', 1, '2018-10-08 19:57:09', '182.73.48.34'),
(135, 1, 'Service Updated', 'Success', 1, '2018-10-08 19:58:28', '182.73.48.34'),
(136, 1, 'Service Added', 'Success', 1, '2018-10-08 20:00:14', '182.73.48.34'),
(137, 1, 'Logged out', 'Logout', 2, '2018-10-08 20:04:34', '182.73.48.34'),
(138, 1, 'Log In', 'Success', 1, '2018-10-09 16:58:52', '103.72.61.87'),
(139, 1, 'Service Added', 'Success', 1, '2018-10-09 17:01:31', '103.72.61.87'),
(140, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:01:50', '103.72.61.87'),
(141, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:01', '103.72.61.87'),
(142, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:19', '103.72.61.87'),
(143, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:35', '103.72.61.87'),
(144, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:04:02', '103.72.61.87'),
(145, 1, 'Log In', 'Success', 1, '2018-10-10 11:22:43', '182.73.48.34'),
(146, 1, 'Log In', 'Success', 1, '2018-10-10 11:57:33', '182.73.48.34'),
(147, 1, 'Logged out', 'Logout', 2, '2018-10-10 12:00:31', '182.73.48.34'),
(148, 1, 'Log In', 'Success', 1, '2018-10-12 13:06:56', '45.251.37.232'),
(149, 1, 'Breakdown Updated', 'Success', 1, '2018-10-12 13:07:24', '45.251.37.232'),
(150, 1, 'Item Updated', 'Success', 1, '2018-10-12 13:08:10', '45.251.37.232'),
(151, 1, 'Log In', 'Success', 1, '2018-10-12 14:42:46', '45.251.37.232'),
(152, 1, 'Logged out', 'Logout', 2, '2018-10-12 14:43:58', '45.251.37.232'),
(153, 1, 'Log In', 'Success', 1, '2018-10-12 14:46:41', '45.251.37.232'),
(154, 41, 'Logged out', 'Logout', 2, '2018-10-12 16:36:29', '192.168.1.38'),
(155, 1, 'Log In', 'Success', 1, '2018-10-12 16:36:46', '192.168.1.38'),
(156, 1, 'Log In', 'Success', 1, '2018-10-12 17:08:04', '192.168.1.38'),
(157, 1, 'Logged out', 'Logout', 2, '2018-10-12 17:59:47', '192.168.1.38'),
(158, 27, 'Log In', 'Success', 1, '2018-10-12 18:01:05', '192.168.1.38'),
(159, 27, 'Log In', 'Success', 1, '2018-10-12 18:51:40', '192.168.1.38'),
(160, 1, 'Log In', 'Success', 1, '2018-10-13 12:02:29', '192.168.1.38'),
(161, 1, 'Log In', 'Success', 1, '2018-10-13 13:26:53', '192.168.1.38'),
(162, 1, 'Log In', 'Success', 1, '2018-10-13 15:15:29', '192.168.1.38'),
(163, 1, 'Log In', 'Success', 1, '2018-10-15 15:43:48', '192.168.1.38'),
(164, 1, 'Log In', 'Success', 1, '2018-10-15 19:24:42', '192.168.1.38'),
(165, 1, 'Log In', 'Success', 1, '2018-10-16 14:39:10', '192.168.1.38'),
(166, 1, 'Log In', 'Success', 1, '2018-10-16 17:53:46', '192.168.1.38'),
(167, 1, 'Log In', 'Success', 1, '2018-10-17 13:16:36', '192.168.1.38'),
(168, 1, 'Log In', 'Success', 1, '2018-10-23 19:08:36', '192.168.1.38'),
(169, 1, 'Log In', 'Success', 1, '2018-10-29 12:39:32', '192.168.1.13'),
(170, 1, 'Log In', 'Success', 1, '2018-10-29 13:01:07', '192.168.1.38'),
(171, 1, 'Log In', 'Success', 1, '2018-10-29 15:16:09', '192.168.1.38'),
(172, 1, 'Log In', 'Success', 1, '2018-11-13 16:32:49', '192.168.1.10'),
(173, 1, 'Log In', 'Success', 1, '2018-11-13 18:31:45', '192.168.1.10'),
(174, 1, 'Log In', 'Success', 1, '2018-11-26 08:35:56', '192.168.1.10'),
(175, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:36:27', '192.168.1.10'),
(176, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:36:40', '192.168.1.10'),
(177, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:43:24', '192.168.1.10'),
(178, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:43:36', '192.168.1.10'),
(179, 1, 'Log In', 'Success', 1, '2018-11-26 08:45:17', '192.168.1.15'),
(180, 1, 'Service Updated', 'Success', 1, '2018-11-26 08:46:02', '192.168.1.15'),
(181, 1, 'Service Updated', 'Success', 1, '2018-11-26 08:46:23', '192.168.1.15'),
(182, 1, 'Service Updated', 'Success', 1, '2018-11-26 08:46:31', '192.168.1.15'),
(183, 1, 'Breakdown Updated', 'Success', 1, '2018-11-26 08:47:35', '192.168.1.15'),
(184, 1, 'Breakdown Updated', 'Success', 1, '2018-11-26 08:47:40', '192.168.1.15'),
(185, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:48:06', '192.168.1.15'),
(186, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:48:29', '192.168.1.15'),
(187, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:48:38', '192.168.1.15'),
(188, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:56:58', '192.168.1.15'),
(189, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:57:19', '192.168.1.15'),
(190, 1, 'Item Updated', 'Success', 1, '2018-11-26 08:57:29', '192.168.1.15'),
(191, 1, 'Service Updated', 'Success', 1, '2018-11-26 08:57:44', '192.168.1.15'),
(192, 1, 'Breakdown Updated', 'Success', 1, '2018-11-26 08:58:05', '192.168.1.15'),
(193, 1, 'Log In', 'Success', 1, '2018-12-01 08:47:39', '192.168.1.10'),
(194, 1, 'Log In', 'Success', 1, '2018-12-01 11:09:43', '192.168.1.10'),
(195, 1, 'Log In', 'Success', 1, '2018-12-03 09:34:05', '192.168.1.10'),
(196, 1, 'Log In', 'Success', 1, '2018-12-03 14:53:56', '192.168.1.15'),
(197, 1, 'Log In', 'Success', 1, '2018-12-19 07:36:26', '192.168.1.10'),
(198, 1, 'Log In', 'Success', 1, '2018-12-19 08:29:32', '192.168.1.13'),
(199, 1, 'City Added', 'Success', 1, '2018-12-19 11:20:58', '192.168.1.13'),
(200, 1, 'Log In', 'Success', 1, '2018-12-19 13:13:10', '192.168.1.13'),
(201, 1, 'Log In', 'Success', 1, '2018-12-19 13:15:42', '192.168.1.13'),
(202, 1, 'Log In', 'Success', 1, '2018-12-19 14:16:32', '192.168.1.10'),
(203, 1, 'City Added', 'Success', 1, '2018-12-19 14:16:56', '192.168.1.10'),
(204, 1, 'City Updated', 'Success', 1, '2018-12-19 14:54:29', '192.168.1.13'),
(205, 1, 'User Added', 'Success', 1, '2018-12-19 15:55:56', '192.168.1.13'),
(206, 1, 'Log In', 'Success', 1, '2018-12-19 16:20:56', '192.168.1.13'),
(207, 1, 'User Updated', 'Success', 1, '2018-12-19 16:22:36', '192.168.1.13'),
(208, 1, 'User Updated', 'Success', 1, '2018-12-19 16:23:24', '192.168.1.13'),
(209, 1, 'User Added', 'Success', 1, '2018-12-19 16:25:54', '192.168.1.13'),
(210, 1, 'Logged out', 'Logout', 2, '2018-12-19 16:33:45', '192.168.1.10'),
(211, 1, 'Log In', 'Success', 1, '2018-12-19 16:39:27', '192.168.1.13'),
(212, 1, 'User Updated', 'Success', 1, '2018-12-19 16:41:57', '192.168.1.13'),
(213, 1, 'User Updated', 'Success', 1, '2018-12-19 16:42:13', '192.168.1.13'),
(214, 1, 'User Updated', 'Success', 1, '2018-12-19 16:45:12', '192.168.1.13'),
(215, 1, 'Logged out', 'Logout', 2, '2018-12-19 16:45:19', '192.168.1.13'),
(216, 70, 'Log In', 'Success', 1, '2018-12-19 16:50:41', '192.168.1.13'),
(217, 70, 'User Updated', 'Success', 1, '2018-12-19 16:50:55', '192.168.1.13'),
(218, 70, 'User Added', 'Success', 1, '2018-12-19 16:51:16', '192.168.1.13'),
(219, 70, 'Logged out', 'Logout', 2, '2018-12-19 16:52:12', '192.168.1.13'),
(220, 71, 'Log In', 'Success', 1, '2018-12-19 16:52:18', '192.168.1.13'),
(221, 71, 'Logged out', 'Logout', 2, '2018-12-19 16:52:20', '192.168.1.13'),
(222, 1, 'Log In', 'Success', 1, '2018-12-20 07:48:48', '192.168.1.13'),
(223, 1, 'Log In', 'Success', 1, '2018-12-20 08:01:01', '192.168.1.10'),
(224, 1, 'User Updated', 'Success', 1, '2018-12-20 08:08:53', '192.168.1.10'),
(225, 1, 'Logged out', 'Logout', 2, '2018-12-20 08:27:00', '192.168.1.13'),
(226, 70, 'Log In', 'Success', 1, '2018-12-20 08:27:27', '192.168.1.13'),
(227, 70, 'Logged out', 'Logout', 2, '2018-12-20 08:27:33', '192.168.1.13'),
(228, 70, 'Log In', 'Success', 1, '2018-12-20 08:27:40', '192.168.1.13'),
(229, 70, 'Log In', 'Success', 1, '2018-12-20 08:28:29', '192.168.1.10'),
(230, 1, 'Log In', 'Success', 1, '2018-12-20 09:09:59', '192.168.1.13'),
(231, 1, 'Log In', 'Success', 1, '2018-12-20 11:49:29', '192.168.1.13'),
(232, 1, 'Log In', 'Success', 1, '2018-12-20 12:26:06', '192.168.1.13'),
(233, 1, 'User Updated', 'Success', 1, '2018-12-20 12:37:53', '192.168.1.13'),
(234, 1, 'Logged out', 'Logout', 2, '2018-12-20 14:16:33', '192.168.1.13'),
(235, 70, 'Log In', 'Success', 1, '2018-12-20 14:16:41', '192.168.1.13'),
(236, 1, 'Log In', 'Success', 1, '2018-12-21 07:49:52', '192.168.1.13'),
(237, 1, 'Log In', 'Success', 1, '2018-12-21 15:25:59', '192.168.1.13'),
(238, 1, 'Log In', 'Success', 1, '2018-12-24 08:09:07', '192.168.1.13'),
(239, 1, 'Log In', 'Success', 1, '2018-12-24 08:13:59', '192.168.1.10'),
(240, 70, 'Log In', 'Success', 1, '2018-12-24 08:17:00', '192.168.1.10'),
(241, 70, 'Log In', 'Success', 1, '2018-12-24 08:21:50', '192.168.1.13'),
(242, 1, 'Log In', 'Success', 1, '2018-12-24 11:49:40', '192.168.1.13'),
(243, 1, 'Log In', 'Success', 1, '2018-12-24 12:04:24', '192.168.1.10'),
(244, 70, 'Log In', 'Success', 1, '2018-12-24 07:44:09', '192.168.1.13'),
(245, 1, 'Log In', 'Success', 1, '2018-12-24 07:48:09', '192.168.1.13'),
(246, 70, 'User Added', 'Success', 1, '2018-12-24 07:52:24', '192.168.1.13'),
(247, 70, 'Logged out', 'Logout', 2, '2018-12-24 07:52:40', '192.168.1.13'),
(248, 70, 'Log In', 'Success', 1, '2018-12-24 07:53:21', '192.168.1.13'),
(249, 1, 'Log In', 'Success', 1, '2018-12-24 08:05:51', '192.168.1.10'),
(250, 1, 'User Updated', 'Success', 1, '2018-12-24 08:08:28', '192.168.1.10'),
(251, 70, 'Logged out', 'Logout', 2, '2018-12-24 08:25:00', '192.168.1.13'),
(252, 70, 'Log In', 'Success', 1, '2018-12-24 08:25:10', '192.168.1.13'),
(253, 1, 'Logged out', 'Logout', 2, '2018-12-24 08:28:40', '192.168.1.13'),
(254, 70, 'Logged out', 'Logout', 2, '2018-12-24 08:28:44', '192.168.1.13'),
(255, 1, 'Log In', 'Success', 1, '2018-12-24 08:28:50', '192.168.1.13'),
(256, 1, 'Logged out', 'Logout', 2, '2018-12-24 08:34:44', '192.168.1.13'),
(257, 70, 'Log In', 'Success', 1, '2018-12-24 08:34:50', '192.168.1.13'),
(258, 70, 'Logged out', 'Logout', 2, '2018-12-24 08:45:10', '192.168.1.13'),
(259, 1, 'Log In', 'Success', 1, '2018-12-24 08:45:15', '192.168.1.13'),
(260, 1, 'Logged out', 'Logout', 2, '2018-12-24 09:00:46', '192.168.1.13'),
(261, 70, 'Log In', 'Success', 1, '2018-12-24 09:00:52', '192.168.1.13'),
(262, 70, 'Logged out', 'Logout', 2, '2018-12-24 09:07:43', '192.168.1.13'),
(263, 1, 'Log In', 'Success', 1, '2018-12-24 09:07:48', '192.168.1.13'),
(264, 1, 'Logged out', 'Logout', 2, '2018-12-24 09:08:52', '192.168.1.13'),
(265, 70, 'Log In', 'Success', 1, '2018-12-24 09:08:59', '192.168.1.13'),
(266, 70, 'Logged out', 'Logout', 2, '2018-12-24 12:06:24', '192.168.1.13'),
(267, 1, 'Log In', 'Success', 1, '2018-12-24 12:06:30', '192.168.1.13'),
(268, 1, 'Log In', 'Success', 1, '2018-12-24 12:10:07', '192.168.1.10'),
(269, 70, 'Log In', 'Success', 1, '2018-12-24 12:33:16', '192.168.1.13'),
(270, 1, 'Logged out', 'Logout', 2, '2018-12-24 13:33:37', '192.168.1.13'),
(271, 70, 'Log In', 'Success', 1, '2018-12-24 13:33:43', '192.168.1.13'),
(272, 70, 'Logged out', 'Logout', 2, '2018-12-24 15:05:32', '192.168.1.13'),
(273, 1, 'Log In', 'Success', 1, '2018-12-24 15:05:38', '192.168.1.13'),
(274, 1, 'Log In', 'Success', 1, '2018-12-24 15:33:56', '192.168.1.10'),
(275, 70, 'Log In', 'Success', 1, '2018-12-24 15:35:05', '192.168.1.10'),
(276, 70, 'Log In', 'Success', 1, '2018-12-24 15:37:24', '192.168.1.13'),
(277, 70, 'Logged out', 'Logout', 2, '2018-12-24 16:00:29', '192.168.1.13'),
(278, 70, 'Log In', 'Success', 1, '2018-12-24 16:00:36', '192.168.1.13'),
(279, 1, 'Log In', 'Success', 1, '2018-12-27 08:22:59', '192.168.1.13'),
(280, 70, 'Log In', 'Success', 1, '2018-12-27 08:29:13', '192.168.1.13'),
(281, 1, 'Log In', 'Success', 1, '2018-12-27 11:58:54', '192.168.1.13'),
(282, 70, 'Log In', 'Success', 1, '2018-12-27 12:00:42', '192.168.1.13'),
(283, 1, 'Logged out', 'Logout', 2, '2018-12-27 13:48:47', '192.168.1.13'),
(284, 1, 'Log In', 'Success', 1, '2018-12-27 13:48:57', '192.168.1.13'),
(285, 70, 'Logged out', 'Logout', 2, '2018-12-27 13:50:03', '192.168.1.13'),
(286, 72, 'Log In', 'Success', 1, '2018-12-27 13:50:22', '192.168.1.13'),
(287, 72, 'Logged out', 'Logout', 2, '2018-12-27 13:50:36', '192.168.1.13'),
(288, 70, 'Log In', 'Success', 1, '2018-12-27 13:50:43', '192.168.1.13'),
(289, 1, 'Log In', 'Success', 1, '2018-12-27 15:07:04', '192.168.1.10'),
(290, 70, 'Log In', 'Success', 1, '2018-12-27 15:07:50', '192.168.1.10'),
(291, 1, 'User Added', 'Success', 1, '2018-12-27 15:16:21', '192.168.1.10'),
(292, 1, 'User Added', 'Success', 1, '2018-12-27 15:17:10', '192.168.1.10'),
(293, 70, 'User Added', 'Success', 1, '2018-12-27 15:19:48', '192.168.1.10'),
(294, 70, 'User Added', 'Success', 1, '2018-12-27 15:22:56', '192.168.1.10'),
(295, 1, 'User Updated', 'Success', 1, '2018-12-27 15:24:58', '192.168.1.10'),
(296, 70, 'User Added', 'Success', 1, '2018-12-27 15:30:10', '192.168.1.10'),
(297, 70, 'Logged out', 'Logout', 2, '2018-12-27 16:02:29', '192.168.1.10'),
(298, 79, 'Log In', 'Success', 1, '2018-12-27 16:04:16', '192.168.1.10'),
(299, 79, 'Logged out', 'Logout', 2, '2018-12-27 16:37:31', '192.168.1.10'),
(300, 70, 'Log In', 'Success', 1, '2018-12-27 16:37:50', '192.168.1.10'),
(301, 70, 'Logged out', 'Logout', 2, '2018-12-27 16:38:28', '192.168.1.10'),
(302, 69, 'Log In', 'Success', 1, '2018-12-27 16:46:57', '192.168.1.10'),
(303, 1, 'Log In', 'Success', 1, '2018-12-28 15:37:33', '192.168.1.10'),
(304, 1, 'Log In', 'Success', 1, '2018-12-29 07:51:59', '192.168.1.10'),
(305, 1, 'Logged out', 'Logout', 2, '2018-12-29 08:05:11', '192.168.1.10'),
(306, 70, 'Log In', 'Success', 1, '2018-12-29 08:05:19', '192.168.1.10'),
(307, 70, 'Logged out', 'Logout', 2, '2018-12-29 08:05:24', '192.168.1.10'),
(308, 1, 'Log In', 'Success', 1, '2018-12-29 08:14:08', '192.168.1.10'),
(309, 1, 'User Updated', 'Success', 1, '2018-12-29 08:14:52', '192.168.1.10'),
(310, 70, 'Log In', 'Success', 1, '2018-12-29 08:21:57', '192.168.1.10'),
(311, 70, 'Logged out', 'Logout', 2, '2018-12-29 08:22:03', '192.168.1.10'),
(312, 70, 'Log In', 'Success', 1, '2018-12-29 08:25:06', '192.168.1.10'),
(313, 70, 'Logged out', 'Logout', 2, '2018-12-29 08:25:13', '192.168.1.10'),
(314, 1, 'User Added', 'Success', 1, '2018-12-29 08:29:44', '192.168.1.10'),
(315, 1, 'User Added', 'Success', 1, '2018-12-29 08:32:47', '192.168.1.10'),
(316, 1, 'User Updated', 'Success', 1, '2018-12-29 08:32:58', '192.168.1.10'),
(317, 1, 'User Added', 'Success', 1, '2018-12-29 08:38:05', '192.168.1.10'),
(318, 1, 'User Added', 'Success', 1, '2018-12-29 08:58:11', '192.168.1.10'),
(319, 83, 'Log In', 'Success', 1, '2018-12-29 09:03:27', '192.168.1.10'),
(320, 70, 'Log In', 'Success', 1, '2018-12-29 09:36:54', '192.168.1.12'),
(321, 70, 'Log In', 'Success', 1, '2018-12-31 07:51:23', '192.168.1.10'),
(322, 1, 'Log In', 'Success', 1, '2018-12-31 07:53:41', '192.168.1.15'),
(323, 1, 'User Updated', 'Success', 1, '2018-12-31 08:01:53', '192.168.1.15'),
(324, 1, 'City Added', 'Success', 1, '2018-12-31 08:02:52', '192.168.1.15'),
(325, 1, 'User Added', 'Success', 1, '2018-12-31 08:05:10', '192.168.1.15'),
(326, 1, 'User Updated', 'Success', 1, '2018-12-31 08:08:51', '192.168.1.15'),
(327, 1, 'User Added', 'Success', 1, '2018-12-31 08:13:56', '192.168.1.15'),
(328, 1, 'User Added', 'Success', 1, '2018-12-31 08:31:37', '192.168.1.15'),
(329, 70, 'Logged out', 'Logout', 2, '2018-12-31 08:46:54', '192.168.1.10'),
(330, 1, 'Log In', 'Success', 1, '2018-12-31 08:46:59', '192.168.1.10'),
(331, 1, 'User Updated', 'Success', 1, '2018-12-31 08:49:51', '192.168.1.10'),
(332, 84, 'Log In', 'Success', 1, '2018-12-31 09:06:56', '192.168.1.15'),
(333, 84, 'Logged out', 'Logout', 2, '2018-12-31 09:07:02', '192.168.1.15'),
(334, 84, 'Log In', 'Success', 1, '2018-12-31 09:07:12', '192.168.1.15'),
(335, 84, 'User Added', 'Success', 1, '2018-12-31 09:16:50', '192.168.1.15'),
(336, 1, 'User Updated', 'Success', 1, '2018-12-31 09:17:18', '192.168.1.15'),
(337, 1, 'User Updated', 'Success', 1, '2018-12-31 09:17:31', '192.168.1.15'),
(338, 84, 'Logged out', 'Logout', 2, '2018-12-31 09:54:00', '192.168.1.15'),
(339, 70, 'Log In', 'Success', 1, '2018-12-31 09:54:05', '192.168.1.15'),
(340, 70, 'Log In', 'Success', 1, '2018-12-31 11:52:19', '192.168.1.10'),
(341, 70, 'Logged out', 'Logout', 2, '2018-12-31 12:44:29', '192.168.1.10'),
(342, 1, 'Log In', 'Success', 1, '2018-12-31 12:44:43', '192.168.1.10'),
(343, 70, 'Log In', 'Success', 1, '2018-12-31 14:44:19', '192.168.1.15'),
(344, 70, 'Log In', 'Success', 1, '2018-12-31 15:31:51', '192.168.1.10'),
(345, 1, 'Log In', 'Success', 1, '2019-01-02 07:34:26', '192.168.1.15'),
(346, 70, 'Log In', 'Success', 1, '2019-01-02 07:35:51', '192.168.1.15'),
(347, 70, 'Logged out', 'Logout', 2, '2019-01-02 07:41:04', '192.168.1.15'),
(348, 84, 'Log In', 'Success', 1, '2019-01-02 07:41:21', '192.168.1.15'),
(349, 84, 'Logged out', 'Logout', 2, '2019-01-02 07:54:43', '192.168.1.15'),
(350, 70, 'Log In', 'Success', 1, '2019-01-02 07:54:58', '192.168.1.15'),
(351, 1, 'Log In', 'Success', 1, '2019-01-02 08:44:11', '192.168.1.10'),
(352, 1, 'User Updated', 'Success', 1, '2019-01-02 09:34:32', '192.168.1.15'),
(353, 1, 'Logged out', 'Logout', 2, '2019-01-02 09:46:07', '192.168.1.10'),
(354, 84, 'Log In', 'Success', 1, '2019-01-02 09:46:29', '192.168.1.10'),
(355, 1, 'User Updated', 'Success', 1, '2019-01-02 12:02:13', '182.73.48.34'),
(356, 1, 'User Updated', 'Success', 1, '2019-01-02 12:02:23', '182.73.48.34'),
(357, 1, 'Log In', 'Success', 1, '2019-01-02 12:37:01', '182.73.48.34'),
(358, 1, 'Log In', 'Success', 1, '2019-01-02 12:58:50', '182.73.48.34'),
(359, 84, 'Log In', 'Success', 1, '2019-01-02 13:42:35', '182.73.48.34'),
(360, 1, 'Service Updated', 'Success', 1, '2019-01-02 14:06:41', '182.73.48.34'),
(361, 1, 'Breakdown Added', 'Success', 1, '2019-01-02 14:08:35', '182.73.48.34'),
(362, 1, 'Logged out', 'Logout', 2, '2019-01-02 14:33:37', '182.73.48.34'),
(363, 1, 'Log In', 'Success', 1, '2019-01-03 08:00:46', '182.73.48.34'),
(364, 1, 'Service Updated', 'Success', 1, '2019-01-03 09:06:17', '182.73.48.34'),
(365, 1, 'User Updated', 'Success', 1, '2019-01-03 09:06:29', '182.73.48.34'),
(366, 1, 'User Updated', 'Success', 1, '2019-01-03 09:06:40', '182.73.48.34'),
(367, 1, 'Log In', 'Success', 1, '2019-01-03 11:23:47', '182.73.48.34'),
(368, 1, 'Log In', 'Success', 1, '2019-01-03 14:09:27', '103.72.57.91'),
(369, 1, 'Log In', 'Success', 1, '2019-01-04 07:40:17', '182.73.48.34'),
(370, 1, 'Log In', 'Success', 1, '2019-01-04 12:08:39', '182.73.48.34'),
(371, 1, 'Log In', 'Success', 1, '2019-01-05 08:07:27', '103.213.27.216'),
(372, 1, 'User Updated', 'Success', 1, '2019-01-05 08:18:40', '103.213.27.216'),
(373, 1, 'User Updated', 'Success', 1, '2019-01-05 08:37:39', '103.213.27.216'),
(374, 1, 'Logged out', 'Logout', 2, '2019-01-05 08:37:44', '103.213.27.216'),
(375, 81, 'Log In', 'Success', 1, '2019-01-05 08:37:51', '103.213.27.216'),
(376, 81, 'User Updated', 'Success', 1, '2019-01-05 08:39:57', '103.213.27.216'),
(377, 81, 'User Added', 'Success', 1, '2019-01-05 08:40:30', '103.213.27.216'),
(378, 81, 'Logged out', 'Logout', 2, '2019-01-05 08:49:10', '103.213.27.216'),
(379, 1, 'Log In', 'Success', 1, '2019-01-05 08:49:15', '103.213.27.216'),
(380, 1, 'Logged out', 'Logout', 2, '2019-01-05 08:49:38', '103.213.27.216'),
(381, 81, 'Log In', 'Success', 1, '2019-01-05 08:49:48', '103.213.27.216'),
(382, 81, 'Logged out', 'Logout', 2, '2019-01-05 09:21:15', '103.213.27.216'),
(383, 1, 'Log In', 'Success', 1, '2019-01-07 08:42:03', '182.73.48.34'),
(384, 1, 'Log In', 'Success', 1, '2019-01-08 08:10:46', '182.73.48.34'),
(385, 1, 'Log In', 'Success', 1, '2019-01-11 08:41:00', '182.73.48.34'),
(386, 1, 'User Updated', 'Success', 1, '2019-01-11 08:51:14', '182.73.48.34'),
(387, 1, 'User Updated', 'Success', 1, '2019-01-11 08:51:35', '182.73.48.34'),
(388, 1, 'User Updated', 'Success', 1, '2019-01-11 08:53:55', '182.73.48.34'),
(389, 84, 'Log In', 'Success', 1, '2019-01-11 08:58:31', '182.73.48.34'),
(390, 1, 'Log In', 'Success', 1, '2019-01-11 09:25:35', '182.73.48.34'),
(391, 1, 'User Updated', 'Success', 1, '2019-01-11 09:56:03', '182.73.48.34'),
(392, 1, 'User Added', 'Success', 1, '2019-01-11 09:59:58', '182.73.48.34'),
(393, 1, 'User Updated', 'Success', 1, '2019-01-11 10:10:22', '182.73.48.34'),
(394, 84, 'Logged out', 'Logout', 2, '2019-01-11 10:14:47', '182.73.48.34'),
(395, 93, 'Log In', 'Success', 1, '2019-01-11 10:15:01', '182.73.48.34'),
(396, 93, 'Logged out', 'Logout', 2, '2019-01-11 10:17:02', '182.73.48.34'),
(397, 27, 'Log In', 'Success', 1, '2019-01-11 10:17:17', '182.73.48.34'),
(398, 1, 'Log In', 'Success', 1, '2019-01-12 13:07:25', '103.39.243.130'),
(399, 1, 'User Updated', 'Success', 1, '2019-01-12 13:08:33', '103.39.243.130'),
(400, 1, 'Logged out', 'Logout', 2, '2019-01-12 13:12:01', '103.39.243.130'),
(401, 81, 'Log In', 'Success', 1, '2019-01-12 13:12:07', '103.39.243.130'),
(402, 1, 'Log In', 'Success', 1, '2019-01-15 15:56:00', '182.73.48.34'),
(403, 84, 'Log In', 'Success', 1, '2019-01-16 09:30:52', '182.73.48.34'),
(404, 84, 'Log In', 'Success', 1, '2019-01-16 12:20:35', '182.73.48.34'),
(405, 1, 'Log In', 'Success', 1, '2019-01-16 12:20:57', '182.73.48.34'),
(406, 1, 'Log In', 'Success', 1, '2019-01-16 15:47:46', '182.73.48.34'),
(407, 84, 'Log In', 'Success', 1, '2019-01-16 15:53:48', '182.73.48.34'),
(408, 1, 'Log In', 'Success', 1, '2019-01-17 07:36:34', '182.73.48.34'),
(409, 1, 'Log In', 'Success', 1, '2019-01-17 09:38:01', '103.39.243.130'),
(410, 1, 'Logged out', 'Logout', 2, '2019-01-17 09:38:48', '103.39.243.130'),
(411, 81, 'Log In', 'Success', 1, '2019-01-17 09:38:55', '103.39.243.130'),
(412, 81, 'Logged out', 'Logout', 2, '2019-01-17 09:54:32', '103.39.243.130'),
(413, 1, 'Log In', 'Success', 1, '2019-01-17 14:33:53', '103.39.243.130'),
(414, 1, 'User Added', 'Success', 1, '2019-01-17 14:35:58', '103.39.243.130'),
(415, 1, 'Log In', 'Success', 1, '2019-01-18 16:29:02', '182.73.48.34'),
(416, 1, 'User Updated', 'Success', 1, '2019-01-18 16:35:35', '182.73.48.34'),
(417, 1, 'Logged out', 'Logout', 2, '2019-01-18 16:35:47', '182.73.48.34'),
(418, 84, 'Log In', 'Success', 1, '2019-01-18 16:35:56', '182.73.48.34'),
(419, 1, 'Log In', 'Success', 1, '2019-01-19 07:04:51', '196.192.183.14'),
(420, 1, 'Log In', 'Success', 1, '2019-01-19 07:06:57', '103.39.243.130'),
(421, 1, 'Logged out', 'Logout', 2, '2019-01-19 07:15:33', '196.192.183.14'),
(422, 81, 'Log In', 'Success', 1, '2019-01-19 07:16:14', '196.192.183.14'),
(423, 81, 'Logged out', 'Logout', 2, '2019-01-19 07:38:54', '196.192.183.14'),
(424, 1, 'Log In', 'Success', 1, '2019-01-19 08:25:16', '182.73.48.34'),
(425, 1, 'User Updated', 'Success', 1, '2019-01-19 08:40:16', '182.73.48.34'),
(426, 84, 'Log In', 'Success', 1, '2019-01-19 08:40:26', '182.73.48.34'),
(427, 1, 'Log In', 'Success', 1, '2019-01-21 08:25:47', '182.73.48.34'),
(428, 1, 'Log In', 'Success', 1, '2019-01-21 11:56:21', '182.73.48.34');

-- --------------------------------------------------------

--
-- Table structure for table `user_quote_request`
--

CREATE TABLE `user_quote_request` (
  `id` int(111) NOT NULL,
  `id_item` int(111) DEFAULT NULL,
  `id_user` int(122) DEFAULT NULL,
  `quantity` varchar(111) DEFAULT NULL,
  `expecteditemdate` date DEFAULT NULL,
  `location` varchar(222) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(222) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `is_archive` tinyint(1) DEFAULT '0' COMMENT '0-> not archieve 1->archieve',
  `add_date` datetime(6) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `discount` float(10,2) DEFAULT NULL,
  `admin_note` text,
  `approve_by` int(11) DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT NULL,
  `rating_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_quote_request`
--

INSERT INTO `user_quote_request` (`id`, `id_item`, `id_user`, `quantity`, `expecteditemdate`, `location`, `id_city`, `comments`, `subcategorydata`, `image`, `status`, `is_archive`, `add_date`, `price`, `discount`, `admin_note`, `approve_by`, `approval_date`, `user_review`, `rating`, `rating_date`) VALUES
(1, 1, 48, '54', NULL, 'jayadev bihar', 1, 'test description', NULL, NULL, 1, 0, '2018-08-30 20:34:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 4, 49, '25', NULL, 'bbsr', 2, 'need frik', NULL, NULL, 1, 0, '2018-09-01 11:45:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 49, '6', NULL, 'ctc', 1, 'tesg hhd hhd hsh hdhbdjjdb jjsjjdb. ujs778s8snndnd njjdjnd jsjjnd jjjdjjd jjdnbd jjdnnd hd nhh hjdbhd', NULL, NULL, 1, 0, '2018-09-01 11:48:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 49, '600', NULL, 'ct', 2, 'trst', NULL, NULL, 1, 0, '2018-09-01 16:19:57.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 4, 51, '8', NULL, 'saheed nagar', 1, 'testing perpose', NULL, NULL, 1, 0, '2018-09-04 14:56:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 2, 48, '45', NULL, 'Jayadev Bihar', 2, 'Test\nTest\nTest\nTest\nTest\nTest', NULL, NULL, 1, 0, '2018-09-04 16:54:55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 2, 48, '88', NULL, 'bbsr', 1, 'Bbsr', NULL, NULL, 1, 0, '2018-09-04 17:14:31.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 2, 48, '566', NULL, 'ff', 2, 'Fggh', NULL, NULL, 1, 0, '2018-09-04 19:26:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 3, 56, '1', NULL, 'dd', 1, 'Dd', NULL, NULL, 1, 0, '2018-09-05 14:42:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 3, 48, '5', NULL, 'cff', 2, 'Cfggg', NULL, NULL, 1, 0, '2018-09-07 12:01:26.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 48, '88', NULL, 'cc', 1, 'Fv\nBh', NULL, NULL, 1, 0, '2018-09-07 12:02:48.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 48, '88', NULL, 'fvv', 2, 'Gvvb', NULL, NULL, 1, 0, '2018-09-07 12:06:39.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 4, 55, '2', NULL, 'jet park', 1, '2.5 ton diesel std mast', NULL, NULL, 1, 0, '2018-09-11 19:53:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 4, 55, '2', NULL, 'tesy', 2, 'Ffvgh', NULL, NULL, 1, 0, '2018-09-12 16:05:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 6, 59, '25', NULL, 'bbsr', 1, 'iLift', NULL, NULL, 1, 0, '2018-09-14 15:53:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 4, 55, '2', NULL, 'Puri', 2, 'Testing', NULL, NULL, 1, 0, '2018-09-14 16:06:13.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 4, 49, '2', NULL, 'puri', 1, 'Test', NULL, NULL, 1, 0, '2018-09-15 12:51:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 4, 49, '1', NULL, 'puri', 2, 'Test', NULL, NULL, 1, 0, '2018-09-15 12:52:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 55, '2', NULL, 'Odisha', 1, 'Twst', NULL, NULL, 1, 0, '2018-09-15 12:54:55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 48, '8656555', NULL, 'dhdhfyg', 2, 'Dgdhfugu', NULL, NULL, 1, 0, '2018-09-17 17:19:31.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 3, 48, '09058', NULL, 'gctc', 1, 'Dyvuvu\nHcuvu\nHcv\nHvyv\nHvjn', NULL, NULL, 1, 0, '2018-09-17 18:49:39.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 4, 55, '22', NULL, 'test', 2, 'Durban', NULL, NULL, 1, 0, '2018-09-19 14:36:10.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 4, 49, '100', NULL, 'Bhubaneswar', 1, 'Required', NULL, NULL, 1, 0, '2018-09-19 19:35:30.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 49, '998', NULL, 'hdh', 2, 'Djjdj', NULL, NULL, 1, 1, '2018-09-19 19:39:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 4, 48, '25', NULL, '', 1, 'Vrgegrv', NULL, NULL, 1, 1, '2018-10-03 19:22:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 4, 48, '54', NULL, '', 2, 'F ttv', NULL, NULL, 1, 1, '2018-10-03 19:22:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 7, 47, '888', NULL, '', 1, 'Xfxc', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '27_resquestquote.jpg', 1, 1, '2018-10-12 15:27:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 7, 47, '25', NULL, '', 2, 'Dhdhfgk', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, 1, '2018-10-15 19:30:07.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 7, 47, '9968', NULL, '', 1, 'Vgv', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '29_resquestquote.jpg', 1, 1, '2018-10-29 17:02:06.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 7, 48, '5', NULL, '', 2, 'F8y8t', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, 1, '2018-12-01 11:20:51.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 7, 48, '500', NULL, '', 1, 'Test', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '31_resquestquote.jpg', 1, 1, '2018-12-03 14:45:19.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 7, 48, '5', NULL, '', 2, 'Gzh', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, 1, '2018-12-03 15:53:34.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 7, 48, '2', NULL, '', 1, 'Fgh', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, 1, '2018-12-03 15:54:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 7, 48, '5', NULL, '', 2, 'Cf', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, 1, '2018-12-03 15:57:27.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 7, 48, '2', NULL, '', 1, 'Fsh', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, 1, '2018-12-03 15:59:37.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 6, 88, '55', NULL, '', 3, 'Test test', '[{\"id_quote\":\"6\",\"name\":\"chjfgjh\",\"type\":\"1\",\"status\":\"1\"}, {\"id_quote\":\"6\",\"name\":\"Product 4\",\"type\":\"2\",\"status\":\"1\"}]', '36_resquestquote.jpg', 1, 0, '2019-01-02 08:32:09.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 6, 88, '12', NULL, '', 3, 'Check with you first', '[{\"id_quote\":\"6\",\"name\":\"chjfgjh\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_quote\":\"6\",\"name\":\"Product 4\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"yes\"}]', '37_resquestquote.jpg', 1, 0, '2019-01-02 08:34:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 7, 47, '23', NULL, '', 1, 'Testing the water', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '38_resquestquote.jpg', 1, 0, '2019-01-11 08:56:24.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 7, 92, '5', NULL, '', 2, 'Test', '[{\"id_quote\":\"7\",\"name\":\"TEST   TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, 0, '2019-01-12 13:10:43.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 4, 92, '1', NULL, '', 2, '2.5 ton diesel', '[]', '40_resquestquote.jpg', 1, 0, '2019-01-19 07:14:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_service_request`
--

CREATE TABLE `user_service_request` (
  `id` int(11) NOT NULL,
  `id_service` int(111) DEFAULT NULL,
  `id_user` int(111) DEFAULT NULL,
  `contact_person` varchar(222) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `product_identification` varchar(222) DEFAULT NULL,
  `expectedservicedate` datetime DEFAULT NULL,
  `location` varchar(111) DEFAULT NULL,
  `id_city` int(111) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(122) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '3',
  `add_date` datetime DEFAULT NULL,
  `assign_executive_id` int(11) DEFAULT NULL,
  `statusnote` text,
  `assigndate` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT '0.0',
  `rating_date` datetime DEFAULT NULL,
  `closeby` int(11) DEFAULT NULL,
  `closedate` datetime DEFAULT NULL,
  `invoice` tinyint(1) DEFAULT '0',
  `signature` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_service_request`
--

INSERT INTO `user_service_request` (`id`, `id_service`, `id_user`, `contact_person`, `contact_number`, `product_identification`, `expectedservicedate`, `location`, `id_city`, `comments`, `subcategorydata`, `image`, `status`, `add_date`, `assign_executive_id`, `statusnote`, `assigndate`, `user_review`, `rating`, `rating_date`, `closeby`, `closedate`, `invoice`, `signature`) VALUES
(1, 1, 48, 'tusar', '5467893120', 'test as new user', NULL, 'jayde', 1, 'test as user', NULL, NULL, 1, '2018-08-30 20:41:49', 27, NULL, '2018-08-30 20:49:40', 'twst ywst', 0.5, '2018-08-30 21:03:08', NULL, NULL, 0, NULL),
(2, 1, 48, 'jagu', '57697686867', 'job post', NULL, 'bbsr', 2, 'test check', NULL, NULL, 1, '2018-08-30 21:28:42', 27, NULL, '2018-08-30 21:30:17', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(3, 2, 49, 'cry', '259636', 'test spe', NULL, 'bbsr', 1, 'prob', NULL, NULL, 1, '2018-09-01 12:05:43', 27, 'check on pri', '2018-09-01 12:06:40', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(4, 1, 49, '777', '989', 'nn', NULL, 'bbs', 2, 'hhzh', NULL, NULL, 4, '2018-09-01 16:20:44', 50, NULL, '2018-09-01 16:30:03', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(5, 2, 51, 'gdfhheri', '9438505144', 'gjijd849ej7', NULL, 'gartew', 1, 'havelipee', NULL, NULL, 4, '2018-09-04 14:58:03', 27, 'CHECK IT', '2018-09-04 15:10:39', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(6, 1, 51, 'Nishikant', '9742016340', 'no forklift', NULL, 'jc road', 2, 'hii', NULL, NULL, 4, '2018-09-04 15:27:10', 27, NULL, '2018-09-17 12:08:59', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(7, 2, 48, 'sanu', '78096541238', 'long hair', NULL, 'jayadev bihar', 1, 'Test\nTest\nTest\nTest\nTest\nTest\nTest test test yrdc', NULL, NULL, 1, '2018-09-04 17:01:26', 27, 'dfgdgh', '2018-09-04 17:43:23', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(8, 1, 48, 'ggh', '566855', 'cgv', NULL, 'gg', 2, 'Cghg\nFgbj', NULL, NULL, 1, '2018-09-04 17:15:51', 27, NULL, '2018-09-04 17:28:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(9, 2, 48, 'ghh', '59', 'fghh', NULL, 'thh', 1, 'Gvghh\nFgghh\nVhj', NULL, NULL, 1, '2018-09-04 19:26:57', 27, 'test', '2018-09-04 19:29:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(10, 2, 48, 'hbnggh', '566999', 'vvhhh', NULL, 'ggh', 2, 'Vvvbh', NULL, NULL, 1, '2018-09-04 19:58:46', 27, 'dfhfsdgf', '2018-09-04 19:58:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(11, 2, 48, 'ggh', '8666', 'cghj', NULL, 'bbh', 1, 'Chjnnn', NULL, NULL, 1, '2018-09-04 20:07:08', 27, 'gyhfhgfhgf', '2018-09-04 20:07:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(12, 3, 48, 'gg', '5658', 'tt', NULL, 'gg', 2, 'Cv', NULL, NULL, 4, '2018-09-04 20:08:37', 27, 'vghfhg', '2018-09-04 20:08:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(13, 2, 49, '???', '66', '??', NULL, '??', 1, '??', NULL, NULL, 1, '2018-09-05 14:11:21', 46, 'ts', '2018-09-05 14:11:49', 'trFf', 1.0, '2018-09-18 13:08:26', NULL, NULL, 0, NULL),
(14, 1, 49, '?', '55', '??', NULL, '?7', 2, 'Hh', NULL, NULL, 1, '2018-09-05 14:12:57', 27, 'kkkkk', '2018-09-05 14:13:16', 'Good', 4.0, '2018-09-18 12:53:12', NULL, NULL, 0, NULL),
(15, 3, 56, 'c', '9556786167', 'a', NULL, 'b', 1, 'Abcd', NULL, NULL, 1, '2018-09-05 14:32:49', 46, '222', '2018-09-05 14:33:43', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(16, 1, 56, 'vv', '95686324117', 'hh', NULL, 'vg', 2, 'Tt', NULL, NULL, 4, '2018-09-05 14:37:07', 50, 'yyy', '2018-09-05 14:37:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(17, 2, 56, 'ytf', '658896854258', 'df', NULL, 'df', 1, 'Se', NULL, NULL, 4, '2018-09-05 14:43:02', 27, NULL, '2018-09-12 15:55:37', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(18, 1, 56, 'cdjk', '8657844288', 'ch', NULL, 'gkgd', 2, 'X6 tu h', NULL, NULL, 4, '2018-09-05 15:31:40', 27, NULL, '2018-09-11 16:55:04', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(19, 3, 56, 'ghvkb', '8555745688', 'fjjv', NULL, 'xigho', 1, 'Chvj', NULL, NULL, 1, '2018-09-05 15:35:51', 50, NULL, '2018-09-05 15:41:44', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(20, 4, 59, 'Xavier', '1234567896', 'rechargeable', NULL, 'bhubaneswar', 2, 'Battery is damaged', NULL, NULL, 4, '2018-09-12 15:54:28', 27, NULL, '2018-09-12 16:13:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(21, 3, 48, 'trsttrst', '21365478999', 'tedt', NULL, 'test', 1, 'Good\nThanks\nThanks\nThanks so much for', NULL, NULL, 4, '2018-09-12 15:59:21', 58, NULL, '2018-09-12 16:00:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(22, 3, 48, 'gusgdig', '68866856657', 'xgjxgix', NULL, 'guxhixgi', 2, 'Hczhfzustud', NULL, NULL, 4, '2018-09-15 15:30:47', 27, NULL, '2018-09-15 15:31:23', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(23, 3, 55, 'Vivebk', '558888', 'test02', NULL, 'Puri', 1, 'Testing', NULL, NULL, 1, '2018-09-15 15:31:59', 50, NULL, '2018-09-15 15:36:25', 'Test', 5.0, '2018-09-15 15:37:18', NULL, NULL, 0, NULL),
(24, 3, 47, 'naresh', '5847123697', 'testing', NULL, 'bbsr', 2, 'Quick', NULL, NULL, 4, '2018-09-17 11:41:48', 27, NULL, '2018-09-17 11:42:26', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(25, 4, 48, 'fhu', '6655', 'ttt', NULL, 'g yuu', 1, 'Fgh', NULL, NULL, 1, '2018-09-17 11:42:46', 27, NULL, '2018-09-17 11:42:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(26, 2, 47, 'fgjj', '85566', 'tyy', NULL, 'fgh', 2, 'Cvbh', NULL, NULL, 3, '2018-09-17 12:23:59', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(27, 1, 48, 'gdegus', '5686582', 'gxjdd', NULL, 'eidige', 1, 'Xdyi\nJxjdkhf\nKdigxyid\nZxjg\nMvxc', NULL, NULL, 4, '2018-09-17 17:13:17', 50, NULL, '2018-09-18 13:05:15', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(28, 2, 48, 'ztda with e', '0986565', 'chxhfh', NULL, 'hfj', 2, 'Zfxhfjgu\nGdufuf\nDyfg', NULL, NULL, 4, '2018-09-17 17:59:42', 50, NULL, '2018-09-18 12:55:02', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(29, 3, 48, 'fxfxgx', '123456789', 'ctxrxrx', NULL, 'yfctx', 1, 'Cguc\nTo fyf\nHfygg', NULL, NULL, 1, '2018-09-17 18:01:15', 46, NULL, '2018-09-17 18:01:45', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(30, 2, 48, 'fghuiu', '55666', 'frffgg', NULL, 'fvhhh', 2, 'Ffhhh', NULL, NULL, 1, '2018-09-17 19:48:14', 27, NULL, '2018-09-17 19:52:36', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(31, 1, 48, 'zgfugi', '865656764', 'chdhfhgh', NULL, 'gdhfhfuf', 1, 'Cxgd\nHdhfm\nHcjh\nTfij', NULL, NULL, 1, '2018-09-17 19:48:47', 27, NULL, '2018-09-17 19:51:53', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(32, 2, 48, 'qqqqqqqqq', '11111111', 'qqqqqqqq', NULL, 'qqqqqqqq', 2, 'Qqqqqqqw', NULL, NULL, 1, '2018-09-17 19:49:17', 27, NULL, '2018-09-17 19:51:02', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(33, 4, 49, 'Sandy', '58755480', 'rechargeable', NULL, 'bbsr', 1, 'Damaged', NULL, NULL, 4, '2018-09-18 13:16:36', 50, NULL, '2018-09-18 13:17:06', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(34, 1, 49, 'hhd', '333', 'hh', NULL, 'bh', 2, 'Hhdhhd', NULL, NULL, 3, '2018-09-19 19:38:32', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(35, 4, 65, 'vijay', '0835084897', 'F34', NULL, 'Jet Park', 1, 'Service my above unit. Hours reading is 255 already', NULL, NULL, 1, '2018-09-27 20:29:32', 27, 'Check & Respond to customer', '2018-09-27 20:30:40', 'glad', 5.0, '2018-09-27 20:39:37', NULL, NULL, 0, NULL),
(36, 5, 48, 'ganesh', '985236147', 'valud infi', NULL, 'bbsr', 2, 'Testing good', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"5\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 3, '2018-10-08 19:59:51', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(37, 6, 48, 'kisu', '94656866', 'go', NULL, 'bbsr', 1, 'Test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"rjjr\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 3, '2018-10-08 20:02:41', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(38, 6, 48, 'kamal', '123456789', 'aaaaaaa', NULL, 'bbsr', 2, 'Test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ok\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}]', NULL, 3, '2018-10-08 20:04:57', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(39, 7, 49, 'hgg', '989989898', 'tedt', NULL, 'pdp', 1, 'Hdhhd', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"10\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2 h\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tesf cell\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 1, '2018-10-09 17:05:11', 27, 'test', '2018-10-09 17:06:27', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(40, 4, 49, 'jj', '6669', 'hh', NULL, 'nj', 2, 'Hjj', '[]', NULL, 3, '2018-10-09 17:30:05', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(41, 4, 47, 'vhh', '0899', 'vghh', NULL, 'vb', 1, 'FgStytyy', '[]', NULL, 3, '2018-10-12 15:13:49', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(42, 7, 47, 'hchch', '965656', 'jjfuc', NULL, 'hchch', 2, 'Ifuufufu', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ufuf\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ufuf\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ururu\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '42_resquestservice.jpg', 1, '2018-10-12 15:15:34', 27, 'dfsdczcx', '2018-10-29 12:39:46', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(43, 7, 47, 'vbb', '566', 'vbb', NULL, 'fgg', 1, 'Vghh', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"vvv\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ggh\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"gh\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '43_resquestservice.jpg', 4, '2018-10-12 15:32:57', 27, 'TEST ', '2018-10-12 17:29:39', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(44, 7, 47, 'w', '4', '2e', NULL, 'd', 2, 'S', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"w\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-29 12:43:57', 27, 'ghghgjh', '2018-10-29 13:01:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(45, 7, 47, 'g', '5', 'f', NULL, 'y', 1, 'G', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\" gh\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"c\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fg\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 3, '2018-10-29 15:08:32', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(46, 4, 44, 'testcon', '62542578899', '258856685', NULL, 'hjiij', 2, 'ghji8o', NULL, NULL, 3, '2018-10-29 15:28:12', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(47, 4, 44, 'testcon', '62542578899', '258856685', NULL, 'hjiij', 1, 'ghji8o', NULL, NULL, 3, '2018-10-29 15:30:08', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(48, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 15:35:53', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(49, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 15:39:36', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(50, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 15:46:08', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(51, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 15:49:09', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(52, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 15:51:42', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(53, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 15:53:47', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(54, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 15:56:33', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(55, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:00:19', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(56, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:32:30', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(57, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:34:28', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(58, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:37:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(59, 7, 47, 'usus', '6434', 'hshs', NULL, 'hshs', 1, 'Hshs', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"bsbs\\njxjjx\\njzjz\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hshsh\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hshsh\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '59_resquestservice.jpg', 3, '2018-10-29 16:38:35', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(60, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:39:58', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(61, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:42:08', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(62, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:44:06', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(63, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:45:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(64, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:46:45', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(65, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:48:02', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(66, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:49:13', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(67, 4, 47, 'kisu', '163487974', 'test', NULL, 'bbsr', 1, 'Testj', '[]', '67_resquestservice.jpg', 3, '2018-10-29 16:49:59', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(68, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:51:16', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(69, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 16:56:58', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(70, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 16:57:21', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(71, 4, 47, 'raju', '123456', 'mark', NULL, 'bbsr', 1, 'Test', '[]', '71_resquestservice.jpg', 3, '2018-10-29 16:57:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(72, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:01:07', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(73, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:02:15', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(74, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:06:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(75, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:08:54', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(76, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:13:06', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(77, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:20:18', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(78, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:30:01', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(79, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:54:03', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(80, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:55:47', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(81, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:56:33', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(82, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 17:57:12', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(83, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 17:59:17', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(84, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 18:00:16', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(85, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 18:15:27', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(86, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 18:18:49', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(87, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 18:20:44', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(88, 6, 47, 'babu', '898989', 'mark in', NULL, 'cuttack', 2, 'Test test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '88_resquestservice.jpg', 3, '2018-10-29 18:20:47', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(89, 6, 47, 'fghjjii', '56999966669', 'cgggy', NULL, 'vvbhjjj', 1, 'Xcvvhnjjj', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ff\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '89_resquestservice.jpg', 3, '2018-10-29 18:22:33', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(90, 6, 47, 'uuuuuuu', '33333333', 'rrrrrr', NULL, 'yyyyyyyy', 2, 'Cgghhhh', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ttttttt\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '90_resquestservice.jpg', 3, '2018-10-29 18:31:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(91, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 18:38:38', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(92, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 19:02:14', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(93, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 1, '2018-10-29 19:04:49', 27, 'sada', '2019-01-18 16:36:11', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(94, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 4, '2018-10-29 19:06:09', 58, 'TEst TEST ', '2018-12-31 08:10:31', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(95, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 3, '2018-10-29 19:09:03', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(96, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 19:12:24', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(97, 7, 47, 'g1', '5', 'f', NULL, 'y1', 1, 'g', NULL, NULL, 1, '2018-10-29 19:16:27', 50, 'test test', '2019-01-19 08:46:54', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(98, 7, 47, 'g1', '5', 'f', NULL, 'y1', 2, 'g', NULL, NULL, 3, '2018-10-29 19:18:14', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(99, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:20:05', 27, 'check', '2019-01-19 08:43:59', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(100, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 3, '2018-10-31 12:22:02', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(101, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:24:03', 27, 'Test Test Test', '2019-01-19 08:37:04', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(102, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 3, '2018-10-31 12:27:12', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(103, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:27:39', 27, 'dtghr', '2019-01-16 15:53:59', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(104, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-31 12:31:49', 91, 'check', '2019-01-17 09:38:26', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(105, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:32:21', 27, 'ertgrgy', '2019-01-16 15:49:10', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(106, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-31 12:33:55', 46, 'tets', '2019-01-02 07:55:15', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(107, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:34:31', 27, 'yyhyhy', '2019-01-16 12:43:02', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(108, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-31 12:35:30', 76, 'TEST TETS ', '2019-01-02 09:44:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(109, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:43:00', 27, NULL, '2019-01-16 12:41:34', NULL, 0.0, NULL, NULL, NULL, 2, NULL),
(110, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-31 12:45:45', 58, NULL, '2018-12-31 12:52:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(111, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 12:47:50', 27, NULL, '2019-01-02 14:27:20', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(112, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 2, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 4, '2018-10-31 12:49:24', 58, NULL, '2018-12-31 12:49:26', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(113, 6, 47, 'dghjj', '578455896488', 'dhdhf', NULL, 'bbsr', 1, 'Dufjg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"455\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-31 13:01:33', 75, NULL, '2018-12-31 13:25:16', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(114, 7, 48, 'puru', '9494', 'bad', NULL, 'bbsr', 2, 'Gsusi', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"6\"}, {\"id_service\":\"7\",\"name\":\"Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tt\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"dfgdfhgfgeryhery\",\"type\":\"2\",\"status\":\"1\"}]', NULL, 4, '2018-12-01 11:11:48', 78, 'Purusottama ', '2018-12-31 08:10:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(115, 7, 48, 'tsuz', '9436251873797', 'ryaua', NULL, 'bbsr', 1, 'Gsusu', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"5\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"dfgdfhgfgeryhery\",\"type\":\"2\",\"status\":\"1\"}]', NULL, 1, '2018-12-04 08:41:10', 27, 'test', '2019-01-02 09:08:44', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(116, 6, 74, 'dt5532', '47690608', 'edsd', NULL, 'efyyt', 2, 'Tzztz', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ww\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\"}]', NULL, 1, '2018-12-27 13:50:30', 78, 'Test TESt ', '2018-12-28 16:40:10', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(117, 3, 88, 'bhuban', '7878', 'test info', NULL, 'bhubaneswar', 3, 'Test', '[]', '117_resquestservice.jpg', 1, '2019-01-02 08:46:29', 87, 'test', '2019-01-02 09:46:46', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(118, 1, 88, 'munu', '456789456', 'test check', NULL, 'bbsr', 3, 'Test trdt', '[{\"id_service\":\"1\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"1\",\"name\":\"TEST2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"check\"}]', '118_resquestservice.jpg', 1, '2019-01-02 08:48:41', 87, 'test', '2019-01-02 09:13:28', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(119, 3, 88, 'ryr', '7575855', 'h hd', NULL, 'fhfxgd', 3, 'Dhfyf', '[]', NULL, 1, '2019-01-02 11:05:53', 87, NULL, '2019-01-16 09:34:14', NULL, 0.0, NULL, NULL, NULL, 2, NULL),
(120, 2, 88, 'babu', '457869', 'test', NULL, 'bhubaneswar', 3, 'Test', '[]', '120_resquestservice.jpg', 4, '2019-01-02 12:45:50', 87, NULL, '2019-01-07 08:51:53', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(121, 2, 88, 'ggg', '9666', 'test', NULL, 'ghg', 3, 'Vghh', '[]', '121_resquestservice.jpg', 4, '2019-01-02 12:52:52', 87, 'rgfvdf', '2019-01-02 13:44:11', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(122, 7, 88, 'gy', '53', 'testt', NULL, 'fgg', 3, 'Df', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fg\"}, {\"id_service\":\"7\",\"name\":\"Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ty\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"rr\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"7\",\"name\":\"dfgdfhgfgeryhery\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tt\"}]', '122_resquestservice.jpg', 1, '2019-01-02 12:54:44', 87, 'test test ', '2019-01-02 13:20:57', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(123, 2, 88, 'hehhe', '62626', 'nrjr', NULL, 'heuue', 3, 'Bdhrh', '[]', '123_resquestservice.jpg', 1, '2019-01-02 13:03:21', 87, 'TETTET', '2019-01-02 13:04:01', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(124, 6, 88, 'jdjd', '65656', 'djdjd', NULL, 'usus', 3, 'Bdhdh', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"mdjd\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '124_resquestservice.jpg', 1, '2019-01-02 15:33:53', 87, NULL, '2019-01-02 15:40:34', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(125, 4, 92, 'hh', '5555596666666666', 'hh', NULL, 'y', 2, 'Hhh', '[]', '125_resquestservice.jpg', 1, '2019-01-05 09:01:57', 91, 'df', '2019-01-05 09:07:25', 'kgf hh', 4.0, '2019-01-05 09:10:00', NULL, NULL, 0, NULL),
(126, 5, 47, 'bunu', '789456', 'yes', NULL, 'bbsr', 1, 'Test test', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"5\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '126_resquestservice.jpg', 1, '2019-01-08 08:02:10', 27, NULL, '2019-01-16 09:44:55', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(127, 6, 47, 'tuna', '898989', 'test', NULL, 'bbsr', 1, 'Gggggggg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ttttttt\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '127_resquestservice.jpg', 1, '2019-01-11 09:04:59', 27, 'test test', '2019-01-11 09:40:34', NULL, 0.0, NULL, 27, '2019-01-11 09:41:58', 2, '127_servicesignature.jpg'),
(128, 5, 47, 'fun', '44444', 'test', NULL, 'bbsr', 1, 'Test', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"5\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"4\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"1\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2019-01-11 09:07:10', 27, 'dfgd', '2019-01-11 09:46:29', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(129, 6, 47, 'gg', '59', 'cc', NULL, 'fg', 1, 'Ch', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fyfy\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2019-01-11 09:07:45', 27, 'test properly', '2019-01-11 09:10:52', NULL, 0.0, NULL, 27, '2019-01-11 09:38:44', 1, '129_servicesignature.jpg'),
(130, 5, 47, 'hdhd', '95665', 'hdhd', NULL, 'hdhd', 1, 'Bxhdh', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"4\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2019-01-11 09:53:14', 27, '543256', '2019-01-16 09:32:47', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(131, 6, 47, 'fun g', '653', 'chchcu', NULL, 'fhf', 1, 'Nvk', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fufcu\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '131_resquestservice.jpg', 1, '2019-01-11 12:24:25', 27, 'bhjfhkhjkhjkh', '2019-01-16 09:31:31', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(132, 6, 47, 'uvyg', '3835', 'uvuv', NULL, 'vygy', 1, 'The ctgyg', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"cyv\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2019-01-11 12:25:03', 27, 'gygty', '2019-01-16 09:29:13', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(133, 6, 47, 'hvu', '693', 'h. hv', NULL, 'cyvy', 1, 'Xttc', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"1\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2019-01-11 12:26:27', 27, NULL, '2019-01-16 09:25:20', NULL, 0.0, NULL, NULL, NULL, 2, NULL),
(134, 6, 47, 'vg', '8666', 'xfg', NULL, 'tty', 1, 'Ghhhj', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '134_resquestservice.jpg', 1, '2019-01-11 12:28:07', 27, NULL, '2019-01-16 09:24:04', NULL, 0.0, NULL, NULL, NULL, 1, NULL),
(135, 7, 92, 'hbdjisie', '89494', 'hhhfb', NULL, 'hhud7bb', 2, 'Bxbbsb', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Amp hour\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"dfgdfhgfgeryhery\",\"type\":\"2\",\"status\":\"1\"}]', NULL, 1, '2019-01-12 13:11:39', 91, 'tttt', '2019-01-12 13:13:47', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(136, 4, 92, 'Vijay', '0835084897', 'FL2', NULL, 'Jet Park', 2, 'Please do 500 hours service', '[]', '136_resquestservice.jpg', 1, '2019-01-19 07:19:29', 91, 'please attend by tomorrow', '2019-01-19 07:21:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_update_history`
--

CREATE TABLE `user_update_history` (
  `id_update_history` int(222) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `firstname` varchar(222) DEFAULT NULL,
  `lastname` varchar(222) DEFAULT NULL,
  `aadhar_number` varchar(50) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `alternate_phone` varchar(50) DEFAULT NULL,
  `email` varchar(222) DEFAULT NULL,
  `tokenId` bigint(20) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `address` text,
  `state` int(11) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `block` varchar(50) DEFAULT NULL,
  `sector` varchar(50) DEFAULT NULL,
  `sub_center` varchar(50) DEFAULT NULL,
  `village` varchar(50) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '1->Active,2->Inactive',
  `user_type` int(11) DEFAULT NULL COMMENT '1->Asha,2->sub center,3->sector ,4->block, 5->Dist,6->state,7->Admin',
  `designation` varchar(50) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `deviceId` varchar(100) DEFAULT NULL,
  `gcmId` text,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `update_reason` text,
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `breakdown`
--
ALTER TABLE `breakdown`
  ADD PRIMARY KEY (`id_breakdown`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id_state`);

--
-- Indexes for table `track_breakdown_request`
--
ALTER TABLE `track_breakdown_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `track_service_request`
--
ALTER TABLE `track_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_breakdown_request`
--
ALTER TABLE `user_breakdown_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id_user_log`);

--
-- Indexes for table `user_quote_request`
--
ALTER TABLE `user_quote_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_service_request`
--
ALTER TABLE `user_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_update_history`
--
ALTER TABLE `user_update_history`
  ADD PRIMARY KEY (`id_update_history`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `breakdown`
--
ALTER TABLE `breakdown`
  MODIFY `id_breakdown` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id_city` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=356;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id_state` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `track_breakdown_request`
--
ALTER TABLE `track_breakdown_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `track_service_request`
--
ALTER TABLE `track_service_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `user_breakdown_request`
--
ALTER TABLE `user_breakdown_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id_user_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=429;

--
-- AUTO_INCREMENT for table `user_quote_request`
--
ALTER TABLE `user_quote_request`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `user_service_request`
--
ALTER TABLE `user_service_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `user_update_history`
--
ALTER TABLE `user_update_history`
  MODIFY `id_update_history` int(222) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
