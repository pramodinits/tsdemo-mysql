-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 12:31 PM
-- Server version: 5.6.43
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilift`
--

-- --------------------------------------------------------

--
-- Table structure for table `breakdown`
--

CREATE TABLE `breakdown` (
  `id_breakdown` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `breakdown`
--

INSERT INTO `breakdown` (`id_breakdown`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'breakdown 1', 'test breakdown', 1, '2018-08-30 20:36:56'),
(2, 'breakdown 2', 'test 2 breakdown', 1, '2018-08-30 20:37:18'),
(3, 'Breakdown 3', 'Test break', 1, '2018-08-30 21:17:25'),
(4, 'Forklift Batteries', 'forklift', 1, '2018-08-31 12:54:59'),
(5, 'Forklift Battery Chargers', 'Test Breakdown 4', 1, '2018-09-07 19:25:59'),
(6, 'Testing Breakdown', 'Test', 1, '2018-10-12 18:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id_item` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id_item`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'product 1', 'test product', 1, '2018-08-30 20:31:15'),
(2, 'Product2', 'test description', 1, '2018-08-30 20:33:14'),
(3, 'Product 3', 'Test product', 1, '2018-08-30 21:18:25'),
(4, 'Forklifts', 'f', 1, '2018-08-31 12:52:55'),
(5, 'test pr', 'test test test', 1, '2018-09-07 16:35:37'),
(6, 'Product 4', 'Test a new Product', 1, '2018-09-07 19:31:02'),
(7, 'Forklift Battery Chargers', 'FBC', 1, '2018-09-27 20:10:32'),
(8, 'Test Product', 'Test', 1, '2018-10-12 19:01:04');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(111) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `sendtype` int(2) DEFAULT NULL,
  `sendflg` smallint(2) DEFAULT NULL,
  `sendto` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `description`, `sendtype`, `sendflg`, `sendto`, `status`, `add_date`) VALUES
(1, 'Registration', 'You have registered Successfully.', 1, 1, 47, 1, '2018-08-30 20:25:16'),
(2, 'kisan Registration', 'kisan has be register, Kindly approve.', 1, 16, 1, 1, '2018-08-30 20:25:16'),
(3, 'Registration', 'You have registered Successfully.', 1, 1, 48, 1, '2018-08-30 20:26:55'),
(4, 'raj Registration', 'raj has be register, Kindly approve.', 1, 16, 1, 1, '2018-08-30 20:26:55'),
(5, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-08-30 20:34:19'),
(6, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-08-30 20:34:19'),
(7, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-08-30 20:38:31'),
(8, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-08-30 20:38:31'),
(9, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-08-30 20:41:49'),
(10, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-08-30 20:41:49'),
(11, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-08-30 20:49:40'),
(12, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-08-30 20:49:40'),
(13, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-08-30 20:49:40'),
(14, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-08-30 20:53:14'),
(15, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-08-30 20:53:14'),
(16, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-08-30 20:53:14'),
(17, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-08-30 21:19:50'),
(18, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-08-30 21:19:50'),
(19, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-08-30 21:19:50'),
(20, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-08-30 21:26:17'),
(21, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-08-30 21:26:17'),
(22, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-08-30 21:26:17'),
(23, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-08-30 21:27:53'),
(24, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-08-30 21:27:53'),
(25, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-08-30 21:28:42'),
(26, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-08-30 21:28:42'),
(27, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-08-30 21:30:17'),
(28, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-08-30 21:30:17'),
(29, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-08-30 21:30:17'),
(30, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-08-30 21:30:41'),
(31, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-08-30 21:30:41'),
(32, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-08-30 21:30:41'),
(33, 'Breakdown request accepted', 'Breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-08-30 21:31:30'),
(34, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-08-30 21:31:30'),
(35, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-08-30 21:31:30'),
(36, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-08-30 21:32:36'),
(37, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-08-30 21:32:36'),
(38, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-08-30 21:32:36'),
(39, 'Registration', 'You have registered successfully.', 1, 1, 49, 1, '2018-08-31 15:27:09'),
(40, 'chitta Registration', 'chitta has be register, Kindly approve.', 1, 16, 1, 1, '2018-08-31 15:27:09'),
(41, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-01 11:45:33'),
(42, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-01 11:45:33'),
(43, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-01 11:48:06'),
(44, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-01 11:48:06'),
(45, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-01 12:05:43'),
(46, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-01 12:05:43'),
(47, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 49, 1, '2018-09-01 12:06:40'),
(48, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-01 12:06:40'),
(49, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-01 12:06:40'),
(50, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-01 16:20:01'),
(51, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-01 16:20:01'),
(52, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 49, 1, '2018-09-01 16:20:27'),
(53, 'Breakdown Request', 'chitta posted a new breakdown request.', 3, 18, 1, 1, '2018-09-01 16:20:27'),
(54, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-01 16:20:44'),
(55, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-01 16:20:44'),
(56, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 49, 1, '2018-09-01 16:30:03'),
(57, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-01 16:30:03'),
(58, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-01 16:30:03'),
(59, 'Registration', 'You have registered successfully.', 1, 1, 51, 1, '2018-09-04 14:55:06'),
(60, 'vivek Registration', 'You have received a sign up request name :vivek, Kindly approve.', 1, 16, 1, 1, '2018-09-04 14:55:06'),
(61, 'Quote Request', 'You have posted a new quote request', 4, 1, 51, 1, '2018-09-04 14:56:24'),
(62, 'Quote Request', 'vivek posted a new quote request.', 4, 20, 1, 1, '2018-09-04 14:56:24'),
(63, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 51, 1, '2018-09-04 14:57:16'),
(64, 'Breakdown Request', 'vivek posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 14:57:16'),
(65, 'New Service Request', 'You have posted a new Service Request', 2, 1, 51, 1, '2018-09-04 14:58:03'),
(66, ' New Service Request', 'vivek posted a new service request.', 2, 18, 1, 1, '2018-09-04 14:58:03'),
(67, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 51, 1, '2018-09-04 15:10:39'),
(68, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 15:10:39'),
(69, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 15:10:39'),
(70, 'New Service Request', 'You have posted a new Service Request', 2, 1, 51, 1, '2018-09-04 15:27:10'),
(71, ' New Service Request', 'vivek posted a new service request.', 2, 18, 1, 1, '2018-09-04 15:27:10'),
(72, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-04 16:54:59'),
(73, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-04 16:54:59'),
(74, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 16:59:38'),
(75, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 16:59:38'),
(76, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 17:01:26'),
(77, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 17:01:26'),
(78, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 17:13:15'),
(79, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 17:13:15'),
(80, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-04 17:14:35'),
(81, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-04 17:14:35'),
(82, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 17:15:07'),
(83, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 17:15:07'),
(84, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 17:15:51'),
(85, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 17:15:51'),
(86, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 17:22:16'),
(87, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 17:22:16'),
(88, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 17:28:48'),
(89, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 17:28:48'),
(90, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 17:28:48'),
(91, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-04 17:30:40'),
(92, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-04 17:30:40'),
(93, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-04 17:30:40'),
(94, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 17:39:28'),
(95, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 17:39:28'),
(96, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 17:39:28'),
(97, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 17:42:19'),
(98, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 17:42:19'),
(99, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 17:42:19'),
(100, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 17:43:23'),
(101, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 17:43:23'),
(102, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 17:43:23'),
(103, 'Registration', 'You have registered successfully.', 1, 1, 52, 1, '2018-09-04 17:58:18'),
(104, 'demo new Registration', 'You have received a sign up request name :demo new, Kindly approve.', 1, 16, 1, 1, '2018-09-04 17:58:18'),
(105, 'Registration', 'You have registered successfully.', 1, 1, 53, 1, '2018-09-04 17:59:07'),
(106, 'ghh Registration', 'You have received a sign up request name :ghh, Kindly approve.', 1, 16, 1, 1, '2018-09-04 17:59:07'),
(107, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 18:22:05'),
(108, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 18:22:05'),
(109, 'Breakdown request accepted', 'Breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 18:22:24'),
(110, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 18:22:24'),
(111, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-04 18:22:24'),
(112, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-04 18:23:31'),
(113, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-04 18:23:31'),
(114, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-04 18:23:31'),
(115, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 18:23:55'),
(116, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 18:23:55'),
(117, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 18:25:31'),
(118, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 18:25:31'),
(119, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 18:25:31'),
(120, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 19:24:47'),
(121, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 19:24:47'),
(122, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-04 19:26:18'),
(123, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-04 19:26:18'),
(124, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 19:26:57'),
(125, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 19:26:57'),
(126, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 19:29:58'),
(127, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 19:29:58'),
(128, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 19:29:58'),
(129, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 19:57:22'),
(130, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 19:57:22'),
(131, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 19:57:49'),
(132, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 19:57:49'),
(133, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 19:57:49'),
(134, 'Service Closed', 'Your service closed', 3, 1, 48, 1, '2018-09-04 19:58:13'),
(135, 'Service Closed', ' Service closed', 3, 18, 1, 1, '2018-09-04 19:58:13'),
(136, 'Service Closed', 'Service Closed', 3, 8, 27, 1, '2018-09-04 19:58:13'),
(137, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 19:58:46'),
(138, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 19:58:46'),
(139, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 19:58:58'),
(140, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 19:58:58'),
(141, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 19:58:58'),
(142, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-04 19:59:26'),
(143, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-04 19:59:26'),
(144, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-04 19:59:26'),
(145, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 20:01:17'),
(146, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 20:01:17'),
(147, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 20:01:30'),
(148, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 20:01:30'),
(149, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 20:01:30'),
(150, 'Brackdown Closed', 'Your Brackdown closed', 3, 1, 48, 1, '2018-09-04 20:01:43'),
(151, 'Brackdown Closed', ' Brackdown closed', 3, 18, 1, 1, '2018-09-04 20:01:43'),
(152, 'Brackdown Closed', 'Brackdown Closed', 3, 8, 27, 1, '2018-09-04 20:01:43'),
(153, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 20:05:42'),
(154, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 20:05:42'),
(155, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 20:06:02'),
(156, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 20:06:02'),
(157, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 20:06:02'),
(158, 'Breakdown Request Closed', 'Your breakdown request closed', 3, 1, 48, 1, '2018-09-04 20:06:23'),
(159, 'Breakdown Request Closed', ' Breakdown request closed', 3, 18, 1, 1, '2018-09-04 20:06:23'),
(160, 'Breakdown Request Closed', 'Breakdown request closed', 3, 8, 27, 1, '2018-09-04 20:06:23'),
(161, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 20:07:08'),
(162, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 20:07:08'),
(163, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 20:07:52'),
(164, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 20:07:52'),
(165, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 20:07:52'),
(166, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-04 20:08:06'),
(167, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-04 20:08:06'),
(168, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-04 20:08:06'),
(169, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-04 20:08:37'),
(170, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-04 20:08:37'),
(171, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 48, 1, '2018-09-04 20:08:48'),
(172, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-04 20:08:48'),
(173, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-04 20:08:48'),
(174, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-04 20:09:36'),
(175, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-04 20:09:36'),
(176, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita das1.', 3, 1, 48, 1, '2018-09-04 20:09:51'),
(177, 'Breakdown request assigned', ' Breakdown service request assigned to Namita das1.', 3, 18, 1, 1, '2018-09-04 20:09:51'),
(178, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-09-04 20:09:51'),
(179, 'Registration', 'You have registered successfully.', 1, 1, 54, 1, '2018-09-05 11:55:08'),
(180, 'nisikantkar Registration', 'You have received a sign up request name :nisikantkar, Kindly approve.', 1, 16, 1, 1, '2018-09-05 11:55:08'),
(181, 'Registration', 'You have registered successfully.', 1, 1, 55, 1, '2018-09-05 12:44:26'),
(182, 'nisikantkar Registration', 'You have received a sign up request name :nisikantkar, Kindly approve.', 1, 16, 1, 1, '2018-09-05 12:44:26'),
(183, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-05 14:11:21'),
(184, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-05 14:11:21'),
(185, 'Service request accepted', 'Your service request assigned to New executive.', 2, 1, 49, 1, '2018-09-05 14:11:49'),
(186, 'Service request assigned', ' Service request assigned to New executive.', 2, 18, 1, 1, '2018-09-05 14:11:49'),
(187, 'Service assigned', 'New service assigned', 2, 8, 46, 1, '2018-09-05 14:11:49'),
(188, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-05 14:12:57'),
(189, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-05 14:12:57'),
(190, 'Service request accepted', 'Your service request assigned to Namita das1.', 2, 1, 49, 1, '2018-09-05 14:13:16'),
(191, 'Service request assigned', ' Service request assigned to Namita das1.', 2, 18, 1, 1, '2018-09-05 14:13:16'),
(192, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-05 14:13:16'),
(193, 'Service Closed', 'Your service closed', 2, 1, 49, 1, '2018-09-05 14:13:42'),
(194, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-05 14:13:42'),
(195, 'Service Closed', 'Service Closed', 2, 8, 46, 1, '2018-09-05 14:13:42'),
(196, 'Service Closed', 'Your service closed', 2, 1, 49, 1, '2018-09-05 14:14:09'),
(197, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-05 14:14:09'),
(198, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-05 14:14:09'),
(199, 'Registration', 'You have registered successfully.', 1, 1, 56, 1, '2018-09-05 14:31:44'),
(200, 'lalit Registration', 'You have received a sign up request name :lalit, Kindly approve.', 1, 16, 1, 1, '2018-09-05 14:31:44'),
(201, 'New Service Request', 'You have posted a new Service Request', 2, 1, 56, 1, '2018-09-05 14:32:49'),
(202, ' New Service Request', 'lalit posted a new service request.', 2, 18, 1, 1, '2018-09-05 14:32:49'),
(203, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 14:33:18'),
(204, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 14:33:18'),
(205, 'Service request accepted', 'Your service request assigned to New executive.', 2, 1, 56, 1, '2018-09-05 14:33:43'),
(206, 'Service request assigned', ' Service request assigned to New executive.', 2, 18, 1, 1, '2018-09-05 14:33:43'),
(207, 'Service assigned', 'New service assigned', 2, 8, 46, 1, '2018-09-05 14:33:43'),
(208, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 56, 1, '2018-09-05 14:34:27'),
(209, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-05 14:34:27'),
(210, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-05 14:34:27'),
(211, 'New Service Request', 'You have posted a new Service Request', 2, 1, 56, 1, '2018-09-05 14:37:07'),
(212, ' New Service Request', 'lalit posted a new service request.', 2, 18, 1, 1, '2018-09-05 14:37:07'),
(213, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 56, 1, '2018-09-05 14:37:48'),
(214, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-05 14:37:48'),
(215, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-05 14:37:48'),
(216, 'Service Closed', 'Your service closed', 2, 1, 56, 1, '2018-09-05 14:39:08'),
(217, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-05 14:39:08'),
(218, 'Service Closed', 'Service Closed', 2, 8, 46, 1, '2018-09-05 14:39:08'),
(219, 'Breakdown Request Closed', 'Your breakdown request closed', 3, 1, 56, 1, '2018-09-05 14:39:38'),
(220, 'Breakdown Request Closed', ' Breakdown request closed', 3, 18, 1, 1, '2018-09-05 14:39:38'),
(221, 'Breakdown Request Closed', 'Breakdown request closed', 3, 8, 50, 1, '2018-09-05 14:39:38'),
(222, 'Quote Request', 'You have posted a new quote request', 4, 1, 56, 1, '2018-09-05 14:42:19'),
(223, 'Quote Request', 'lalit posted a new quote request.', 4, 20, 1, 1, '2018-09-05 14:42:19'),
(224, 'New Service Request', 'You have posted a new Service Request', 2, 1, 56, 1, '2018-09-05 14:43:02'),
(225, ' New Service Request', 'lalit posted a new service request.', 2, 18, 1, 1, '2018-09-05 14:43:02'),
(226, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 14:43:36'),
(227, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 14:43:36'),
(228, 'Breakdown request accepted', 'Your breakdown service request assigned to Bichitra.', 3, 1, 56, 1, '2018-09-05 14:46:53'),
(229, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-05 14:46:53'),
(230, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 50, 1, '2018-09-05 14:46:53'),
(231, 'Breakdown Request Closed', 'Your breakdown request closed', 3, 1, 56, 1, '2018-09-05 15:29:53'),
(232, 'Breakdown Request Closed', ' Breakdown request closed', 3, 18, 1, 1, '2018-09-05 15:29:53'),
(233, 'Breakdown Request Closed', 'Breakdown request closed', 3, 8, 50, 1, '2018-09-05 15:29:53'),
(234, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 15:31:20'),
(235, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 15:31:20'),
(236, 'New Service Request', 'You have posted a new Service Request', 2, 1, 56, 1, '2018-09-05 15:31:40'),
(237, ' New Service Request', 'lalit posted a new service request.', 2, 18, 1, 1, '2018-09-05 15:31:40'),
(238, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 15:32:11'),
(239, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 15:32:11'),
(240, 'Breakdown request accepted', 'Your breakdown service request assigned to Bichitra.', 3, 1, 56, 1, '2018-09-05 15:34:17'),
(241, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-05 15:34:17'),
(242, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 50, 1, '2018-09-05 15:34:17'),
(243, 'New Service Request', 'You have posted a new Service Request', 2, 1, 56, 1, '2018-09-05 15:35:51'),
(244, ' New Service Request', 'lalit posted a new service request.', 2, 18, 1, 1, '2018-09-05 15:35:51'),
(245, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 15:36:39'),
(246, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 15:36:39'),
(247, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 56, 1, '2018-09-05 15:38:01'),
(248, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-05 15:38:01'),
(249, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-05 15:38:01'),
(250, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 51, 1, '2018-09-05 15:39:34'),
(251, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-05 15:39:34'),
(252, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-05 15:39:34'),
(253, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 56, 1, '2018-09-05 15:40:38'),
(254, 'Breakdown Request', 'lalit posted a new breakdown request.', 3, 18, 1, 1, '2018-09-05 15:40:38'),
(255, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 56, 1, '2018-09-05 15:41:44'),
(256, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-05 15:41:44'),
(257, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-05 15:41:44'),
(258, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 56, 1, '2018-09-05 15:44:01'),
(259, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-05 15:44:01'),
(260, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 50, 1, '2018-09-05 15:44:01'),
(261, 'Service Closed', 'Your service closed', 2, 1, 56, 1, '2018-09-05 15:45:25'),
(262, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-05 15:45:25'),
(263, 'Service Closed', 'Service Closed', 2, 8, 50, 1, '2018-09-05 15:45:25'),
(264, 'Registration', 'You have registered successfully.', 1, 1, 57, 1, '2018-09-05 17:10:09'),
(265, 'chitta Registration', 'You have received a sign up request name :chitta, Kindly approve.', 1, 16, 1, 1, '2018-09-05 17:10:09'),
(266, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-07 12:01:29'),
(267, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-07 12:01:29'),
(268, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-07 12:02:51'),
(269, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-07 12:02:51'),
(270, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-07 12:06:43'),
(271, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-07 12:06:43'),
(272, 'Registration', 'You have registered successfully.', 1, 1, 59, 1, '2018-09-11 16:39:16'),
(273, 'shalini.rath Registration', 'You have received a sign up request name :shalini.rath, Kindly approve.', 1, 16, 1, 1, '2018-09-11 16:39:16'),
(274, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 59, 1, '2018-09-11 16:53:07'),
(275, 'Breakdown Request', 'shalini.rath posted a new breakdown request.', 3, 18, 1, 1, '2018-09-11 16:53:07'),
(276, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 56, 1, '2018-09-11 16:55:04'),
(277, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-11 16:55:04'),
(278, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-11 16:55:04'),
(279, 'Registration', 'You have registered successfully.', 1, 1, 60, 1, '2018-09-11 19:43:58'),
(280, 'S T Registration', 'You have received a sign up request name :S T, Kindly approve.', 1, 16, 1, 1, '2018-09-11 19:43:58'),
(281, 'Registration', 'You have registered successfully.', 1, 1, 61, 1, '2018-09-11 19:49:03'),
(282, 'Ramesh Registration', 'You have received a sign up request name :Ramesh, Kindly approve.', 1, 16, 1, 1, '2018-09-11 19:49:03'),
(283, 'Quote Request', 'You have posted a new quote request', 4, 1, 55, 1, '2018-09-11 19:53:11'),
(284, 'Quote Request', 'Vijay posted a new quote request.', 4, 20, 1, 1, '2018-09-11 19:53:11'),
(285, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 55, 1, '2018-09-11 19:56:21'),
(286, 'Breakdown Request', 'Vijay posted a new breakdown request.', 3, 18, 1, 1, '2018-09-11 19:56:21'),
(287, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 55, 1, '2018-09-11 20:02:29'),
(288, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-11 20:02:29'),
(289, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-11 20:02:29'),
(290, 'New Service Request', 'You have posted a new Service Request', 2, 1, 59, 1, '2018-09-12 15:54:28'),
(291, ' New Service Request', 'shalini.rath posted a new service request.', 2, 18, 1, 1, '2018-09-12 15:54:28'),
(292, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 56, 1, '2018-09-12 15:55:37'),
(293, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-12 15:55:37'),
(294, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-12 15:55:37'),
(295, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-12 15:59:21'),
(296, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-12 15:59:21'),
(297, 'Service request accepted', 'Your service request assigned to Demo Executive.', 2, 1, 48, 1, '2018-09-12 16:00:49'),
(298, 'Service request assigned', ' Service request assigned to Demo Executive.', 2, 18, 1, 1, '2018-09-12 16:00:49'),
(299, 'Service assigned', 'New service assigned', 2, 8, 58, 1, '2018-09-12 16:00:49'),
(300, 'Quote Request', 'You have posted a new quote request', 4, 1, 55, 1, '2018-09-12 16:06:03'),
(301, 'Quote Request', 'nisikantkar posted a new quote request.', 4, 20, 1, 1, '2018-09-12 16:06:03'),
(302, 'Registration', 'You have registered successfully.', 1, 1, 62, 1, '2018-09-12 16:07:34'),
(303, 'Vivek Ilift Registration', 'You have received a sign up request name :Vivek Ilift, Kindly approve.', 1, 16, 1, 1, '2018-09-12 16:07:34'),
(304, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 59, 1, '2018-09-12 16:13:58'),
(305, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-12 16:13:58'),
(306, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-12 16:13:58'),
(307, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 59, 1, '2018-09-14 15:52:42'),
(308, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-14 15:52:42'),
(309, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-14 15:52:42'),
(310, 'Quote Request', 'You have posted a new quote request', 4, 1, 59, 1, '2018-09-14 15:53:33'),
(311, 'Quote Request', 'shalini.rath posted a new quote request.', 4, 20, 1, 1, '2018-09-14 15:53:33'),
(312, 'Quote Request', 'You have posted a new quote request', 4, 1, 55, 1, '2018-09-14 16:06:17'),
(313, 'Quote Request', 'nisikantkar posted a new quote request.', 4, 20, 1, 1, '2018-09-14 16:06:17'),
(314, 'Registration', 'You have registered successfully.', 1, 1, 63, 1, '2018-09-15 12:49:42'),
(315, 'rajesh Registration', 'You have received a sign up request name :rajesh, Kindly approve.', 1, 16, 1, 1, '2018-09-15 12:49:42'),
(316, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-15 12:51:11'),
(317, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-15 12:51:11'),
(318, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-15 12:52:18'),
(319, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-15 12:52:18'),
(320, 'Quote Request', 'You have posted a new quote request', 4, 1, 55, 1, '2018-09-15 12:54:58'),
(321, 'Quote Request', 'nisikantkar posted a new quote request.', 4, 20, 1, 1, '2018-09-15 12:54:58'),
(322, 'Registration', 'You have registered successfully.', 1, 1, 64, 1, '2018-09-15 12:56:51'),
(323, 'vvek Registration', 'You have received a sign up request name :vvek, Kindly approve.', 1, 16, 1, 1, '2018-09-15 12:56:51'),
(324, 'Breakdown request accepted', 'Breakdown service request assigned to New executive.', 3, 1, 56, 1, '2018-09-15 15:22:05'),
(325, 'Breakdown request assigned', ' Breakdown service request assigned to New executive.', 3, 18, 1, 1, '2018-09-15 15:22:05'),
(326, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 46, 1, '2018-09-15 15:22:05'),
(327, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 49, 1, '2018-09-15 15:22:18'),
(328, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-15 15:22:18'),
(329, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-15 15:22:18'),
(330, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 55, 1, '2018-09-15 15:22:44'),
(331, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-15 15:22:44'),
(332, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-15 15:22:44'),
(333, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-15 15:27:55'),
(334, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-15 15:27:55'),
(335, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-15 15:29:00'),
(336, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-15 15:29:00'),
(337, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-15 15:29:00'),
(338, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-15 15:30:47'),
(339, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-15 15:30:47'),
(340, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 48, 1, '2018-09-15 15:31:23'),
(341, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-15 15:31:23'),
(342, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-15 15:31:23'),
(343, 'New Service Request', 'You have posted a new Service Request', 2, 1, 55, 1, '2018-09-15 15:31:59'),
(344, ' New Service Request', 'nisikantkar posted a new service request.', 2, 18, 1, 1, '2018-09-15 15:31:59'),
(345, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-15 15:33:04'),
(346, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-15 15:33:04'),
(347, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-15 15:33:52'),
(348, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-15 15:33:52'),
(349, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-15 15:33:52'),
(350, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 55, 1, '2018-09-15 15:36:25'),
(351, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-15 15:36:25'),
(352, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-15 15:36:25'),
(353, 'Service Closed', 'Your service closed', 2, 1, 55, 1, '2018-09-15 15:36:50'),
(354, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-15 15:36:50'),
(355, 'Service Closed', 'Service Closed', 2, 8, 50, 1, '2018-09-15 15:36:50'),
(356, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-15 16:49:11'),
(357, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-15 16:49:11'),
(358, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 11:38:26'),
(359, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 11:38:26'),
(360, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 11:38:26'),
(361, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-09-17 11:41:48'),
(362, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-09-17 11:41:48'),
(363, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-09-17 11:42:26'),
(364, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 11:42:26'),
(365, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 11:42:26'),
(366, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 11:42:46'),
(367, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 11:42:46'),
(368, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 48, 1, '2018-09-17 11:42:58'),
(369, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 11:42:58'),
(370, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 11:42:58'),
(371, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 11:45:34'),
(372, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 11:45:34'),
(373, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 11:45:34'),
(374, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 51, 1, '2018-09-17 12:08:59'),
(375, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 12:08:59'),
(376, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 12:08:59'),
(377, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-09-17 12:09:58'),
(378, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 12:09:58'),
(379, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-09-17 12:13:51'),
(380, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 12:13:51'),
(381, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-09-17 12:14:50'),
(382, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 12:14:50'),
(383, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-09-17 12:23:59'),
(384, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-09-17 12:23:59'),
(385, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 16:58:31'),
(386, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 16:58:31'),
(387, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 16:59:10'),
(388, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 16:59:10'),
(389, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 16:59:10'),
(390, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 17:00:12'),
(391, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 17:00:12'),
(392, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 17:03:49'),
(393, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 17:03:49'),
(394, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 17:03:49'),
(395, 'Breakdown Request Closed', 'Your breakdown request closed', 3, 1, 48, 1, '2018-09-17 17:05:27'),
(396, 'Breakdown Request Closed', ' Breakdown request closed', 3, 18, 1, 1, '2018-09-17 17:05:27'),
(397, 'Breakdown Request Closed', 'Breakdown request closed', 3, 8, 27, 1, '2018-09-17 17:05:27'),
(398, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 17:13:17'),
(399, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 17:13:17'),
(400, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 17:15:04'),
(401, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 17:15:04'),
(402, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 17:15:58'),
(403, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 17:15:58'),
(404, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 17:15:58'),
(405, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 17:16:32'),
(406, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 17:16:32'),
(407, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 17:16:32'),
(408, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 17:17:44'),
(409, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 17:17:44'),
(410, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 17:17:44'),
(411, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-17 17:19:34'),
(412, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-17 17:19:34'),
(413, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 17:20:58'),
(414, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 17:20:58'),
(415, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 17:20:58'),
(416, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-17 17:24:46'),
(417, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-17 17:24:46'),
(418, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-17 17:24:46'),
(419, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 17:55:39'),
(420, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 17:55:39'),
(421, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 17:56:05'),
(422, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 17:56:05'),
(423, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 17:56:05'),
(424, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 17:57:25'),
(425, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 17:57:25'),
(426, 'Breakdown request accepted', 'Breakdown service request assigned to Bichitra.', 3, 1, 48, 1, '2018-09-17 17:57:49'),
(427, 'Breakdown request assigned', ' Breakdown service request assigned to Bichitra.', 3, 18, 1, 1, '2018-09-17 17:57:49'),
(428, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 50, 1, '2018-09-17 17:57:49'),
(429, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 17:58:10'),
(430, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 17:58:10'),
(431, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 50, 1, '2018-09-17 17:58:10'),
(432, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 17:58:37'),
(433, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 17:58:37'),
(434, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 17:58:37'),
(435, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 17:59:42'),
(436, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 17:59:42'),
(437, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 18:01:15'),
(438, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 18:01:15'),
(439, 'Service request accepted', 'Your service request assigned to New executive.', 2, 1, 48, 1, '2018-09-17 18:01:45'),
(440, 'Service request assigned', ' Service request assigned to New executive.', 2, 18, 1, 1, '2018-09-17 18:01:45'),
(441, 'Service assigned', 'New service assigned', 2, 8, 46, 1, '2018-09-17 18:01:45'),
(442, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-09-17 18:49:42'),
(443, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-09-17 18:49:42'),
(444, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 18:50:40'),
(445, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 18:50:40'),
(446, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 18:52:30'),
(447, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 18:52:30'),
(448, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 18:52:30'),
(449, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 18:53:25'),
(450, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 18:53:25'),
(451, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 18:53:25'),
(452, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 18:54:23'),
(453, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 18:54:23'),
(454, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 18:54:23'),
(455, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 19:12:48'),
(456, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 19:12:48'),
(457, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 19:12:48'),
(458, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 59, 1, '2018-09-17 19:36:21'),
(459, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 19:36:21'),
(460, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 50, 1, '2018-09-17 19:36:21'),
(461, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 19:42:03'),
(462, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 19:42:03'),
(463, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 19:42:29'),
(464, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 19:42:29'),
(465, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-09-17 19:42:49'),
(466, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-09-17 19:42:49'),
(467, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 19:43:18'),
(468, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 19:43:18'),
(469, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 19:43:18'),
(470, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 19:44:18'),
(471, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 19:44:18'),
(472, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 19:44:18'),
(473, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 48, 1, '2018-09-17 19:45:05'),
(474, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-09-17 19:45:05'),
(475, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-09-17 19:45:05'),
(476, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 19:45:49');
INSERT INTO `notification` (`id`, `title`, `description`, `sendtype`, `sendflg`, `sendto`, `status`, `add_date`) VALUES
(477, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 19:45:49'),
(478, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 19:45:49'),
(479, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 19:46:54'),
(480, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 19:46:54'),
(481, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 19:46:54'),
(482, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-17 19:47:31'),
(483, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-17 19:47:31'),
(484, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-17 19:47:31'),
(485, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 19:48:14'),
(486, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 19:48:14'),
(487, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 19:48:47'),
(488, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 19:48:47'),
(489, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-09-17 19:49:17'),
(490, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-09-17 19:49:17'),
(491, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 48, 1, '2018-09-17 19:51:02'),
(492, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 19:51:02'),
(493, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 19:51:02'),
(494, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 48, 1, '2018-09-17 19:51:53'),
(495, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 19:51:53'),
(496, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 19:51:53'),
(497, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 48, 1, '2018-09-17 19:52:36'),
(498, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-17 19:52:36'),
(499, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-17 19:52:36'),
(500, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-17 19:53:00'),
(501, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-17 19:53:00'),
(502, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-17 19:53:00'),
(503, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-17 19:53:37'),
(504, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-17 19:53:37'),
(505, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-17 19:53:37'),
(506, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-09-17 19:54:23'),
(507, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-17 19:54:23'),
(508, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-17 19:54:23'),
(509, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 48, 1, '2018-09-18 12:55:02'),
(510, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-18 12:55:02'),
(511, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-18 12:55:02'),
(512, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 48, 1, '2018-09-18 13:05:15'),
(513, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-18 13:05:15'),
(514, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-18 13:05:15'),
(515, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-18 13:16:36'),
(516, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-18 13:16:36'),
(517, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 49, 1, '2018-09-18 13:17:06'),
(518, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-09-18 13:17:06'),
(519, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-09-18 13:17:06'),
(520, 'Quote Request', 'You have posted a new quote request', 4, 1, 55, 1, '2018-09-19 14:36:14'),
(521, 'Quote Request', 'nisikantkar posted a new quote request.', 4, 20, 1, 1, '2018-09-19 14:36:14'),
(522, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-19 19:35:34'),
(523, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-19 19:35:34'),
(524, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 49, 1, '2018-09-19 19:36:39'),
(525, 'Breakdown Request', 'chitta posted a new breakdown request.', 3, 18, 1, 1, '2018-09-19 19:36:39'),
(526, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-09-19 19:38:32'),
(527, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-09-19 19:38:32'),
(528, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-09-19 19:39:07'),
(529, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-09-19 19:39:07'),
(530, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 48, 1, '2018-09-22 15:47:06'),
(531, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-09-22 15:47:06'),
(532, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-09-22 15:47:06'),
(533, 'Registration', 'You have registered successfully.', 1, 1, 65, 1, '2018-09-27 20:07:00'),
(534, 'Vijay Registration', 'You have received a sign up request name :Vijay, Kindly approve.', 1, 16, 1, 1, '2018-09-27 20:07:00'),
(535, 'New Service Request', 'You have posted a new Service Request', 2, 1, 65, 1, '2018-09-27 20:29:32'),
(536, ' New Service Request', 'Vijay posted a new service request.', 2, 18, 1, 1, '2018-09-27 20:29:32'),
(537, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 65, 1, '2018-09-27 20:30:40'),
(538, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-09-27 20:30:40'),
(539, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-09-27 20:30:40'),
(540, 'Service Closed', 'Your service closed', 2, 1, 65, 1, '2018-09-27 20:33:32'),
(541, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-09-27 20:33:32'),
(542, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-09-27 20:33:32'),
(543, 'Service Closed', 'Your service closed', 2, 1, 48, 1, '2018-10-03 15:16:35'),
(544, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-03 15:16:35'),
(545, 'Service Closed', 'Service Closed', 2, 8, 46, 1, '2018-10-03 15:16:35'),
(546, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-10-03 19:22:32'),
(547, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-10-03 19:22:32'),
(548, 'Quote Request', 'You have posted a new quote request', 4, 1, 48, 1, '2018-10-03 19:23:02'),
(549, 'Quote Request', 'raj posted a new quote request.', 4, 20, 1, 1, '2018-10-03 19:23:02'),
(550, 'Registration', 'You have registered successfully.', 1, 1, 66, 1, '2018-10-04 11:35:19'),
(551, 'inf Registration', 'You have received a sign up request name :inf, Kindly approve.', 1, 16, 1, 1, '2018-10-04 11:35:19'),
(552, 'Registration', 'You have registered successfully.', 1, 1, 67, 1, '2018-10-04 11:37:21'),
(553, 'dd Registration', 'You have received a sign up request name :dd, Kindly approve.', 1, 16, 1, 1, '2018-10-04 11:37:21'),
(554, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-10-08 19:59:51'),
(555, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-10-08 19:59:51'),
(556, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-10-08 20:02:41'),
(557, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-10-08 20:02:41'),
(558, 'New Service Request', 'You have posted a new Service Request', 2, 1, 48, 1, '2018-10-08 20:04:57'),
(559, ' New Service Request', 'raj posted a new service request.', 2, 18, 1, 1, '2018-10-08 20:04:57'),
(560, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-10-09 17:05:11'),
(561, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-10-09 17:05:11'),
(562, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 49, 1, '2018-10-09 17:06:27'),
(563, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-09 17:06:27'),
(564, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-09 17:06:27'),
(565, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-10-09 17:30:05'),
(566, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-10-09 17:30:05'),
(567, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 48, 1, '2018-10-10 16:42:56'),
(568, 'Breakdown Request', 'raj posted a new breakdown request.', 3, 18, 1, 1, '2018-10-10 16:42:56'),
(569, 'Service Closed', 'Your service closed', 2, 1, 49, 1, '2018-10-11 12:07:39'),
(570, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-11 12:07:39'),
(571, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-11 12:07:39'),
(572, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 15:13:49'),
(573, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 15:13:49'),
(574, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 15:15:03'),
(575, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 15:15:03'),
(576, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 15:15:34'),
(577, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 15:15:34'),
(578, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 15:18:15'),
(579, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 15:18:15'),
(580, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 15:21:23'),
(581, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 15:21:23'),
(582, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 15:27:06'),
(583, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 15:27:06'),
(584, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 15:31:10'),
(585, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 15:31:10'),
(586, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-12 15:32:00'),
(587, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-12 15:32:00'),
(588, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-12 15:32:00'),
(589, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 15:32:57'),
(590, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 15:32:57'),
(591, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 16:25:55'),
(592, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 16:25:55'),
(593, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 17:22:03'),
(594, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 17:22:03'),
(595, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 17:24:11'),
(596, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 17:24:11'),
(597, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 17:25:09'),
(598, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 17:25:09'),
(599, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 17:26:00'),
(600, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 17:26:00'),
(601, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 17:27:56'),
(602, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 17:27:56'),
(603, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 17:28:27'),
(604, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 17:28:27'),
(605, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 17:44:17'),
(606, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 17:44:17'),
(607, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 17:47:53'),
(608, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 17:47:53'),
(609, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-12 17:54:31'),
(610, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-12 17:54:31'),
(611, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-12 17:54:31'),
(612, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-12 17:58:23'),
(613, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 17:58:23'),
(614, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-12 17:58:23'),
(615, 'Service Closed', 'Your service closed', 2, 1, 49, 1, '2018-10-12 18:34:16'),
(616, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 18:34:16'),
(617, 'Service Closed', 'Service Closed', 2, 8, 50, 1, '2018-10-12 18:34:16'),
(618, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-12 18:36:54'),
(619, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-12 18:36:54'),
(620, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-12 18:36:54'),
(621, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-12 18:37:40'),
(622, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-12 18:37:40'),
(623, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-12 18:37:40'),
(624, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-12 18:39:22'),
(625, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-12 18:39:22'),
(626, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-12 18:39:22'),
(627, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-12 18:40:30'),
(628, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 18:40:30'),
(629, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-12 18:40:30'),
(630, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-12 18:53:30'),
(631, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-12 18:53:30'),
(632, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-10-12 18:53:30'),
(633, 'Breakdown Request Closed', 'Your breakdown request closed', 3, 1, 47, 1, '2018-10-12 18:54:12'),
(634, 'Breakdown Request Closed', ' Breakdown request closed', 3, 18, 1, 1, '2018-10-12 18:54:12'),
(635, 'Breakdown Request Closed', 'Breakdown request closed', 3, 8, 27, 1, '2018-10-12 18:54:12'),
(636, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 19:04:39'),
(637, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 19:04:39'),
(638, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 19:26:11'),
(639, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 19:26:11'),
(640, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 19:30:30'),
(641, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 19:30:30'),
(642, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 19:31:40'),
(643, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 19:31:40'),
(644, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-12 19:33:31'),
(645, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-12 19:33:31'),
(646, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-12 19:35:18'),
(647, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-12 19:35:18'),
(648, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-12 19:35:18'),
(649, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-12 19:36:55'),
(650, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 19:36:55'),
(651, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-12 19:36:55'),
(652, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-12 19:39:19'),
(653, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-12 19:39:19'),
(654, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-12 19:39:19'),
(655, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-12 19:39:51'),
(656, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 19:39:51'),
(657, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-12 19:39:51'),
(658, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-12 19:41:55'),
(659, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-12 19:41:55'),
(660, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-12 19:41:55'),
(661, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-12 19:42:56'),
(662, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-12 19:42:56'),
(663, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-12 19:42:56'),
(664, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 19:52:39'),
(665, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 19:52:39'),
(666, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 19:54:13'),
(667, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 19:54:13'),
(668, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 19:55:54'),
(669, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 19:55:54'),
(670, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-12 19:56:24'),
(671, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-12 19:56:24'),
(672, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 19:57:21'),
(673, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 19:57:21'),
(674, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 19:58:17'),
(675, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 19:58:17'),
(676, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 20:01:36'),
(677, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 20:01:36'),
(678, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-12 20:01:57'),
(679, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-12 20:01:57'),
(680, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-12 20:16:59'),
(681, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-12 20:16:59'),
(682, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-10-12 20:16:59'),
(683, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-12 20:18:07'),
(684, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-12 20:18:07'),
(685, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-12 20:18:07'),
(686, 'Breakdown request accepted', 'Your breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-12 20:19:08'),
(687, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-12 20:19:08'),
(688, 'Breakdown assigned', 'New breakdown service assigned', 3, 8, 27, 1, '2018-10-12 20:19:08'),
(689, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-12 20:19:23'),
(690, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-12 20:19:23'),
(691, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-12 20:19:23'),
(692, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-10-13 11:09:04'),
(693, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-10-13 11:09:04'),
(694, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 49, 1, '2018-10-13 11:10:48'),
(695, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-10-13 11:10:48'),
(696, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-10-13 11:10:48'),
(697, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 49, 1, '2018-10-13 11:17:53'),
(698, 'Breakdown Request', 'chitta posted a new breakdown request.', 3, 18, 1, 1, '2018-10-13 11:17:53'),
(699, 'Quote Request', 'You have posted a new quote request', 4, 1, 49, 1, '2018-10-13 11:22:38'),
(700, 'Quote Request', 'chitta posted a new quote request.', 4, 20, 1, 1, '2018-10-13 11:22:38'),
(701, 'Service Closed', 'Your service closed', 2, 1, 49, 1, '2018-10-13 11:33:34'),
(702, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-13 11:33:34'),
(703, 'Service Closed', 'Service Closed', 2, 8, 50, 1, '2018-10-13 11:33:34'),
(704, 'Service request accepted', 'Your service request assigned to Bichitra.', 2, 1, 47, 1, '2018-10-13 11:46:00'),
(705, 'Service request assigned', ' Service request assigned to Bichitra.', 2, 18, 1, 1, '2018-10-13 11:46:00'),
(706, 'Service assigned', 'New service assigned', 2, 8, 50, 1, '2018-10-13 11:46:00'),
(707, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-13 11:46:38'),
(708, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-13 11:46:38'),
(709, 'Service Closed', 'Service Closed', 2, 8, 50, 1, '2018-10-13 11:46:38'),
(710, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-13 15:26:22'),
(711, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-13 15:26:22'),
(712, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-13 15:26:22'),
(713, 'Quote Request', 'You have posted a new quote request', 4, 1, 65, 1, '2018-10-13 21:26:28'),
(714, 'Quote Request', 'Vijay posted a new quote request.', 4, 20, 1, 1, '2018-10-13 21:26:28'),
(715, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 65, 1, '2018-10-13 21:29:48'),
(716, 'Breakdown Request', 'Vijay posted a new breakdown request.', 3, 18, 1, 1, '2018-10-13 21:29:48'),
(717, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 65, 1, '2018-10-13 21:30:32'),
(718, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-13 21:30:32'),
(719, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-13 21:30:32'),
(720, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-15 16:38:38'),
(721, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-15 16:38:38'),
(722, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 65, 1, '2018-10-16 17:35:58'),
(723, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-16 17:35:58'),
(724, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-16 17:35:58'),
(725, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-16 17:58:35'),
(726, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-16 17:58:35'),
(727, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-16 17:58:35'),
(728, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-16 17:59:13'),
(729, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-16 17:59:13'),
(730, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-16 17:59:13'),
(731, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-16 17:59:53'),
(732, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-16 17:59:53'),
(733, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-16 17:59:53'),
(734, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-16 18:01:02'),
(735, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-16 18:01:02'),
(736, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-16 18:01:02'),
(737, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-17 14:52:03'),
(738, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-17 14:52:03'),
(739, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-17 14:54:40'),
(740, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-17 14:54:40'),
(741, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-17 14:56:22'),
(742, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-17 14:56:22'),
(743, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-17 14:57:34'),
(744, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-17 14:57:34'),
(745, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-17 14:59:50'),
(746, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-17 14:59:50'),
(747, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-17 15:00:17'),
(748, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-17 15:00:17'),
(749, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-17 15:01:57'),
(750, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-17 15:01:57'),
(751, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-17 15:01:57'),
(752, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-17 15:03:46'),
(753, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-17 15:03:46'),
(754, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-17 15:03:46'),
(755, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-17 15:04:48'),
(756, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-17 15:04:48'),
(757, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-17 15:04:48'),
(758, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-17 15:05:59'),
(759, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-17 15:05:59'),
(760, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-17 15:05:59'),
(761, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-17 15:07:05'),
(762, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-17 15:07:05'),
(763, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-17 15:07:05'),
(764, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-17 15:09:39'),
(765, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-17 15:09:39'),
(766, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-17 15:09:39'),
(767, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-17 15:12:06'),
(768, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:12:06'),
(769, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:12:06'),
(770, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-17 15:12:38'),
(771, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-17 15:12:38'),
(772, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-17 15:12:38'),
(773, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-17 15:13:34'),
(774, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:13:34'),
(775, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:13:34'),
(776, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-17 15:14:15'),
(777, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-17 15:14:15'),
(778, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-17 15:14:15'),
(779, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-17 15:15:57'),
(780, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:15:57'),
(781, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:15:57'),
(782, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-17 15:16:32'),
(783, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-17 15:16:32'),
(784, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-17 15:16:32'),
(785, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-17 15:18:11'),
(786, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-17 15:18:11'),
(787, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-17 15:18:42'),
(788, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-17 15:18:42'),
(789, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-17 15:18:58'),
(790, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:18:58'),
(791, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:18:58'),
(792, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-17 15:19:29'),
(793, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-17 15:19:29'),
(794, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-17 15:19:29'),
(795, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-17 15:25:14'),
(796, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-17 15:25:14'),
(797, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-17 15:25:46'),
(798, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-17 15:25:46'),
(799, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-17 15:26:36'),
(800, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:26:36'),
(801, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:26:36'),
(802, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-17 15:27:02'),
(803, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-17 15:27:02'),
(804, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-17 15:27:02'),
(805, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-17 15:27:36'),
(806, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-17 15:27:36'),
(807, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-17 15:27:36'),
(808, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-17 15:28:12'),
(809, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-17 15:28:12'),
(810, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-17 15:28:12'),
(811, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 49, 1, '2018-10-17 15:38:58'),
(812, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-17 15:38:58'),
(813, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-17 15:38:58'),
(814, 'Breakdown request accepted', 'Breakdown service request assigned to Namita Das.', 3, 1, 47, 1, '2018-10-22 13:06:16'),
(815, 'Breakdown request assigned', ' Breakdown service request assigned to Namita Das.', 3, 18, 1, 1, '2018-10-22 13:06:16'),
(816, 'Breakdown request assigned', 'Breakdown service request assigned', 3, 8, 27, 1, '2018-10-22 13:06:16'),
(817, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 49, 1, '2018-10-22 13:06:49'),
(818, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-22 13:06:49'),
(819, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-22 13:06:49'),
(820, 'Breakdown request closed', 'Breakdown service request closed.', 3, 1, 47, 1, '2018-10-22 14:45:39'),
(821, 'Breakdown request closed', ' Breakdown service request closed.', 3, 18, 1, 1, '2018-10-22 14:45:39'),
(822, 'Breakdown request closed', 'Breakdown service request closed', 3, 8, 27, 1, '2018-10-22 14:45:39'),
(823, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-22 14:50:58'),
(824, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-22 14:50:58'),
(825, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-22 14:50:58'),
(826, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-22 14:52:27'),
(827, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-22 14:52:27'),
(828, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-22 14:52:27'),
(829, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-22 14:53:02'),
(830, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-22 14:53:02'),
(831, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-22 14:53:02'),
(832, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-22 14:54:07'),
(833, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-22 14:54:07'),
(834, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-22 14:54:07'),
(835, 'Quote Request', 'You have posted a new quote request', 4, 1, 65, 1, '2018-10-24 15:39:35'),
(836, 'Quote Request', 'Vijay posted a new quote request.', 4, 20, 1, 1, '2018-10-24 15:39:35'),
(837, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 65, 1, '2018-10-24 15:40:36'),
(838, 'Breakdown Request', 'Vijay posted a new breakdown request.', 3, 18, 1, 1, '2018-10-24 15:40:36'),
(839, 'New Service Request', 'You have posted a new Service Request', 2, 1, 65, 1, '2018-10-24 15:42:12'),
(840, ' New Service Request', 'Vijay posted a new service request.', 2, 18, 1, 1, '2018-10-24 15:42:12'),
(841, 'New Service Request', 'You have posted a new Service Request', 2, 1, 49, 1, '2018-10-24 16:01:28'),
(842, ' New Service Request', 'chitta posted a new service request.', 2, 18, 1, 1, '2018-10-24 16:01:28'),
(843, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 49, 1, '2018-10-24 17:56:45'),
(844, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-24 17:56:45'),
(845, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-24 17:56:45'),
(846, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-29 12:37:53'),
(847, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-29 12:37:53'),
(848, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-10-29 12:41:32'),
(849, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-10-29 12:41:32'),
(850, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-10-29 12:41:32'),
(851, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-10-29 12:41:43'),
(852, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-10-29 12:41:43'),
(853, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-10-29 12:41:43'),
(854, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-29 19:06:33'),
(855, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-29 19:06:33'),
(856, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-29 19:07:17'),
(857, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-29 19:07:17'),
(858, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-29 19:08:13'),
(859, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-29 19:08:13'),
(860, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-29 19:12:37'),
(861, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-29 19:12:37'),
(862, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-29 19:13:41'),
(863, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-29 19:13:41'),
(864, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-29 19:15:23'),
(865, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-29 19:15:23'),
(866, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-29 19:34:36'),
(867, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-29 19:34:36'),
(868, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-29 19:35:59'),
(869, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-29 19:35:59'),
(870, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 11:48:21'),
(871, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 11:48:21'),
(872, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 12:24:30'),
(873, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 12:24:30'),
(874, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 12:31:20'),
(875, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 12:31:20'),
(876, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 12:42:01'),
(877, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 12:42:01'),
(878, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 12:44:03'),
(879, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 12:44:03'),
(880, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 12:53:22'),
(881, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 12:53:22'),
(882, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 13:11:30'),
(883, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 13:11:30'),
(884, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 13:13:32'),
(885, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 13:13:32'),
(886, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 13:38:38'),
(887, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 13:38:38'),
(888, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 13:46:19'),
(889, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 13:46:19'),
(890, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 15:44:09'),
(891, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 15:44:09'),
(892, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 15:58:43'),
(893, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 15:58:43'),
(894, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 16:01:07'),
(895, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 16:01:07'),
(896, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-30 16:02:57'),
(897, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-30 16:02:57'),
(898, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 16:06:49'),
(899, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 16:06:49'),
(900, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 16:09:30'),
(901, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 16:09:30'),
(902, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-10-30 16:21:10'),
(903, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-10-30 16:21:10'),
(904, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-30 16:21:53'),
(905, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-30 16:21:53'),
(906, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-10-30 16:22:18'),
(907, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-10-30 16:22:18'),
(908, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-10-30 16:22:38'),
(909, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-10-30 16:22:38'),
(910, 'Registration', 'You have registered successfully.', 1, 1, 68, 1, '2018-10-30 16:36:47'),
(911, 'sup Registration', 'You have received a sign up request name :sup, Kindly approve.', 1, 16, 1, 1, '2018-10-30 16:36:47'),
(912, 'Registration', 'You have registered successfully.', 1, 1, 69, 1, '2018-10-30 16:44:47'),
(913, 'deepak Registration', 'You have received a sign up request name :deepak, Kindly approve.', 1, 16, 1, 1, '2018-10-30 16:44:47'),
(914, 'Registration', 'You have registered successfully.', 1, 1, 70, 1, '2018-10-30 16:58:54'),
(915, 'papa Registration', 'You have received a sign up request name :papa, Kindly approve.', 1, 16, 1, 1, '2018-10-30 16:58:54'),
(916, 'Registration', 'You have registered successfully.', 1, 1, 71, 1, '2018-10-30 17:06:00'),
(917, 'Suparijita Registration', 'You have received a sign up request name :Suparijita, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:06:00'),
(918, 'Registration', 'You have registered successfully.', 1, 1, 72, 1, '2018-10-30 17:09:02'),
(919, 'ggggggu Registration', 'You have received a sign up request name :ggggggu, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:09:02'),
(920, 'Registration', 'You have registered successfully.', 1, 1, 73, 1, '2018-10-30 17:26:40'),
(921, 'Deepak N Registration', 'You have received a sign up request name :Deepak N, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:26:40'),
(922, 'Registration', 'You have registered successfully.', 1, 1, 74, 1, '2018-10-30 17:33:04'),
(923, 'Deepak N Registration', 'You have received a sign up request name :Deepak N, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:33:04'),
(924, 'Registration', 'You have registered successfully.', 1, 1, 75, 1, '2018-10-30 17:53:23'),
(925, 'Deepak N Registration', 'You have received a sign up request name :Deepak N, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:53:23'),
(926, 'Registration', 'You have registered successfully.', 1, 1, 76, 1, '2018-10-30 17:56:28'),
(927, 'chitta Registration', 'You have received a sign up request name :chitta, Kindly approve.', 1, 16, 1, 1, '2018-10-30 17:56:28'),
(928, 'Registration', 'You have registered successfully.', 1, 1, 77, 1, '2018-10-30 18:01:01'),
(929, 'kisan Registration', 'You have received a sign up request name :kisan, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:01:01'),
(930, 'Registration', 'You have registered successfully.', 1, 1, 78, 1, '2018-10-30 18:02:39'),
(931, 'kisan Registration', 'You have received a sign up request name :kisan, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:02:39'),
(932, 'Registration', 'You have registered successfully.', 1, 1, 79, 1, '2018-10-30 18:07:32'),
(933, 'Deepak Registration', 'You have received a sign up request name :Deepak, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:07:32'),
(934, 'Registration', 'You have registered successfully.', 1, 1, 80, 1, '2018-10-30 18:12:30'),
(935, 'jjj Registration', 'You have received a sign up request name :jjj, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:12:30'),
(936, 'Registration', 'You have registered successfully.', 1, 1, 81, 1, '2018-10-30 18:14:55'),
(937, 'dbjdjd Registration', 'You have received a sign up request name :dbjdjd, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:14:55'),
(938, 'Registration', 'You have registered successfully.', 1, 1, 82, 1, '2018-10-30 18:24:20'),
(939, 'hdgh Registration', 'You have received a sign up request name :hdgh, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:24:20'),
(940, 'Registration', 'You have registered successfully.', 1, 1, 83, 1, '2018-10-30 18:31:24'),
(941, 'hh Registration', 'You have received a sign up request name :hh, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:31:24'),
(942, 'Registration', 'You have registered successfully.', 1, 1, 84, 1, '2018-10-30 18:33:24'),
(943, 'ccc Registration', 'You have received a sign up request name :ccc, Kindly approve.', 1, 16, 1, 1, '2018-10-30 18:33:24'),
(944, 'Registration', 'You have registered successfully.', 1, 1, 85, 1, '2018-10-30 19:14:29'),
(945, 'hhj Registration', 'You have received a sign up request name :hhj, Kindly approve.', 1, 16, 1, 1, '2018-10-30 19:14:29'),
(946, 'Registration', 'You have registered successfully.', 1, 1, 86, 1, '2018-10-30 19:19:27'),
(947, 'gyeyys Registration', 'You have received a sign up request name :gyeyys, Kindly approve.', 1, 16, 1, 1, '2018-10-30 19:19:27'),
(948, 'Registration', 'You have registered successfully.', 1, 1, 87, 1, '2018-10-31 15:46:34'),
(949, 'Nisikant Registration', 'You have received a sign up request name :Nisikant, Kindly approve.', 1, 16, 1, 1, '2018-10-31 15:46:34'),
(950, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-01 11:33:11'),
(951, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-01 11:33:11'),
(952, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-01 11:39:15'),
(953, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-01 11:39:15'),
(954, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-11-01 11:39:53'),
(955, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-11-01 11:39:53'),
(956, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-01 11:40:55'),
(957, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-01 11:40:55'),
(958, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-11-01 11:41:29'),
(959, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-11-01 11:41:29'),
(960, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-11-12 16:42:03'),
(961, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-11-12 16:42:03'),
(962, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-12 16:48:59'),
(963, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-12 16:48:59'),
(964, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-11-12 16:54:40'),
(965, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-11-12 16:54:40'),
(966, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-12 17:16:16');
INSERT INTO `notification` (`id`, `title`, `description`, `sendtype`, `sendflg`, `sendto`, `status`, `add_date`) VALUES
(967, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-12 17:16:16'),
(968, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-11-12 17:45:11'),
(969, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-11-12 17:45:11'),
(970, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-11-12 18:07:57'),
(971, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-11-12 18:07:57'),
(972, 'Quote Request', 'You have posted a new quote request', 4, 1, 47, 1, '2018-11-12 18:31:41'),
(973, 'Quote Request', 'kisan posted a new quote request.', 4, 20, 1, 1, '2018-11-12 18:31:41'),
(974, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-12 18:34:52'),
(975, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-12 18:34:52'),
(976, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-11-12 18:37:06'),
(977, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-11-12 18:37:06'),
(978, 'Service request accepted', 'Your service request assigned to Namita Das.', 2, 1, 47, 1, '2018-11-12 18:38:18'),
(979, 'Service request assigned', ' Service request assigned to Namita Das.', 2, 18, 1, 1, '2018-11-12 18:38:18'),
(980, 'Service assigned', 'New service assigned', 2, 8, 27, 1, '2018-11-12 18:38:18'),
(981, 'Service Closed', 'Your service closed', 2, 1, 47, 1, '2018-11-12 18:40:06'),
(982, 'Service Closed', ' Service closed', 2, 18, 1, 1, '2018-11-12 18:40:06'),
(983, 'Service Closed', 'Service Closed', 2, 8, 27, 1, '2018-11-12 18:40:06'),
(984, 'New Service Request', 'You have posted a new Service Request', 2, 1, 47, 1, '2018-11-12 18:42:44'),
(985, ' New Service Request', 'kisan posted a new service request.', 2, 18, 1, 1, '2018-11-12 18:42:44'),
(986, 'Breakdown Request', 'You have posted a new Breakdown Request', 3, 1, 47, 1, '2018-11-12 18:44:41'),
(987, 'Breakdown Request', 'kisan posted a new breakdown request.', 3, 18, 1, 1, '2018-11-12 18:44:41'),
(988, 'Registration', 'You have registered successfully.', 1, 1, 88, 1, '2018-11-12 18:53:28'),
(989, 'jxvud Registration', 'You have received a sign up request name :jxvud, Kindly approve.', 1, 16, 1, 1, '2018-11-12 18:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` int(12) NOT NULL,
  `name` varchar(222) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `image` varchar(222) DEFAULT NULL,
  `description` text,
  `links` varchar(222) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer`
--

INSERT INTO `offer` (`id`, `name`, `start_date`, `end_date`, `image`, `description`, `links`, `status`, `add_date`, `added_by`) VALUES
(1, 'New Image', '2018-08-01 00:00:00', '2018-09-30 00:00:00', '1_9gms8hhf6i37c0fohdcvle.jpg', '', 'www.google.com', 2, '2018-08-30 20:44:20', 1),
(2, 'New Test', '2018-08-01 00:00:00', '2018-08-28 00:00:00', NULL, 'Test', 'www.google.com', 2, '2018-08-30 20:45:35', 1),
(3, 'TEST2', '2018-08-29 00:00:00', '2018-09-12 00:00:00', '3_apple.jpg', '', 'https://www.google.com/', 1, '2018-08-30 20:47:46', 1),
(4, 'Free Download', '2018-09-05 00:00:00', '2018-09-05 00:00:00', '4_Chrysanthemum.jpg', 'Super Deals. ... Snapdeal Offers of the Day: UP TO 80% OFF on Electronics, Mobiles, Fashion & more. ... Snapdeal Online Shopping Offers & Discounts Today.', 'google.com', 2, '2018-09-05 14:04:34', 1),
(5, 'Discount in Battery', '2018-09-14 00:00:00', '2018-11-29 00:00:00', '5_orizen-battery-banner-1.jpg', NULL, 'http://www.orizengroup.com/products/batteries/industrial-batteries/', 1, '2018-09-14 16:08:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id_service` int(11) NOT NULL,
  `title` text,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id_service`, `title`, `description`, `status`, `add_date`) VALUES
(1, 'Rental of Forklifts', 'RF', 1, '2018-08-30 20:35:50'),
(2, 'Preventative Maintenance Program', 'PMP', 1, '2018-08-30 20:36:20'),
(3, 'Repairs & Maintenance Services', 'Test service 3', 1, '2018-08-30 21:16:55'),
(4, 'Forklift', 'EF', 1, '2018-09-07 19:24:38'),
(5, 'Family Information', 'Family Information', 1, '2018-10-08 19:46:15'),
(6, 'Breakdown 3', 'Test', 1, '2018-10-08 20:00:14'),
(7, 'Forklift Battery', 'FBC', 1, '2018-10-09 17:01:31'),
(8, 'Testing Service', 'Test', 1, '2018-10-12 18:58:37'),
(9, 'Ilift Testing', 'Ilift Testing', 1, '2018-10-12 20:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id_state` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1->Active,2->In-active',
  `add_date` datetime NOT NULL,
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_breakdown`
--

CREATE TABLE `sub_breakdown` (
  `id_breakdown` int(11) DEFAULT NULL,
  `name` varchar(222) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_breakdown`
--

INSERT INTO `sub_breakdown` (`id_breakdown`, `name`, `type`, `status`) VALUES
(5, 'TEST', 1, 1),
(4, 'Hour Meter', 2, 1),
(6, 'sub1', 1, 1),
(6, 'sub2', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_quote`
--

CREATE TABLE `sub_quote` (
  `id_quote` int(122) DEFAULT NULL,
  `name` varchar(222) DEFAULT NULL,
  `type` smallint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_quote`
--

INSERT INTO `sub_quote` (`id_quote`, `name`, `type`, `status`) VALUES
(7, 'TEST', 1, 1),
(8, 'Sub2', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_service`
--

CREATE TABLE `sub_service` (
  `id_service` int(11) DEFAULT NULL,
  `name` text,
  `type` int(2) DEFAULT '0' COMMENT '1->Radio 2->Text',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_service`
--

INSERT INTO `sub_service` (`id_service`, `name`, `type`, `status`) VALUES
(1, 'TEST', 1, 1),
(1, 'TEST2', 2, 1),
(5, 'Total Member', 2, 1),
(5, 'Male', 2, 1),
(5, 'Female', 2, 1),
(5, 'LPG Connection', 1, 1),
(5, 'Electricity', 1, 1),
(6, 'Sub1', 2, 1),
(6, 'Sub2', 1, 1),
(7, 'Voltage or Number of Cells', 2, 1),
(7, ' Amp hour', 2, 1),
(7, 'Cell type', 2, 1),
(7, 'Water Filling System', 1, 1),
(7, 'Battery Tank', 1, 1),
(8, 'SUB1', 1, 1),
(8, 'Sub2', 1, 1),
(8, 'Sub3', 2, 1),
(4, 'Hour Meter', 2, 1),
(9, 'Funality Test', 1, 1),
(9, 'Overall Test', 1, 1),
(9, 'Comments', 2, 1),
(9, 'UI Flow', 1, 1),
(9, 'test2', 1, 1),
(9, 'test3', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `track_breakdown_request`
--

CREATE TABLE `track_breakdown_request` (
  `id` int(11) NOT NULL,
  `id_breakdown_request` int(11) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `note` text,
  `added_by` int(12) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_breakdown_request`
--

INSERT INTO `track_breakdown_request` (`id`, `id_breakdown_request`, `status`, `note`, `added_by`, `add_date`, `end_date`) VALUES
(1, 1, 1, 'test for new progress', 1, '2018-08-30 21:19:50', NULL),
(2, 1, 1, 'check properly', 2, '2018-08-30 21:25:15', NULL),
(3, 1, 1, 'ok good job', 1, '2018-08-30 21:25:50', NULL),
(4, 1, 1, ' Service Closed', 1, '2018-08-30 21:26:18', NULL),
(5, 2, 1, 'test', 1, '2018-08-30 21:31:30', NULL),
(6, 2, 1, ' Service Closed', 1, '2018-08-30 21:32:36', NULL),
(7, 8, 1, 'test', 1, '2018-09-04 17:39:28', NULL),
(8, 8, 1, 'work in progress', 1, '2018-09-04 17:40:03', NULL),
(9, 8, 1, 'testtestt', 27, '2018-09-04 17:40:20', NULL),
(10, 8, 1, 'service closed', 1, '2018-09-04 17:41:03', NULL),
(11, 5, 1, 'drtdtdtdtgdtdt', 1, '2018-09-04 17:42:19', NULL),
(12, 5, 1, 'work in progress', 1, '2018-09-04 17:42:33', NULL),
(13, 6, 1, 'xvvgv', 1, '2018-09-04 18:22:24', NULL),
(14, 6, 1, ' Service Closed', 1, '2018-09-04 18:23:32', NULL),
(15, 10, 1, 'ghgfhg', 1, '2018-09-04 18:25:31', NULL),
(16, 10, 1, 'closed status', 1, '2018-09-04 18:26:22', NULL),
(17, 12, 1, 'hfghfgh', 2, '2018-09-04 19:57:49', NULL),
(18, 12, 1, 'service closed', 27, '2018-09-04 19:58:13', NULL),
(19, 13, 1, 'ytguygyujgh', 2, '2018-09-04 20:01:30', NULL),
(20, 13, 1, 'service closed', 2, '2018-09-04 20:01:43', NULL),
(21, 14, 1, 'fghfghfh', 2, '2018-09-04 20:06:03', NULL),
(22, 14, 1, 'service closed', 27, '2018-09-04 20:06:23', NULL),
(23, 15, 1, 'hfghfhg', 2, '2018-09-04 20:09:51', NULL),
(24, 16, 1, 'jio bichitra', 2, '2018-09-05 14:34:27', NULL),
(25, 16, 1, '88888', 1, '2018-09-05 14:39:38', NULL),
(26, 17, 1, '4745', 1, '2018-09-05 14:46:53', NULL),
(27, 17, 1, 'close', 1, '2018-09-05 15:28:06', NULL),
(28, 17, 1, 'closed', 1, '2018-09-05 15:29:53', NULL),
(29, 19, 1, 'check it', 1, '2018-09-05 15:34:17', NULL),
(30, 19, 1, 'tezf', 1, '2018-09-05 15:38:02', NULL),
(31, 4, 1, 'wip', 2, '2018-09-05 15:39:34', NULL),
(32, 19, 1, ' Service Closed', 2, '2018-09-05 15:44:01', NULL),
(33, 23, 1, 'revert', 1, '2018-09-11 20:02:29', NULL),
(34, 23, 1, 'vinay will arrive on Thursday around 2pm', 27, '2018-09-11 20:06:57', NULL),
(35, 22, 1, 'tt', 1, '2018-09-14 15:52:42', NULL),
(36, 21, 1, 'Test', 1, '2018-09-15 15:22:06', NULL),
(37, 3, 1, 'test', 1, '2018-09-15 15:22:18', NULL),
(38, 23, 1, ' Service Closed', 1, '2018-09-15 15:22:44', NULL),
(39, 24, 1, 'xugdyidugd', 1, '2018-09-15 15:29:00', NULL),
(40, 25, 1, 'hpvojcououvuo', 1, '2018-09-15 15:33:53', NULL),
(41, 26, 1, 'xjguxic', 1, '2018-09-17 11:38:26', NULL),
(42, 11, 1, 'hdhdh', 1, '2018-09-17 11:45:34', NULL),
(43, 30, 1, 'xgkchk hkc', 1, '2018-09-17 16:59:10', NULL),
(44, 30, 1, 'frhfgreg', 1, '2018-09-17 17:03:22', NULL),
(45, 30, 1, ' Service Closed', 1, '2018-09-17 17:03:49', NULL),
(46, 26, 1, 'service closed', 1, '2018-09-17 17:05:27', NULL),
(47, 32, 1, 'fiicof', 2, '2018-09-17 17:15:58', NULL),
(48, 31, 1, 'cbjjgsjdd', 1, '2018-09-17 17:16:38', NULL),
(49, 32, 1, ' Service Closed', 1, '2018-09-17 17:17:45', NULL),
(50, 31, 1, ' Service Closed', 1, '2018-09-17 17:21:10', NULL),
(51, 33, 1, 'dgudgusfus', 2, '2018-09-17 17:56:06', NULL),
(52, 34, 1, 'Ttygggh', 1, '2018-09-17 17:57:49', NULL),
(53, 34, 1, ' Service Closed', 1, '2018-09-17 17:58:10', NULL),
(54, 33, 1, ' Service Closed', 2, '2018-09-17 17:58:37', NULL),
(55, 35, 1, 'cghhhjj', 2, '2018-09-17 18:52:31', NULL),
(56, 35, 1, ' Service Closed', 27, '2018-09-17 18:53:26', NULL),
(57, 25, 1, ' Service Closed', 27, '2018-09-17 18:54:23', NULL),
(58, 24, 1, ' Service Closed', 1, '2018-09-17 19:12:49', NULL),
(59, 22, 1, 'ok done ', 27, '2018-09-17 19:36:21', NULL),
(60, 37, 1, 'jfigjgigigigigiy', 2, '2018-09-17 19:43:19', NULL),
(61, 38, 1, 'chfugigjgjfug', 1, '2018-09-17 19:44:18', NULL),
(62, 36, 1, 'fufugiykyigigigi', 2, '2018-09-17 19:45:05', NULL),
(63, 36, 1, ' Service Closed', 27, '2018-09-17 19:45:49', NULL),
(64, 37, 1, ' Service Closed', 2, '2018-09-17 19:46:54', NULL),
(65, 38, 1, ' Service Closed', 1, '2018-09-17 19:47:32', NULL),
(66, 11, 1, ' Service Closed', 2, '2018-09-22 15:47:06', NULL),
(67, 44, 1, 'test', 1, '2018-10-12 15:32:00', NULL),
(68, 46, 1, 'tedt', 2, '2018-10-12 18:36:55', NULL),
(69, 46, 1, ' Service Closed', 27, '2018-10-12 18:37:40', NULL),
(70, 47, 1, 'cgfgh', 1, '2018-10-12 18:53:31', NULL),
(71, 47, 1, 'service closed', 27, '2018-10-12 18:54:12', NULL),
(72, 44, 1, 'TEST 2', 27, '2018-10-12 20:06:23', NULL),
(73, 48, 1, 'tets', 1, '2018-10-12 20:16:59', NULL),
(74, 48, 1, ' Service Closed', 27, '2018-10-12 20:18:07', NULL),
(75, 51, 1, 'fghsgfjerg', 2, '2018-10-12 20:19:08', NULL),
(76, 51, 1, ' Service Closed', 27, '2018-10-12 20:19:23', NULL),
(77, 44, 1, ' Service Closed', 2, '2018-10-13 15:26:22', NULL),
(78, 53, 1, 'check', 1, '2018-10-13 21:30:33', NULL),
(79, 53, 1, ' Service Closed', 1, '2018-10-16 17:35:58', NULL),
(80, 56, 1, 'cycy', 2, '2018-10-17 15:12:07', NULL),
(81, 56, 1, ' Service Closed', 27, '2018-10-17 15:12:38', NULL),
(82, 55, 1, 'vugug', 2, '2018-10-17 15:13:34', NULL),
(83, 55, 1, ' Service Closed', 2, '2018-10-17 15:14:15', NULL),
(84, 54, 1, 'gghjj', 1, '2018-10-17 15:15:57', NULL),
(85, 54, 1, ' Service Closed', 1, '2018-10-17 15:16:32', NULL),
(86, 57, 1, 'gugu', 2, '2018-10-17 15:18:59', NULL),
(87, 57, 1, ' Service Closed', 1, '2018-10-17 15:19:29', NULL),
(88, 58, 1, 'cichic', 2, '2018-10-17 15:26:36', NULL),
(89, 58, 1, ' Service Closed', 1, '2018-10-17 15:27:02', NULL),
(90, 52, 1, 'vjvugiggg', 1, '2018-10-17 15:38:58', NULL),
(91, 50, 1, 'vgh', 2, '2018-10-22 13:06:16', NULL),
(92, 52, 1, ' Service Closed', 2, '2018-10-22 13:06:49', NULL),
(93, 50, 1, ' Service Closed', 2, '2018-10-22 14:45:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `track_service_request`
--

CREATE TABLE `track_service_request` (
  `id` int(11) NOT NULL,
  `id_service_request` int(11) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `note` text,
  `added_by` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_service_request`
--

INSERT INTO `track_service_request` (`id`, `id_service_request`, `status`, `note`, `added_by`, `add_date`, `end_date`) VALUES
(1, 1, 1, 'test test', 1, '2018-08-30 20:49:40', NULL),
(2, 1, 1, 'Demo', 27, '2018-08-30 20:50:34', NULL),
(3, 1, 1, 'check', 2, '2018-08-30 20:51:44', NULL),
(4, 1, 1, 'work in progress', 1, '2018-08-30 20:52:02', NULL),
(5, 1, 1, ' Service Closed', 27, '2018-08-30 20:53:14', NULL),
(6, 2, 1, 'test', 1, '2018-08-30 21:30:17', NULL),
(7, 2, 1, 'check', 1, '2018-08-30 21:30:35', NULL),
(8, 2, 1, ' Service Closed', 1, '2018-08-30 21:30:41', NULL),
(9, 3, 1, 'check on pri', 1, '2018-09-01 12:06:40', NULL),
(10, 3, 1, 'checked & working fine', 27, '2018-09-01 12:08:43', NULL),
(11, 3, 1, 'confirmed from customer, it is working fine now', 27, '2018-09-01 12:09:14', NULL),
(12, 4, 1, 'yy', 2, '2018-09-01 16:30:03', NULL),
(13, 5, 1, 'CHECK IT', 1, '2018-09-04 15:10:40', NULL),
(14, 8, 1, 'test', 1, '2018-09-04 17:28:48', NULL),
(15, 8, 1, 'testtest', 27, '2018-09-04 17:29:28', NULL),
(16, 8, 1, ' Service Closed', 27, '2018-09-04 17:30:40', NULL),
(17, 7, 1, 'dfgdgh', 1, '2018-09-04 17:43:23', NULL),
(18, 7, 1, 'service closed', 1, '2018-09-04 17:43:51', NULL),
(19, 9, 1, 'test', 2, '2018-09-04 19:29:58', NULL),
(20, 9, 1, 'fgfgh', 27, '2018-09-04 19:31:20', NULL),
(21, 9, 1, 'service closed', 27, '2018-09-04 19:32:01', NULL),
(22, 10, 1, 'dfhfsdgf', 2, '2018-09-04 19:58:59', NULL),
(23, 10, 1, 'dfgsadgfgsd', 2, '2018-09-04 19:59:26', NULL),
(24, 11, 1, 'gyhfhgfhgf', 2, '2018-09-04 20:07:52', NULL),
(25, 11, 1, 'service closed', 2, '2018-09-04 20:08:06', NULL),
(26, 12, 1, 'vghfhg', 2, '2018-09-04 20:08:48', NULL),
(27, 4, 1, 'what about it check it & found some problem in it & need to', 1, '2018-09-05 13:20:15', NULL),
(28, 4, 1, 'what about it check it & found some problem in it & need to  what about it check it & found some problem in it & need to kjd kljkd kljkld ljkldj kljd ', 1, '2018-09-05 13:21:05', NULL),
(29, 13, 1, 'ts', 1, '2018-09-05 14:11:49', NULL),
(30, 14, 1, 'kkkkk', 1, '2018-09-05 14:13:16', NULL),
(31, 13, 1, 'hh', 1, '2018-09-05 14:13:42', NULL),
(32, 14, 1, '6yyy', 1, '2018-09-05 14:14:09', NULL),
(33, 15, 1, '222', 1, '2018-09-05 14:33:43', NULL),
(34, 16, 1, 'yyy', 1, '2018-09-05 14:37:49', NULL),
(35, 15, 1, '5555555555555555555', 1, '2018-09-05 14:39:08', NULL),
(36, 19, 1, 'test', 2, '2018-09-05 15:41:44', NULL),
(37, 19, 1, 'ii', 1, '2018-09-05 15:45:25', NULL),
(38, 12, 1, 'sggdj', 27, '2018-09-06 16:06:17', NULL),
(39, 16, 1, 'sggd', 27, '2018-09-06 16:06:36', NULL),
(40, 18, 1, 'dg', 2, '2018-09-11 16:55:04', NULL),
(41, 17, 1, 'test', 1, '2018-09-12 15:55:38', NULL),
(42, 21, 1, 'started', 1, '2018-09-12 16:00:50', NULL),
(43, 20, 1, 'gy', 1, '2018-09-12 16:13:58', NULL),
(44, 22, 1, 'xigixg uh xuodood', 2, '2018-09-15 15:31:24', NULL),
(45, 23, 1, 'test', 1, '2018-09-15 15:36:25', NULL),
(46, 23, 1, ' Service Closed', 1, '2018-09-15 15:36:50', NULL),
(47, 24, 1, 'xgnxgjxjx', 1, '2018-09-17 11:42:27', NULL),
(48, 25, 1, 'gusyidyid', 1, '2018-09-17 11:42:58', NULL),
(49, 6, 1, 'star', 1, '2018-09-17 12:08:59', NULL),
(50, 25, 1, ' Service Closed', 1, '2018-09-17 17:24:46', NULL),
(51, 29, 1, 'Fddhgug', 1, '2018-09-17 18:01:45', NULL),
(52, 32, 1, 'hkfulfchchk', 1, '2018-09-17 19:51:02', NULL),
(53, 31, 1, 'ec4vtvt', 2, '2018-09-17 19:51:53', NULL),
(54, 30, 1, 'efgg', 2, '2018-09-17 19:52:36', NULL),
(55, 30, 1, ' Service Closed', 27, '2018-09-17 19:53:02', NULL),
(56, 31, 1, ' Service Closed', 2, '2018-09-17 19:53:37', NULL),
(57, 32, 1, ' Service Closed', 1, '2018-09-17 19:54:24', NULL),
(58, 28, 1, 'hh', 1, '2018-09-18 12:55:03', NULL),
(59, 27, 1, 'tesf', 1, '2018-09-18 13:05:15', NULL),
(60, 33, 1, 'check it', 1, '2018-09-18 13:17:07', NULL),
(61, 35, 1, 'Check & Respond to customer', 1, '2018-09-27 20:30:40', NULL),
(62, 35, 1, ' Service Closed', 27, '2018-09-27 20:33:32', NULL),
(63, 29, 1, 'service closed', 1, '2018-10-03 15:16:35', NULL),
(64, 39, 1, 'test', 1, '2018-10-09 17:06:27', NULL),
(65, 39, 1, ' Service Closed', 2, '2018-10-11 12:07:39', NULL),
(66, 45, 1, 'test test', 1, '2018-10-12 17:54:31', NULL),
(67, 45, 1, 'test123', 2, '2018-10-12 17:56:52', NULL),
(68, 45, 1, 'tedt tesft', 1, '2018-10-12 17:57:14', NULL),
(69, 45, 1, ' Service Closed', 27, '2018-10-12 17:58:23', NULL),
(70, 33, 1, ' Service Closed', 2, '2018-10-12 18:34:17', NULL),
(71, 44, 1, 'hhhhhb', 2, '2018-10-12 18:39:22', NULL),
(72, 44, 1, ' Service Closed', 2, '2018-10-12 18:40:30', NULL),
(73, 47, 1, 'Test', 2, '2018-10-12 19:35:19', NULL),
(74, 47, 1, ' Service Closed', 27, '2018-10-12 19:36:55', NULL),
(75, 49, 1, 'Test', 2, '2018-10-12 19:39:19', NULL),
(76, 49, 1, ' Service Closed', 27, '2018-10-12 19:39:51', NULL),
(77, 48, 1, 'Test', 1, '2018-10-12 19:41:55', NULL),
(78, 48, 1, ' Service Closed', 27, '2018-10-12 19:42:57', NULL),
(79, 51, 1, 'check', 1, '2018-10-13 11:10:48', NULL),
(80, 51, 1, 'done', 50, '2018-10-13 11:33:34', NULL),
(81, 50, 1, 'hh', 2, '2018-10-13 11:46:00', NULL),
(82, 50, 1, ' Service Closed', 2, '2018-10-13 11:46:38', NULL),
(83, 46, 1, 'tedt', 2, '2018-10-16 17:58:35', NULL),
(84, 43, 1, 'hdhd', 2, '2018-10-16 17:59:13', NULL),
(85, 46, 1, ' Service Closed', 2, '2018-10-16 17:59:53', NULL),
(86, 43, 1, ' Service Closed', 2, '2018-10-16 18:01:02', NULL),
(87, 53, 1, 'test', 2, '2018-10-17 15:01:57', NULL),
(88, 53, 1, ' Service Closed', 27, '2018-10-17 15:03:46', NULL),
(89, 52, 1, 'tedt', 2, '2018-10-17 15:04:48', NULL),
(90, 52, 1, ' Service Closed', 2, '2018-10-17 15:06:00', NULL),
(91, 54, 1, 'test', 2, '2018-10-17 15:07:06', NULL),
(92, 54, 1, 'fjfgiti', 27, '2018-10-17 15:07:31', NULL),
(93, 54, 1, ' Service Closed', 27, '2018-10-17 15:09:39', NULL),
(94, 56, 1, 'cohfy8dyd', 1, '2018-10-17 15:27:36', NULL),
(95, 56, 1, 'cbcgi', 2, '2018-10-17 15:28:12', NULL),
(96, 55, 1, 'Fhfyfu', 2, '2018-10-22 14:50:58', NULL),
(97, 55, 1, ' Service Closed', 2, '2018-10-22 14:52:27', NULL),
(98, 42, 1, 'T7tt', 2, '2018-10-22 14:53:03', NULL),
(99, 42, 1, ' Service Closed', 2, '2018-10-22 14:54:07', NULL),
(100, 58, 1, 'test', 1, '2018-10-24 17:56:45', NULL),
(101, 59, 1, 'hbvhjbjhn', 1, '2018-10-29 12:41:32', NULL),
(102, 59, 1, 'closed status', 1, '2018-10-29 12:41:43', NULL),
(103, 69, 1, 'fugug', 1, '2018-11-12 18:38:18', NULL),
(104, 69, 1, ' Service Closed', 27, '2018-11-12 18:40:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(222) NOT NULL,
  `firstname` varchar(222) DEFAULT NULL,
  `lastname` varchar(222) DEFAULT NULL,
  `companyname` varchar(222) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `alternate_phone` varchar(50) DEFAULT NULL,
  `email` varchar(222) NOT NULL,
  `reg_no` varchar(222) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `address` text,
  `is_admin` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '1->Active,2->Inactive',
  `user_type` int(2) DEFAULT NULL COMMENT '1-&gt;Service manager 2-&gt;Sales manager 3-&gt;Service Executive 4-&gt;customer',
  `added_by` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `deviceId` varchar(100) DEFAULT NULL,
  `gcmId` text,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `firstname`, `lastname`, `companyname`, `phone`, `alternate_phone`, `email`, `reg_no`, `location`, `city`, `password`, `address`, `is_admin`, `status`, `user_type`, `added_by`, `add_date`, `deviceId`, `gcmId`, `ip`) VALUES
(1, 'iliftadmin', 'iliftadmin', '', '9861601016', '8280030338', 'info@iliftforklifts.co.za', NULL, NULL, NULL, '111111', 'BBsr', 1, 1, 7, NULL, '2018-07-17 14:06:07', 'f4a4437be57983ee', 'd6Tjqyh4ncc:APA91bGYcVTeCVbXWnrZnngY5GLFds4_sDaiAz2auEHWHE9bfiEWFuKoBebCZfLmvmnUfyDKWOYuHEeKZiKEsnCPoBCYJ5itIWxM-A-BYkdhcoXGexL_ZmkdWkupLM1CF6RP-iBtqbkb', NULL),
(2, 'mahesh SM', NULL, '', '9439328268', '4545455556', 'servicemanager@ilift.com', NULL, NULL, NULL, '111111', 'bbsr', 0, 1, 1, NULL, '2018-07-17 16:45:08', 'f4a4437be57983ee', '', NULL),
(22, 'surplus1', NULL, '', '4741255452', '4568566665', 'salesmanager@ilift.com', NULL, NULL, NULL, '111111', 'bbsr', 0, 1, 2, NULL, '2018-07-17 18:12:05', '949349623e96d4ee', '', NULL),
(23, 'Orimark', NULL, '', '4554578455', '5656655644', 'career@orimarktechnologies.com', NULL, NULL, NULL, '123456', 'Sahid Nagar', 0, 1, 2, NULL, '2018-07-17 18:24:04', NULL, NULL, NULL),
(25, 'test puru', NULL, '', '7878784561', '', 'testpuru@gmail.com', NULL, NULL, NULL, NULL, 'khordha', 0, 2, 3, NULL, '2018-07-17 19:18:19', NULL, NULL, NULL),
(27, 'Namita Das', NULL, '', '3232323232', '', 'namitadas1@gmail.com', NULL, NULL, NULL, '111111', '32323', 0, 1, 3, NULL, '2018-07-17 19:22:05', 'f4a4437be57983ee', '', NULL),
(46, 'New executive', NULL, NULL, '2346578324', '', 'patrakisan.92@gmail.com', NULL, NULL, NULL, '111111', 'BBSR', 0, 1, 3, NULL, '2018-08-30 18:13:11', NULL, NULL, NULL),
(47, 'kisan', NULL, 'kumar group', '4567891234', NULL, 'kisanpatra.923@gmail.com', '', 'jayadev bihar', 'Bhubaneswar', '111111', NULL, 0, 1, 4, NULL, '2018-08-30 20:25:16', 'f4a4437be57983ee', '', NULL),
(48, 'raj', NULL, 'Raj industry', '4567891235', NULL, 'raj@gmail.com', '', 'Jayadeva', 'Bhubaneswar', '111111', NULL, 0, 1, 4, NULL, '2018-08-30 20:26:55', 'f4a4437be57983ee', '', NULL),
(49, 'chitta', NULL, 'orimark', '9437302803', NULL, 'systemadmin@orimark.com', '', 'Sahid Nagar', 'bhubanesqar', '111111', NULL, 0, 1, 4, NULL, '2018-08-31 15:27:09', '9d67c3bc1ef8568', '', NULL),
(50, 'Bichitra', NULL, NULL, '9437302805', NULL, 'bichitra@gmail.com', NULL, NULL, NULL, '111111', NULL, 0, 1, 3, 2, '2018-09-01 16:13:01', '9d67c3bc1ef8568', '', NULL),
(51, 'vivek', NULL, 'univate', '9438505144', NULL, 'vivek.das@univate.in', '', 'Saheed nagar', 'Bhubaneswar', '1234', NULL, 0, 1, 4, NULL, '2018-09-04 14:55:06', '6e5ff888bf6ff565', '', NULL),
(52, 'demo new', NULL, 'test test', '1234567995', NULL, 'kis@gmail.com', '', 'vhhjk', 'fghjjj', '111111', NULL, 0, 1, 4, NULL, '2018-09-04 17:58:18', NULL, NULL, NULL),
(53, 'ghh', NULL, 'cgh', '8569321470', NULL, 'kisan.patra@thoughtspheres.com', '', 'vgvb', 'ggbb', '111111', NULL, 0, 1, 4, NULL, '2018-09-04 17:59:07', NULL, NULL, NULL),
(54, 'l;k;lkk', NULL, 'jljl', '9938989941', NULL, 'nisir@orikkk.com', '', 'Saheednagar', 'Bhubaneswar', 'linku1', NULL, 0, 1, 4, NULL, '2018-09-05 11:55:08', NULL, NULL, NULL),
(55, 'nisikantkar', NULL, 'Univate', '9438181899', NULL, 'nisikant.kar@univate.in', '', 'bbsr', 'Bbsr', '123456', NULL, 0, 1, 4, NULL, '2018-09-05 12:44:26', '547ec9b56adaa751', '', NULL),
(56, 'lalit', NULL, 'otpl', '9556786167', NULL, 'lalitrath24@gmail.com', '', 'bbsr', 'bbsr', '1234', NULL, 0, 1, 4, NULL, '2018-09-05 14:31:44', '1154f36d5239aa9e', '', NULL),
(57, 'chitta', NULL, 'ori', '9437302804', NULL, 'chitta@orimarktechnologies.com', '', 'bbst', 'sahid', '111111', NULL, 0, 1, 4, NULL, '2018-09-05 17:10:09', NULL, NULL, NULL),
(58, 'Demo Executive', NULL, NULL, '7474543757', NULL, 'exe@gmail.com', NULL, NULL, NULL, '111111', NULL, 0, 1, 3, NULL, '2018-09-07 19:29:52', NULL, NULL, NULL),
(59, 'shalini.rath', NULL, 'Swarna', '8763413139', NULL, 'rathshalini@yahoo.in', '', 'India', 'Bhubaneswar', 'intexP2009', NULL, 0, 1, 4, NULL, '2018-09-11 16:39:16', '602c5820236917be', '', NULL),
(60, 'S T', NULL, 'Sudhir', '9876543211', NULL, 'nisikant.kar@koolfeedback.com', '', 'bbsr', 'odisha', '111111', NULL, 0, 1, 4, NULL, '2018-09-11 19:43:58', '9d67c3bc1ef8568', '', NULL),
(61, 'Ramesh', NULL, 'ISIS', '9876598765', NULL, 'rfff@hh.bb', '', 'test', 'tesf', '12', NULL, 0, 1, 4, NULL, '2018-09-11 19:49:03', NULL, NULL, NULL),
(62, 'Vivek Ilift', NULL, 'Orizen', '9439008434', NULL, 'vibek@orizen.com', '', 'puri', 'puri', '123456', NULL, 0, 1, 4, NULL, '2018-09-12 16:07:34', NULL, NULL, NULL),
(63, 'rajesh', NULL, 'Infotech systech', '9040092345', NULL, 'rakesh@gmail.com', '', 'sahid nagar', 'bbsr', '111111', NULL, 0, 1, 4, NULL, '2018-09-15 12:49:42', NULL, NULL, NULL),
(64, 'vvek', NULL, 'Univate', '9938989901', NULL, 'vibek@gmail.com', '', 'Odisha', 'test', '123456', NULL, 0, 1, 4, NULL, '2018-09-15 12:56:51', NULL, NULL, NULL),
(65, 'Vijay', NULL, 'Orizen', '0835084897', NULL, 'vijay@orizengroup.com', '', 'Jet Park', 'Johannesburg', '123456', NULL, 0, 1, 4, NULL, '2018-09-27 20:07:00', '01133c1ad83ae435', '', NULL),
(66, 'inf', NULL, 'Infotech', '9437302809', NULL, 'infi@hh.ddd', '', 'tst', 'ygg', '123', NULL, 0, 1, 4, NULL, '2018-10-04 11:35:19', NULL, NULL, NULL),
(73, 'fgfgfg', NULL, 'Sggg', '1234567444', NULL, 'deepa444@or.com', '', 'ghghh', 'ghghgh', '1111@!', NULL, 0, 1, 4, NULL, '2018-10-30 17:26:40', NULL, NULL, NULL),
(74, 'dffdfd', NULL, 'fgdgdg', '2323233353', NULL, 'deen@orimologies.com', '', 'gdgdg', 'dgdd', '123456', NULL, 0, 1, 4, NULL, '2018-10-30 17:33:04', NULL, NULL, NULL),
(75, 'Deepak N', NULL, 'SEO 2', '1234567891', NULL, 'deepak.np@orimarktechnologies.com', '', 'tesf', 'trdf', '123456', NULL, 0, 1, 4, NULL, '2018-10-30 17:53:23', NULL, NULL, NULL),
(76, 'chittaghghghgh', NULL, 'ghghgh', '1212121121', NULL, 'liccghgha@gmail.com', '', 'cc', 'vg', '123456', NULL, 0, 1, 4, NULL, '2018-10-30 17:56:28', NULL, NULL, NULL),
(79, 'Deepak', NULL, 'orimark', '4567893546', NULL, 'deepak.n@orimarktechnologies.com', '', 'test', 'bbsr', '123456', NULL, 0, 1, 4, NULL, '2018-10-30 18:07:32', NULL, NULL, NULL),
(80, 'jjj', NULL, 'hj', '1212121223', NULL, 'fgfgitta@gmail.com', '', 'vg', 'bhsh', '1234', NULL, 0, 1, 4, NULL, '2018-10-30 18:12:30', NULL, NULL, NULL),
(81, 'dbjdjd', NULL, 'hjdd', '1245899245', NULL, 'kjjlkjlkjklja@gmail.com', '', 'ggs', 'shhs', '1234', NULL, 0, 1, 4, NULL, '2018-10-30 18:14:55', NULL, NULL, NULL),
(82, 'hdgh', NULL, 'tedt', '9898665686', NULL, 'kisandfdfdfpatra.921@gmail.com', '', 'bxhxh', 'ydyd', '111111', NULL, 0, 1, 4, NULL, '2018-10-30 18:24:20', NULL, NULL, NULL),
(83, 'hh', NULL, 'hh', '2222225555', NULL, 'kisanpatra.921@gmail.com', '', 'gf', 'yy', '1111', NULL, 0, 1, 4, NULL, '2018-10-30 18:31:24', NULL, NULL, NULL),
(84, 'ccc', NULL, 'chitta', '6644554455', NULL, 'licchitta@gmail.com', '', 'ggg', 'gys', '1234', NULL, 0, 1, 4, NULL, '2018-10-30 18:33:24', NULL, NULL, NULL),
(85, 'hhj', NULL, 'ghy', '8552655566', NULL, 'kisanpatra.928@gmail.com', '', 'vfvhh', 'bbsr', '111111', NULL, 0, 1, 4, NULL, '2018-10-30 19:14:29', NULL, NULL, NULL),
(86, 'gyeyys', NULL, 'hxh', '9464664649', NULL, 'kisanpatra.923434@gmail.com', '', 'hsgsg', 'hdydyys', '111111', NULL, 0, 1, 4, NULL, '2018-10-30 19:19:27', NULL, NULL, NULL),
(87, 'Nisikant', NULL, 'OT', '9938989900', NULL, 'nisikantkar@gmail.com', '', 'Odi', 'BBSR', '111111', NULL, 0, 1, 4, NULL, '2018-10-31 15:46:34', NULL, NULL, NULL),
(88, 'jxvud', NULL, 'vjdvd', '8984068686', NULL, 'kisanpatraukytityu@gmail.com', '', 'gchf', 'xgxg', '111111', NULL, 0, 1, 4, NULL, '2018-11-12 18:53:28', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_breakdown_request`
--

CREATE TABLE `user_breakdown_request` (
  `id` int(11) NOT NULL,
  `id_breakdown` int(111) DEFAULT NULL,
  `id_user` int(111) DEFAULT NULL,
  `contact_person` varchar(222) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `product_identification` varchar(222) DEFAULT NULL,
  `expectedbreakdowndate` datetime DEFAULT NULL,
  `location` varchar(111) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(222) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '3',
  `add_date` datetime DEFAULT NULL,
  `assign_executive_id` int(11) DEFAULT NULL,
  `statusnote` text,
  `assigndate` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT '0.0',
  `rating_date` datetime DEFAULT NULL,
  `closeby` int(11) DEFAULT NULL,
  `closedate` datetime DEFAULT NULL,
  `invoice` smallint(1) DEFAULT '0',
  `signature` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_breakdown_request`
--

INSERT INTO `user_breakdown_request` (`id`, `id_breakdown`, `id_user`, `contact_person`, `contact_number`, `product_identification`, `expectedbreakdowndate`, `location`, `comments`, `subcategorydata`, `image`, `status`, `add_date`, `assign_executive_id`, `statusnote`, `assigndate`, `user_review`, `rating`, `rating_date`, `closeby`, `closedate`, `invoice`, `signature`) VALUES
(1, 1, 48, 'sanu', '74589631458', 'test as new product', NULL, 'Jayadeva', 'test test test', NULL, NULL, 1, '2018-08-30 20:38:31', 27, 'test for new progress', '2018-08-30 21:19:50', 'test', 2.5, '2018-08-30 21:29:17', NULL, NULL, 0, NULL),
(2, 3, 48, 'test123', '8456797979', 'test', NULL, 'bbsr', 'best', NULL, NULL, 1, '2018-08-30 21:27:53', 27, NULL, '2018-08-30 21:31:30', 'test', 3.5, '2018-08-30 21:33:26', NULL, NULL, 0, NULL),
(3, 2, 49, 'hhh', '666', 'hhj', NULL, 'ctc', 'hh', NULL, NULL, 4, '2018-09-01 16:20:27', 50, NULL, '2018-09-15 15:22:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(4, 3, 51, 'vivek', '7978782185', 'forklift 1', NULL, 'saheednagar', 'text', NULL, NULL, 4, '2018-09-04 14:57:16', 50, NULL, '2018-09-05 15:39:34', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(5, 4, 48, 'sam', '456789052', 'long product', NULL, 'jayadev bihar', 'Test\nTest\nTest\nTest up\nTest\nTest\nTest\nTest', NULL, NULL, 1, '2018-09-04 16:59:38', 27, 'drtdtdtdtgdtdt', '2018-09-04 17:42:19', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(6, 1, 48, 'bikas', '7589669888', 'good', NULL, 'Jayadev Bihar', 'Cfgv\nXgv\nCvvb\nBbb\nBb', NULL, NULL, 1, '2018-09-04 17:13:15', 27, NULL, '2018-09-04 18:22:24', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(7, 2, 48, 'ghh', '566', 'vv', NULL, 'ghh', 'Vvv', NULL, NULL, 3, '2018-09-04 17:15:07', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(8, 2, 48, 'hhj', '66', 'gbj', NULL, 'ghj', 'Ghh\nBhjj', NULL, NULL, 1, '2018-09-04 17:22:16', 27, 'test', '2018-09-04 17:39:28', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(9, 2, 48, 'ydyd', '6868686868683', 'hdhd', NULL, 'hdhd', 'Hxhxh', NULL, NULL, 3, '2018-09-04 18:22:05', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(10, 3, 48, 'rth', '55788868', 'edfd', NULL, 'rdfgh', 'Gzdhdhfuofh', NULL, NULL, 1, '2018-09-04 18:23:55', 27, 'ghgfhg', '2018-09-04 18:25:31', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(11, 3, 48, 'gghh', '556', 'gghh', NULL, 'ygg', 'Vvvbbbbbb', NULL, NULL, 1, '2018-09-04 19:24:47', 27, NULL, '2018-09-17 11:45:34', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(12, 3, 48, 'gghh', '5563', 'vggg', NULL, 'ggh', 'Vbbhhu', NULL, NULL, 1, '2018-09-04 19:57:22', 27, 'hfghfgh', '2018-09-04 19:57:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(13, 2, 48, 'yzhzh', '6868', 'habxhx', NULL, 'hdhdh', 'Hdhxhjxjxhx', NULL, NULL, 1, '2018-09-04 20:01:17', 27, 'ytguygyujgh', '2018-09-04 20:01:30', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(14, 2, 48, 'tghh', '59666', 'ggh', NULL, 'fghh', 'Cvbjk', NULL, NULL, 1, '2018-09-04 20:05:42', 27, 'fghfghfh', '2018-09-04 20:06:02', 'test test test', 3.5, '2018-09-07 16:40:34', NULL, NULL, 0, NULL),
(15, 2, 48, 'hh', '66', 'fggh', NULL, 'fggg', 'Gh', NULL, NULL, 4, '2018-09-04 20:09:36', 27, 'hfghfhg', '2018-09-04 20:09:51', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(16, 3, 56, 'ggg', '9556786167', 'h', NULL, 'ff', 'Abcd', NULL, NULL, 1, '2018-09-05 14:33:18', 50, NULL, '2018-09-05 14:34:27', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(17, 3, 56, 'gzgh', '5442608428', 'fd', NULL, 'cjf', 'Dggf', NULL, NULL, 1, '2018-09-05 14:43:36', 50, '4745', '2018-09-05 14:46:53', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(18, 1, 56, 'fjhh', '2156274250', 'fg', NULL, 'fzg', 'Tifg', NULL, NULL, 3, '2018-09-05 15:31:20', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(19, 3, 56, 'dugg', '5485284458', 'fhh', NULL, 'fdj', 'Ch', NULL, NULL, 1, '2018-09-05 15:32:11', 50, 'check it', '2018-09-05 15:38:01', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(20, 1, 56, 'cc', '558', 'ff', NULL, 'vv', 'Vvv', NULL, NULL, 3, '2018-09-05 15:36:39', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(21, 2, 56, 'vv', '555', 'gf', NULL, 'gg', 'Vv', NULL, NULL, 4, '2018-09-05 15:40:38', 46, NULL, '2018-09-15 15:22:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(22, 4, 59, 'Xavier', '1231415280', 'rechargeable batteries', NULL, 'Bhubaneswar', 'The batteries is slightly damaged', NULL, NULL, 1, '2018-09-11 16:53:07', 50, NULL, '2018-09-14 15:52:42', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(23, 4, 55, 'vijay', '0835084897', 'A24', NULL, 'Jet park', 'Battery not lasting', NULL, NULL, 1, '2018-09-11 19:56:21', 27, NULL, '2018-09-11 20:02:29', 'Good and intime service', 5.0, '2018-09-15 15:23:19', NULL, NULL, 0, NULL),
(24, 2, 48, 'test tsst', '7567644435537', 'test demo', NULL, 'bbsr', 'Zhfzfusyrs\nJfzyfsuts\nUfzy\nDidiydyi\nFihdiy', NULL, NULL, 1, '2018-09-15 15:27:55', 27, NULL, '2018-09-15 15:29:00', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(25, 3, 48, 'gusigdig', '86856737537357', 'kdihfih', NULL, 'djxigxuy', 'Hzgjzugxud', NULL, NULL, 1, '2018-09-15 15:33:04', 27, NULL, '2018-09-15 15:33:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(26, 4, 48, 'hxhududh', '68668686898', 'judud', NULL, 'hxhux', 'Gdgdgdg\nGgxgxgx\nHxhchc\nHdhdh\nHxhd', NULL, NULL, 1, '2018-09-15 16:49:11', 27, NULL, '2018-09-17 11:38:26', 'ttttt', 4.5, '2018-09-17 17:05:54', NULL, NULL, 0, NULL),
(27, 3, 47, 'hdyd', '8686', 'xfu', NULL, 'duf7', 'Xhdufugif\nXhdufugif\nBxh', NULL, NULL, 3, '2018-09-17 12:09:58', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(28, 3, 47, 'ydhdu', '6565665', 'jdjjd', NULL, 'hdjdj', 'Hdhd\nHdhd\nHdhd\nHdhd\nUdud', NULL, NULL, 3, '2018-09-17 12:13:51', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(29, 3, 47, 'ydhdu', '6565665', 'jdjjd', NULL, 'hdjdj', 'Hdhd\nHdhd\nHdhd\nHdhd\nUdud', NULL, NULL, 3, '2018-09-17 12:14:50', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(30, 1, 48, 'hkdjgd', '556668', 'jgdd', NULL, 'kdgdy', 'Zshfsd\nKvdc\n\nHkf', NULL, NULL, 1, '2018-09-17 16:58:31', 27, NULL, '2018-09-17 16:59:10', 'test', 2.5, '2018-09-17 17:04:34', NULL, NULL, 0, NULL),
(31, 2, 48, 'xbfjr', '9895', 'cjfi', NULL, 'bcncu', 'Vxhdurjc\nGdhf', NULL, NULL, 1, '2018-09-17 17:00:12', 27, NULL, '2018-09-17 17:16:32', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(32, 3, 48, 'chf', '865', 'ydgidd', NULL, 'xhch', 'Xhxhdhdhch\nDyd', NULL, NULL, 1, '2018-09-17 17:15:04', 27, NULL, '2018-09-17 17:15:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(33, 3, 48, 'hxhchchf', '98686595', 'fjfjfut', NULL, 'fhhhf', 'Vxgxcgug', NULL, NULL, 1, '2018-09-17 17:55:39', 27, NULL, '2018-09-17 17:56:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(34, 2, 48, 'f', '868686', 'fkhfvjx', NULL, 'jxgj', 'Xxgjxgjd\nGdfjguu\nVchchf\nGxhcf\nBchfhf cute', NULL, NULL, 1, '2018-09-17 17:57:25', 50, NULL, '2018-09-17 17:57:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(35, 2, 48, 'xgxycyf', '8656565', 'bchfhf', NULL, 'dhjgjg', 'Gdyfuv\nHfjgu\nHfugi\nHvugk\nHfugi\nJhij', NULL, NULL, 1, '2018-09-17 18:50:40', 27, NULL, '2018-09-17 18:52:30', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(36, 2, 48, 'xhxhf', '8683', 'xgx', NULL, 'hcjg', 'Hchcjhhi', NULL, NULL, 1, '2018-09-17 19:42:03', 27, NULL, '2018-09-17 19:45:05', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(37, 2, 48, 'ghhj', '5966', 'fghhh', NULL, 'ghhh', 'Cbnn', NULL, NULL, 1, '2018-09-17 19:42:29', 27, NULL, '2018-09-17 19:43:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(38, 2, 48, 'fhi', '8666', 'tyy', NULL, 'rgh', 'Xhjiig', NULL, NULL, 1, '2018-09-17 19:42:49', 27, NULL, '2018-09-17 19:44:18', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(39, 4, 49, 'hhs', '333', 'hh', NULL, 'hh', 'Bbfb', NULL, NULL, 3, '2018-09-19 19:36:39', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(40, 5, 48, 'rfy', '22699', 'er', NULL, 'rtt', 'Fgyy', NULL, NULL, 3, '2018-10-10 16:42:56', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(41, 5, 47, 'titi', '868686868', 't66', NULL, 'rty', 'Test', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '41_resquestbreakdown.jpg', 3, '2018-10-12 15:15:03', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(42, 4, 47, 'hdhys', '166464', 'nenej', NULL, 'jdjd', 'Bdbfhf', '[]', '42_resquestbreakdown.jpg', 3, '2018-10-12 15:18:15', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(43, 5, 47, 'bb', '9895665', 'identify', NULL, 'sit', 'Bdbbdhxhxhh', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 3, '2018-10-12 15:21:23', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(44, 5, 47, 'bahu3', '1234', 'identify1', NULL, 'location 2', 'Tedt5', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '44_resquestbreakdown.jpg', 1, '2018-10-12 15:31:10', 27, NULL, '2018-10-12 15:32:00', NULL, 0.0, NULL, 2, '2018-10-13 15:26:22', 2, '44_breakdownsignature.jpg'),
(45, 5, 47, 'tsys', '5464', '5w6w', NULL, 'fsya', 'Gsys', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 3, '2018-10-12 16:25:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(46, 5, 47, 'te', '2323', 'test123', NULL, 'bbst', 'Test1', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '46_resquestbreakdown.jpg', 1, '2018-10-12 17:27:56', 27, NULL, '2018-10-12 18:36:54', NULL, 0.0, NULL, 27, '2018-10-12 18:37:40', 1, '46_breakdownsignature.jpg'),
(47, 4, 47, 'ghi', '86', 'ghj', NULL, 'gjj', 'Fgj', '[]', NULL, 1, '2018-10-12 17:28:27', 27, 'cgfgh', '2018-10-12 18:53:30', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(48, 6, 47, 'hchchf', '68686', 'chchc', NULL, 'bchch', 'Chchcchkc\nChcu', '[{\"id_breakdown\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tedt\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '48_resquestbreakdown.jpg', 1, '2018-10-12 19:52:39', 27, 'tets', '2018-10-12 20:16:59', 'test a new', 3.5, '2018-10-13 12:21:18', 27, '2018-10-12 20:18:07', 1, '48_breakdownsignature.jpg'),
(49, 2, 47, 'cycuc', '68868', 'vuuvu', NULL, 'cucu', 'H hvj', '[]', '49_resquestbreakdown.jpg', 3, '2018-10-12 19:54:13', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(50, 6, 47, 'gxhchc', '68683', 'gutu', NULL, 'fgu', 'Cugugkh\nXhfiu', '[{\"id_breakdown\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"jguf\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-12 19:55:54', 27, NULL, '2018-10-22 13:06:16', NULL, 0.0, NULL, 2, '2018-10-22 14:45:39', 1, '50_breakdownsignature.jpg'),
(51, 3, 47, 'cgihi', '6866', 'but', NULL, 'fjfu', 'Cjjj', '[]', NULL, 1, '2018-10-12 19:56:24', 27, 'fghsgfjerg', '2018-10-12 20:19:08', NULL, 0.0, NULL, 27, '2018-10-12 20:19:23', 2, '51_breakdownsignature.jpg'),
(52, 5, 49, 'ndjdj', '3313335', 'tshinhhs', NULL, 'hhdhd', 'Bxbhhdhf', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '52_resquestbreakdown.jpg', 1, '2018-10-13 11:17:53', 27, NULL, '2018-10-17 15:38:58', NULL, 0.0, NULL, 2, '2018-10-22 13:06:49', 1, '52_breakdownsignature.jpg'),
(53, 4, 65, 'vijay', '0835084897', 'dwff11', NULL, 'jet park', 'Battery not lasting', '[{\"id_breakdown\":\"4\",\"name\":\"Hour Meter\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"not applicable\"}]', '53_resquestbreakdown.jpg', 1, '2018-10-13 21:29:48', 27, NULL, '2018-10-13 21:30:32', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(54, 4, 47, 'hjxkhchifkxgjxgj', '9868668683565', 'guxugduy', NULL, 'hkchkckhfhk', 'Fhdgdbchf', '[{\"id_breakdown\":\"4\",\"name\":\"Hour Meter\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"jgzfhxut\"}]', '54_resquestbreakdown.jpg', 1, '2018-10-17 14:56:22', 27, NULL, '2018-10-17 15:15:57', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(55, 5, 47, 'igxgucih', '53853868', 'utcctu', NULL, 'vohciu', 'Gucgux', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '55_resquestbreakdown.jpg', 1, '2018-10-17 14:57:34', 27, NULL, '2018-10-17 15:13:34', NULL, 0.0, NULL, 2, '2018-10-17 15:14:15', 2, '55_breakdownsignature.jpg'),
(56, 4, 47, 'shtstj', '64560886568', 'did dyi', NULL, 'xghzgj', 'Zhgzgdg', '[{\"id_breakdown\":\"4\",\"name\":\"Hour Meter\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"gjxgj\"}]', NULL, 1, '2018-10-17 15:00:17', 27, NULL, '2018-10-17 15:12:06', NULL, 0.0, NULL, 27, '2018-10-17 15:12:38', 1, '56_breakdownsignature.jpg'),
(57, 3, 47, 'church', '8383', 'yfy', NULL, 'chcu', 'Vhcy', '[]', NULL, 1, '2018-10-17 15:18:42', 27, NULL, '2018-10-17 15:18:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(58, 3, 47, 'uxgx', '53853', 'gu u', NULL, 'guys', 'Fy,guxgx', '[]', NULL, 1, '2018-10-17 15:25:14', 27, NULL, '2018-10-17 15:26:36', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(59, 5, 65, 'Vijay', '0835084897', '58', NULL, 'jet park', 'Charger showing red light', '[{\"id_breakdown\":\"5\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '59_resquestbreakdown.jpg', 3, '2018-10-24 15:40:36', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(60, 4, 47, 'cgtchjj', '9658569', 'cgghhgg', NULL, 'testg', 'Ggggg', '[{\"id_breakdown\":\"4\",\"name\":\"Hour Meter\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tttt\"}]', '60_resquestbreakdown.jpg', 3, '2018-10-29 19:07:17', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(61, 2, 47, 'hfchchcucuuchuf', '86686556535', 'chchchvuvjv', NULL, 'hchchvuvuug', 'Cychfhf', '[]', '61_resquestbreakdown.jpg', 3, '2018-10-29 19:13:41', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(62, 6, 47, 'gghji', '5666', 'fvghh', NULL, 'fgghhj', 'Cghh', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"yhhuj\"}]', '62_resquestbreakdown.jpg', 3, '2018-10-29 19:35:59', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(63, 6, 47, 'supa', '123456', 'mark', NULL, 'bbsr1', 'Test', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"chhcy\"}]', '63_resquestbreakdown.jpg', 3, '2018-10-30 11:48:21', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(64, 6, 47, 'cv', '69', 'ghj', NULL, 'vb', 'Vb', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ft\"}]', NULL, 3, '2018-10-30 13:46:19', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(65, 6, 47, 'ghh', '563', 'hhjghu', NULL, 'gj', 'Gh', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ghhh\"}]', NULL, 3, '2018-10-30 16:01:07', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(66, 6, 47, 'ftt', '5556', 'ghh', NULL, 'ggg', 'Cvhn', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tty\"}]', NULL, 3, '2018-10-30 16:06:49', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(67, 6, 47, 'df', '55', 'fg', NULL, 'gg', 'Dff', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fg\"}]', NULL, 3, '2018-10-30 16:09:30', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(68, 6, 47, 'hdhd', '65665', 'hrhr', NULL, 'hshs', 'Dhhd no', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ueur\"}]', NULL, 3, '2018-10-30 16:21:10', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(69, 6, 47, 'bulu', '123456789', 'mark', NULL, 'bbsr', 'Check', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\"}]', '69_resquestbreakdown.jpg', 3, '2018-11-01 11:33:11', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(70, 6, 47, 'bxbdh', '1234', 'hdhd', NULL, 'bxbx', 'Bdhdhs', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"thh\"}]', '70_resquestbreakdown.jpg', 3, '2018-11-01 11:39:15', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(71, 1, 47, 'manu', '123456', 'maek', NULL, 'bbsr', 'Test', '[]', NULL, 3, '2018-11-01 11:40:55', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(72, 6, 47, 'jogi', '896532', 'mark it on my phone', NULL, 'bad location', 'Work for you', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"group by\"}]', NULL, 3, '2018-11-12 16:48:59', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(73, 6, 47, 'bbbbbbbbbbb', '8956235689', 'marking', NULL, 'Bhubaneswar', 'Thank you\nThank you\nThank you\nThank you\n\n\n\n\n\n\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you. thank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ttttttttttyyy\\nttttttttttyyy\\nthank\\nthank\\nthank\\nthank\\nthank\\nthank\\nthank\\nthank you\\n\"}]', NULL, 3, '2018-11-12 17:16:16', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(74, 6, 47, 'chcu', '8683', 'gxxh', NULL, 'igit', 'Hxhf\nN\nM\nM I\'m nn\nM\nM\nM\nM\nM mm\nM\nM\nM\nM\n\n\n\n\n\nM\nM\nM\nN\nN\nN\nN\nB n.\nV', '[{\"id_breakdown\":\"6\",\"name\":\"sub1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_breakdown\":\"6\",\"name\":\"sub2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"c\\nn\\nm\\nm\\nm\\nm\\nm\\nm\\nm\\nm\\nm\\njmmmm\\nn\\nm\\nm\\nm\\nm\\nn\\nm\\nm\\nm\\nm\\nm\\nm\\nn\"}]', NULL, 3, '2018-11-12 18:34:52', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(75, 1, 47, 'fxr', '082', 'xtfvyg', NULL, 'vgu', 'It\'s\nHugu\nUgug\nHigu', '[]', '75_resquestbreakdown.jpg', 3, '2018-11-12 18:44:41', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id_user_log` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `events` varchar(222) DEFAULT NULL,
  `log_status` varchar(222) DEFAULT NULL,
  `operation_mode` tinyint(4) NOT NULL COMMENT '1->Through Web,2->Through App',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id_user_log`, `id_user`, `events`, `log_status`, `operation_mode`, `add_date`, `ip`) VALUES
(1, 1, 'Log In', 'Success', 1, '2018-08-30 20:10:57', '182.73.48.34'),
(2, 2, 'Log In', 'Success', 1, '2018-08-30 20:11:42', '182.73.48.34'),
(3, 2, 'Logged out', 'Logout', 2, '2018-08-30 21:08:10', '182.73.48.34'),
(4, 1, 'Service Added', 'Success', 1, '2018-08-30 21:16:55', '182.73.48.34'),
(5, 1, 'Breakdown Added', 'Success', 1, '2018-08-30 21:17:25', '182.73.48.34'),
(6, 1, 'Item Added', 'Success', 1, '2018-08-30 21:18:25', '182.73.48.34'),
(7, 1, 'Log In', 'Success', 1, '2018-08-31 11:18:40', '182.73.48.34'),
(8, 1, 'Log In', 'Success', 1, '2018-08-31 12:46:25', '103.39.243.254'),
(9, 1, 'Logged out', 'Logout', 2, '2018-08-31 12:49:20', '103.39.243.254'),
(10, 1, 'Log In', 'Success', 1, '2018-08-31 12:49:44', '103.39.243.254'),
(11, 1, 'Item Added', 'Success', 1, '2018-08-31 12:52:55', '103.39.243.254'),
(12, 1, 'Breakdown Added', 'Success', 1, '2018-08-31 12:54:59', '103.39.243.254'),
(13, 1, 'Logged out', 'Logout', 2, '2018-08-31 13:25:54', '182.73.48.34'),
(14, 27, 'Log In', 'Success', 1, '2018-08-31 13:26:04', '182.73.48.34'),
(15, 1, 'Log In', 'Success', 1, '2018-08-31 16:00:38', '103.39.243.254'),
(16, 27, 'Logged out', 'Logout', 2, '2018-08-31 16:27:57', '182.73.48.34'),
(17, 1, 'Log In', 'Success', 1, '2018-08-31 16:28:04', '182.73.48.34'),
(18, 1, 'Logged out', 'Logout', 2, '2018-08-31 16:57:54', '182.73.48.34'),
(19, 27, 'Log In', 'Success', 1, '2018-08-31 16:58:06', '182.73.48.34'),
(20, 1, 'Log In', 'Success', 1, '2018-09-01 11:41:08', '103.39.243.8'),
(21, 2, 'Log In', 'Success', 1, '2018-09-01 11:59:48', '103.39.243.8'),
(22, 2, 'Logged out', 'Logout', 2, '2018-09-01 11:59:58', '103.39.243.8'),
(23, 27, 'Log In', 'Success', 1, '2018-09-01 12:03:05', '103.39.243.8'),
(24, 27, 'Logged out', 'Logout', 2, '2018-09-01 12:12:37', '103.39.243.8'),
(25, 2, 'Log In', 'Success', 1, '2018-09-01 12:12:52', '103.39.243.8'),
(26, 1, 'Log In', 'Success', 1, '2018-09-01 16:10:42', '103.213.27.0'),
(27, 1, 'Log In', 'Success', 1, '2018-09-04 10:45:29', '182.73.48.34'),
(28, 1, 'Logged out', 'Logout', 2, '2018-09-04 10:45:38', '182.73.48.34'),
(29, 1, 'Log In', 'Success', 1, '2018-09-04 10:47:10', '182.73.48.34'),
(30, 1, 'Log In', 'Success', 1, '2018-09-04 12:01:51', '182.73.48.34'),
(31, 1, 'Logged out', 'Logout', 2, '2018-09-04 13:08:22', '182.73.48.34'),
(32, 1, 'Log In', 'Success', 1, '2018-09-04 13:31:53', '103.39.243.8'),
(33, 1, 'Logged out', 'Logout', 2, '2018-09-04 13:34:38', '103.39.243.8'),
(34, 1, 'Log In', 'Success', 1, '2018-09-04 14:53:26', '103.39.243.8'),
(35, 1, 'Log In', 'Success', 1, '2018-09-04 16:47:00', '182.73.48.34'),
(36, 1, 'User Updated', 'Success', 1, '2018-09-04 16:51:34', '182.73.48.34'),
(37, 1, 'Logged out', 'Logout', 2, '2018-09-04 17:50:36', '182.73.48.34'),
(38, 27, 'Log In', 'Success', 1, '2018-09-04 17:50:43', '182.73.48.34'),
(39, 27, 'Logged out', 'Logout', 2, '2018-09-04 17:54:56', '182.73.48.34'),
(40, 1, 'Log In', 'Success', 1, '2018-09-04 17:55:02', '182.73.48.34'),
(41, 1, 'Log In', 'Success', 1, '2018-09-04 18:25:17', '182.73.48.34'),
(42, 1, 'Logged out', 'Logout', 2, '2018-09-04 19:18:07', '182.73.48.34'),
(43, 2, 'Log In', 'Success', 1, '2018-09-04 19:18:27', '182.73.48.34'),
(44, 2, 'Logged out', 'Logout', 2, '2018-09-04 19:20:36', '182.73.48.34'),
(45, 22, 'Log In', 'Success', 1, '2018-09-04 19:20:51', '182.73.48.34'),
(46, 22, 'Logged out', 'Logout', 2, '2018-09-04 19:21:09', '182.73.48.34'),
(47, 22, 'Log In', 'Success', 1, '2018-09-04 19:21:26', '182.73.48.34'),
(48, 22, 'Logged out', 'Logout', 2, '2018-09-04 19:22:43', '182.73.48.34'),
(49, 2, 'Log In', 'Success', 1, '2018-09-04 19:23:08', '182.73.48.34'),
(50, 27, 'Log In', 'Success', 1, '2018-09-04 19:29:05', '182.73.48.34'),
(51, 1, 'Log In', 'Success', 1, '2018-09-05 11:53:29', '182.73.48.34'),
(52, 1, 'Breakdown Updated', 'Success', 1, '2018-09-05 11:55:25', '182.73.48.34'),
(53, 1, 'Logged out', 'Logout', 2, '2018-09-05 12:03:56', '182.73.48.34'),
(54, 27, 'Log In', 'Success', 1, '2018-09-05 12:04:37', '182.73.48.34'),
(55, 27, 'Logged out', 'Logout', 2, '2018-09-05 12:06:20', '182.73.48.34'),
(56, 2, 'Log In', 'Success', 1, '2018-09-05 12:06:27', '182.73.48.34'),
(57, 2, 'Logged out', 'Logout', 2, '2018-09-05 12:08:39', '182.73.48.34'),
(58, 22, 'Log In', 'Success', 1, '2018-09-05 12:08:52', '182.73.48.34'),
(59, 22, 'Logged out', 'Logout', 2, '2018-09-05 12:09:42', '182.73.48.34'),
(60, 1, 'Log In', 'Success', 1, '2018-09-05 13:17:46', '103.213.25.37'),
(61, 1, 'Logged out', 'Logout', 2, '2018-09-05 13:33:11', '103.213.25.37'),
(62, 2, 'Log In', 'Success', 1, '2018-09-05 13:33:32', '103.213.25.37'),
(63, 2, 'Logged out', 'Logout', 2, '2018-09-05 13:49:21', '103.213.25.37'),
(64, 1, 'Log In', 'Success', 1, '2018-09-05 13:49:27', '103.213.25.37'),
(65, 1, 'User Updated', 'Success', 1, '2018-09-05 13:49:53', '103.213.25.37'),
(66, 1, 'Log In', 'Success', 1, '2018-09-05 17:53:50', '182.73.48.34'),
(67, 1, 'Log In', 'Success', 1, '2018-09-07 11:59:36', '182.73.48.34'),
(68, 1, 'Log In', 'Success', 1, '2018-09-07 12:10:01', '103.213.25.235'),
(69, 1, 'Log In', 'Success', 1, '2018-09-07 16:23:19', '182.73.48.34'),
(70, 1, 'Log In', 'Success', 1, '2018-09-07 19:04:23', '182.73.48.34'),
(71, 1, 'Logged out', 'Logout', 2, '2018-09-07 19:10:01', '182.73.48.34'),
(72, 1, 'Log In', 'Success', 1, '2018-09-07 19:10:14', '182.73.48.34'),
(73, 1, 'Log In', 'Success', 1, '2018-09-07 19:17:48', '182.73.48.34'),
(74, 1, 'Service Added', 'Success', 1, '2018-09-07 19:24:38', '182.73.48.34'),
(75, 1, 'Breakdown Added', 'Success', 1, '2018-09-07 19:25:59', '182.73.48.34'),
(76, 1, 'User Added', 'Success', 1, '2018-09-07 19:29:52', '182.73.48.34'),
(77, 1, 'Item Added', 'Success', 1, '2018-09-07 19:31:02', '182.73.48.34'),
(78, 1, 'Log In', 'Success', 1, '2018-09-08 11:47:52', '103.56.221.32'),
(79, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:49:42', '103.56.221.32'),
(80, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:50:10', '103.56.221.32'),
(81, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:50:35', '103.56.221.32'),
(82, 1, 'Service Updated', 'Success', 1, '2018-09-08 11:53:26', '103.56.221.32'),
(83, 1, 'Breakdown Updated', 'Success', 1, '2018-09-08 11:53:58', '103.56.221.32'),
(84, 1, 'Log In', 'Success', 1, '2018-09-08 14:40:49', '103.56.221.32'),
(85, 1, 'Log In', 'Success', 1, '2018-09-11 11:38:18', '182.73.48.34'),
(86, 1, 'Breakdown Updated', 'Success', 1, '2018-09-11 12:32:43', '182.73.48.34'),
(87, 1, 'Log In', 'Success', 1, '2018-09-11 15:41:54', '103.213.24.205'),
(88, 1, 'Logged out', 'Logout', 2, '2018-09-11 15:46:03', '103.213.24.205'),
(89, 1, 'Log In', 'Success', 1, '2018-09-11 15:52:07', '103.213.24.205'),
(90, 1, 'User Updated', 'Success', 1, '2018-09-11 15:53:04', '103.213.24.205'),
(91, 1, 'Log In', 'Success', 1, '2018-09-11 16:02:03', '182.73.48.34'),
(92, 1, 'Log In', 'Success', 1, '2018-09-11 16:02:25', '182.73.48.34'),
(93, 1, 'Logged out', 'Logout', 2, '2018-09-11 16:35:24', '103.213.24.205'),
(94, 27, 'Log In', 'Success', 1, '2018-09-11 16:35:38', '103.213.24.205'),
(95, 27, 'Logged out', 'Logout', 2, '2018-09-11 16:35:45', '103.213.24.205'),
(96, 1, 'Log In', 'Success', 1, '2018-09-11 16:35:51', '103.213.24.205'),
(97, 1, 'User Updated', 'Success', 1, '2018-09-11 16:36:11', '103.213.24.205'),
(98, 1, 'Log In', 'Success', 1, '2018-09-11 20:09:40', '103.56.222.187'),
(99, 1, 'User Updated', 'Success', 1, '2018-09-11 20:11:49', '103.56.222.187'),
(100, 1, 'Log In', 'Success', 1, '2018-09-14 11:24:18', '182.73.48.34'),
(101, 1, 'Log In', 'Success', 1, '2018-09-14 15:58:12', '103.213.24.131'),
(102, 1, 'User Updated', 'Success', 1, '2018-09-14 15:59:45', '103.213.24.131'),
(103, 1, 'Logged out', 'Logout', 2, '2018-09-14 15:59:55', '103.213.24.131'),
(104, 1, 'Log In', 'Success', 1, '2018-09-14 16:06:48', '103.213.24.131'),
(105, 1, 'Log In', 'Success', 1, '2018-09-15 15:25:44', '103.213.27.182'),
(106, 1, 'Log In', 'Success', 1, '2018-09-15 15:26:13', '45.251.37.24'),
(107, 1, 'Logged out', 'Logout', 2, '2018-09-15 15:31:13', '45.251.37.24'),
(108, 1, 'Log In', 'Success', 1, '2018-09-15 15:46:40', '45.251.37.24'),
(109, 1, 'Logged out', 'Logout', 2, '2018-09-15 16:00:33', '103.213.27.182'),
(110, 1, 'Log In', 'Success', 1, '2018-09-15 16:16:32', '45.251.37.24'),
(111, 1, 'Logged out', 'Logout', 2, '2018-09-15 16:17:18', '45.251.37.24'),
(112, 27, 'Log In', 'Success', 1, '2018-09-15 16:18:54', '45.251.37.24'),
(113, 27, 'Logged out', 'Logout', 2, '2018-09-15 16:19:42', '45.251.37.24'),
(114, 1, 'Log In', 'Success', 1, '2018-09-17 11:28:29', '45.251.37.186'),
(115, 1, 'Log In', 'Success', 1, '2018-09-18 11:44:29', '182.73.48.34'),
(116, 1, 'Log In', 'Success', 1, '2018-09-18 12:55:33', '103.213.25.20'),
(117, 1, 'Log In', 'Success', 1, '2018-09-19 12:29:49', '182.73.48.34'),
(118, 1, 'Log In', 'Success', 1, '2018-09-19 19:05:39', '103.56.221.76'),
(119, 1, 'Log In', 'Success', 1, '2018-09-20 17:42:27', '182.73.48.34'),
(120, 1, 'Log In', 'Success', 1, '2018-09-21 12:05:05', '182.73.48.34'),
(121, 1, 'Log In', 'Success', 1, '2018-09-21 15:24:39', '182.73.48.34'),
(122, 1, 'Log In', 'Success', 1, '2018-09-27 19:51:12', '103.56.220.104'),
(123, 1, 'Item Added', 'Success', 1, '2018-09-27 20:10:32', '103.56.220.104'),
(124, 1, 'Log In', 'Success', 1, '2018-10-01 19:01:13', '182.73.48.34'),
(125, 1, 'Log In', 'Success', 1, '2018-10-01 19:21:38', '182.73.48.34'),
(126, 1, 'Log In', 'Success', 1, '2018-10-03 15:16:09', '182.73.48.34'),
(127, 1, 'Logged out', 'Logout', 2, '2018-10-03 15:17:22', '182.73.48.34'),
(128, 1, 'Log In', 'Success', 1, '2018-10-04 19:10:47', '103.56.223.4'),
(129, 1, 'Log In', 'Success', 1, '2018-10-08 11:43:01', '182.73.48.34'),
(130, 1, 'Log In', 'Success', 1, '2018-10-08 17:26:42', '182.73.48.34'),
(131, 1, 'Logged out', 'Logout', 2, '2018-10-08 19:11:04', '182.73.48.34'),
(132, 1, 'Log In', 'Success', 1, '2018-10-08 19:41:54', '182.73.48.34'),
(133, 1, 'Log In', 'Success', 1, '2018-10-08 19:45:00', '182.73.48.34'),
(134, 1, 'Service Updated', 'Success', 1, '2018-10-08 19:57:09', '182.73.48.34'),
(135, 1, 'Service Updated', 'Success', 1, '2018-10-08 19:58:28', '182.73.48.34'),
(136, 1, 'Service Added', 'Success', 1, '2018-10-08 20:00:14', '182.73.48.34'),
(137, 1, 'Logged out', 'Logout', 2, '2018-10-08 20:04:34', '182.73.48.34'),
(138, 1, 'Log In', 'Success', 1, '2018-10-09 16:58:52', '103.72.61.87'),
(139, 1, 'Service Added', 'Success', 1, '2018-10-09 17:01:31', '103.72.61.87'),
(140, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:01:50', '103.72.61.87'),
(141, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:01', '103.72.61.87'),
(142, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:19', '103.72.61.87'),
(143, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:03:35', '103.72.61.87'),
(144, 1, 'Service Updated', 'Success', 1, '2018-10-09 17:04:02', '103.72.61.87'),
(145, 1, 'Log In', 'Success', 1, '2018-10-10 11:22:43', '182.73.48.34'),
(146, 1, 'Log In', 'Success', 1, '2018-10-10 11:57:33', '182.73.48.34'),
(147, 1, 'Logged out', 'Logout', 2, '2018-10-10 12:00:31', '182.73.48.34'),
(148, 1, 'Log In', 'Success', 1, '2018-10-12 13:06:56', '45.251.37.232'),
(149, 1, 'Breakdown Updated', 'Success', 1, '2018-10-12 13:07:24', '45.251.37.232'),
(150, 1, 'Item Updated', 'Success', 1, '2018-10-12 13:08:10', '45.251.37.232'),
(151, 1, 'Log In', 'Success', 1, '2018-10-12 14:42:46', '45.251.37.232'),
(152, 1, 'Logged out', 'Logout', 2, '2018-10-12 14:43:58', '45.251.37.232'),
(153, 1, 'Log In', 'Success', 1, '2018-10-12 14:46:41', '45.251.37.232'),
(154, 1, 'Log In', 'Success', 1, '2018-10-12 16:49:47', '45.251.37.232'),
(155, 1, 'Log In', 'Success', 1, '2018-10-12 18:02:31', '45.251.37.232'),
(156, 1, 'Log In', 'Success', 1, '2018-10-12 18:15:14', '45.251.37.232'),
(157, 27, 'Log In', 'Success', 1, '2018-10-12 18:17:31', '45.251.37.232'),
(158, 2, 'Log In', 'Success', 1, '2018-10-12 18:25:30', '182.73.48.34'),
(159, 1, 'Logged out', 'Logout', 2, '2018-10-12 18:29:31', '182.73.48.34'),
(160, 27, 'Log In', 'Success', 1, '2018-10-12 18:29:54', '182.73.48.34'),
(161, 1, 'Log In', 'Success', 1, '2018-10-12 18:31:12', '45.251.37.232'),
(162, 1, 'Service Added', 'Success', 1, '2018-10-12 18:58:37', '45.251.37.232'),
(163, 1, 'Service Updated', 'Success', 1, '2018-10-12 18:58:58', '182.73.48.34'),
(164, 1, 'Breakdown Added', 'Success', 1, '2018-10-12 18:59:31', '182.73.48.34'),
(165, 1, 'Breakdown Updated', 'Success', 1, '2018-10-12 18:59:51', '182.73.48.34'),
(166, 1, 'Item Added', 'Success', 1, '2018-10-12 19:01:04', '182.73.48.34'),
(167, 27, 'Logged out', 'Logout', 2, '2018-10-12 19:34:08', '45.251.37.232'),
(168, 1, 'Log In', 'Success', 1, '2018-10-12 19:34:22', '45.251.37.232'),
(169, 1, 'Logged out', 'Logout', 2, '2018-10-12 19:59:51', '45.251.37.232'),
(170, 27, 'Log In', 'Success', 1, '2018-10-12 20:00:11', '45.251.37.232'),
(171, 27, 'Logged out', 'Logout', 2, '2018-10-12 20:06:37', '45.251.37.232'),
(172, 1, 'Log In', 'Success', 1, '2018-10-12 20:06:42', '45.251.37.232'),
(173, 1, 'Service Added', 'Success', 1, '2018-10-12 20:11:33', '45.251.37.232'),
(174, 1, 'Log In', 'Success', 1, '2018-10-13 11:03:41', '103.56.223.208'),
(175, 1, 'Service Updated', 'Success', 1, '2018-10-13 11:23:54', '103.56.223.208'),
(176, 1, 'Service Updated', 'Success', 1, '2018-10-13 11:25:01', '103.56.223.208'),
(177, 1, 'Service Updated', 'Success', 1, '2018-10-13 11:31:27', '103.56.223.208'),
(178, 1, 'Breakdown Updated', 'Success', 1, '2018-10-13 11:32:00', '103.56.223.208'),
(179, 1, 'Log In', 'Success', 1, '2018-10-13 12:11:38', '45.251.37.232'),
(180, 1, 'Log In', 'Success', 1, '2018-10-13 12:25:50', '45.251.37.232'),
(181, 1, 'Logged out', 'Logout', 2, '2018-10-13 12:28:55', '45.251.37.232'),
(182, 2, 'Log In', 'Success', 1, '2018-10-13 12:29:03', '45.251.37.232'),
(183, 2, 'Logged out', 'Logout', 2, '2018-10-13 12:29:38', '45.251.37.232'),
(184, 27, 'Log In', 'Success', 1, '2018-10-13 12:29:45', '45.251.37.232'),
(185, 1, 'Log In', 'Success', 1, '2018-10-13 13:27:44', '45.251.37.232'),
(186, 1, 'Log In', 'Success', 1, '2018-10-13 15:20:09', '103.56.223.208'),
(187, 1, 'Item Updated', 'Success', 1, '2018-10-13 15:54:20', '182.73.48.34'),
(188, 1, 'Log In', 'Success', 1, '2018-10-13 15:55:15', '182.73.48.34'),
(189, 1, 'Service Updated', 'Success', 1, '2018-10-13 16:02:39', '182.73.48.34'),
(190, 1, 'Breakdown Updated', 'Success', 1, '2018-10-13 16:05:56', '182.73.48.34'),
(191, 1, 'Service Updated', 'Success', 1, '2018-10-13 16:23:57', '182.73.48.34'),
(192, 27, 'Logged out', 'Logout', 2, '2018-10-13 16:24:41', '182.73.48.34'),
(193, 1, 'Log In', 'Success', 1, '2018-10-13 16:24:46', '182.73.48.34'),
(194, 1, 'Service Updated', 'Success', 1, '2018-10-13 16:25:22', '182.73.48.34'),
(195, 1, 'Service Updated', 'Success', 1, '2018-10-13 16:26:02', '182.73.48.34'),
(196, 1, 'Service Updated', 'Success', 1, '2018-10-13 16:26:28', '182.73.48.34'),
(197, 1, 'Breakdown Updated', 'Success', 1, '2018-10-13 16:32:07', '182.73.48.34'),
(198, 1, 'Breakdown Updated', 'Success', 1, '2018-10-13 16:32:20', '182.73.48.34'),
(199, 1, 'Breakdown Updated', 'Success', 1, '2018-10-13 16:33:12', '182.73.48.34'),
(200, 1, 'User Updated', 'Success', 1, '2018-10-13 16:41:01', '182.73.48.34'),
(201, 1, 'Log In', 'Success', 1, '2018-10-13 18:32:04', '182.73.48.34'),
(202, 1, 'Logged out', 'Logout', 2, '2018-10-13 18:35:57', '182.73.48.34'),
(203, 1, 'Log In', 'Success', 1, '2018-10-15 11:15:02', '182.73.48.34'),
(204, 1, 'Logged out', 'Logout', 2, '2018-10-15 12:53:47', '182.73.48.34'),
(205, 1, 'Log In', 'Success', 1, '2018-10-15 19:25:33', '182.73.48.34'),
(206, 1, 'Log In', 'Success', 1, '2018-10-16 11:27:14', '182.73.48.34'),
(207, 1, 'Log In', 'Success', 1, '2018-10-16 16:04:19', '182.73.48.34'),
(208, 1, 'Log In', 'Success', 1, '2018-10-16 18:01:44', '182.73.48.34'),
(209, 1, 'Log In', 'Success', 1, '2018-10-17 10:49:17', '182.73.48.34'),
(210, 1, 'Item Updated', 'Success', 1, '2018-10-17 11:32:01', '182.73.48.34'),
(211, 1, 'Log In', 'Success', 1, '2018-10-17 14:46:44', '182.73.48.34'),
(212, 1, 'Log In', 'Success', 1, '2018-10-22 17:18:32', '103.213.26.237'),
(213, 1, 'Log In', 'Success', 1, '2018-10-24 17:28:50', '103.213.24.93'),
(214, 1, 'Service Updated', 'Success', 1, '2018-10-24 17:48:50', '103.213.24.93'),
(215, 1, 'Service Updated', 'Success', 1, '2018-10-24 17:49:01', '103.213.24.93'),
(216, 1, 'Log In', 'Success', 1, '2018-10-29 12:41:17', '182.73.48.34'),
(217, 1, 'Log In', 'Success', 1, '2018-10-29 19:33:52', '182.73.48.34'),
(218, 1, 'Log In', 'Success', 1, '2018-10-30 16:35:54', '182.73.48.34'),
(219, 1, 'User Updated', 'Success', 1, '2018-10-30 16:36:20', '182.73.48.34'),
(220, 1, 'Log In', 'Success', 1, '2018-10-30 16:45:21', '103.56.223.35'),
(221, 1, 'User Updated', 'Success', 1, '2018-10-30 16:58:17', '182.73.48.34'),
(222, 1, 'User Updated', 'Success', 1, '2018-10-30 17:05:13', '182.73.48.34'),
(223, 1, 'User Updated', 'Success', 1, '2018-10-30 17:08:17', '182.73.48.34'),
(224, 1, 'Log In', 'Success', 1, '2018-10-30 17:18:18', '182.73.48.34'),
(225, 1, 'User Updated', 'Success', 1, '2018-10-30 17:29:45', '103.56.223.35'),
(226, 1, 'User Updated', 'Success', 1, '2018-10-30 17:52:04', '103.56.223.35'),
(227, 1, 'User Updated', 'Success', 1, '2018-10-30 18:02:12', '182.73.48.34'),
(228, 1, 'User Updated', 'Success', 1, '2018-10-30 18:07:28', '182.73.48.34'),
(229, 1, 'User Updated', 'Success', 1, '2018-10-30 18:10:38', '103.56.223.35'),
(230, 1, 'User Updated', 'Success', 1, '2018-10-30 18:13:49', '103.56.223.35'),
(231, 1, 'Logged out', 'Logout', 2, '2018-10-30 18:13:53', '103.56.223.35'),
(232, 1, 'Log In', 'Success', 1, '2018-10-30 18:13:59', '103.56.223.35'),
(233, 1, 'Logged out', 'Logout', 2, '2018-10-30 18:15:57', '103.56.223.35'),
(234, 1, 'Log In', 'Success', 1, '2018-10-30 18:17:45', '103.56.223.35'),
(235, 1, 'User Updated', 'Success', 1, '2018-10-30 18:18:34', '103.56.223.35'),
(236, 1, 'User Updated', 'Success', 1, '2018-10-30 18:30:18', '103.56.223.35'),
(237, 1, 'User Updated', 'Success', 1, '2018-10-30 19:12:10', '182.73.48.34'),
(238, 1, 'User Updated', 'Success', 1, '2018-10-30 19:12:31', '182.73.48.34'),
(239, 1, 'User Updated', 'Success', 1, '2018-10-30 19:19:09', '182.73.48.34'),
(240, 1, 'Log In', 'Success', 1, '2018-10-31 15:44:38', '103.56.221.191'),
(241, 1, 'User Updated', 'Success', 1, '2018-10-31 15:45:29', '103.56.221.191'),
(242, 1, 'Log In', 'Success', 1, '2018-11-01 11:31:39', '182.73.48.34'),
(243, 1, 'Log In', 'Success', 1, '2018-11-01 13:34:52', '182.73.48.34'),
(244, 1, 'Log In', 'Success', 1, '2018-11-12 18:50:27', '182.73.48.34'),
(245, 1, 'User Updated', 'Success', 1, '2018-11-12 18:53:15', '182.73.48.34'),
(246, 1, 'User Updated', 'Success', 1, '2018-11-12 18:54:28', '182.73.48.34'),
(247, 1, 'Log In', 'Success', 1, '2018-11-13 13:01:01', '182.73.48.34'),
(248, 1, 'Log In', 'Success', 1, '2018-11-13 15:01:57', '182.73.48.34'),
(249, 1, 'Log In', 'Success', 1, '2018-11-13 16:30:02', '182.73.48.34'),
(250, 1, 'Logged out', 'Logout', 2, '2018-11-13 16:30:53', '182.73.48.34'),
(251, 1, 'Log In', 'Success', 1, '2018-11-26 11:09:59', '182.73.48.34'),
(252, 1, 'Log In', 'Success', 1, '2018-12-04 11:02:03', '182.73.48.34'),
(253, 1, 'Log In', 'Success', 1, '2018-12-04 16:06:26', '182.73.48.34'),
(254, 1, 'Log In', 'Success', 1, '2018-12-05 10:51:05', '182.73.48.34'),
(255, 1, 'Log In', 'Success', 1, '2018-12-05 18:13:25', '182.73.48.34'),
(256, 1, 'Log In', 'Success', 1, '2018-12-06 10:38:14', '182.73.48.34'),
(257, 1, 'Log In', 'Success', 1, '2018-12-07 11:15:21', '182.73.48.34'),
(258, 1, 'Log In', 'Success', 1, '2019-01-02 11:59:40', '182.73.48.34'),
(259, 1, 'Log In', 'Success', 1, '2019-01-16 12:43:59', '182.73.48.34'),
(260, 27, 'Log In', 'Success', 1, '2019-01-16 12:49:11', '182.73.48.34'),
(261, 27, 'Logged out', 'Logout', 2, '2019-01-16 12:49:18', '182.73.48.34'),
(262, 1, 'Log In', 'Success', 1, '2019-01-28 12:13:21', '182.73.48.34'),
(263, 1, 'Log In', 'Success', 1, '2019-02-11 12:32:34', '103.194.235.197'),
(264, 1, 'Log In', 'Success', 1, '2019-03-19 16:38:47', '103.55.103.35'),
(265, 1, 'Logged out', 'Logout', 2, '2019-03-19 16:38:52', '103.55.103.35'),
(266, 1, 'Log In', 'Success', 1, '2019-03-19 16:40:23', '103.55.103.35'),
(267, 1, 'Logged out', 'Logout', 2, '2019-03-19 16:40:58', '103.55.103.35');

-- --------------------------------------------------------

--
-- Table structure for table `user_quote_request`
--

CREATE TABLE `user_quote_request` (
  `id` int(111) NOT NULL,
  `id_item` int(111) DEFAULT NULL,
  `id_user` int(122) DEFAULT NULL,
  `quantity` varchar(111) DEFAULT NULL,
  `expecteditemdate` date DEFAULT NULL,
  `location` varchar(222) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(222) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `add_date` datetime(6) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `discount` float(10,2) DEFAULT NULL,
  `admin_note` text,
  `approve_by` int(11) DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT NULL,
  `rating_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_quote_request`
--

INSERT INTO `user_quote_request` (`id`, `id_item`, `id_user`, `quantity`, `expecteditemdate`, `location`, `comments`, `subcategorydata`, `image`, `status`, `add_date`, `price`, `discount`, `admin_note`, `approve_by`, `approval_date`, `user_review`, `rating`, `rating_date`) VALUES
(1, 1, 48, '54', NULL, 'jayadev bihar', 'test description', NULL, NULL, 1, '2018-08-30 20:34:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 4, 49, '25', NULL, 'bbsr', 'need frik', NULL, NULL, 1, '2018-09-01 11:45:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 49, '6', NULL, 'ctc', 'tesg hhd hhd hsh hdhbdjjdb jjsjjdb. ujs778s8snndnd njjdjnd jsjjnd jjjdjjd jjdnbd jjdnnd hd nhh hjdbhd', NULL, NULL, 1, '2018-09-01 11:48:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 49, '600', NULL, 'ct', 'trst', NULL, NULL, 1, '2018-09-01 16:19:57.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 4, 51, '8', NULL, 'saheed nagar', 'testing perpose', NULL, NULL, 1, '2018-09-04 14:56:21.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 2, 48, '45', NULL, 'Jayadev Bihar', 'Test\nTest\nTest\nTest\nTest\nTest', NULL, NULL, 1, '2018-09-04 16:54:55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 2, 48, '88', NULL, 'bbsr', 'Bbsr', NULL, NULL, 1, '2018-09-04 17:14:31.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 2, 48, '566', NULL, 'ff', 'Fggh', NULL, NULL, 1, '2018-09-04 19:26:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 3, 56, '1', NULL, 'dd', 'Dd', NULL, NULL, 1, '2018-09-05 14:42:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 3, 48, '5', NULL, 'cff', 'Cfggg', NULL, NULL, 1, '2018-09-07 12:01:26.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 48, '88', NULL, 'cc', 'Fv\nBh', NULL, NULL, 1, '2018-09-07 12:02:48.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 48, '88', NULL, 'fvv', 'Gvvb', NULL, NULL, 1, '2018-09-07 12:06:39.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 4, 55, '2', NULL, 'jet park', '2.5 ton diesel std mast', NULL, NULL, 1, '2018-09-11 19:53:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 4, 55, '2', NULL, 'tesy', 'Ffvgh', NULL, NULL, 1, '2018-09-12 16:05:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 6, 59, '25', NULL, 'bbsr', 'iLift', NULL, NULL, 1, '2018-09-14 15:53:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 4, 55, '2', NULL, 'Puri', 'Testing', NULL, NULL, 1, '2018-09-14 16:06:13.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 4, 49, '2', NULL, 'puri', 'Test', NULL, NULL, 1, '2018-09-15 12:51:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 4, 49, '1', NULL, 'puri', 'Test', NULL, NULL, 1, '2018-09-15 12:52:15.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 55, '2', NULL, 'Odisha', 'Twst', NULL, NULL, 1, '2018-09-15 12:54:55.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 48, '8656555', NULL, 'dhdhfyg', 'Dgdhfugu', NULL, NULL, 1, '2018-09-17 17:19:31.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 3, 48, '09058', NULL, 'gctc', 'Dyvuvu\nHcuvu\nHcv\nHvyv\nHvjn', NULL, NULL, 1, '2018-09-17 18:49:39.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 4, 55, '22', NULL, 'test', 'Durban', NULL, NULL, 1, '2018-09-19 14:36:10.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 4, 49, '100', NULL, 'Bhubaneswar', 'Required', NULL, NULL, 1, '2018-09-19 19:35:30.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 49, '998', NULL, 'hdh', 'Djjdj', NULL, NULL, 1, '2018-09-19 19:39:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 4, 48, '25', NULL, '', 'Vrgegrv', NULL, NULL, 1, '2018-10-03 19:22:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 4, 48, '54', NULL, '', 'F ttv', NULL, NULL, 1, '2018-10-03 19:22:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 7, 47, '888', NULL, '', 'Xfxc', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '27_resquestquote.jpg', 1, '2018-10-12 15:27:03.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 7, 47, '58', NULL, '', 'Test1', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-12 17:21:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 7, 47, '66', NULL, '', 'Test2', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '29_resquestquote.jpg', 1, '2018-10-12 17:24:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 6, 47, '4', NULL, '', 'Tedt3', '[]', '30_resquestquote.jpg', 1, '2018-10-12 17:25:06.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 6, 47, '1223', NULL, '', 'Tedt4', '[]', NULL, 1, '2018-10-12 17:25:57.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 8, 47, '86865', NULL, '', 'Hcjgu', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_quote\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ugugivugi\"}]', '32_resquestquote.jpg', 1, '2018-10-12 19:57:18.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 3, 47, '363836', NULL, '', 'Vhcjg', '[]', '33_resquestquote.jpg', 1, '2018-10-12 19:58:14.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 8, 47, '6565', NULL, '', 'A Hchfu\nChvu', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_quote\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"fydufu\"}]', NULL, 1, '2018-10-12 20:01:32.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 3, 47, '53835', NULL, '', 'Chcy', '[]', NULL, 1, '2018-10-12 20:01:54.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 7, 49, '6', NULL, '', 'Hhh', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '36_resquestquote.jpg', 1, '2018-10-13 11:22:36.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 7, 65, '1', NULL, '', 'For 500 Ah battery', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '37_resquestquote.jpg', 1, '2018-10-13 21:26:24.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 7, 47, '25', NULL, '', 'Dhdhfgk', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-15 16:38:34.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 7, 65, '1', NULL, '', 'Charger number 57 not charging', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '39_resquestquote.jpg', 1, '2018-10-24 15:39:31.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 6, 47, '222222', NULL, '', 'Test', '[]', '40_resquestquote.jpg', 1, '2018-10-29 19:06:30.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 7, 47, '0506098', NULL, '', ',ffxggx', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '41_resquestquote.jpg', 1, '2018-10-29 19:12:35.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 8, 47, '2555', NULL, '', 'Ffffff', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '42_resquestquote.jpg', 1, '2018-10-30 12:24:26.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 8, 47, '2323', NULL, '', 'Cgtcv', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-30 12:31:16.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 8, 47, '232323', NULL, '', 'C to huh', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-30 12:41:57.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 8, 47, '543', NULL, '', 'Gsh', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-30 12:43:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 8, 47, '089', NULL, '', 'Vbb', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-30 12:53:19.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 7, 47, '2323', NULL, '', 'Test', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-30 13:11:27.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 3, 47, '856', NULL, '', 'Gjjj', '[]', NULL, 1, '2018-10-30 13:13:29.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 8, 47, '1545', NULL, '', 'Vxv', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-30 13:38:35.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 7, 47, '25', NULL, '', 'Dhdhfgk', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-30 15:44:04.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 8, 47, '555', NULL, '', 'Sdc', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-30 15:58:40.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 3, 47, '56', NULL, '', 'Fv', '[]', NULL, 1, '2018-10-30 16:22:35.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 8, 47, '50', NULL, '', 'Check\nCheck\nThanks\n\nThank yo so much for your\nThank you\nThank you', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-11-12 16:41:59.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 8, 47, '588', NULL, '', 'Thank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank you\nThank\nThank\nThan\n\n\n\n\n\nThank\nThank\nThank\nThank\nThank\n\n\n\nThank\nThank\nThank\nThank\nThank you\nFfggv\n\nThank you\n\n\n\n\n\n\n\n\n\n\n.fgjkkuj\nFbljhhjkhhjkjj\nThank you', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-11-12 17:45:08.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 7, 47, '88', NULL, '', 'Cc\nG\nH\nH\nH\nH\nG\nH\nH\nN\nH\nH\nB b\n\n\n\n\n\n\n\n\n\nH\nB\nN\nN\nN\nB\nN\nB\nN\nN\nN\n.\n.\nN\n.\nN\nB\nB be\nB\nN\nN\nN.n\nN.n.m.b.m\nM\nN\nN\nB\nB\nB\nH', '[{\"id_quote\":\"7\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-11-12 18:07:54.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 8, 47, '56', NULL, '', 'B\nH\nH\nH\nH\nH\nH\nJn\nNn\nJ\nN\nN\nN\nN\nM\nJ\nM\nM\nM\nM\nM\n.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nN\nM\nM\nM\nM\nM\nM\nM\nM\nM', '[{\"id_quote\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-11-12 18:31:37.000000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_service_request`
--

CREATE TABLE `user_service_request` (
  `id` int(11) NOT NULL,
  `id_service` int(111) DEFAULT NULL,
  `id_user` int(111) DEFAULT NULL,
  `contact_person` varchar(222) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `product_identification` varchar(222) DEFAULT NULL,
  `expectedservicedate` datetime DEFAULT NULL,
  `location` varchar(111) DEFAULT NULL,
  `comments` text,
  `subcategorydata` text,
  `image` varchar(122) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '3',
  `add_date` datetime DEFAULT NULL,
  `assign_executive_id` int(11) DEFAULT NULL,
  `statusnote` text,
  `assigndate` datetime DEFAULT NULL,
  `user_review` text,
  `rating` float(10,1) DEFAULT '0.0',
  `rating_date` datetime DEFAULT NULL,
  `closeby` int(11) DEFAULT NULL,
  `closedate` datetime DEFAULT NULL,
  `invoice` tinyint(1) DEFAULT '0',
  `signature` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_service_request`
--

INSERT INTO `user_service_request` (`id`, `id_service`, `id_user`, `contact_person`, `contact_number`, `product_identification`, `expectedservicedate`, `location`, `comments`, `subcategorydata`, `image`, `status`, `add_date`, `assign_executive_id`, `statusnote`, `assigndate`, `user_review`, `rating`, `rating_date`, `closeby`, `closedate`, `invoice`, `signature`) VALUES
(1, 1, 48, 'tusar', '5467893120', 'test as new user', NULL, 'jayde', 'test as user', NULL, NULL, 1, '2018-08-30 20:41:49', 27, NULL, '2018-08-30 20:49:40', 'twst ywst', 0.5, '2018-08-30 21:03:08', NULL, NULL, 0, NULL),
(2, 1, 48, 'jagu', '57697686867', 'job post', NULL, 'bbsr', 'test check', NULL, NULL, 1, '2018-08-30 21:28:42', 27, NULL, '2018-08-30 21:30:17', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(3, 2, 49, 'cry', '259636', 'test spe', NULL, 'bbsr', 'prob', NULL, NULL, 1, '2018-09-01 12:05:43', 27, 'check on pri', '2018-09-01 12:06:40', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(4, 1, 49, '777', '989', 'nn', NULL, 'bbs', 'hhzh', NULL, NULL, 4, '2018-09-01 16:20:44', 50, NULL, '2018-09-01 16:30:03', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(5, 2, 51, 'gdfhheri', '9438505144', 'gjijd849ej7', NULL, 'gartew', 'havelipee', NULL, NULL, 4, '2018-09-04 14:58:03', 27, 'CHECK IT', '2018-09-04 15:10:39', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(6, 1, 51, 'Nishikant', '9742016340', 'no forklift', NULL, 'jc road', 'hii', NULL, NULL, 4, '2018-09-04 15:27:10', 27, NULL, '2018-09-17 12:08:59', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(7, 2, 48, 'sanu', '78096541238', 'long hair', NULL, 'jayadev bihar', 'Test\nTest\nTest\nTest\nTest\nTest\nTest test test yrdc', NULL, NULL, 1, '2018-09-04 17:01:26', 27, 'dfgdgh', '2018-09-04 17:43:23', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(8, 1, 48, 'ggh', '566855', 'cgv', NULL, 'gg', 'Cghg\nFgbj', NULL, NULL, 1, '2018-09-04 17:15:51', 27, NULL, '2018-09-04 17:28:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(9, 2, 48, 'ghh', '59', 'fghh', NULL, 'thh', 'Gvghh\nFgghh\nVhj', NULL, NULL, 1, '2018-09-04 19:26:57', 27, 'test', '2018-09-04 19:29:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(10, 2, 48, 'hbnggh', '566999', 'vvhhh', NULL, 'ggh', 'Vvvbh', NULL, NULL, 1, '2018-09-04 19:58:46', 27, 'dfhfsdgf', '2018-09-04 19:58:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(11, 2, 48, 'ggh', '8666', 'cghj', NULL, 'bbh', 'Chjnnn', NULL, NULL, 1, '2018-09-04 20:07:08', 27, 'gyhfhgfhgf', '2018-09-04 20:07:52', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(12, 3, 48, 'gg', '5658', 'tt', NULL, 'gg', 'Cv', NULL, NULL, 4, '2018-09-04 20:08:37', 27, 'vghfhg', '2018-09-04 20:08:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(13, 2, 49, '???', '66', '??', NULL, '??', '??', NULL, NULL, 1, '2018-09-05 14:11:21', 46, 'ts', '2018-09-05 14:11:49', 'trFf', 1.0, '2018-09-18 13:08:26', NULL, NULL, 0, NULL),
(14, 1, 49, '?', '55', '??', NULL, '?7', 'Hh', NULL, NULL, 1, '2018-09-05 14:12:57', 27, 'kkkkk', '2018-09-05 14:13:16', 'Good', 4.0, '2018-09-18 12:53:12', NULL, NULL, 0, NULL),
(15, 3, 56, 'c', '9556786167', 'a', NULL, 'b', 'Abcd', NULL, NULL, 1, '2018-09-05 14:32:49', 46, '222', '2018-09-05 14:33:43', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(16, 1, 56, 'vv', '95686324117', 'hh', NULL, 'vg', 'Tt', NULL, NULL, 4, '2018-09-05 14:37:07', 50, 'yyy', '2018-09-05 14:37:48', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(17, 2, 56, 'ytf', '658896854258', 'df', NULL, 'df', 'Se', NULL, NULL, 4, '2018-09-05 14:43:02', 27, NULL, '2018-09-12 15:55:37', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(18, 1, 56, 'cdjk', '8657844288', 'ch', NULL, 'gkgd', 'X6 tu h', NULL, NULL, 4, '2018-09-05 15:31:40', 27, NULL, '2018-09-11 16:55:04', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(19, 3, 56, 'ghvkb', '8555745688', 'fjjv', NULL, 'xigho', 'Chvj', NULL, NULL, 1, '2018-09-05 15:35:51', 50, NULL, '2018-09-05 15:41:44', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(20, 4, 59, 'Xavier', '1234567896', 'rechargeable', NULL, 'bhubaneswar', 'Battery is damaged', NULL, NULL, 4, '2018-09-12 15:54:28', 27, NULL, '2018-09-12 16:13:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(21, 3, 48, 'trsttrst', '21365478999', 'tedt', NULL, 'test', 'Good\nThanks\nThanks\nThanks so much for', NULL, NULL, 4, '2018-09-12 15:59:21', 58, NULL, '2018-09-12 16:00:49', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(22, 3, 48, 'gusgdig', '68866856657', 'xgjxgix', NULL, 'guxhixgi', 'Hczhfzustud', NULL, NULL, 4, '2018-09-15 15:30:47', 27, NULL, '2018-09-15 15:31:23', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(23, 3, 55, 'Vivebk', '558888', 'test02', NULL, 'Puri', 'Testing', NULL, NULL, 1, '2018-09-15 15:31:59', 50, NULL, '2018-09-15 15:36:25', 'Test', 5.0, '2018-09-15 15:37:18', NULL, NULL, 0, NULL),
(24, 3, 47, 'naresh', '5847123697', 'testing', NULL, 'bbsr', 'Quick', NULL, NULL, 4, '2018-09-17 11:41:48', 27, NULL, '2018-09-17 11:42:26', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(25, 4, 48, 'fhu', '6655', 'ttt', NULL, 'g yuu', 'Fgh', NULL, NULL, 1, '2018-09-17 11:42:46', 27, NULL, '2018-09-17 11:42:58', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(26, 2, 47, 'fgjj', '85566', 'tyy', NULL, 'fgh', 'Cvbh', NULL, NULL, 3, '2018-09-17 12:23:59', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(27, 1, 48, 'gdegus', '5686582', 'gxjdd', NULL, 'eidige', 'Xdyi\nJxjdkhf\nKdigxyid\nZxjg\nMvxc', NULL, NULL, 4, '2018-09-17 17:13:17', 50, NULL, '2018-09-18 13:05:15', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(28, 2, 48, 'ztda with e', '0986565', 'chxhfh', NULL, 'hfj', 'Zfxhfjgu\nGdufuf\nDyfg', NULL, NULL, 4, '2018-09-17 17:59:42', 50, NULL, '2018-09-18 12:55:02', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(29, 3, 48, 'fxfxgx', '123456789', 'ctxrxrx', NULL, 'yfctx', 'Cguc\nTo fyf\nHfygg', NULL, NULL, 1, '2018-09-17 18:01:15', 46, NULL, '2018-09-17 18:01:45', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(30, 2, 48, 'fghuiu', '55666', 'frffgg', NULL, 'fvhhh', 'Ffhhh', NULL, NULL, 1, '2018-09-17 19:48:14', 27, NULL, '2018-09-17 19:52:36', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(31, 1, 48, 'zgfugi', '865656764', 'chdhfhgh', NULL, 'gdhfhfuf', 'Cxgd\nHdhfm\nHcjh\nTfij', NULL, NULL, 1, '2018-09-17 19:48:47', 27, NULL, '2018-09-17 19:51:53', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(32, 2, 48, 'qqqqqqqqq', '11111111', 'qqqqqqqq', NULL, 'qqqqqqqq', 'Qqqqqqqw', NULL, NULL, 1, '2018-09-17 19:49:17', 27, NULL, '2018-09-17 19:51:02', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(33, 4, 49, 'Sandy', '58755480', 'rechargeable', NULL, 'bbsr', 'Damaged', NULL, NULL, 1, '2018-09-18 13:16:36', 50, NULL, '2018-09-18 13:17:06', NULL, 0.0, NULL, 2, '2018-10-12 18:34:16', 1, '33_servicesignature.jpg'),
(34, 1, 49, 'hhd', '333', 'hh', NULL, 'bh', 'Hhdhhd', NULL, NULL, 3, '2018-09-19 19:38:32', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(35, 4, 65, 'vijay', '0835084897', 'F34', NULL, 'Jet Park', 'Service my above unit. Hours reading is 255 already', NULL, NULL, 1, '2018-09-27 20:29:32', 27, 'Check & Respond to customer', '2018-09-27 20:30:40', 'glad', 5.0, '2018-09-27 20:39:37', NULL, NULL, 0, NULL),
(36, 5, 48, 'ganesh', '985236147', 'valud infi', NULL, 'bbsr', 'Testing good', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"5\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"3\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 3, '2018-10-08 19:59:51', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(37, 6, 48, 'kisu', '94656866', 'go', NULL, 'bbsr', 'Test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"rjjr\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 3, '2018-10-08 20:02:41', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(38, 6, 48, 'kamal', '123456789', 'aaaaaaa', NULL, 'bbsr', 'Test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ok\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}]', NULL, 3, '2018-10-08 20:04:57', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(39, 7, 49, 'hgg', '989989898', 'tedt', NULL, 'pdp', 'Hdhhd', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"10\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"2 h\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tesf cell\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"no\"}]', NULL, 1, '2018-10-09 17:05:11', 27, 'test', '2018-10-09 17:06:27', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(40, 4, 49, 'jj', '6669', 'hh', NULL, 'nj', 'Hjj', '[]', NULL, 3, '2018-10-09 17:30:05', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(41, 4, 47, 'vhh', '0899', 'vghh', NULL, 'vb', 'FgStytyy', '[]', NULL, 3, '2018-10-12 15:13:49', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(42, 7, 47, 'hchch', '965656', 'jjfuc', NULL, 'hchch', 'Ifuufufu', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ufuf\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ufuf\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ururu\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '42_resquestservice.jpg', 1, '2018-10-12 15:15:34', 27, NULL, '2018-10-22 14:53:02', NULL, 0.0, NULL, 2, '2018-10-22 14:54:07', 1, '42_servicesignature.jpg'),
(43, 7, 47, 'vbb', '566', 'vbb', NULL, 'fgg', 'Vghh', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"vvv\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ggh\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"gh\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', '43_resquestservice.jpg', 1, '2018-10-12 15:32:57', 27, NULL, '2018-10-16 17:59:13', NULL, 0.0, NULL, 2, '2018-10-16 18:01:02', 1, '43_servicesignature.jpg'),
(44, 7, 47, 'hsheh', '64665', 'hdhd', NULL, 'gshwy', 'Bshhs', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hrhr\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hsheh\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hehe\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-12 17:44:17', 27, NULL, '2018-10-12 18:39:22', NULL, 0.0, NULL, 2, '2018-10-12 18:40:30', 1, '44_servicesignature.jpg'),
(45, 7, 47, 'cjcjf', '068353', 'nchf', NULL, 'chfufu', 'Jcjcff', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"gh\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"vbbh\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"vbhh\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '45_resquestservice.jpg', 1, '2018-10-12 17:47:53', 27, NULL, '2018-10-12 17:54:31', NULL, 0.0, NULL, 27, '2018-10-12 17:58:23', 1, '45_servicesignature.jpg'),
(46, 8, 47, 'person', '456789', 'identify', NULL, 'location', 'Test', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\"}]', '46_resquestservice.jpg', 1, '2018-10-12 19:04:39', 27, NULL, '2018-10-16 17:58:35', NULL, 0.0, NULL, 2, '2018-10-16 17:59:53', 1, '46_servicesignature.jpg'),
(47, 8, 47, 'bdhud', '656656', 'vxhxhd', NULL, 'hshd', 'Bxbbx', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\"}]', '47_resquestservice.jpg', 1, '2018-10-12 19:26:11', 27, 'Test', '2018-10-12 19:35:18', NULL, 0.0, NULL, 27, '2018-10-12 19:36:55', 1, '47_servicesignature.jpg'),
(48, 4, 47, 'chichcy', '8558', 'gj', NULL, 'gixhd', 'Chchcchkc', '[]', '48_resquestservice.jpg', 1, '2018-10-12 19:30:30', 27, 'Test', '2018-10-12 19:41:55', 'new', 2.5, '2018-10-13 12:23:30', 27, '2018-10-12 19:42:56', 1, '48_servicesignature.jpg'),
(49, 8, 47, 'chcuf', '8686', 'xhch', NULL, 'xhxh', 'Chchc\nCjvi', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hfhdh\"}]', NULL, 1, '2018-10-12 19:31:40', 27, 'Test', '2018-10-12 19:39:19', NULL, 0.0, NULL, 27, '2018-10-12 19:39:51', 2, '49_servicesignature.jpg'),
(50, 3, 47, 'xgcyc', '8986855', 'hcjc', NULL, 'xxhcy', 'Vxhxgcc\nBxhchc', '[]', NULL, 1, '2018-10-12 19:33:31', 50, NULL, '2018-10-13 11:46:00', NULL, 0.0, NULL, 2, '2018-10-13 11:46:38', 2, '50_servicesignature.jpg'),
(51, 1, 49, 'chitta', '9437302803', 'Sll no 123', NULL, 'cyc', 'Problem', '[{\"id_service\":\"1\",\"name\":\"TEST\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"1\",\"name\":\"TEST2\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"09\"}]', '51_resquestservice.jpg', 1, '2018-10-13 11:09:04', 50, 'check', '2018-10-13 11:10:48', NULL, 0.0, NULL, 50, '2018-10-13 11:33:34', 1, '51_servicesignature.jpg'),
(52, 6, 47, 'bahu', '847348885', 'identify', NULL, 'location', 'Test test test test test test', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '52_resquestservice.jpg', 1, '2018-10-17 14:52:03', 27, NULL, '2018-10-17 15:04:48', NULL, 0.0, NULL, 2, '2018-10-17 15:05:59', 1, '52_servicesignature.jpg'),
(53, 5, 47, 'ccfeghffg', '9541888', 'fuyfujvy', NULL, 'chvhvhv', 'Vgfghhuj\nGgh\nGhh', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"gygx\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ihvyoc\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hiccgfh\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '53_resquestservice.jpg', 1, '2018-10-17 14:54:40', 27, NULL, '2018-10-17 15:01:57', NULL, 0.0, NULL, 27, '2018-10-17 15:03:46', 1, '53_servicesignature.jpg'),
(54, 6, 47, 'xtudti', '86552', 'dtudti', NULL, 'stusu', 'Zfhzfh', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"tustudt\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-17 14:59:50', 27, NULL, '2018-10-17 15:07:05', NULL, 0.0, NULL, 27, '2018-10-17 15:09:39', 2, '54_servicesignature.jpg'),
(55, 7, 47, 'gfyf', '86866868', 'gdy', NULL, 'cfu', 'Ucuf', '[{\"id_service\":\"7\",\"name\":\"Voltage or Number of Cells\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"i\"}, {\"id_service\":\"7\",\"name\":\" Amp hour\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"????\"}, {\"id_service\":\"7\",\"name\":\"Cell type\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"cchcy\"}, {\"id_service\":\"7\",\"name\":\"Water Filling System\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"7\",\"name\":\"Battery Tank\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}]', NULL, 1, '2018-10-17 15:18:11', 27, NULL, '2018-10-22 14:50:58', NULL, 0.0, NULL, 2, '2018-10-22 14:52:27', 2, '55_servicesignature.jpg'),
(56, 6, 47, 'gjzgxgux', '375575675', 'yfxt', NULL, 'jgux', ',vhzghxgxu', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"chcyu\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-17 15:25:46', 27, NULL, '2018-10-17 15:27:36', NULL, 0.0, NULL, 2, '2018-10-17 15:28:12', 1, '56_servicesignature.jpg'),
(57, 4, 65, 'Vijay', '0835084897', 'F14', NULL, 'jet park', 'Need service', '[{\"id_service\":\"4\",\"name\":\"Hour Meter\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"600\"}]', '57_resquestservice.jpg', 3, '2018-10-24 15:42:12', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(58, 3, 49, 'chitta', '988888885', 'firklifts', NULL, 'bbsr', 'Test', '[]', '58_resquestservice.jpg', 4, '2018-10-24 16:01:28', 27, 'test', '2018-10-24 17:56:45', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(59, 9, 47, 'g', '5', 'b', NULL, 'h', 'Gvg', '[{\"id_service\":\"9\",\"name\":\"Funality Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"Overall Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"Comments\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"v\"}, {\"id_service\":\"9\",\"name\":\"UI Flow\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"test2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"test3\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 1, '2018-10-29 12:37:53', 27, 'hbvhjbjhn', '2018-10-29 12:41:32', NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(60, 3, 47, 'hdhdhdjdjjfjfjfjfuf', '656566565656565', 'jsjdjudjdhdhdudhdhudud', NULL, 'bdjdjjdjdjfjdjuduffu', 'Hdhdhdhhd', '[]', '60_resquestservice.jpg', 3, '2018-10-29 19:08:13', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(61, 5, 47, 'g', '6', 'gt', NULL, 'y', 'G', '[{\"id_service\":\"5\",\"name\":\"Total Member\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ghy\"}, {\"id_service\":\"5\",\"name\":\"Male\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"yy\"}, {\"id_service\":\"5\",\"name\":\"Female\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"sff\"}, {\"id_service\":\"5\",\"name\":\"LPG Connection\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"5\",\"name\":\"Electricity\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '61_resquestservice.jpg', 3, '2018-10-29 19:15:23', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(62, 8, 47, 'yyyuuj', '566666', 'ghh', NULL, 'ghhghhh', 'Fgghhj no', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ffg\"}]', '62_resquestservice.jpg', 3, '2018-10-29 19:34:36', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(63, 8, 47, 'fg', '55', 'fg', NULL, 'fg', 'Fg', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"ghh\"}]', NULL, 3, '2018-10-30 16:02:57', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(64, 6, 47, 'xbx', '8668', 'hdhd', NULL, 'gdhhd', 'Hdhd', '[{\"id_service\":\"6\",\"name\":\"Sub1\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"hxhrhdh\"}, {\"id_service\":\"6\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 3, '2018-10-30 16:21:53', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(65, 2, 47, 'gh', '56', 'cg', NULL, 'gh', 'Vv', '[]', NULL, 3, '2018-10-30 16:22:18', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(66, 8, 47, 'babu', '789465', 'mark', NULL, 'bbsr', 'Check', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"check\"}]', NULL, 3, '2018-11-01 11:39:53', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(67, 3, 47, 'kuna', '123454679', 'matk test', NULL, 'bbsr', 'Work properly', '[]', NULL, 3, '2018-11-01 11:41:29', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(68, 9, 47, 'babu', '784512', 'marking', NULL, 'Bhubaneswar', 'Thank you\nThanks\nThank you\nThanks\nThank you\nThanks\nThank you\n\n\nThank you very much for all\nThank\nThank', '[{\"id_service\":\"9\",\"name\":\"Funality Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"9\",\"name\":\"Overall Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"Comments\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"test\\nthank you\\nthank you\\nthank you\\nthank you\\nthank you\"}, {\"id_service\":\"9\",\"name\":\"UI Flow\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"9\",\"name\":\"test2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"test3\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', NULL, 3, '2018-11-12 16:54:40', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL),
(69, 8, 47, 'vhvu', '686860', 'chvu', NULL, 'gugu', 'Gchcug\nJ\n.\nM\nM\nj\nN\nN\nM\nM MN\nM\nMm .j\n\n\n\n\n\nN\nJmn', '[{\"id_service\":\"8\",\"name\":\"SUB1\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"8\",\"name\":\"Sub2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"8\",\"name\":\"Sub3\",\"type\":\"2\",\"status\":\"1\",\"answer\":\" cy\\nin\\nugu\\nb\\nn\\nh\\ny\\ng\\nd\\nh\\nI\\nf\\nf\\nhim\\n\\n\\nhf\\nh\"}]', NULL, 1, '2018-11-12 18:37:06', 27, NULL, '2018-11-12 18:38:18', 'tedt', 3.5, '2018-11-12 18:40:49', 27, '2018-11-12 18:40:06', 1, '69_servicesignature.jpg'),
(70, 9, 47, 'vug', '88', 'chf', NULL, 'cyf', 'Chcy\nJ\nN\nM\nM\nN\nM\nN\nN\nN\nM m\nN\nM\nM\n\n\nM\n\nN\nM\nM\nN\n\n\n\n\n\n\n\nNnm\nJ n\nI\'m J', '[{\"id_service\":\"9\",\"name\":\"Funality Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"Overall Test\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"9\",\"name\":\"Comments\",\"type\":\"2\",\"status\":\"1\",\"answer\":\"xxgtc\"}, {\"id_service\":\"9\",\"name\":\"UI Flow\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}, {\"id_service\":\"9\",\"name\":\"test2\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"Yes\"}, {\"id_service\":\"9\",\"name\":\"test3\",\"type\":\"1\",\"status\":\"1\",\"answer\":\"No\"}]', '70_resquestservice.jpg', 3, '2018-11-12 18:42:44', NULL, NULL, NULL, NULL, 0.0, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_update_history`
--

CREATE TABLE `user_update_history` (
  `id_update_history` int(222) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `firstname` varchar(222) DEFAULT NULL,
  `lastname` varchar(222) DEFAULT NULL,
  `aadhar_number` varchar(50) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `alternate_phone` varchar(50) DEFAULT NULL,
  `email` varchar(222) DEFAULT NULL,
  `tokenId` bigint(20) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `address` text,
  `state` int(11) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `block` varchar(50) DEFAULT NULL,
  `sector` varchar(50) DEFAULT NULL,
  `sub_center` varchar(50) DEFAULT NULL,
  `village` varchar(50) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '1->Active,2->Inactive',
  `user_type` int(11) DEFAULT NULL COMMENT '1->Asha,2->sub center,3->sector ,4->block, 5->Dist,6->state,7->Admin',
  `designation` varchar(50) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `deviceId` varchar(100) DEFAULT NULL,
  `gcmId` text,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `update_reason` text,
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `breakdown`
--
ALTER TABLE `breakdown`
  ADD PRIMARY KEY (`id_breakdown`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id_state`);

--
-- Indexes for table `track_breakdown_request`
--
ALTER TABLE `track_breakdown_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `track_service_request`
--
ALTER TABLE `track_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_breakdown_request`
--
ALTER TABLE `user_breakdown_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id_user_log`);

--
-- Indexes for table `user_quote_request`
--
ALTER TABLE `user_quote_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_service_request`
--
ALTER TABLE `user_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_update_history`
--
ALTER TABLE `user_update_history`
  ADD PRIMARY KEY (`id_update_history`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `breakdown`
--
ALTER TABLE `breakdown`
  MODIFY `id_breakdown` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=990;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id_state` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `track_breakdown_request`
--
ALTER TABLE `track_breakdown_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `track_service_request`
--
ALTER TABLE `track_service_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `user_breakdown_request`
--
ALTER TABLE `user_breakdown_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id_user_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `user_quote_request`
--
ALTER TABLE `user_quote_request`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `user_service_request`
--
ALTER TABLE `user_service_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `user_update_history`
--
ALTER TABLE `user_update_history`
  MODIFY `id_update_history` int(222) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
