-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 12:37 PM
-- Server version: 5.6.43
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manageadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `fe_product`
--

CREATE TABLE `fe_product` (
  `id_product` int(11) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` float(6,2) DEFAULT NULL,
  `weight` varchar(23) DEFAULT NULL,
  `description` text,
  `add_date_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fe_product`
--

INSERT INTO `fe_product` (`id_product`, `category`, `name`, `price`, `weight`, `description`, `add_date_time`, `status`) VALUES
(1, 'Health Care', 'Badam Pak', 130.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(3, 'Health Care', 'Chyawanprash', 190.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(4, 'Health Care', 'Special Chyawanprash', 250.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(6, 'Health Care', 'Pachak Ajwain', 46.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(7, 'Health Care', 'Pachak Anardana Goli', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(8, 'Health Care', 'Pachak Chhuhara', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(9, 'Health Care', 'Pachak Hing Goli', 55.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(10, 'Health Care', 'Pachak Hing Peda', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(12, 'Health Care', 'Pachack Jal Jeera', 60.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(13, 'Health Care', 'Pachak Jeera Khatti Mithi Goli', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(14, 'Health Care', 'Pachak Methi Nimbu', 32.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(15, 'Health Care', 'Pachak Shodhit Harad', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(16, 'Health Care', 'Jamun Vinegar-L', 65.00, '570 gm', NULL, '2017-08-22 16:24:18', 1),
(17, 'Health Care', 'Gulab Sharbat-L', 100.00, '750 gm', NULL, '2017-08-22 16:24:18', 1),
(18, 'Health Care', 'Litchi Drink', 5.00, '65 ml', NULL, '2017-08-22 16:24:18', 1),
(19, 'Health Care', 'Orange Squash', 75.00, '750 ml', NULL, '2017-08-22 16:24:18', 1),
(20, 'Health Care', 'Patanjali Mango Drink', 5.00, '65 gm', NULL, '2017-08-22 16:24:18', 1),
(21, 'Health Care', 'Desi Ghee', 270.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(23, 'Health Care', 'Amrit Rasayana', 85.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(25, 'Health Care', 'Ashvagandha Capsule', 50.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(26, 'Health Care', 'Ashvashila-capsule', 70.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(27, 'Health Care', 'Gulkand', 55.00, '400 gm', NULL, '2017-08-22 16:24:18', 1),
(28, 'Health Care', 'Isabgol Bhusi', 90.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(29, 'Health Care', 'Kesar', 285.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(30, 'Health Care', 'Musli Pak', 350.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(31, 'Health Care', 'Shilajeet Capsule', 85.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(32, 'Health Care', 'Shilajeet Rasayan', 35.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(34, 'Health Care', 'Shilajeet Sat', 100.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(35, 'Health Care', 'Patanjali Power Vita', 205.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(36, 'Health Care', 'Patanjali Cow’s Whole Milk Powder', 65.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(37, 'Health Care', 'Badam Rogan', 110.00, '60 ml', NULL, '2017-08-22 16:24:18', 1),
(38, 'Health Care', 'Balm', 40.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(39, 'Health Care', 'Shatavar Churna', 90.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(40, 'Health Care', 'Youvan Churna', 200.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(41, 'Health Care', 'Aloe Vera Juice With Orange Flavour-L', 200.00, '1 ltr', NULL, '2017-08-22 16:24:18', 1),
(42, 'Health Care', 'Aloevera Juice With Fiber-L', 180.00, '1 ltr', NULL, '2017-08-22 16:24:18', 1),
(43, 'Health Care', 'Amla Aloevera Juice with Lichi Flavour', 5.00, '70 gm', NULL, '2017-08-22 16:24:18', 1),
(44, 'Health Care', 'Amla Juice-L', 55.00, '500 ml', NULL, '2017-08-22 16:24:18', 1),
(46, 'Health Care', 'Arjun Amla Juice', 80.00, '500 ml', NULL, '2017-08-22 16:24:18', 1),
(47, 'Health Care', 'Giloy Juice', 80.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(48, 'Health Care', 'Giloy Amla Juice', 90.00, '500 ml', NULL, '2017-08-22 16:24:18', 1),
(49, 'Health Care', 'Karela Amla Juice-L', 75.00, '570 gm', NULL, '2017-08-22 16:24:18', 1),
(50, 'Health Care', 'Lauki Amla Juice-L', 90.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(51, 'Health Care', 'Thandai Powder', 190.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(52, 'Health Care', 'Tulsi Panchang Juice', 90.00, '570 gm', NULL, '2017-08-22 16:24:18', 1),
(53, 'Health Care', 'Pure Honey Multiflora', 75.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(54, 'Health Care', 'Pure Honey', 70.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(56, 'Grocery', 'Marie Biscuits', 12.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(57, 'Grocery', 'Nutty Delite', 15.00, '100gm', NULL, '2017-08-22 16:24:18', 1),
(58, 'Grocery', 'Patanjali Doodh Biscuits', 5.00, '50gm', NULL, '2017-08-22 16:24:18', 1),
(60, 'Grocery', 'Nariyal Biscuit', 10.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(62, 'Grocery', 'Orange Delite Biscuits', 10.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(63, 'Grocery', 'Elaichi Biscuit', 10.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(64, 'Grocery', 'Aarogya Biscuit', 10.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(65, 'Grocery', 'Bandhani Hing', 20.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(67, 'Grocery', 'Black Pepper Powder', 125.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(68, 'Grocery', 'Black Pepper Whole', 120.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(69, 'Grocery', 'Chaat Masala', 36.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(70, 'Grocery', 'Choley Masala', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(71, 'Grocery', 'Coriander Powder', 30.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(72, 'Grocery', 'Cumin Whole', 40.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(73, 'Grocery', 'Garam Masala', 65.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(74, 'Grocery', 'Red Chilli Powder', 60.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(75, 'Grocery', 'Sabzi Masala', 38.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(76, 'Grocery', 'Turmeric Powder', 24.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(78, 'Grocery', 'Ajowan', 38.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(79, 'Grocery', 'Amla Candy', 140.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(80, 'Grocery', 'Amla Chatpata', 145.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(81, 'Grocery', 'Bel Candy', 140.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(82, 'Grocery', 'Mango Candy', 55.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(83, 'Grocery', 'Divya Peya', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(84, 'Grocery', 'Divya Herbal Peya', 70.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(85, 'Grocery', 'Mix Fruit Jam', 70.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(86, 'Grocery', 'Pineapple Jam', 70.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(87, 'Grocery', 'Guava Jam', 150.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(88, 'Grocery', 'Amla Murraba', 120.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(89, 'Grocery', 'Apple Murraba', 140.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(90, 'Grocery', 'Bel Murabba', 110.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(91, 'Grocery', 'Harad Murabba', 115.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(92, 'Grocery', 'Chocolate Soan Papdi', 160.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(93, 'Grocery', 'Elaichi Soan Papdi', 105.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(95, 'Grocery', 'Orange Soan Papdi', 55.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(96, 'Grocery', 'Madhuram Sugar Jaggery Powder', 60.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(97, 'Grocery', 'Barley Dalia', 30.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(98, 'Grocery', 'Wheat Dalia', 22.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(99, 'Grocery', 'Pushtahar Dalia', 40.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(100, 'Grocery', 'Patanjali Besan', 90.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(101, 'Grocery', 'Amla Pickle', 95.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(102, 'Grocery', 'Lemon Pickle', 65.00, '400 gm', NULL, '2017-08-22 16:24:18', 1),
(103, 'Grocery', 'Tomato Ketchup', 70.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(104, 'Grocery', 'Apple Chutney', 70.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(105, 'Grocery', 'Kachi Ghani Mustard Oil-L', 135.00, '950 gm', NULL, '2017-08-22 16:24:18', 1),
(106, 'Grocery', 'Choco Flakes', 55.00, '125 gm', NULL, '2017-08-22 16:24:18', 1),
(107, 'Grocery', 'Corn Flakes Mix', 85.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(109, 'Grocery', 'Unpolished Rajma-chitra', 100.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(110, 'Grocery', 'Unpolished Chana', 58.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(111, 'Grocery', 'Unpolished Arhar Dal', 88.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(112, 'Grocery', 'Unpolished Chhole', 65.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(113, 'Grocery', 'Basmati Rice Diamond', 145.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(115, 'Grocery', 'Basmati Rice Gold', 110.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(117, 'Grocery', 'Basmati Rice Silver', 75.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(119, 'Grocery', 'Atta Noodles', 15.00, '70 gm', NULL, '2017-08-22 16:24:18', 1),
(120, 'Grocery', 'Atta Noodles Chatpata', 10.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(121, 'Grocery', 'Atta Noodles Classic', 10.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(122, 'Grocery', 'Patanjali Bura', 65.00, '1 kg', NULL, '2017-08-22 16:24:18', 1),
(123, 'Grocery', 'Oats', 35.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(125, 'Grocery', 'Kali Mirch Papad', 50.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(126, 'Medicine', 'Dashmool Kwath', 20.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(127, 'Medicine', 'Giloya Kwath', 20.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(128, 'Medicine', 'Jwarnashak Kwath', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(129, 'Medicine', 'Kayakalp Kwath', 40.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(130, 'Medicine', 'Medha Kwath', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(131, 'Medicine', 'Mulethi Kwath', 30.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(132, 'Medicine', 'Nirgundi Kwath', 15.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(133, 'Medicine', 'Peedantak Kwath', 30.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(134, 'Medicine', 'Phal Ghrit', 330.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(135, 'Medicine', 'Parijat Kwath', 15.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(136, 'Medicine', 'Sarvkalp Kwath', 25.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(137, 'Medicine', 'Swasari Kwath', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(138, 'Medicine', 'Totala Kwath', 20.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(139, 'Medicine', 'Vrikkdoshhar Kwath', 40.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(140, 'Medicine', 'Arogya Vati', 60.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(141, 'Medicine', 'Arogyavardhani Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(143, 'Medicine', 'Arshkalp Vati', 35.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(145, 'Medicine', 'Chandraprabha Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(148, 'Medicine', 'Chitrakadi Vati', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(149, 'Medicine', 'Giloy Ghanvati', 90.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(150, 'Medicine', 'Hridyamrit Vati', 100.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(152, 'Medicine', 'Jwarnashak Vati', 45.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(153, 'Medicine', 'Kayakalp Vati', 70.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(155, 'Medicine', 'Khadiradi Vati', 45.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(156, 'Medicine', 'Kutajghan Vati', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(157, 'Medicine', 'Lavangadi Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(158, 'Medicine', 'Madhukalp Vati', 60.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(159, 'Medicine', 'Maha Sudarshan Vati', 80.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(160, 'Medicine', 'Medha Vati', 80.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(162, 'Medicine', 'Medohar Vati', 80.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(163, 'Medicine', 'Madhunashini', 200.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(164, 'Medicine', 'Madhu Kalp Vati', 60.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(165, 'Medicine', 'Mukta Vati', 190.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(166, 'Medicine', 'Neem Ghan Vati', 90.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(167, 'Medicine', 'Peedantak Vati', 45.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(169, 'Medicine', 'Punarnavadi Mandur', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(171, 'Medicine', 'Putrajeevak Beej', 75.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(172, 'Medicine', 'Raj Pravartani Vati', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(173, 'Medicine', 'Sanjivani Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(175, 'Medicine', 'Sarivadi Vati', 60.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(176, 'Medicine', 'Stri Rasayan Vati', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(178, 'Medicine', 'Tulsi Ghanvati', 90.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(179, 'Medicine', 'Udramrit Vati', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(181, 'Medicine', 'Vistindukadi Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(182, 'Medicine', 'Vriddhivadhika Vati', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(184, 'Medicine', 'Vrikkdoshhar Vati', 60.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(185, 'Medicine', 'Younamrit Vati', 250.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(186, 'Medicine', 'Abhrak Bhasm', 20.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(187, 'Medicine', 'Godanti Bhasm', 5.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(189, 'Medicine', 'Hajrul Yahud Bhasm', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(190, 'Medicine', 'Hirak Bhasm', 720.00, '300 mg', NULL, '2017-08-22 16:24:18', 1),
(191, 'Medicine', 'Kapardhak Bhasm', 10.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(193, 'Medicine', 'Kasis Bhasm', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(194, 'Medicine', 'Kulya Mishran', 75.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(195, 'Medicine', 'Lauh Bhasm', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(196, 'Medicine', 'Makar Dhwaj', 85.00, '2 gm', NULL, '2017-08-22 16:24:18', 1),
(197, 'Medicine', 'Mandur Bhasm', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(198, 'Medicine', 'Mukta Sukti Bhasm', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(200, 'Medicine', 'Rajat Bhasm', 200.00, '2 gm', NULL, '2017-08-22 16:24:18', 1),
(201, 'Medicine', 'Saptamrit Lauh', 20.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(202, 'Medicine', 'Shankh Bhasm', 5.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(204, 'Medicine', 'Sphatik Bhasm', 10.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(205, 'Medicine', 'Tamra Bhasm', 25.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(206, 'Medicine', 'Tamra Sindoor', 25.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(207, 'Medicine', 'Tankan Bhasm', 5.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(208, 'Medicine', 'Trivang Bhasm', 25.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(209, 'Medicine', 'Vang Bhasm', 25.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(210, 'Medicine', 'Ajmodadi Churna', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(211, 'Medicine', 'Amla Churna', 30.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(212, 'Medicine', 'Ashwagandha Churna', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(213, 'Medicine', 'Avipattikar Churna', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(214, 'Medicine', 'Baheda Churna', 6.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(215, 'Medicine', 'Bakuchi Churna', 25.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(216, 'Medicine', 'Bilwadi Churna', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(217, 'Medicine', 'Brahmi Churna', 30.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(218, 'Medicine', 'Divya Churna', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(219, 'Medicine', 'Gashar Churna', 85.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(220, 'Medicine', 'Haridrakhand', 70.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(221, 'Medicine', 'Haritki Churna', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(222, 'Medicine', 'Kutki Churna', 115.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(223, 'Medicine', 'Lavan Bhaskar Churna', 65.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(224, 'Medicine', 'Mulethi Churna', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(225, 'Medicine', 'Naag Kesar Churna', 90.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(226, 'Medicine', 'Panchkol Churna', 40.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(227, 'Medicine', 'Pushyanug Churna', 55.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(228, 'Medicine', 'Sitopaladi Churna', 10.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(231, 'Medicine', 'Swet Mushli', 325.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(232, 'Medicine', 'Shuddh Konch Beej Churan', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(233, 'Medicine', 'Trikatu Churna', 15.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(235, 'Medicine', 'Triphala Churna', 25.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(236, 'Medicine', 'Udarkalp Churna', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(237, 'Medicine', 'Vatari Churna', 55.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(239, 'Medicine', 'Gokshuradi Guggul', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(241, 'Medicine', 'Kaishore Guggul', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(243, 'Medicine', 'Kanchnar Guggul', 35.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(245, 'Medicine', 'Lakshadi Guggu', 40.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(246, 'Medicine', 'Mahayograj Guggul', 55.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(248, 'Medicine', 'Saptvisanti Guggul', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(249, 'Medicine', 'Singhnad Guggul', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(251, 'Medicine', 'Trayodshang Guggul', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(253, 'Medicine', 'Triphla Guggul', 35.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(255, 'Medicine', 'Yograj Guggul', 30.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(257, 'Medicine', 'Ekangveer Ras', 30.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(258, 'Medicine', 'Kamdudha Ras', 20.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(260, 'Medicine', 'Kumar Kalyan Ras', 1000.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(261, 'Medicine', 'Laxmi Vilas Ras', 60.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(263, 'Medicine', 'Mahawat Vidhwansan', 30.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(264, 'Medicine', 'Ras Manikya', 10.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(265, 'Medicine', 'Ras Raj Ras', 515.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(266, 'Medicine', 'Ras Sindoor', 15.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(267, 'Medicine', 'Shila Sindoor', 25.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(268, 'Medicine', 'Swarna Vasant Malti', 315.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(269, 'Medicine', 'Swasari Ras', 15.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(270, 'Medicine', 'Swarna Makshik', 20.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(271, 'Medicine', 'Tal Sindoor', 25.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(272, 'Medicine', 'Swet Parpati', 10.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(273, 'Medicine', 'Tribhuvankirti Ras', 50.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(274, 'Medicine', 'V.V.Chintamani Ras', 500.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(275, 'Medicine', 'Vasant Kusmakar Ras', 370.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(276, 'Medicine', 'Yogender Ras', 815.00, '1 gm', NULL, '2017-08-22 16:24:18', 1),
(277, 'Medicine', 'Anu Taila', 60.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(278, 'Medicine', 'Bala Taila', 130.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(279, 'Medicine', 'Kayakalp Taila', 70.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(280, 'Medicine', 'Ksirbala Taila', 135.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(281, 'Medicine', 'Mahamash Taila', 210.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(282, 'Medicine', 'Prasarini Taila', 150.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(283, 'Medicine', 'Saindhavadi Taila', 75.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(284, 'Medicine', 'Sahacharadi Taila', 180.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(285, 'Medicine', 'Somraaji Taila', 60.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(286, 'Medicine', 'Triphaladi Taila', 145.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(287, 'Medicine', 'Godhan Ark-L', 40.00, '480 gm', NULL, '2017-08-22 16:24:18', 1),
(288, 'Medicine', 'Jahar Mohra Pishti', 20.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(289, 'Medicine', 'Kaharava Pishti', 40.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(290, 'Medicine', 'Mukta Pishti', 60.00, '2 gm', NULL, '2017-08-22 16:24:18', 1),
(292, 'Medicine', 'Praval Panchamrit', 80.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(293, 'Medicine', 'Praval Pishti', 30.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(295, 'Medicine', 'Sangeyasav Pishti', 15.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(296, 'Medicine', 'Giloy Sat', 25.00, '5 gm', NULL, '2017-08-22 16:24:18', 1),
(298, 'Medicine', 'Abhayarishta-L', 75.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(299, 'Medicine', 'Arjunarishta-L', 80.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(300, 'Medicine', 'Ashokarishta-L', 75.00, '450 ml', NULL, '2017-08-22 16:24:18', 1),
(301, 'Medicine', 'Ashwagandharista-L', 100.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(302, 'Medicine', 'Dashmularishta-L', 105.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(303, 'Medicine', 'Khadirarishta-L', 75.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(304, 'Medicine', 'Kutjarishta-L', 60.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(305, 'Medicine', 'Punarnavarishta-L', 60.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(306, 'Medicine', 'Sarswatarishta-L', 105.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(307, 'Medicine', 'Arvindasava-L', 55.00, '270 gm', NULL, '2017-08-22 16:24:18', 1),
(308, 'Medicine', 'Kumaryasava-L', 75.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(309, 'Medicine', 'Lohasava-L', 85.00, '530 gm', NULL, '2017-08-22 16:24:18', 1),
(310, 'Medicine', 'Mahamanjishthadi Kwath-pravahi-L', 75.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(311, 'Medicine', 'Patrangasava-L', 85.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(312, 'Medicine', 'Ushirasava-L', 65.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(313, 'Medicine', 'Vidangasava-L', 60.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(314, 'Medicine', 'Divya Dhara', 30.00, '10 ml', NULL, '2017-08-22 16:24:18', 1),
(315, 'Medicine', 'Liv D 38 Syrup-l', 75.00, '280 gm', NULL, '2017-08-22 16:24:18', 1),
(316, 'Medicine', 'Liv D 38 Tablet', 70.00, '40 gm', NULL, '2017-08-22 16:24:18', 1),
(317, 'Medicine', 'Pradarsudha Syrup-for Leucorrhea', 75.00, '200 ml', NULL, '2017-08-22 16:24:18', 1),
(318, 'Medicine', 'Pradarsudha Syrup-for Menorrhagia', 80.00, '210 gm', NULL, '2017-08-22 16:24:18', 1),
(319, 'Medicine', 'Swasari Pravahi', 50.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(320, 'Home Care', 'Madhuram Amber', 20.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(321, 'Home Care', 'Madhuram Jasmine', 20.00, '28 gm', NULL, '2017-08-22 16:24:18', 1),
(322, 'Home Care', 'Madhuram Konark', 20.00, '24 gm', NULL, '2017-08-22 16:24:18', 1),
(323, 'Home Care', 'Madhuram Lavender', 20.00, '28 gm', NULL, '2017-08-22 16:24:18', 1),
(324, 'Home Care', 'Madhuram Meditation', 20.00, '28 gm', NULL, '2017-08-22 16:24:18', 1),
(325, 'Home Care', 'Madhuram Oudh', 20.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(326, 'Home Care', 'Madhuram Rose', 20.00, '28 gm', NULL, '2017-08-22 16:24:18', 1),
(327, 'Home Care', 'Madhuram Sandal', 20.00, '28 gm', NULL, '2017-08-22 16:24:18', 1),
(328, 'Home Care', 'Madhuram Utsav', 20.00, '24 gm', NULL, '2017-08-22 16:24:18', 1),
(329, 'Home Care', 'Madhuram Vangandha', 20.00, '24 gm', NULL, '2017-08-22 16:24:18', 1),
(330, 'Home Care', 'Madhuram White Flower', 20.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(331, 'Home Care', 'Madhuram Yajna Sugandham', 20.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(332, 'Home Care', 'Super Dish Wash Bar', 10.00, '175 gm', NULL, '2017-08-22 16:24:18', 1),
(333, 'Home Care', 'Divya Hawan Samagri', 50.00, '500 gm', NULL, '2017-08-22 16:24:18', 1),
(334, 'Personal Care', 'Body Ubtan', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(335, 'Personal Care', 'Lip Balm Strawberry', 25.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(336, 'Personal Care', 'Gulab Jal', 30.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(337, 'Personal Care', 'Herbal Suhag Teeka', 75.00, '3 gm', NULL, '2017-08-22 16:24:18', 1),
(338, 'Personal Care', 'Peedantak Oil', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(339, 'Personal Care', 'Switraghan Lep', 40.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(340, 'Personal Care', 'Aquafresh Body Cleanser', 24.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(341, 'Personal Care', 'Haldi Chandan Kanti Body Cleanser', 13.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(342, 'Personal Care', 'Haldi Chandan Soap', 25.00, '150 gm', NULL, '2017-08-22 16:24:18', 1),
(343, 'Personal Care', 'Kanti Almond Kesar', 19.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(344, 'Personal Care', 'Kanti Aloevera', 13.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(346, 'Personal Care', 'Kanti Neem', 15.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(347, 'Personal Care', 'Kanti Panchgavya', 13.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(348, 'Personal Care', 'Kanti Rose', 19.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(349, 'Personal Care', 'Lemon Honey Kanti', 19.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(350, 'Personal Care', 'Lemon Body Cleanser', 45.00, '125 gm', NULL, '2017-08-22 16:24:18', 1),
(351, 'Personal Care', 'Mogra Body Cleanser', 25.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(352, 'Personal Care', 'Multani Mitti Body Cleanser', 35.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(353, 'Personal Care', 'Ojas Aquafresh', 24.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(354, 'Personal Care', 'Patanjali Mint Tulsi', 24.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(355, 'Personal Care', 'Patanjali Rose Body Cleanser', 45.00, '125 gm', NULL, '2017-08-22 16:24:18', 1),
(356, 'Personal Care', 'Saundarya Cream Body Cleanser', 13.00, '75 gm', NULL, '2017-08-22 16:24:18', 1),
(357, 'Personal Care', 'Saundarya Mysore Super', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(358, 'Personal Care', 'Coconut Hair Wash', 95.00, '150 ml', NULL, '2017-08-22 16:24:18', 1),
(359, 'Personal Care', 'Conditioner Damage Control', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(360, 'Personal Care', 'Hair Conditioner Colour Protection', 60.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(361, 'Personal Care', 'Hair Conditioner Olive Almond', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(362, 'Personal Care', 'Herbal Mehandi', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(363, 'Personal Care', 'Drishti Eye Drop', 25.00, '15 ml', NULL, '2017-08-22 16:24:18', 1),
(364, 'Personal Care', 'Herbal Kajal', 90.00, '3 gm', NULL, '2017-08-22 16:24:18', 1),
(365, 'Personal Care', 'Mahatrifala Ghrit', 380.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(366, 'Personal Care', 'Aloevera Gel', 40.00, '60 ml', NULL, '2017-08-22 16:24:18', 1),
(368, 'Personal Care', 'Aloevera Moisturizing Cream', 75.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(369, 'Personal Care', 'Anti Wrinkle Cream', 150.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(370, 'Personal Care', 'Beauty Cream', 70.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(371, 'Personal Care', 'Boro Safe', 40.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(372, 'Personal Care', 'Moisturizer Cream', 75.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(373, 'Personal Care', 'Kanti Lep', 70.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(374, 'Personal Care', 'Saundarya Anti Aging Cream', 300.00, '15 gm', NULL, '2017-08-22 16:24:18', 1),
(375, 'Personal Care', 'Saundarya Swarn Kanti Fairness Cream', 399.00, '15 gm', NULL, '2017-08-22 16:24:18', 1),
(376, 'Personal Care', 'Sun Screen Cream', 100.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(377, 'Personal Care', 'Honey Orange Face Wash', 45.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(378, 'Personal Care', 'Neem Tulsi Face Wash', 45.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(379, 'Personal Care', 'Orange Aloevera Face Wash', 45.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(380, 'Personal Care', 'Lemon Honey Face Wash', 45.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(381, 'Personal Care', 'Rose Face Wash', 45.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(382, 'Personal Care', 'Saundraya Face Wash', 60.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(383, 'Personal Care', 'Apricot Face Scrub', 60.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(384, 'Personal Care', 'Face Pack Multani Mitti', 60.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(385, 'Personal Care', 'Neem Aloevera With Cucumber Face Pack', 60.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(386, 'Personal Care', 'Crack Heal Cream', 60.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(387, 'Personal Care', 'Herbal Shaving Cream', 55.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(388, 'Personal Care', 'Shave Gel', 40.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(389, 'Personal Care', 'Patanjali Activated Carbon Facial Foam', 60.00, '60 gm', NULL, '2017-08-22 16:24:18', 1),
(390, 'Personal Care', 'Kesh Kanti Anti Dandruff Hair Cleanser', 110.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(391, 'Personal Care', 'Kesh Kanti Anti Dandruff Pouch', 3.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(392, 'Personal Care', 'Kesh Kanti Milk Protein Hair Cleanser-Pouch', 3.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(393, 'Personal Care', 'Kesh Kanti Milk Protein', 95.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(394, 'Personal Care', 'Kesh Kanti Natural', 75.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(395, 'Personal Care', 'Kesh Kanti Natural Pouch', 2.00, '8 gm', NULL, '2017-08-22 16:24:18', 1),
(396, 'Personal Care', 'Kesh Kanti Reetha', 85.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(397, 'Personal Care', 'Kesh Kanti Reetha Hair Cleanser-Pouch', 3.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(398, 'Personal Care', 'Kesh Kanti Shikakai', 95.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(399, 'Personal Care', 'Kesh Kanti Aloe Vera Hair Cleanser', 75.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(400, 'Personal Care', 'Almond Oil', 50.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(401, 'Personal Care', 'Amla Hair Oil', 40.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(402, 'Personal Care', 'Coconut Hair Oil', 65.00, '210 gm', NULL, '2017-08-22 16:24:18', 1),
(404, 'Personal Care', 'Kesh Kanti Hair Oil', 130.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(405, 'Personal Care', 'Patanjali Kesh Kanti Oil', 250.00, '300 ml', NULL, '2017-08-22 16:24:18', 1),
(406, 'Personal Care', 'Sheetal Oil', 55.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(407, 'Personal Care', 'Tejus Tailum', 60.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(408, 'Personal Care', 'Dant Kanti', 10.00, '25 gm', NULL, '2017-08-22 16:24:18', 1),
(411, 'Personal Care', 'Dant Kanti Junior', 35.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(412, 'Personal Care', 'Dant Kanti Medicated', 45.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(413, 'Personal Care', 'Dant Kanti Advanced', 90.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(414, 'Personal Care', 'Active Care Tooth Brush', 20.00, '70 gm', NULL, '2017-08-22 16:24:18', 1),
(415, 'Personal Care', 'Curvy Tooth Brush', 15.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(416, 'Personal Care', 'Junior Tooth Brush', 15.00, '20 gm', NULL, '2017-08-22 16:24:18', 1),
(417, 'Personal Care', 'Sensitive Tooth Brush', 30.00, '22 gm', NULL, '2017-08-22 16:24:18', 1),
(418, 'Personal Care', 'Soft Tooth Brush', 10.00, '30 gm', NULL, '2017-08-22 16:24:18', 1),
(419, 'Personal Care', 'Tooth Brush', 8.00, '10 gm', NULL, '2017-08-22 16:24:18', 1),
(420, 'Personal Care', 'Triple Action Tooth Brush', 25.00, '90 gm', NULL, '2017-08-22 16:24:18', 1),
(421, 'Personal Care', 'Divya Dant Manjan', 65.00, '100 gm', NULL, '2017-08-22 16:24:18', 1),
(422, 'Personal Care', 'Detergent Cake Popular', 6.00, '120 gm', NULL, '2017-08-22 16:24:18', 1),
(424, 'Personal Care', 'Detergent Cake Superior', 8.00, '125 gm', NULL, '2017-08-22 16:24:18', 1),
(426, 'Personal Care', 'Detergent Powder Popular', 13.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(429, 'Personal Care', 'Detergent Powder Superior', 17.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(432, 'Personal Care', 'Detergent Powder Ujjwal', 17.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(433, 'Personal Care', 'Somya Liquid Detergent', 80.00, '500 ml', NULL, '2017-08-22 16:24:18', 1),
(434, 'Personal Care', 'Hand Wash', 55.00, '250 ml', NULL, '2017-08-22 16:24:18', 1),
(435, 'Personal Care', 'Hand Wash Almond Kesar', 90.00, '175ml', NULL, '2017-08-22 16:24:18', 1),
(436, 'Personal Care', 'Hand Wash Refil Pack', 40.00, '200 ml', NULL, '2017-08-22 16:24:18', 1),
(437, 'Personal Care', 'Olive Hand Wash', 90.00, '175ml', NULL, '2017-08-22 16:24:18', 1),
(438, 'Personal Care', 'Shishu Care Body Lotion', 85.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(439, 'Personal Care', 'Shishu Care Body Wash Gel', 75.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(440, 'Personal Care', 'Shishu Care Hair Oil', 70.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(441, 'Personal Care', 'Shishu Care Massage Oil', 85.00, '100 ml', NULL, '2017-08-22 16:24:18', 1),
(442, 'Personal Care', 'Jal Neti Pot', 40.00, '50 gm', NULL, '2017-08-22 16:24:18', 1),
(443, 'Personal Care', 'Anulom Vilom Yantra', 500.00, '200 gm', NULL, '2017-08-22 16:24:18', 1),
(444, 'Personal Care', 'Cold Relief Inhaler', 40.00, '1 ml', NULL, '2017-08-22 16:24:18', 1),
(445, 'Health Care', 'Honey', 130.00, '250 gm', NULL, '2017-08-22 16:24:18', 1),
(446, 'Personal Care', 'ASSURE HAND WASH', 140.00, '250 ml', 'New and improved Assure Hand Wash gives the hands a gentle care with mild yet effective cleansing agents. It is enriched with Neem and Aloe Vera extracts that moisturise and provide gentle protection without drying out the skin. The refreshing fragrance keeps the hands fresh for long.', '2018-06-04 19:20:19', 1),
(447, 'Personal Care', 'ASSURE COMPLEXION BAR', 70.00, '75 gm', 'Assure Complexion bar is a nourishing and moisturising bathing bar that cleanses the skin leaving it clean, soft and supple. It is enriched with brightening extracts of Kesar, Olive and honey that enhances the complexion.', '2018-06-04 19:27:06', 1),
(448, 'Personal Care', 'ASSURE NOURISHING & MOISTURISING SOAP', 35.00, '100 gm', 'Assure soap is a unique blend of Neem, Tulsi and Pudina. It protects the skin without drying it out. It also disinfects and refreshes the skin leaving it feeling fresh', '2018-06-06 12:29:51', 1),
(449, 'Health Care', 'VESTIGE FLAX OIL', 605.00, '90 caps', 'Flaxseed oil is a rich source of the essential fatty acids such as alpha-linolenic acid (ALA), Omega-3 and Omega - 6. Omega 3 fatty acids are good fats that have heart-healthy effects. It is also a rich source of Vitamin E which is a powerful antioxidant required for maintaining the integrity of cell membranes by protecting it from harmful free-radicals. Flaxseed helps maintain a healthy cholesterol level and aids in regulating high blood pressure. Each soft gelatin capsule contains Flax seed Oil (Linseed Oil) 500 mg. \r\nDosage: One Capsule thrice a day after meals.', '2018-06-13 16:58:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fe_user`
--

CREATE TABLE `fe_user` (
  `id_user` int(222) NOT NULL,
  `firstname` varchar(222) DEFAULT NULL,
  `lastname` varchar(222) DEFAULT NULL,
  `email` varchar(222) NOT NULL,
  `username` varchar(222) DEFAULT NULL,
  `password` varchar(222) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `added_by` int(222) DEFAULT NULL COMMENT 'franchise or admin id',
  `status` tinyint(1) DEFAULT '1' COMMENT '1->Active,2->Inactive',
  `user_type` tinyint(4) DEFAULT '1' COMMENT '1->Fe user , 2->Vendor user 3-> pickup boy 4-> both pickup and feboy',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fe_user`
--

INSERT INTO `fe_user` (`id_user`, `firstname`, `lastname`, `email`, `username`, `password`, `is_admin`, `added_by`, `status`, `user_type`, `add_date`, `ip`) VALUES
(1, 'Admin', 'FeAdmin', 'admin@gmail.com', 'admin', 'admin123', 1, NULL, 1, 1, '2015-11-19 11:34:20', '192.168.1.24');

-- --------------------------------------------------------

--
-- Table structure for table `orderplace`
--

CREATE TABLE `orderplace` (
  `id_order` int(11) NOT NULL,
  `orderId` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `deviceid` varchar(222) DEFAULT NULL,
  `description` text,
  `total_price` float(10,2) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_weight` varchar(11) DEFAULT NULL,
  `total_item` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT 'Processing',
  `date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderplace`
--

INSERT INTO `orderplace` (`id_order`, `orderId`, `user_id`, `deviceid`, `description`, `total_price`, `item_name`, `item_weight`, `total_item`, `status`, `date_time`) VALUES
(21, 21262, 5, NULL, 'Order placed from AI', 13000.00, 'Honey', '250 gm', 100, 'Processing', '2018-01-30 17:04:09'),
(22, 22367, 5, NULL, 'Order placed from AI', 5000.00, 'Special Chyawanprash', '1 kg', 20, 'Processing', '2018-01-30 17:16:51'),
(23, 23378, 5, NULL, 'Order placed from AI', 14000.00, 'Special Chyawanprash', '1 kg', 56, 'Processing', '2018-01-30 17:22:53'),
(24, 24955, 5, NULL, 'Order placed from AI', 14000.00, 'Special Chyawanprash', '1 kg', 56, 'Processing', '2018-01-30 17:22:55'),
(25, 25768, 5, NULL, 'Order placed from AI', 7280.00, 'Honey', '250 gm', 56, 'Processing', '2018-01-30 17:49:51'),
(26, 26788, 5, NULL, 'Order placed from AI', 2990.00, 'Honey', '250 gm', 23, 'Processing', '2018-01-30 18:12:24'),
(27, 27427, 7, NULL, 'Order placed from AI', 13000.00, 'Honey', '250 gm', 100, 'Processing', '2018-01-31 15:05:45'),
(28, 28641, 7, NULL, 'Order placed from AI', 130.00, 'Badam Pak', '250 gm', 1, 'Processing', '2018-01-31 15:23:33'),
(29, 29699, 6, NULL, 'Order placed from AI', 13000.00, 'Honey', '250 gm', 100, 'Processing', '2018-01-31 20:07:22'),
(30, 30696, 5, NULL, 'Order placed from AI', 35.00, 'Oats', '200 gm', 1, 'Processing', '2018-02-01 11:21:21'),
(31, 31809, 5, NULL, 'Order placed from AI', 30.00, 'Atta Noodles', '70 gm', 2, 'Processing', '2018-02-01 11:26:01'),
(32, 32612, 5, NULL, 'Order placed from AI', 7800.00, 'Honey', '250 gm', 60, 'Processing', '2018-02-01 11:44:22'),
(33, 33864, 5, NULL, 'Order placed from AI', 100.00, 'Kali Mirch Papad', '200 gm', 2, 'Processing', '2018-02-01 11:45:57'),
(34, 34283, 9, NULL, 'Order placed from AI', 1300.00, 'Honey', '250 gm', 10, 'Processing', '2018-02-01 16:16:02'),
(35, 35728, 5, NULL, 'Order placed from AI', 1560.00, 'Honey', '250 gm', 12, 'Processing', '2018-02-09 15:32:19'),
(36, 36286, 5, NULL, 'Order placed from AI', 660.00, 'Hand Wash', '250 ml', 12, 'Processing', '2018-02-09 15:32:54'),
(37, 37346, 5, NULL, 'Order placed from AI', 1300.00, 'Honey', '250 gm', 10, 'Processing', '2018-02-09 16:03:51'),
(38, 38903, 5, NULL, 'Order placed from AI', 550.00, 'Hand Wash', '250 ml', 10, 'Processing', '2018-02-09 16:04:39'),
(39, 39113, 5, NULL, 'Order placed from AI', 390.00, 'Honey', '250 gm', 3, 'Processing', '2018-02-09 16:09:27'),
(40, 40350, 5, NULL, 'Order placed from AI', 2990.00, 'Honey', '250 gm', 23, 'Processing', '2018-02-09 16:22:14'),
(41, 41590, 5, NULL, 'Order placed from AI', 660.00, 'Hand Wash', '250 ml', 12, 'Processing', '2018-02-09 16:23:11'),
(42, 42151, 5, NULL, 'Order placed from AI', 1300.00, 'Honey', '250 gm', 10, 'Processing', '2018-02-09 16:27:56'),
(43, 43536, 5, NULL, 'Order placed from AI', 550.00, 'Hand Wash', '250 ml', 10, 'Processing', '2018-02-09 16:29:38'),
(44, 44267, 5, NULL, 'Order placed from AI', 13000.00, 'Honey', '250 gm', 100, 'Processing', '2018-02-10 16:01:53'),
(45, 45865, 5, NULL, 'Order placed from AI', 390.00, 'Honey', '250 gm', 3, 'Processing', '2018-02-16 11:29:35'),
(46, 46157, 5, NULL, 'Order placed from AI', 260.00, 'Honey', '250 gm', 2, 'Processing', '2018-02-16 11:31:09'),
(47, 47172, 5, NULL, 'Order placed from AI', 260.00, 'Honey', '250 gm', 2, 'Processing', '2018-02-21 18:55:01'),
(48, 48461, 5, NULL, 'Order placed from AI', 1300.00, 'Honey', '250 gm', 10, 'Processing', '2018-02-21 19:00:20'),
(49, 49631, 10, NULL, 'Order placed from AI', 32500.00, 'Honey', '250 gm', 250, 'Processing', '2018-02-27 13:59:44'),
(50, 50666, 12, NULL, 'Order placed from AI', 260.00, 'Honey', '250 gm', 2, 'Processing', '2018-05-23 20:49:01'),
(51, 51852, 11, NULL, 'Order placed from AI', 250.00, 'Trivang Bhasm', '5 gm', 10, 'Processing', '2018-05-24 07:02:17'),
(52, 52622, 14, NULL, 'Order placed from AI', 80.00, 'Tooth Brush', '10 gm', 10, 'Processing', '2018-05-27 01:11:31'),
(53, 53156, 5, NULL, 'Order placed from AI', 420.00, 'ASSURE HAND WASH', '250 ml', 3, 'Processing', '2018-06-04 19:32:18'),
(54, 54608, 5, NULL, 'Order placed from AI', 0.00, 'ASSURE NOURISHING & MOISTURISING SOAP', '100 gm', NULL, 'Processing', '2018-06-07 18:59:51'),
(55, 55425, 5, NULL, 'Order placed from AI', 560.00, 'ASSURE HAND WASH', '250 ml', 4, 'Processing', '2018-06-07 19:51:37'),
(56, 56697, 16, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2018-06-11 22:54:41'),
(57, 57631, 16, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2018-06-11 22:57:08'),
(58, 58673, 16, NULL, 'Order placed from AI', 70.00, 'ASSURE NOURISHING & MOISTURISING SOAP', '100 gm', 2, 'Processing', '2018-06-12 19:07:39'),
(59, 59817, 5, NULL, 'Order placed from AI', 0.00, 'VESTIGE FLAX OIL', '90 caps', NULL, 'Processing', '2018-06-13 16:59:56'),
(60, 60643, 5, NULL, 'Order placed from AI', 605.00, 'VESTIGE FLAX OIL', '90 caps', 1, 'Processing', '2018-06-13 17:03:44'),
(61, 61656, 5, NULL, 'Order placed from AI', 605.00, 'VESTIGE FLAX OIL', '90 caps', 1, 'Processing', '2018-06-13 17:41:28'),
(62, 62885, 16, NULL, 'Order placed from AI', 0.00, NULL, NULL, 2, 'Processing', '2018-06-14 18:43:25'),
(63, 63606, 13, NULL, 'Order placed from AI', 260.00, 'Honey', '250 gm', 2, 'Processing', '2018-06-15 13:36:41'),
(64, 64574, 16, NULL, 'Order placed from AI', 840.00, 'ASSURE HAND WASH', '250 ml', 6, 'Processing', '2018-08-11 12:06:34'),
(65, 65407, 16, NULL, 'Order placed from AI', 700.00, 'ASSURE HAND WASH', '250 ml', 5, 'Processing', '2018-08-12 21:37:52'),
(66, 66915, 16, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2018-08-19 11:06:43'),
(67, 67402, 16, NULL, 'Order placed from AI', 13860.00, 'ASSURE HAND WASH', '250 ml', 99, 'Processing', '2018-09-11 01:43:22'),
(68, 68972, 16, NULL, 'Order placed from AI', 560.00, 'ASSURE HAND WASH', '250 ml', 4, 'Processing', '2018-10-03 18:50:52'),
(69, 69932, 16, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2018-10-17 07:08:44'),
(70, 70933, 16, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2018-10-26 09:26:09'),
(71, 71875, 5, NULL, 'Order placed from AI', 1210.00, 'VESTIGE FLAX OIL', '90 caps', 2, 'Processing', '2018-12-21 22:28:02'),
(72, 72267, 17, NULL, 'Order placed from AI', 420.00, 'ASSURE HAND WASH', '250 ml', 3, 'Processing', '2018-12-22 07:19:17'),
(73, 73342, 17, NULL, 'Order placed from AI', 1815.00, 'VESTIGE FLAX OIL', '90 caps', 3, 'Processing', '2018-12-22 07:32:35'),
(74, 74340, 9, NULL, 'Order placed from AI', 60500.00, 'VESTIGE FLAX OIL', '90 caps', 100, 'Processing', '2019-03-13 17:40:45'),
(75, 75701, 17, NULL, 'Order placed from AI', 1210.00, 'VESTIGE FLAX OIL', '90 caps', 2, 'Processing', '2019-04-07 19:18:46'),
(76, 76302, 17, NULL, 'Order placed from AI', 280.00, 'ASSURE HAND WASH', '250 ml', 2, 'Processing', '2019-04-10 19:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` text,
  `phone` varchar(10) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `credit_card_no` varchar(250) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `name`, `phone`, `address`, `credit_card_no`, `status`, `datetime`) VALUES
(5, 'Nagen Biswal', '9938118270', 'testing address', '11111', 1, '2018-01-29 12:26:32'),
(6, 'kisan', '8984065328', 'bbsr', '11111', 1, '2018-01-31 11:10:34'),
(7, 'minaketan', '8984056289', 'bbsr', '11111', 1, '2018-01-31 14:48:41'),
(8, 'kisan', '8240964588', 'bbsr', '11111', 1, '2018-02-01 10:44:41'),
(9, 'saroj', '7008827628', 'hig-45', '11111', 1, '2018-02-01 16:10:27'),
(10, 'nazim', '0506890238', 'Mumbai', '11111', 1, '2018-02-27 13:55:03'),
(11, 'Binaya', '9008043141', 'Bangalore', '11111', 1, '2018-05-13 11:17:55'),
(12, 'Sandeep kumar', '9090909090', 'Nayapali, Bhubaneswar', '11111', 1, '2018-05-23 19:43:48'),
(13, 'soumyaa', '9668664828', 'test', '11111', 1, '2018-05-26 11:06:48'),
(14, 'temp', '9865623321', 'aaaaa', '11111', 1, '2018-05-27 01:01:56'),
(15, 'Pradeep Mallik', '9412110694', 'Shamli', '11111', 1, '2018-06-11 20:33:42'),
(16, 'Sangram Keshari Mahanty', '9040089999', 'Berhampur', '11111', 1, '2018-06-11 22:52:46'),
(17, 'Sangram Mahanty', '6371087227', 'Odisha', '11111', 1, '2018-12-22 07:17:27'),
(18, 'Test Spheres', '1234567890', 'Lorem ipsum lorem ipsum', '11111', 1, '2019-03-14 13:09:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fe_product`
--
ALTER TABLE `fe_product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `fe_user`
--
ALTER TABLE `fe_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `orderplace`
--
ALTER TABLE `orderplace`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fe_product`
--
ALTER TABLE `fe_product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=450;

--
-- AUTO_INCREMENT for table `fe_user`
--
ALTER TABLE `fe_user`
  MODIFY `id_user` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orderplace`
--
ALTER TABLE `orderplace`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
