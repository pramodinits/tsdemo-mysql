-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2019 at 12:15 PM
-- Server version: 5.6.43
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `auctionportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `sps__bank`
--

CREATE TABLE `sps__bank` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__bank`
--

INSERT INTO `sps__bank` (`id`, `name`) VALUES
(1, 'SBI');

-- --------------------------------------------------------

--
-- Table structure for table `sps__buyer_product_requirement`
--

CREATE TABLE `sps__buyer_product_requirement` (
  `id` int(123) NOT NULL,
  `user_id` int(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `brand_name` varchar(200) DEFAULT NULL,
  `country_origin` int(20) DEFAULT NULL,
  `category` int(12) DEFAULT NULL,
  `sub_category` int(12) DEFAULT NULL,
  `quantity` int(12) DEFAULT NULL,
  `unit` varchar(22) DEFAULT NULL,
  `goods_capacity_carton` int(11) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `summary` text,
  `expected_date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->Active,2->In-active',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__buyer_product_requirement`
--

INSERT INTO `sps__buyer_product_requirement` (`id`, `user_id`, `name`, `brand_name`, `country_origin`, `category`, `sub_category`, `quantity`, `unit`, `goods_capacity_carton`, `price`, `summary`, `expected_date`, `status`, `add_date`, `ip`) VALUES
(1, 26, 'Pineapple', 'abc', 1, 11, 0, 100, 'kg', NULL, 50.00, '<p>test</p>', '2017-11-30', 2, '2017-11-18 17:20:41', '192.168.1.108'),
(4, 26, 'apple', 'xyz', 1, 11, 0, 75, 'kg', NULL, 100.00, '<p>testing</p>', '2017-12-21', 2, '2017-11-30 13:29:03', '192.168.1.108'),
(5, 82, 'apple', 'Indian', 1, 10, 0, 2, 'kg', NULL, 23.00, '', '2017-12-28', 1, '2017-12-07 12:22:25', '192.168.1.111'),
(6, 83, 'cucumber', 'Indian', 1, 10, 0, 2, 'kg', NULL, 23.00, '', '2017-12-08', 1, '2017-12-07 13:10:08', '192.168.1.111'),
(7, 82, 'Tata', 'tata', 1, 10, 0, 2, 'kg', NULL, 12.00, '', '2017-12-09', 1, '2017-12-08 13:02:23', '192.168.1.111'),
(8, 87, 'aaa', 'aa', 1, 15, 0, 50, '500gm', 40, 50.00, '<p>test</p>', '2018-01-11', 1, '2018-01-04 17:49:42', '192.168.1.108'),
(9, 26, 'apple', 'test', 3, 13, 14, 2, 'test', 2, 2.00, '<p>test test</p>', '2018-01-27', 1, '2018-01-20 18:07:51', '192.168.1.38'),
(10, 59, '', '', 0, 0, 0, 0, '', 0, 0.00, '', '2018-02-23', 1, '2018-02-16 13:02:54', '223.31.228.226'),
(11, 26, 'apple', 'test', 7, 12, 0, 100, '200', 100, 200.00, '<p>test</p>', '2018-03-15', 2, '2018-03-01 14:00:05', '157.41.224.45'),
(12, 26, 'apple', 'test', 7, 11, 0, 200, '200', 200, 28.00, '<p>test</p>', '2018-03-08', 2, '2018-03-01 14:02:13', '157.41.224.45'),
(13, 26, 'fish', 'test', 4, 7, 0, 100, '200', 200, 299.00, '<p>test</p>', '2018-03-08', 2, '2018-03-01 14:04:35', '157.41.224.45'),
(21, 140, 'Carrot', 'Indian', 4, 10, 0, 1000, '1 kg', 100, 20.00, '', '2018-03-27', 1, '2018-03-19 12:19:02', '223.31.228.226'),
(15, 129, 'Patato', 'indian', 4, 10, 0, 5, '2', 0, 10.00, '', '2018-03-13', 1, '2018-03-06 17:23:10', '223.31.228.226'),
(16, 133, 'Carrot', 'Indian', 4, 3, 0, 3, '2', 0, 19.00, '', '2018-04-30', 1, '2018-03-06 19:01:14', '223.31.228.226'),
(17, 129, 'orange', 'Indian', 4, 15, 0, 2, '1', 0, 20.00, '', '2018-05-31', 1, '2018-03-06 20:25:29', '223.31.228.226'),
(18, 129, 'safasdf', 'asdfd', 4, 9, 0, 32412, 'asdf', 0, 1234.00, '', '2018-03-17', 1, '2018-03-10 12:57:01', '223.31.228.226'),
(19, 129, 'ASDF', 'ASFSA', 3, 8, 0, 1, '1', 0, 1.00, '<p>DFBJBFGGERHFGRB &nbsp;VRHBVRHEVRTH HRBFRHEWGER GHRGAErherhgbjhbghrtbghsgvrtg rgerfgrwtw</p>', '2018-03-17', 1, '2018-03-10 12:57:50', '223.31.228.226'),
(20, 141, 'mango', 'Indian', 4, 15, 0, 20, '1', 0, 20.00, '', '2018-03-31', 1, '2018-03-10 17:47:45', '223.31.228.226'),
(22, 141, 'broccoli', 'Indian', 4, 10, 0, 30, '1 kg', 0, 20.00, '', '2018-03-30', 1, '2018-03-23 11:31:03', '223.31.228.226'),
(23, 141, 'blackberry', 'Indian', 4, 11, 0, 0, '1 kg', 0, 20.00, '', '2018-03-30', 1, '2018-03-23 12:53:52', '223.31.228.226'),
(24, 141, 'xyzz', 'Indian', 4, 10, 0, 1, '1 kg', 1, 1.00, '', '2018-03-30', 1, '2018-03-23 18:04:20', '223.31.228.226'),
(25, 141, 'qwrwe', 'Indian', 4, 10, 0, 1, '1 kg', 1, 1.00, '', '2018-03-30', 1, '2018-03-23 18:06:02', '223.31.228.226'),
(26, 26, 'Patato', 'Indian', 4, 11, 0, 50, '1 kg', 0, 30.00, '', '2018-04-09', 1, '2018-04-02 15:40:13', '223.31.228.226'),
(27, 26, 'tg', 'ff', 4, 7, NULL, NULL, 'KG', 5, 599.00, '', NULL, 1, '2018-04-04 21:11:28', '223.31.228.226'),
(28, 26, 'rf', 'fg', 4, 11, NULL, NULL, 'KG', 8, 8.00, 'dff', NULL, 1, '2018-04-04 21:12:32', '223.31.228.226'),
(29, 141, 'Patato', 'Indian', 4, 10, 0, 300, '1kg', 20, 20.00, '', '2018-04-13', 1, '2018-04-05 12:16:09', '223.31.228.226'),
(30, 156, 'hhh', 'fh', 4, 15, NULL, NULL, '1 KG', 5, 50.00, '', '2018-05-07', 1, '2018-04-07 16:36:02', '223.31.228.226'),
(31, 156, 'vg', 'gyh', 4, 10, NULL, NULL, '1 KG', 1, 1.00, '', '2018-05-07', 1, '2018-04-07 16:36:58', '223.31.228.226');

-- --------------------------------------------------------

--
-- Table structure for table `sps__categories`
--

CREATE TABLE `sps__categories` (
  `id` int(20) NOT NULL,
  `parent_id` int(12) DEFAULT NULL,
  `level` int(1) NOT NULL,
  `name` varchar(50) NOT NULL,
  `images` varchar(222) DEFAULT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `cbm_capacity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__categories`
--

INSERT INTO `sps__categories` (`id`, `parent_id`, `level`, `name`, `images`, `description`, `status`, `cbm_capacity`) VALUES
(1, NULL, 0, 'Snacks', 'cuttlefish.jpg', 'category1 category1', 1, 500),
(2, 7, 0, 'subcat1', NULL, 'subcat1', 1, NULL),
(3, NULL, 0, 'Sea Fish', 'salmonfillets.jpg', 'Sea fish', 1, 500),
(6, NULL, 0, 'Sprouts', 'brusselsprouts.jpg', 'Sprouts', 1, 500),
(7, NULL, 0, 'Fish', 'rohufish.jpg', 'Rohi machha', 1, 500),
(8, NULL, 0, 'Chicken', 'ckndrumstick.jpg', 'Chicken drumstick', 1, 500),
(9, NULL, 0, 'Corn', 'cornoncob.jpg', 'corns', 1, 500),
(10, NULL, 0, 'Vegetable', 'babycarrots.jpg', 'baby carrots', 1, 500),
(11, NULL, 0, 'Fruits', 'pomogranateKernels.jpg', 'Fruits', 1, 500),
(12, NULL, 0, 'Cheese', 'mozzarellaCheese.jpg', 'Cheese', 1, 500),
(13, NULL, 0, 'Butter', 'butter.jpg', 'Delicious Butter', 1, 500),
(14, 13, 0, 'Cream butter', NULL, 'Cream butter', 1, NULL),
(15, NULL, 0, 'Fruit', 'fruits.jpg', 'test1', 1, 500),
(16, 7, 0, 'subcategory2', NULL, 'subcategory2', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sps__city`
--

CREATE TABLE `sps__city` (
  `id` int(12) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_state` int(11) DEFAULT '0',
  `id_country` int(11) DEFAULT '0',
  `add_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__city`
--

INSERT INTO `sps__city` (`id`, `name`, `id_state`, `id_country`, `add_date`) VALUES
(5, 'Bur Dubai', 6, 1, '2017-12-19'),
(17, 'Others', 3, 1, '2017-12-27'),
(6, 'Deira', 6, 1, '2017-12-19'),
(7, 'Sheikh Zayed Road', 6, 1, '2017-12-19'),
(8, 'Jumeirah', 6, 1, '2017-12-19'),
(9, 'Burj al Arab', 6, 1, '2017-12-19'),
(10, 'Dubai Marina', 6, 1, '2017-12-19'),
(11, 'Inner suburb', 6, 1, '2017-12-19'),
(12, 'Sharjah', 4, 1, '2017-12-19'),
(13, 'Sharjah-Others', 4, 1, '2017-12-19'),
(14, 'Central Capital District', 3, 1, '2017-12-19'),
(15, 'Al Gharbia', 3, 1, '2017-12-19'),
(16, 'Al Ain', 3, 1, '2017-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `sps__configuration`
--

CREATE TABLE `sps__configuration` (
  `id` int(122) NOT NULL,
  `attribute` varchar(222) NOT NULL,
  `value` varchar(222) NOT NULL,
  `add_date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__country`
--

CREATE TABLE `sps__country` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `origin_flg` int(11) NOT NULL DEFAULT '1' COMMENT '1->country,2->country of origin',
  `add_date` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__country`
--

INSERT INTO `sps__country` (`id`, `name`, `origin_flg`, `add_date`) VALUES
(1, 'UAE', 1, '2017-09-06 21:23:23'),
(3, 'China', 2, NULL),
(4, 'India', 2, NULL),
(5, 'Pakistan', 2, NULL),
(6, 'Srilanka', 2, NULL),
(7, 'Bangladesh', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sps__exchange`
--

CREATE TABLE `sps__exchange` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__exchange`
--

INSERT INTO `sps__exchange` (`id`, `name`, `description`) VALUES
(1, 'AKM Exchange', 'AKM Exchange');

-- --------------------------------------------------------

--
-- Table structure for table `sps__gst`
--

CREATE TABLE `sps__gst` (
  `id` int(23) NOT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__gst`
--

INSERT INTO `sps__gst` (`id`, `month`, `year`, `amount`, `status`, `add_date`) VALUES
(1, 1, 2018, 51.13, 1, '2018-02-28 19:15:33');

-- --------------------------------------------------------

--
-- Table structure for table `sps__logintrack`
--

CREATE TABLE `sps__logintrack` (
  `id` int(222) NOT NULL,
  `user_id` int(111) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__logs`
--

CREATE TABLE `sps__logs` (
  `id` int(222) NOT NULL,
  `action` varchar(100) DEFAULT NULL COMMENT 'action method',
  `message` text,
  `user_id` int(12) DEFAULT NULL COMMENT 'login userid',
  `status` tinyint(4) DEFAULT NULL COMMENT '1->success 2->unsuccess ',
  `timestamp` timestamp NULL DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__migrations`
--

CREATE TABLE `sps__migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sps__migrations`
--

INSERT INTO `sps__migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_18_092102_create_task_table', 2),
(4, '2017_01_19_060342_create_table_todos', 3),
(5, '2017_02_17_101731_create_store_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sps__notification`
--

CREATE TABLE `sps__notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1->register 2->regards product 3-> bid 4->settlement etc',
  `subject` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__notification`
--

INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(1, 58, 4, 'Selected for Auction : 5843001', 'Selected for Auction : 5843001', 0, 0, '2018-02-07 12:11:41'),
(2, 82, 4, 'Selected for Auction : 8249317', 'Selected for Auction : 8249317', 0, 0, '2018-02-07 12:11:43'),
(3, 26, 4, 'Selected for Auction : 2645795', 'Selected for Auction : 2645795', 0, 0, '2018-02-07 12:11:44'),
(4, 58, 4, 'Selected for Auction : 5843001', 'Selected for Auction : 5843001', 0, 0, '2018-02-07 12:16:58'),
(5, 82, 4, 'Selected for Auction : 8249317', 'Selected for Auction : 8249317', 0, 0, '2018-02-07 12:16:59'),
(6, 26, 4, 'Selected for Auction : 2645795', 'Selected for Auction : 2645795', 0, 0, '2018-02-07 12:17:00'),
(7, 82, 4, 'Selected for Auction : 8216844', 'Selected for Auction : 8216844', 0, 0, '2018-02-07 12:17:40'),
(8, 58, 4, 'Selected for Auction : 5818710', 'Selected for Auction : 5818710', 0, 0, '2018-02-07 12:17:41'),
(9, 117, 4, 'Selected for Auction : 11714206', 'Selected for Auction : 11714206', 0, 0, '2018-02-07 12:17:42'),
(10, 87, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 13:03:47', 0, 0, '2018-02-07 13:03:47'),
(11, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-07 15:10:58', 0, 0, '2018-02-07 15:10:58'),
(12, 1, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 15:12:05', 0, 0, '2018-02-07 15:12:05'),
(13, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 15:15:34', 0, 0, '2018-02-07 15:15:34'),
(14, 87, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 15:32:25', 0, 0, '2018-02-07 15:32:25'),
(15, 1, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 15:39:17', 0, 0, '2018-02-07 15:39:17'),
(16, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 16:04:05', 0, 0, '2018-02-07 16:04:05'),
(17, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 16:05:35', 0, 0, '2018-02-07 16:05:35'),
(18, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 16:26:55', 0, 0, '2018-02-07 16:26:55'),
(19, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 16:31:56', 0, 0, '2018-02-07 16:31:56'),
(20, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-07 16:47:26', 0, 0, '2018-02-07 16:47:26'),
(21, 59, 4, 'Order #5959162 cancelled', 'Order #5959162 cancelled', 0, 0, '2018-02-07 16:54:50'),
(22, 58, 4, 'order #5959162 cancelled', 'order #5959162 cancelled', 0, 0, '2018-02-07 16:54:52'),
(23, 59, 4, 'Order #5959162 cancelled', 'Order #5959162 cancelled', 0, 0, '2018-02-07 16:59:01'),
(24, 58, 4, 'order #5959162 cancelled', 'order #5959162 cancelled', 0, 0, '2018-02-07 16:59:03'),
(25, 59, 4, 'Order #5959162 cancelled', 'Order #5959162 cancelled', 0, 0, '2018-02-07 16:59:46'),
(26, 58, 4, 'order #5959162 cancelled', 'order #5959162 cancelled', 0, 0, '2018-02-07 16:59:49'),
(27, 59, 4, 'Pay Slip Upload for : 5959162', 'Pay Slip Upload for : 5959162', 0, 0, '2018-02-07 17:03:30'),
(28, 59, 4, 'Payment success for ordernumber : 5959162', 'Payment success for ordernumber : 5959162', 0, 0, '2018-02-07 17:03:58'),
(29, 58, 4, 'Payment success for ordernumber : 5959162', 'Payment success for ordernumber : 5959162', 0, 0, '2018-02-07 17:03:59'),
(30, 59, 4, 'Order #5959162 cancelled', 'Order #5959162 cancelled', 0, 0, '2018-02-07 17:12:26'),
(31, 58, 4, 'order #5959162 cancelled', 'order #5959162 cancelled', 0, 0, '2018-02-07 17:12:27'),
(32, 59, 4, 'Order #5959162 cancelled', 'Order #5959162 cancelled', 0, 0, '2018-02-07 17:13:22'),
(33, 58, 4, 'order #5959162 cancelled', 'order #5959162 cancelled', 0, 0, '2018-02-07 17:13:23'),
(34, 58, 4, 'Auction Pending Confirm Email : 5843001', 'Auction Pending Confirm Emailn : 5843001', 0, 0, '2018-02-08 11:31:07'),
(35, 26, 4, 'Auction Pending Confirm Email : 2645795', 'Auction Pending Confirm Emailn : 2645795', 0, 0, '2018-02-08 11:31:09'),
(36, 82, 4, 'Auction Pending Confirm Email : 8249317', 'Auction Pending Confirm Emailn : 8249317', 0, 0, '2018-02-08 11:31:11'),
(37, 82, 4, 'Auction Pending Confirm Email : 8216844', 'Auction Pending Confirm Emailn : 8216844', 0, 0, '2018-02-08 11:31:13'),
(38, 58, 4, 'Auction Pending Confirm Email : 5818710', 'Auction Pending Confirm Emailn : 5818710', 0, 0, '2018-02-08 11:31:13'),
(39, 117, 4, 'Auction Pending Confirm Email : 11714206', 'Auction Pending Confirm Emailn : 11714206', 0, 0, '2018-02-08 11:31:14'),
(40, 26, 3, 'Successful send proforma invoice : 2653620', 'Successful send proforma invoice 2018-02-08 11:45:30 ', 0, 0, '2018-02-08 11:45:30'),
(41, 58, 3, 'Successful send proforma invoice : 2653620', 'Successful send proforma invoice 2018-02-08 11:45:33 ', 0, 0, '2018-02-08 11:45:33'),
(42, 26, 4, 'Pay Slip Upload for : 2653620', 'Pay Slip Upload for : 2653620', 0, 0, '2018-02-08 11:46:10'),
(43, 26, 4, 'Payment success for ordernumber : 2653620', 'Payment success for ordernumber : 2653620', 0, 0, '2018-02-08 11:46:56'),
(44, 58, 4, 'Payment success for ordernumber : 2653620', 'Payment success for ordernumber : 2653620', 0, 0, '2018-02-08 11:46:58'),
(45, 26, 4, 'Your order #2653620 has been successfully delivered', 'Your order #2653620 has been successfully delivered', 0, 0, '2018-02-08 11:54:22'),
(46, 58, 4, 'Your order #2653620 has been successfully delivered', 'Your order #2653620 has been successfully delivered', 0, 0, '2018-02-08 11:54:23'),
(47, 58, 4, 'Balance info for the month:February,Year :2018', 'Balance info for the month:February,Year :2018 is 683.35', 0, 0, '2018-02-08 11:59:43'),
(48, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 13:16:56', 0, 0, '2018-02-08 13:16:56'),
(49, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-08 14:53:11', 0, 0, '2018-02-08 14:53:11'),
(50, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-08 14:56:45', 0, 0, '2018-02-08 14:56:45'),
(51, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-08 15:00:32', 0, 0, '2018-02-08 15:00:32'),
(52, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-08 15:02:41', 0, 0, '2018-02-08 15:02:41'),
(53, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-08 15:06:42', 0, 0, '2018-02-08 15:06:42'),
(54, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:09:55', 0, 0, '2018-02-08 15:09:55'),
(55, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:10:12', 0, 0, '2018-02-08 15:10:12'),
(56, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:10:28', 0, 0, '2018-02-08 15:10:28'),
(57, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:10:43', 0, 0, '2018-02-08 15:10:43'),
(58, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:10:58', 0, 0, '2018-02-08 15:10:58'),
(59, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:11:40', 0, 0, '2018-02-08 15:11:40'),
(60, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:13:01', 0, 0, '2018-02-08 15:13:01'),
(61, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:13:47', 0, 0, '2018-02-08 15:13:47'),
(62, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-08 15:44:44', 0, 0, '2018-02-08 15:44:44'),
(63, 58, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-08 15:47:04', 0, 0, '2018-02-08 15:47:04'),
(64, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-08 15:47:04', 0, 0, '2018-02-08 15:47:04'),
(65, 58, 3, 'Successful send proforma invoice : 5836612', 'Successful send proforma invoice 2018-02-08 15:48:39 ', 0, 0, '2018-02-08 15:48:39'),
(66, 59, 3, 'Successful send proforma invoice : 5836612', 'Successful send proforma invoice 2018-02-08 15:48:42 ', 0, 0, '2018-02-08 15:48:42'),
(67, 58, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-08 15:50:36', 0, 0, '2018-02-08 15:50:36'),
(68, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-08 15:50:36', 0, 0, '2018-02-08 15:50:36'),
(69, 58, 3, 'Successful send proforma invoice : 5831822', 'Successful send proforma invoice 2018-02-08 15:51:02 ', 0, 0, '2018-02-08 15:51:02'),
(70, 59, 3, 'Successful send proforma invoice : 5831822', 'Successful send proforma invoice 2018-02-08 15:51:05 ', 0, 0, '2018-02-08 15:51:05'),
(71, 58, 4, 'Remind Order Payment : 5836612', 'Remind Order Payment : 5836612', 0, 0, '2018-02-08 15:56:33'),
(72, 58, 4, 'Remind Order Payment : 5831822', 'Remind Order Payment : 5831822', 0, 0, '2018-02-08 15:56:35'),
(73, 82, 3, 'Successful send proforma invoice : 8269159', 'Successful send proforma invoice 2018-02-08 15:58:20 ', 0, 0, '2018-02-08 15:58:20'),
(74, 58, 3, 'Successful send proforma invoice : 8269159', 'Successful send proforma invoice 2018-02-08 15:58:24 ', 0, 0, '2018-02-08 15:58:24'),
(75, 58, 4, 'Remind Order Payment : 5836612', 'Remind Order Payment : 5836612', 0, 0, '2018-02-08 15:58:39'),
(76, 58, 4, 'Remind Order Payment : 5831822', 'Remind Order Payment : 5831822', 0, 0, '2018-02-08 15:58:40'),
(77, 82, 4, 'Remind Order Payment : 8269159', 'Remind Order Payment : 8269159', 0, 0, '2018-02-08 15:58:41'),
(78, 26, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-08 19:24:25', 0, 0, '2018-02-08 19:24:25'),
(79, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-08 19:24:25', 0, 0, '2018-02-08 19:24:25'),
(80, 26, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-08 19:28:17', 0, 0, '2018-02-08 19:28:17'),
(81, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-08 19:28:17', 0, 0, '2018-02-08 19:28:17'),
(82, 84, 3, 'Successful send proforma invoice : 8436733', 'Successful send proforma invoice 2018-02-09 10:50:03 ', 0, 0, '2018-02-09 10:50:03'),
(83, 59, 3, 'Successful send proforma invoice : 8436733', 'Successful send proforma invoice 2018-02-09 10:50:05 ', 0, 0, '2018-02-09 10:50:05'),
(84, 117, 3, 'Successful send proforma invoice : 11737741', 'Successful send proforma invoice 2018-02-09 10:50:29 ', 0, 0, '2018-02-09 10:50:29'),
(85, 59, 3, 'Successful send proforma invoice : 11737741', 'Successful send proforma invoice 2018-02-09 10:50:30 ', 0, 0, '2018-02-09 10:50:30'),
(86, 82, 3, 'Successful send proforma invoice : 8232184', 'Successful send proforma invoice 2018-02-09 10:50:51 ', 0, 0, '2018-02-09 10:50:51'),
(87, 59, 3, 'Successful send proforma invoice : 8232184', 'Successful send proforma invoice 2018-02-09 10:50:52 ', 0, 0, '2018-02-09 10:50:52'),
(88, 117, 4, 'Pay Slip Upload for : 11737741', 'Pay Slip Upload for : 11737741', 0, 0, '2018-02-09 10:51:17'),
(89, 82, 4, 'Pay Slip Upload for : 8232184', 'Pay Slip Upload for : 8232184', 0, 0, '2018-02-09 10:51:36'),
(90, 84, 4, 'Pay Slip Upload for : 8436733', 'Pay Slip Upload for : 8436733', 0, 0, '2018-02-09 10:52:04'),
(91, 82, 4, 'Payment success for ordernumber : 8232184', 'Payment success for ordernumber : 8232184', 0, 0, '2018-02-09 10:52:16'),
(92, 59, 4, 'Payment success for ordernumber : 8232184', 'Payment success for ordernumber : 8232184', 0, 0, '2018-02-09 10:52:17'),
(93, 82, 4, 'Your order #8232184 has been successfully delivered', 'Your order #8232184 has been successfully delivered', 0, 0, '2018-02-09 11:13:24'),
(94, 59, 4, 'Your order #8232184 has been successfully delivered', 'Your order #8232184 has been successfully delivered', 0, 0, '2018-02-09 11:13:25'),
(95, 117, 4, 'Payment success for ordernumber : 11737741', 'Payment success for ordernumber : 11737741', 0, 0, '2018-02-09 11:14:11'),
(96, 59, 4, 'Payment success for ordernumber : 11737741', 'Payment success for ordernumber : 11737741', 0, 0, '2018-02-09 11:14:13'),
(97, 59, 4, 'Balance info for the month:January,Year :2018', 'Balance info for the month:January,Year :2018 is 278.7', 0, 0, '2018-02-09 11:41:01'),
(98, 26, 3, 'Successful bid in auction of product named apple', 'Successful bid in auction of product named apple on 2018-02-09 12:43:24', 0, 0, '2018-02-09 12:43:24'),
(99, 58, 3, 'One user successfully bid in auction of product named apple', 'One user successfully bid in auction of product named apple on 2018-02-09 12:43:24', 0, 0, '2018-02-09 12:43:24'),
(100, 26, 3, 'Successful bid in auction of product named apple', 'Successful bid in auction of product named apple on 2018-02-09 12:43:45', 0, 0, '2018-02-09 12:43:45'),
(101, 58, 3, 'One user successfully bid in auction of product named apple', 'One user successfully bid in auction of product named apple on 2018-02-09 12:43:45', 0, 0, '2018-02-09 12:43:45'),
(102, 59, 4, 'Balance info for the month:January,Year :2018', 'Balance info for the month:January,Year :2018 is 299.7', 0, 0, '2018-02-09 14:57:24'),
(103, 59, 4, 'Balance info for the month:January,Year :2018', 'Balance info for the month:January,Year :2018 is 305.7', 0, 0, '2018-02-09 15:01:02'),
(104, 26, 3, 'Successful send proforma invoice : 2653563', 'Successful send proforma invoice 2018-02-09 18:55:41 ', 0, 0, '2018-02-09 18:55:41'),
(105, 58, 3, 'Successful send proforma invoice : 2653563', 'Successful send proforma invoice 2018-02-09 18:55:44 ', 0, 0, '2018-02-09 18:55:44'),
(106, 26, 3, 'Successful send proforma invoice : 2657905', 'Successful send proforma invoice 2018-02-09 19:55:15 ', 0, 0, '2018-02-09 19:55:15'),
(107, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-09 19:55:17 ', 0, 0, '2018-02-09 19:55:17'),
(108, 26, 3, 'Successful send proforma invoice : 2653499', 'Successful send proforma invoice 2018-02-09 19:57:08 ', 0, 0, '2018-02-09 19:57:08'),
(109, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-09 19:57:10 ', 0, 0, '2018-02-09 19:57:10'),
(110, 26, 3, 'Successful send proforma invoice : 2658671', 'Successful send proforma invoice 2018-02-09 20:11:07 ', 0, 0, '2018-02-09 20:11:07'),
(111, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-09 20:11:08 ', 0, 0, '2018-02-09 20:11:08'),
(112, 26, 3, 'Successful send proforma invoice : 2631404', 'Successful send proforma invoice 2018-02-09 20:16:25 ', 0, 0, '2018-02-09 20:16:25'),
(113, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-02-09 20:16:27 ', 0, 0, '2018-02-09 20:16:27'),
(114, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:02:13', 0, 0, '2018-02-10 15:02:13'),
(115, 117, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:33:00', 0, 0, '2018-02-10 15:33:00'),
(116, 117, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:33:25', 0, 0, '2018-02-10 15:33:25'),
(117, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:34:51', 0, 0, '2018-02-10 15:34:51'),
(118, 58, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:37:41', 0, 0, '2018-02-10 15:37:41'),
(119, 117, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 15:57:40', 0, 0, '2018-02-10 15:57:40'),
(120, 117, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 16:07:05', 0, 0, '2018-02-10 16:07:05'),
(121, 117, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 16:10:17', 0, 0, '2018-02-10 16:10:17'),
(122, 85, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 16:25:55', 0, 0, '2018-02-10 16:25:55'),
(123, 118, 1, 'Successfully Registered', 'Successfully Registered on 2018-02-10 16:33:27', 0, 0, '2018-02-10 16:33:27'),
(124, 118, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-02-10 16:35:23', 0, 0, '2018-02-10 16:35:23'),
(125, 84, 4, 'Payslip rejected for ordernumber : 8436733', 'Payslip rejected for ordernumber : 8436733', 0, 0, '2018-02-10 17:31:30'),
(126, 84, 4, 'Pay Slip Upload for : 8436733', 'Pay Slip Upload for : 8436733', 0, 0, '2018-02-10 17:32:53'),
(127, 84, 4, 'Payslip rejected for ordernumber : 8436733', 'Payslip rejected for ordernumber : 8436733', 0, 0, '2018-02-10 17:36:31'),
(128, 82, 3, 'Successful send proforma invoice : 82139799', 'Successful send proforma invoice 2018-02-10 18:47:32 ', 0, 0, '2018-02-10 18:47:32'),
(129, 59, 3, 'One buyer buy your goods: litchi', 'One buyer buy your goods: litchi on 2018-02-10 18:47:33 ', 0, 0, '2018-02-10 18:47:33'),
(130, 59, 3, 'Successful bid in auction of product named apple', 'Successful bid in auction of product named apple on 2018-02-12 11:07:02', 0, 0, '2018-02-12 11:07:02'),
(131, 58, 3, 'One user successfully bid in auction of product named apple', 'One user successfully bid in auction of product named apple on 2018-02-12 11:07:02', 0, 0, '2018-02-12 11:07:02'),
(132, 59, 3, 'Successful send proforma invoice : 59277491', 'Successful send proforma invoice 2018-02-12 11:32:18 ', 0, 0, '2018-02-12 11:32:18'),
(133, 58, 3, 'One buyer buy your goods: Green Banana', 'One buyer buy your goods: Green Banana on 2018-02-12 11:32:20 ', 0, 0, '2018-02-12 11:32:20'),
(134, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-12 18:02:11', 0, 0, '2018-02-12 18:02:11'),
(135, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-12 18:02:11', 0, 0, '2018-02-12 18:02:11'),
(136, 117, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 18:58:07', 0, 0, '2018-02-12 18:58:07'),
(137, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 18:58:07', 0, 0, '2018-02-12 18:58:07'),
(138, 84, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 18:58:29', 0, 0, '2018-02-12 18:58:29'),
(139, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 18:58:29', 0, 0, '2018-02-12 18:58:29'),
(140, 82, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 18:59:19', 0, 0, '2018-02-12 18:59:19'),
(141, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 18:59:19', 0, 0, '2018-02-12 18:59:19'),
(142, 82, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 18:59:47', 0, 0, '2018-02-12 18:59:47'),
(143, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 18:59:47', 0, 0, '2018-02-12 18:59:47'),
(144, 117, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 19:01:15', 0, 0, '2018-02-12 19:01:15'),
(145, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 19:01:15', 0, 0, '2018-02-12 19:01:15'),
(146, 117, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 19:01:44', 0, 0, '2018-02-12 19:01:44'),
(147, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 19:01:44', 0, 0, '2018-02-12 19:01:44'),
(148, 79, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 19:15:37', 0, 0, '2018-02-12 19:15:37'),
(149, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 19:15:37', 0, 0, '2018-02-12 19:15:37'),
(150, 87, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 19:16:43', 0, 0, '2018-02-12 19:16:43'),
(151, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 19:16:43', 0, 0, '2018-02-12 19:16:43'),
(152, 82, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-12 19:17:49', 0, 0, '2018-02-12 19:17:49'),
(153, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-12 19:17:49', 0, 0, '2018-02-12 19:17:49'),
(154, 82, 3, 'Successful send proforma invoice : 8289931', 'Successful send proforma invoice 2018-02-13 11:29:56 ', 0, 0, '2018-02-13 11:29:56'),
(155, 59, 3, 'One buyer buy your goods: Redpeeper', 'One buyer buy your goods: Redpeeper on 2018-02-13 11:29:58 ', 0, 0, '2018-02-13 11:29:58'),
(156, 82, 3, 'Successful send proforma invoice : 8252728', 'Successful send proforma invoice 2018-02-21 11:47:56 ', 0, 0, '2018-02-21 11:47:56'),
(157, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-21 11:47:59 ', 0, 0, '2018-02-21 11:47:59'),
(158, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-21 12:33:12', 0, 0, '2018-02-21 12:33:12'),
(159, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-21 12:33:12', 0, 0, '2018-02-21 12:33:12'),
(160, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-21 12:33:48', 0, 0, '2018-02-21 12:33:48'),
(161, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-21 12:33:48', 0, 0, '2018-02-21 12:33:48'),
(162, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-21 12:34:24', 0, 0, '2018-02-21 12:34:24'),
(163, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-21 12:34:24', 0, 0, '2018-02-21 12:34:24'),
(164, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-21 12:34:47', 0, 0, '2018-02-21 12:34:47'),
(165, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-21 12:34:47', 0, 0, '2018-02-21 12:34:47'),
(166, 82, 3, 'Successful bid in auction of product named Redpeeper', 'Successful bid in auction of product named Redpeeper on 2018-02-21 12:40:28', 0, 0, '2018-02-21 12:40:28'),
(167, 59, 3, 'One user successfully bid in auction of product named Redpeeper', 'One user successfully bid in auction of product named Redpeeper on 2018-02-21 12:40:28', 0, 0, '2018-02-21 12:40:28'),
(168, 117, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-02-21 15:12:41', 0, 0, '2018-02-21 15:12:41'),
(169, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-02-21 15:12:41', 0, 0, '2018-02-21 15:12:41'),
(170, 82, 4, 'Order #8252728 cancelled', 'Order #8252728 cancelled', 0, 0, '2018-02-21 17:04:16'),
(171, 58, 4, 'order #8252728 cancelled', 'order #8252728 cancelled', 0, 0, '2018-02-21 17:04:17'),
(172, 82, 4, 'Order #8289931 cancelled', 'Order #8289931 cancelled', 0, 0, '2018-02-21 17:06:14'),
(173, 59, 4, 'order #8289931 cancelled', 'order #8289931 cancelled', 0, 0, '2018-02-21 17:06:15'),
(174, 82, 4, 'Pay Slip Upload for : 8289931', 'Pay Slip Upload for : 8289931', 0, 0, '2018-02-21 17:07:06'),
(175, 82, 3, 'Successful send proforma invoice : 8254858', 'Successful send proforma invoice 2018-02-21 17:08:55 ', 0, 0, '2018-02-21 17:08:55'),
(176, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-21 17:08:57 ', 0, 0, '2018-02-21 17:08:57'),
(177, 82, 4, 'Order #8254858 cancelled', 'Order #8254858 cancelled', 0, 0, '2018-02-21 17:09:34'),
(178, 58, 4, 'order #8254858 cancelled', 'order #8254858 cancelled', 0, 0, '2018-02-21 17:09:36'),
(179, 82, 3, 'Successful send proforma invoice : 8259918', 'Successful send proforma invoice 2018-02-21 17:10:51 ', 0, 0, '2018-02-21 17:10:51'),
(180, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-21 17:10:53 ', 0, 0, '2018-02-21 17:10:53'),
(181, 82, 4, 'Pay Slip Upload for : 8259918', 'Pay Slip Upload for : 8259918', 0, 0, '2018-02-21 17:11:30'),
(182, 82, 4, 'Pay Slip Upload for : 8259918', 'Pay Slip Upload for : 8259918', 0, 0, '2018-02-21 17:18:59'),
(183, 82, 4, 'Payment success for ordernumber : 8259918', 'Payment success for ordernumber : 8259918', 0, 0, '2018-02-21 17:21:28'),
(184, 58, 4, 'Payment success for ordernumber : 8259918', 'Payment success for ordernumber : 8259918', 0, 0, '2018-02-21 17:21:30'),
(185, 82, 4, 'Order #8259918 cancelled', 'Order #8259918 cancelled', 0, 0, '2018-02-21 17:24:52'),
(186, 58, 4, 'order #8259918 cancelled', 'order #8259918 cancelled', 0, 0, '2018-02-21 17:24:53'),
(187, 82, 4, 'Payment success for ordernumber : 8289931', 'Payment success for ordernumber : 8289931', 0, 0, '2018-02-21 17:27:11'),
(188, 59, 4, 'Payment success for ordernumber : 8289931', 'Payment success for ordernumber : 8289931', 0, 0, '2018-02-21 17:27:12'),
(189, 82, 3, 'Successful send proforma invoice : 8259896', 'Successful send proforma invoice 2018-02-21 17:28:47 ', 0, 0, '2018-02-21 17:28:47'),
(190, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-21 17:28:48 ', 0, 0, '2018-02-21 17:28:48'),
(191, 82, 4, 'Pay Slip Upload for : 8259896', 'Pay Slip Upload for : 8259896', 0, 0, '2018-02-21 17:30:46'),
(192, 82, 4, 'Payment success for ordernumber : 8259896', 'Payment success for ordernumber : 8259896', 0, 0, '2018-02-21 17:31:04'),
(193, 58, 4, 'Payment success for ordernumber : 8259896', 'Payment success for ordernumber : 8259896', 0, 0, '2018-02-21 17:31:05'),
(194, 82, 4, 'Order #8259896 cancelled', 'Order #8259896 cancelled', 0, 0, '2018-02-21 17:31:37'),
(195, 58, 4, 'order #8259896 cancelled', 'order #8259896 cancelled', 0, 0, '2018-02-21 17:31:38'),
(196, 82, 3, 'Successful send proforma invoice : 8252566', 'Successful send proforma invoice 2018-02-21 17:32:40 ', 0, 0, '2018-02-21 17:32:40'),
(197, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-21 17:32:41 ', 0, 0, '2018-02-21 17:32:41'),
(198, 82, 4, 'Pay Slip Upload for : 8252566', 'Pay Slip Upload for : 8252566', 0, 0, '2018-02-21 17:33:11'),
(199, 82, 4, 'Payment success for ordernumber : 8252566', 'Payment success for ordernumber : 8252566', 0, 0, '2018-02-21 17:33:15'),
(200, 58, 4, 'Payment success for ordernumber : 8252566', 'Payment success for ordernumber : 8252566', 0, 0, '2018-02-21 17:33:21'),
(201, 82, 4, 'Your order #8252566 has been successfully delivered', 'Your order #8252566 has been successfully delivered', 0, 0, '2018-02-21 17:33:47'),
(202, 58, 4, 'Your order #8252566 has been successfully delivered', 'Your order #8252566 has been successfully delivered', 0, 0, '2018-02-21 17:33:49'),
(203, 82, 4, 'Your order #8252566 returned successfully', 'Your order #8252566 returned successfully', 0, 0, '2018-02-21 17:35:53'),
(204, 58, 4, 'Your order #8252566 returned successfully', 'Your order #8252566 returned successfully', 0, 0, '2018-02-21 17:35:54'),
(205, 58, 4, 'Auction Pending Confirm Email : 5843001', 'Auction Pending Confirm Emailn : 5843001', 0, 0, '2018-02-23 14:49:10'),
(206, 26, 4, 'Auction Pending Confirm Email : 2645795', 'Auction Pending Confirm Emailn : 2645795', 0, 0, '2018-02-23 14:49:12'),
(207, 82, 4, 'Auction Pending Confirm Email : 8249317', 'Auction Pending Confirm Emailn : 8249317', 0, 0, '2018-02-23 14:49:13'),
(208, 82, 4, 'Auction Pending Confirm Email : 8216844', 'Auction Pending Confirm Emailn : 8216844', 0, 0, '2018-02-23 14:49:14'),
(209, 58, 4, 'Auction Pending Confirm Email : 5818710', 'Auction Pending Confirm Emailn : 5818710', 0, 0, '2018-02-23 14:49:15'),
(210, 117, 4, 'Auction Pending Confirm Email : 11714206', 'Auction Pending Confirm Emailn : 11714206', 0, 0, '2018-02-23 14:49:16'),
(211, 58, 4, 'Remind Order Payment : 5836612', 'Remind Order Payment : 5836612', 0, 0, '2018-02-23 14:49:33'),
(212, 58, 4, 'Remind Order Payment : 5831822', 'Remind Order Payment : 5831822', 0, 0, '2018-02-23 14:49:34'),
(213, 82, 4, 'Remind Order Payment : 8269159', 'Remind Order Payment : 8269159', 0, 0, '2018-02-23 14:49:36'),
(214, 84, 4, 'Remind Order Payment : 8436733', 'Remind Order Payment : 8436733', 0, 0, '2018-02-23 14:49:37'),
(215, 26, 4, 'Remind Order Payment : 2655134', 'Remind Order Payment : 2655134', 0, 0, '2018-02-23 14:49:38'),
(216, 26, 4, 'Remind Order Payment : 2652188', 'Remind Order Payment : 2652188', 0, 0, '2018-02-23 14:49:39'),
(217, 26, 4, 'Remind Order Payment : 2653563', 'Remind Order Payment : 2653563', 0, 0, '2018-02-23 14:49:40'),
(218, 26, 4, 'Remind Order Payment : 2658808', 'Remind Order Payment : 2658808', 0, 0, '2018-02-23 14:49:41'),
(219, 26, 4, 'Remind Order Payment : 2658671', 'Remind Order Payment : 2658671', 0, 0, '2018-02-23 14:49:42'),
(220, 26, 4, 'Remind Order Payment : 2631404', 'Remind Order Payment : 2631404', 0, 0, '2018-02-23 14:49:43'),
(221, 82, 4, 'Remind Order Payment : 82139799', 'Remind Order Payment : 82139799', 0, 0, '2018-02-23 14:49:45'),
(222, 59, 4, 'Remind Order Payment : 59277491', 'Remind Order Payment : 59277491', 0, 0, '2018-02-23 14:49:46'),
(223, 82, 4, 'Remind Order Payment : 8252728', 'Remind Order Payment : 8252728', 0, 0, '2018-02-23 14:49:47'),
(224, 82, 4, 'Remind Order Payment : 8254858', 'Remind Order Payment : 8254858', 0, 0, '2018-02-23 14:49:48'),
(225, 58, 4, 'Order Cancelled : 5836612', 'Order Cancelled due to payment delay   : 5836612', 0, 0, '2018-02-23 14:50:05'),
(226, 58, 4, 'Order Cancelled : 5831822', 'Order Cancelled due to payment delay   : 5831822', 0, 0, '2018-02-23 14:50:06'),
(227, 82, 4, 'Order Cancelled : 8269159', 'Order Cancelled due to payment delay   : 8269159', 0, 0, '2018-02-23 14:50:08'),
(228, 84, 4, 'Order Cancelled : 8436733', 'Order Cancelled due to payment delay   : 8436733', 0, 0, '2018-02-23 14:50:09'),
(229, 26, 4, 'Order Cancelled : 2655134', 'Order Cancelled due to payment delay   : 2655134', 0, 0, '2018-02-23 14:50:10'),
(230, 26, 4, 'Order Cancelled : 2652188', 'Order Cancelled due to payment delay   : 2652188', 0, 0, '2018-02-23 14:50:11'),
(231, 26, 4, 'Order Cancelled : 2653563', 'Order Cancelled due to payment delay   : 2653563', 0, 0, '2018-02-23 14:50:12'),
(232, 26, 4, 'Order Cancelled : 2658808', 'Order Cancelled due to payment delay   : 2658808', 0, 0, '2018-02-23 14:50:13'),
(233, 26, 4, 'Order Cancelled : 2658671', 'Order Cancelled due to payment delay   : 2658671', 0, 0, '2018-02-23 14:50:15'),
(234, 26, 4, 'Order Cancelled : 2631404', 'Order Cancelled due to payment delay   : 2631404', 0, 0, '2018-02-23 14:50:16'),
(235, 82, 4, 'Order Cancelled : 82139799', 'Order Cancelled due to payment delay   : 82139799', 0, 0, '2018-02-23 14:50:17'),
(236, 59, 4, 'Order Cancelled : 59277491', 'Order Cancelled due to payment delay   : 59277491', 0, 0, '2018-02-23 14:50:18'),
(237, 58, 5, 'Product Return notification : apple', 'Product return notification due to expair closer : apple Product code :08-025828', 0, 0, '2018-02-23 14:50:27'),
(238, 58, 5, 'Product Return notification : Orange', 'Product return notification due to expair closer : Orange Product code :08-025826', 0, 0, '2018-02-23 14:50:28'),
(239, 59, 5, 'Product Return notification : litchi', 'Product return notification due to expair closer : litchi Product code :06-025913', 0, 0, '2018-02-23 14:50:29'),
(240, 59, 5, 'Product Return notification : Broccoli', 'Product return notification due to expair closer : Broccoli Product code :06-025912', 0, 0, '2018-02-23 14:50:30'),
(241, 59, 5, 'Product Return notification : bitter melon', 'Product return notification due to expair closer : bitter melon Product code :06-025910', 0, 0, '2018-02-23 14:50:31'),
(242, 59, 5, 'Product Return notification : Watermelon', 'Product return notification due to expair closer : Watermelon Product code :06-02599', 0, 0, '2018-02-23 14:50:32'),
(243, 59, 5, 'Product Return notification : Redpeeper', 'Product return notification due to expair closer : Redpeeper Product code :06-02598', 0, 0, '2018-02-23 14:50:34'),
(244, 58, 5, 'Product Return notification : Strawberry', 'Product return notification due to expair closer : Strawberry Product code :06-02586', 0, 0, '2018-02-23 14:50:35'),
(245, 58, 5, 'Product Return notification : Yellow Pepper', 'Product return notification due to expair closer : Yellow Pepper Product code :06-02585', 0, 0, '2018-02-23 14:50:36'),
(246, 117, 4, 'Selected for Auction : 117134110', 'Selected for Auction : 117134110', 0, 0, '2018-02-23 14:50:57'),
(247, 82, 4, 'Selected for Auction : 82133326', 'Selected for Auction : 82133326', 0, 0, '2018-02-23 14:56:59'),
(248, 87, 4, 'Selected for Auction : 87137062', 'Selected for Auction : 87137062', 0, 0, '2018-02-23 14:57:00'),
(249, 79, 4, 'Selected for Auction : 79136060', 'Selected for Auction : 79136060', 0, 0, '2018-02-23 14:57:02'),
(250, 82, 4, 'Selected for Auction : 8288063', 'Selected for Auction : 8288063', 0, 0, '2018-02-23 14:57:54'),
(251, 58, 4, 'Selected for Auction : 5887540', 'Selected for Auction : 5887540', 0, 0, '2018-02-23 14:57:55'),
(252, 26, 3, 'Successful bid in auction of product named Watermelon', 'Successful bid in auction of product named Watermelon on 2018-02-26 15:12:55', 0, 0, '2018-02-26 15:12:55'),
(253, 59, 3, 'One user successfully bid in auction of product named Watermelon', 'One user successfully bid in auction of product named Watermelon on 2018-02-26 15:12:55', 0, 0, '2018-02-26 15:12:55'),
(254, 26, 3, 'Successful bid in auction of product named bitter melon', 'Successful bid in auction of product named bitter melon on 2018-02-26 15:13:29', 0, 0, '2018-02-26 15:13:29'),
(255, 59, 3, 'One user successfully bid in auction of product named bitter melon', 'One user successfully bid in auction of product named bitter melon on 2018-02-26 15:13:29', 0, 0, '2018-02-26 15:13:29'),
(256, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-26 17:36:24', 0, 0, '2018-02-26 17:36:24'),
(257, 82, 3, 'Successful send proforma invoice : 8254447', 'Successful send proforma invoice 2018-02-26 17:40:25 ', 0, 0, '2018-02-26 17:40:25'),
(258, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-26 17:40:27 ', 0, 0, '2018-02-26 17:40:27'),
(259, 82, 4, 'Order #8254447 cancelled', 'Order #8254447 cancelled', 0, 0, '2018-02-26 17:41:27'),
(260, 58, 4, 'order #8254447 cancelled', 'order #8254447 cancelled', 0, 0, '2018-02-26 17:41:28'),
(261, 82, 3, 'Successful send proforma invoice : 8258087', 'Successful send proforma invoice 2018-02-26 18:34:15 ', 0, 0, '2018-02-26 18:34:15'),
(262, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-26 18:34:17 ', 0, 0, '2018-02-26 18:34:17'),
(263, 82, 4, 'Pay Slip Upload for : 8258087', 'Pay Slip Upload for : 8258087', 0, 0, '2018-02-26 18:36:14'),
(264, 82, 4, 'Pay Slip Upload for : 8258087', 'Pay Slip Upload for : 8258087', 0, 0, '2018-02-26 18:39:57'),
(265, 82, 4, 'Payment success for ordernumber : 8258087', 'Payment success for ordernumber : 8258087', 0, 0, '2018-02-26 18:40:52'),
(266, 58, 4, 'Payment success for ordernumber : 8258087', 'Payment success for ordernumber : 8258087', 0, 0, '2018-02-26 18:40:53'),
(267, 82, 4, 'Order #8258087 cancelled', 'Order #8258087 cancelled', 0, 0, '2018-02-26 18:42:31'),
(268, 58, 4, 'order #8258087 cancelled', 'order #8258087 cancelled', 0, 0, '2018-02-26 18:42:32'),
(269, 82, 3, 'Successful send proforma invoice : 8259773', 'Successful send proforma invoice 2018-02-26 18:43:52 ', 0, 0, '2018-02-26 18:43:52'),
(270, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-02-26 18:43:54 ', 0, 0, '2018-02-26 18:43:54'),
(271, 82, 4, 'Pay Slip Upload for : 8259773', 'Pay Slip Upload for : 8259773', 0, 0, '2018-02-26 18:44:44'),
(272, 82, 4, 'Pay Slip Upload for : 8259773', 'Pay Slip Upload for : 8259773', 0, 0, '2018-02-26 18:46:47'),
(273, 82, 4, 'Payment success for ordernumber : 8259773', 'Payment success for ordernumber : 8259773', 0, 0, '2018-02-26 18:53:54'),
(274, 58, 4, 'Payment success for ordernumber : 8259773', 'Payment success for ordernumber : 8259773', 0, 0, '2018-02-26 18:53:56'),
(275, 82, 4, 'Your order #8259773 has been successfully delivered', 'Your order #8259773 has been successfully delivered', 0, 0, '2018-02-26 18:58:59'),
(276, 58, 4, 'Your order #8259773 has been successfully delivered', 'Your order #8259773 has been successfully delivered', 0, 0, '2018-02-26 18:59:01'),
(277, 82, 3, 'Successful bid in auction of product named bitter melon', 'Successful bid in auction of product named bitter melon on 2018-02-27 11:22:00', 0, 0, '2018-02-27 11:22:00'),
(278, 59, 3, 'One user successfully bid in auction of product named bitter melon', 'One user successfully bid in auction of product named bitter melon on 2018-02-27 11:22:00', 0, 0, '2018-02-27 11:22:00'),
(279, 26, 4, 'Selected for Auction : 2698536', 'Selected for Auction : 2698536', 0, 0, '2018-02-27 12:11:45'),
(280, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-27 12:24:00', 0, 0, '2018-02-27 12:24:00'),
(281, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-27 12:25:04', 0, 0, '2018-02-27 12:25:04'),
(282, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-27 12:26:15', 0, 0, '2018-02-27 12:26:15'),
(283, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-27 12:29:14', 0, 0, '2018-02-27 12:29:14'),
(284, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-02-27 12:30:39', 0, 0, '2018-02-27 12:30:39'),
(285, 82, 4, 'Selected for Auction : 82101086', 'Selected for Auction : 82101086', 0, 0, '2018-02-27 17:00:16'),
(286, 82, 4, 'Your order #8259773 returned successfully', 'Your order #8259773 returned successfully', 0, 0, '2018-02-27 17:04:11'),
(287, 58, 4, 'Your order #8259773 returned successfully', 'Your order #8259773 returned successfully', 0, 0, '2018-02-27 17:04:28'),
(288, 82, 3, 'Successful send proforma invoice : 82101086', 'Successful send proforma invoice 2018-02-27 17:12:46 ', 0, 0, '2018-02-27 17:12:46'),
(289, 59, 3, 'One buyer buy your goods: bitter melon', 'One buyer buy your goods: bitter melon on 2018-02-27 17:12:49 ', 0, 0, '2018-02-27 17:12:49'),
(290, 59, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-02-27 17:23:08', 0, 0, '2018-02-27 17:23:08'),
(291, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-02-27 17:23:08', 0, 0, '2018-02-27 17:23:08'),
(292, 82, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-02-28 18:22:41', 0, 0, '2018-02-28 18:22:41'),
(293, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-02-28 18:22:41', 0, 0, '2018-02-28 18:22:41'),
(294, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:20:42', 0, 0, '2018-03-01 10:20:42'),
(295, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:23:11', 0, 0, '2018-03-01 10:23:11'),
(296, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:33:00', 0, 0, '2018-03-01 10:33:00'),
(297, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:33:24', 0, 0, '2018-03-01 10:33:24'),
(298, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:36:13', 0, 0, '2018-03-01 10:36:13'),
(299, 59, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-01 10:41:01', 0, 0, '2018-03-01 10:41:01'),
(300, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-01 10:41:01', 0, 0, '2018-03-01 10:41:01'),
(301, 59, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-01 10:41:53', 0, 0, '2018-03-01 10:41:53'),
(302, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-01 10:41:53', 0, 0, '2018-03-01 10:41:53'),
(303, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:42:21', 0, 0, '2018-03-01 10:42:21'),
(304, 82, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-01 10:42:52', 0, 0, '2018-03-01 10:42:52'),
(305, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-01 10:42:52', 0, 0, '2018-03-01 10:42:52'),
(306, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:45:33', 0, 0, '2018-03-01 10:45:33'),
(307, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:51:34', 0, 0, '2018-03-01 10:51:34'),
(308, 59, 3, 'Successful send proforma invoice : 5968363', 'Successful send proforma invoice 2018-03-01 10:53:22 ', 0, 0, '2018-03-01 10:53:22'),
(309, 58, 3, 'One buyer buy your goods: Strawberry', 'One buyer buy your goods: Strawberry on 2018-03-01 10:53:24 ', 0, 0, '2018-03-01 10:53:24'),
(310, 59, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-01 10:53:47', 0, 0, '2018-03-01 10:53:47'),
(311, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-01 10:53:47', 0, 0, '2018-03-01 10:53:47'),
(312, 59, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:55:05', 0, 0, '2018-03-01 10:55:05'),
(313, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 10:55:46', 0, 0, '2018-03-01 10:55:46'),
(314, 82, 3, 'Successful send proforma invoice : 8254582', 'Successful send proforma invoice 2018-03-01 11:40:31 ', 0, 0, '2018-03-01 11:40:31'),
(315, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-03-01 11:40:33 ', 0, 0, '2018-03-01 11:40:33'),
(316, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 11:44:48', 0, 0, '2018-03-01 11:44:48'),
(317, 82, 4, 'Pay Slip Upload for : 8254582', 'Pay Slip Upload for : 8254582', 0, 0, '2018-03-01 11:50:09'),
(318, 82, 4, 'Pay Slip Upload for : 8254582', 'Pay Slip Upload for : 8254582', 0, 0, '2018-03-01 11:56:27'),
(319, 82, 4, 'Payment success for ordernumber : 8254582', 'Payment success for ordernumber : 8254582', 0, 0, '2018-03-01 11:57:42'),
(320, 58, 4, 'Payment success for ordernumber : 8254582', 'Payment success for ordernumber : 8254582', 0, 0, '2018-03-01 11:57:44'),
(321, 82, 4, 'Your order #8254582 has been successfully delivered', 'Your order #8254582 has been successfully delivered', 0, 0, '2018-03-01 12:15:04'),
(322, 58, 4, 'Your order #8254582 has been successfully delivered', 'Your order #8254582 has been successfully delivered', 0, 0, '2018-03-01 12:15:06'),
(323, 82, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-01 12:27:06', 0, 0, '2018-03-01 12:27:06'),
(324, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-01 12:27:06', 0, 0, '2018-03-01 12:27:06'),
(325, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 12:38:16', 0, 0, '2018-03-01 12:38:16'),
(326, 84, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-01 12:42:08'),
(327, 26, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-01 12:42:09', 0, 0, '2018-03-01 12:42:09'),
(328, 84, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 12:51:37', 0, 0, '2018-03-01 12:51:37'),
(329, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 12:51:37', 0, 0, '2018-03-01 12:51:37'),
(330, 82, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 12:52:38', 0, 0, '2018-03-01 12:52:38'),
(331, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 12:52:38', 0, 0, '2018-03-01 12:52:38'),
(332, 84, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 12:53:20', 0, 0, '2018-03-01 12:53:20'),
(333, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 12:53:20', 0, 0, '2018-03-01 12:53:20'),
(334, 82, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 12:53:50', 0, 0, '2018-03-01 12:53:50'),
(335, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 12:53:50', 0, 0, '2018-03-01 12:53:50'),
(336, 84, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 13:00:27', 0, 0, '2018-03-01 13:00:27'),
(337, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 13:00:27', 0, 0, '2018-03-01 13:00:27'),
(338, 84, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-01 13:03:49', 0, 0, '2018-03-01 13:03:49'),
(339, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-01 13:03:49', 0, 0, '2018-03-01 13:03:49'),
(340, 84, 4, 'Selected for Auction : 84511480', 'Selected for Auction : 84511480', 0, 0, '2018-03-01 13:04:12'),
(341, 84, 3, 'Successful send proforma invoice : 84511480', 'Successful send proforma invoice 2018-03-01 13:07:40 ', 0, 0, '2018-03-01 13:07:40'),
(342, 26, 3, 'One buyer buy your goods: Chicken Sausage', 'One buyer buy your goods: Chicken Sausage on 2018-03-01 13:07:42 ', 0, 0, '2018-03-01 13:07:42'),
(343, 84, 4, 'Pay Slip Upload for : 84511480', 'Pay Slip Upload for : 84511480', 0, 0, '2018-03-01 13:09:09'),
(344, 84, 4, 'Payment success for ordernumber : 84511480', 'Payment success for ordernumber : 84511480', 0, 0, '2018-03-01 13:10:11'),
(345, 26, 4, 'Payment success for ordernumber : 84511480', 'Payment success for ordernumber : 84511480', 0, 0, '2018-03-01 13:10:12'),
(346, 84, 4, 'Your order #84511480 has been successfully delivered', 'Your order #84511480 has been successfully delivered', 0, 0, '2018-03-01 13:12:14'),
(347, 26, 4, 'Your order #84511480 has been successfully delivered', 'Your order #84511480 has been successfully delivered', 0, 0, '2018-03-01 13:12:15'),
(348, 59, 4, 'Balance info for the month:January,Year :2018', 'Balance info for the month:January,Year :2018 is 158.7', 0, 0, '2018-03-01 13:16:40'),
(349, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-01 16:07:24', 0, 0, '2018-03-01 16:07:24'),
(350, 126, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-05 18:09:41', 0, 0, '2018-03-05 18:09:41'),
(351, 127, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-05 19:12:10', 0, 0, '2018-03-05 19:12:10'),
(352, 127, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-05 19:21:23', 0, 0, '2018-03-05 19:21:23');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(353, 127, 3, 'Successful send proforma invoice : 12753902', 'Successful send proforma invoice 2018-03-05 19:25:22 ', 0, 0, '2018-03-05 19:25:22'),
(354, 58, 3, 'One buyer buy your goods: Yellow Pepper', 'One buyer buy your goods: Yellow Pepper on 2018-03-05 19:25:23 ', 0, 0, '2018-03-05 19:25:23'),
(355, 128, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-06 11:26:17', 0, 0, '2018-03-06 11:26:17'),
(356, 128, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-06 11:49:11', 0, 0, '2018-03-06 11:49:11'),
(357, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-06 11:49:11', 0, 0, '2018-03-06 11:49:11'),
(358, 129, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-06 12:23:52', 0, 0, '2018-03-06 12:23:52'),
(359, 129, 3, 'Successful send proforma invoice : 12965297', 'Successful send proforma invoice 2018-03-06 13:33:05 ', 0, 0, '2018-03-06 13:33:05'),
(360, 58, 3, 'One buyer buy your goods: Strawberry', 'One buyer buy your goods: Strawberry on 2018-03-06 13:33:07 ', 0, 0, '2018-03-06 13:33:07'),
(361, 130, 3, 'Successful bid', 'Successful bid on 2018-03-06 14:13:48', 0, 0, '2018-03-06 14:13:48'),
(362, 58, 3, 'Successful bid', 'Successful bid on 2018-03-06 14:13:49', 0, 0, '2018-03-06 14:13:49'),
(363, 131, 3, 'Successful bid', 'Successful bid on 2018-03-06 14:23:59', 0, 0, '2018-03-06 14:23:59'),
(364, 58, 3, 'Successful bid', 'Successful bid on 2018-03-06 14:24:01', 0, 0, '2018-03-06 14:24:01'),
(365, 132, 3, 'Successful bid', 'Successful bid on 2018-03-06 15:29:53', 0, 0, '2018-03-06 15:29:53'),
(366, 58, 3, 'Successful bid', 'Successful bid on 2018-03-06 15:29:54', 0, 0, '2018-03-06 15:29:54'),
(367, 129, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-06 15:33:41', 0, 0, '2018-03-06 15:33:41'),
(368, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-06 15:33:41', 0, 0, '2018-03-06 15:33:41'),
(369, 133, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-06 16:20:55', 0, 0, '2018-03-06 16:20:55'),
(370, 133, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-06 16:31:37', 0, 0, '2018-03-06 16:31:37'),
(371, 129, 4, 'My requrement : #Fruit ', 'I want Fruit category product of unit 1 expected price per carton 20 with in  2018-03-13 ', 0, 0, '2018-03-06 17:17:13'),
(372, 133, 4, 'Buyer requrement : #Fruit ', 'Buyer want Fruit category product of unit 1 expected price per carton 20 with in  2018-03-13 ', 0, 0, '2018-03-06 17:17:14'),
(373, 129, 4, 'My requrement : #Vegetable ', 'I want Vegetable category product of unit 2 expected price per carton 10 with in  2018-03-13 ', 0, 0, '2018-03-06 17:23:10'),
(374, 26, 4, 'Buyer requrement : #Vegetable ', 'Buyer want Vegetable category product of unit 2 expected price per carton 10 with in  2018-03-13 ', 0, 0, '2018-03-06 17:23:11'),
(375, 133, 4, 'My requrement : #Sea Fish ', 'I want Sea Fish category product of unit 2 expected price per carton 19 with in  2018-04-30', 0, 0, '2018-03-06 19:01:14'),
(376, 40, 4, 'Buyer requrement : #Sea Fish ', 'Buyer want Sea Fish category product of unit 2 expected price per carton 19 with in  2018-04-30', 0, 0, '2018-03-06 19:01:15'),
(377, 87, 4, 'Buyer requrement : #Sea Fish ', 'Buyer want Sea Fish category product of unit 2 expected price per carton 19 with in  2018-04-30', 0, 0, '2018-03-06 19:01:16'),
(378, 26, 4, 'Buyer requrement : #Sea Fish ', 'Buyer want Sea Fish category product of unit 2 expected price per carton 19 with in  2018-04-30', 0, 0, '2018-03-06 19:01:18'),
(379, 133, 1, 'Reject bank details', 'Reject bank details ', 0, 0, '2018-03-06 20:05:46'),
(380, 133, 1, 'Reject bank details', 'Reject bank details ', 0, 0, '2018-03-06 20:08:20'),
(381, 133, 1, 'Successfully varify bank details', 'Successfully varify bank details ', 0, 0, '2018-03-06 20:13:21'),
(382, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-06 20:18:29'),
(383, 133, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-06 20:18:30', 0, 0, '2018-03-06 20:18:30'),
(384, 129, 4, 'My requrement : #Fruit ', 'I want Fruit category product of unit 1 expected price per carton 20 with in  2018-05-31', 0, 0, '2018-03-06 20:25:29'),
(385, 133, 4, 'Buyer requrement : #Fruit ', 'Buyer want Fruit category product of unit 1 expected price per carton 20 with in  2018-05-31', 0, 0, '2018-03-06 20:25:30'),
(386, 133, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-06 20:28:31', 0, 0, '2018-03-06 20:28:31'),
(387, 129, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-06 20:32:21', 0, 0, '2018-03-06 20:32:21'),
(388, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-06 20:32:21', 0, 0, '2018-03-06 20:32:21'),
(389, 129, 3, 'Successful send proforma invoice : 12962202', 'Successful send proforma invoice 2018-03-06 20:37:59 ', 0, 0, '2018-03-06 20:37:59'),
(390, 58, 3, 'One buyer buy your goods: Strawberry', 'One buyer buy your goods: Strawberry on 2018-03-06 20:38:01 ', 0, 0, '2018-03-06 20:38:01'),
(391, 133, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-07 12:51:29', 0, 0, '2018-03-07 12:51:29'),
(392, 133, 2, 'Product not verified', 'Please give proper document', 0, 0, '2018-03-07 12:56:15'),
(393, 133, 2, 'Product Reject', 'Please give proper document with proper product', 0, 0, '2018-03-07 12:58:59'),
(394, 133, 2, 'Product Reject', 'Please give proper document with proper product please please', 0, 0, '2018-03-07 13:01:08'),
(395, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-07 13:02:43'),
(396, 133, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-07 13:02:44', 0, 0, '2018-03-07 13:02:44'),
(397, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-07 13:03:46'),
(398, 133, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-07 13:03:47', 0, 0, '2018-03-07 13:03:47'),
(399, 129, 3, 'Successful send proforma invoice : 129563501', 'Successful send proforma invoice 2018-03-07 13:12:15 ', 0, 0, '2018-03-07 13:12:15'),
(400, 133, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-07 13:12:17 ', 0, 0, '2018-03-07 13:12:17'),
(401, 129, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-07 13:20:38', 0, 0, '2018-03-07 13:20:38'),
(402, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-07 13:20:38', 0, 0, '2018-03-07 13:20:38'),
(403, 129, 4, 'Selected for Auction : 129541328', 'Selected for Auction : 129541328', 0, 0, '2018-03-07 13:35:50'),
(404, 129, 4, 'Pay Slip Upload for : 129563501', 'Pay Slip Upload for : 129563501', 0, 0, '2018-03-07 13:47:14'),
(405, 129, 4, 'Payslip rejected for ordernumber : 129563501', 'Payslip rejected for ordernumber : 129563501', 0, 0, '2018-03-07 15:22:45'),
(406, 129, 4, 'Pay Slip Upload for : 129563501', 'Pay Slip Upload for : 129563501', 0, 0, '2018-03-07 15:25:01'),
(407, 129, 4, 'Payment success for ordernumber : 129563501', 'Payment success for ordernumber : 129563501', 0, 0, '2018-03-07 15:25:39'),
(408, 133, 4, 'Payment success for ordernumber : 129563501', 'Payment success for ordernumber : 129563501', 0, 0, '2018-03-07 15:25:40'),
(409, 129, 4, 'Order #129563501 cancelled', 'Order #129563501 cancelled', 0, 0, '2018-03-07 15:29:32'),
(410, 133, 4, 'order #129563501 cancelled', 'order #129563501 cancelled', 0, 0, '2018-03-07 15:29:33'),
(411, 134, 3, 'Successful bid', 'Successful bid on 2018-03-07 15:40:46', 0, 0, '2018-03-07 15:40:46'),
(412, 58, 3, 'Successful bid', 'Successful bid on 2018-03-07 15:40:47', 0, 0, '2018-03-07 15:40:47'),
(413, 129, 4, 'Pay Slip Upload for : 12962202', 'Pay Slip Upload for : 12962202', 0, 0, '2018-03-07 17:56:43'),
(414, 129, 4, 'Payment success for ordernumber : 12962202', 'Payment success for ordernumber : 12962202', 0, 0, '2018-03-07 17:58:50'),
(415, 58, 4, 'Payment success for ordernumber : 12962202', 'Payment success for ordernumber : 12962202', 0, 0, '2018-03-07 17:58:52'),
(416, 129, 4, 'Your order #12962202 has been successfully delivered', 'Your order #12962202 has been successfully delivered', 0, 0, '2018-03-07 17:59:04'),
(417, 58, 4, 'Your order #12962202 has been successfully delivered', 'Your order #12962202 has been successfully delivered', 0, 0, '2018-03-07 17:59:05'),
(418, 129, 4, 'Your order #12962202 returned successfully', 'Your order #12962202 returned successfully', 0, 0, '2018-03-07 18:00:28'),
(419, 58, 4, 'Your order #12962202 returned successfully', 'Your order #12962202 returned successfully', 0, 0, '2018-03-07 18:00:29'),
(420, 26, 3, 'Successful bid in auction of product named bitter melon', 'Successful bid in auction of product named bitter melon on 2018-03-07 18:02:52', 0, 0, '2018-03-07 18:02:52'),
(421, 59, 3, 'One user successfully bid in auction of product named bitter melon', 'One user successfully bid in auction of product named bitter melon on 2018-03-07 18:02:52', 0, 0, '2018-03-07 18:02:52'),
(422, 26, 3, 'Successful bid in auction of product named Watermelon', 'Successful bid in auction of product named Watermelon on 2018-03-07 19:05:45', 0, 0, '2018-03-07 19:05:45'),
(423, 59, 3, 'One user successfully bid in auction of product named Watermelon', 'One user successfully bid in auction of product named Watermelon on 2018-03-07 19:05:45', 0, 0, '2018-03-07 19:05:45'),
(424, 135, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-08 12:28:42', 0, 0, '2018-03-08 12:28:42'),
(425, 129, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-08 13:54:18', 0, 0, '2018-03-08 13:54:18'),
(426, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-08 13:54:18', 0, 0, '2018-03-08 13:54:18'),
(427, 127, 4, 'Pay Slip Upload for : 12753902', 'Pay Slip Upload for : 12753902', 0, 0, '2018-03-08 16:32:37'),
(428, 127, 4, 'Payment success for ordernumber : 12753902', 'Payment success for ordernumber : 12753902', 0, 0, '2018-03-08 16:33:09'),
(429, 58, 4, 'Payment success for ordernumber : 12753902', 'Payment success for ordernumber : 12753902', 0, 0, '2018-03-08 16:33:11'),
(430, 127, 4, 'Your order #12753902 has been successfully delivered', 'Your order #12753902 has been successfully delivered', 0, 0, '2018-03-08 16:33:41'),
(431, 58, 4, 'Your order #12753902 has been successfully delivered', 'Your order #12753902 has been successfully delivered', 0, 0, '2018-03-08 16:33:43'),
(432, 136, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-08 16:47:20', 0, 0, '2018-03-08 16:47:20'),
(433, 134, 4, 'Order #13464277 cancelled', 'Order #13464277 cancelled', 0, 0, '2018-03-08 17:13:18'),
(434, 58, 4, 'order #13464277 cancelled', 'order #13464277 cancelled', 0, 0, '2018-03-08 17:13:19'),
(435, 137, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-08 18:16:09', 0, 0, '2018-03-08 18:16:09'),
(436, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-08 19:10:59'),
(437, 59, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-08 19:11:01', 0, 0, '2018-03-08 19:11:01'),
(438, 137, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-08 19:57:52', 0, 0, '2018-03-08 19:57:52'),
(439, 87, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-08 20:53:59', 0, 0, '2018-03-08 20:53:59'),
(440, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-08 20:53:59', 0, 0, '2018-03-08 20:53:59'),
(441, 129, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-10 11:24:44', 0, 0, '2018-03-10 11:24:44'),
(442, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-10 11:24:44', 0, 0, '2018-03-10 11:24:44'),
(443, 129, 4, 'My requrement : #Corn ', 'I want Corn category product of unit asdf expected price per carton AED 1234 with in  2018-03-17 ', 0, 0, '2018-03-10 12:57:01'),
(444, 26, 4, 'Buyer requrement : #Corn ', 'Buyer want Corn category product of unit asdf expected price per carton AED 1234 with in  2018-03-17 ', 0, 0, '2018-03-10 12:57:02'),
(445, 58, 4, 'Buyer requrement : #Corn ', 'Buyer want Corn category product of unit asdf expected price per carton AED 1234 with in  2018-03-17 ', 0, 0, '2018-03-10 12:57:03'),
(446, 129, 4, 'My requrement : #Chicken ', 'I want Chicken category product of unit 1 expected price per carton AED 1 with in  2018-03-17 ', 0, 0, '2018-03-10 12:57:50'),
(447, 26, 4, 'Buyer requrement : #Chicken ', 'Buyer want Chicken category product of unit 1 expected price per carton AED 1 with in  2018-03-17 ', 0, 0, '2018-03-10 12:57:51'),
(448, 129, 3, 'Successful send proforma invoice : 129541328', 'Successful send proforma invoice 2018-03-10 13:00:26 ', 0, 0, '2018-03-10 13:00:26'),
(449, 133, 3, 'One buyer buy your goods: Orange', 'One buyer buy your goods: Orange on 2018-03-10 13:00:28 ', 0, 0, '2018-03-10 13:00:28'),
(450, 129, 4, 'Pay Slip Upload for : 129541328', 'Pay Slip Upload for : 129541328', 0, 0, '2018-03-10 13:00:45'),
(451, 129, 4, 'Payment success for ordernumber : 129541328', 'Payment success for ordernumber : 129541328', 0, 0, '2018-03-10 13:01:27'),
(452, 133, 4, 'Payment success for ordernumber : 129541328', 'Payment success for ordernumber : 129541328', 0, 0, '2018-03-10 13:01:29'),
(453, 129, 4, 'Your order #129541328 has been successfully delivered', 'Your order #129541328 has been successfully delivered', 0, 0, '2018-03-10 13:01:36'),
(454, 133, 4, 'Your order #129541328 has been successfully delivered', 'Your order #129541328 has been successfully delivered', 0, 0, '2018-03-10 13:01:37'),
(455, 129, 4, 'Your order #129541328 returned successfully', 'Your order #129541328 returned successfully', 0, 0, '2018-03-10 13:02:23'),
(456, 133, 4, 'Your order #129541328 returned successfully', 'Your order #129541328 returned successfully', 0, 0, '2018-03-10 13:02:24'),
(457, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-10 16:19:54'),
(458, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-10 16:19:55', 0, 0, '2018-03-10 16:19:55'),
(459, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-10 16:25:53'),
(460, 58, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-10 16:25:54', 0, 0, '2018-03-10 16:25:54'),
(461, 139, 3, 'Successful bid', 'Successful bid on 2018-03-10 16:42:46', 0, 0, '2018-03-10 16:42:46'),
(462, 59, 3, 'Successful bid', 'Successful bid on 2018-03-10 16:42:48', 0, 0, '2018-03-10 16:42:48'),
(463, 129, 3, 'Successful send proforma invoice : 12933846', 'Successful send proforma invoice 2018-03-10 16:53:14 ', 0, 0, '2018-03-10 16:53:14'),
(464, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-10 16:53:16 ', 0, 0, '2018-03-10 16:53:16'),
(465, 140, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-10 17:00:23', 0, 0, '2018-03-10 17:00:23'),
(466, 141, 1, 'Successfully Registered', 'Successfully Registered on 2018-03-10 17:05:53', 0, 0, '2018-03-10 17:05:53'),
(467, 142, 3, 'Successful bid', 'Successful bid on 2018-03-10 17:18:18', 0, 0, '2018-03-10 17:18:18'),
(468, 58, 3, 'Successful bid', 'Successful bid on 2018-03-10 17:18:19', 0, 0, '2018-03-10 17:18:19'),
(469, 140, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-10 17:30:53', 0, 0, '2018-03-10 17:30:53'),
(470, 140, 2, 'Product Reject', 'Not upload Bank details', 0, 0, '2018-03-10 17:33:37'),
(471, 140, 2, 'Product Reject', 'Not upload Bank details please upload', 0, 0, '2018-03-10 17:36:12'),
(472, 140, 1, 'Reject bank details', 'Reject bank details ', 0, 0, '2018-03-10 17:38:51'),
(473, 140, 1, 'Successfully varify bank details', 'Successfully varify bank details ', 0, 0, '2018-03-10 17:40:07'),
(474, 129, 2, 'Product is available on site', 'Product is available on site', 0, 0, '2018-03-10 17:42:34'),
(475, 140, 2, 'Successfully verified all documents', 'Successfully verified all documents on 2018-03-10 17:42:36', 0, 0, '2018-03-10 17:42:36'),
(476, 141, 4, 'My requrement : #Fruit ', 'I want Fruit category product of unit 1 expected price per carton AED 20 with in  2018-03-31', 0, 0, '2018-03-10 17:47:45'),
(477, 133, 4, 'Buyer requrement : #Fruit ', 'Buyer want Fruit category product of unit 1 expected price per carton AED 20 with in  2018-03-31', 0, 0, '2018-03-10 17:47:46'),
(478, 141, 3, 'Successful send proforma invoice : 14132029', 'Successful send proforma invoice 2018-03-12 14:57:11 ', 0, 0, '2018-03-12 14:57:11'),
(479, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-12 14:57:13 ', 0, 0, '2018-03-12 14:57:13'),
(480, 141, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-12 17:37:50', 0, 0, '2018-03-12 17:37:50'),
(481, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-12 17:37:50', 0, 0, '2018-03-12 17:37:50'),
(482, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-12 17:42:00', 0, 0, '2018-03-12 17:42:00'),
(483, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-12 17:42:00', 0, 0, '2018-03-12 17:42:00'),
(484, 141, 3, 'Successful send proforma invoice : 141588503', 'Successful send proforma invoice 2018-03-12 17:49:27 ', 0, 0, '2018-03-12 17:49:27'),
(485, 140, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-12 17:49:28 ', 0, 0, '2018-03-12 17:49:28'),
(486, 141, 3, 'Successful bid in auction of product named Mango', 'Successful bid in auction of product named Mango on 2018-03-12 17:53:08', 0, 0, '2018-03-12 17:53:08'),
(487, 140, 3, 'One user successfully bid in auction of product named Mango', 'One user successfully bid in auction of product named Mango on 2018-03-12 17:53:08', 0, 0, '2018-03-12 17:53:08'),
(488, 141, 4, 'Selected for Auction : 141586873', 'Selected for Auction : 141586873', 0, 0, '2018-03-12 17:54:51'),
(489, 141, 3, 'Successful send proforma invoice : 141586873', 'Successful send proforma invoice 2018-03-12 18:52:39 ', 0, 0, '2018-03-12 18:52:39'),
(490, 140, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-12 18:52:40 ', 0, 0, '2018-03-12 18:52:40'),
(491, 141, 4, 'Pay Slip Upload for : 141586873', 'Pay Slip Upload for : 141586873', 0, 0, '2018-03-12 19:04:17'),
(492, 141, 4, 'Payment success for ordernumber : 141586873', 'Payment success for ordernumber : 141586873', 0, 0, '2018-03-12 19:05:37'),
(493, 140, 4, 'Payment success for ordernumber : 141586873', 'Payment success for ordernumber : 141586873', 0, 0, '2018-03-12 19:05:38'),
(494, 141, 4, 'Your order #141586873 has been successfully delivered', 'Your order #141586873 has been successfully delivered', 0, 0, '2018-03-12 19:05:45'),
(495, 140, 4, 'Your order #141586873 has been successfully delivered', 'Your order #141586873 has been successfully delivered', 0, 0, '2018-03-12 19:05:46'),
(496, 141, 4, 'Pay Slip Upload for : 141588503', 'Pay Slip Upload for : 141588503', 0, 0, '2018-03-12 19:11:32'),
(497, 141, 4, 'Payment success for ordernumber : 141588503', 'Payment success for ordernumber : 141588503', 0, 0, '2018-03-12 19:11:54'),
(498, 140, 4, 'Payment success for ordernumber : 141588503', 'Payment success for ordernumber : 141588503', 0, 0, '2018-03-12 19:11:56'),
(499, 141, 4, 'Your order #141588503 has been successfully delivered', 'Your order #141588503 has been successfully delivered', 0, 0, '2018-03-12 19:12:04'),
(500, 140, 4, 'Your order #141588503 has been successfully delivered', 'Your order #141588503 has been successfully delivered', 0, 0, '2018-03-12 19:12:05'),
(501, 143, 3, 'Successful bid', 'Successful bid on 2018-03-12 19:45:12', 0, 0, '2018-03-12 19:45:12'),
(502, 59, 3, 'Successful bid', 'Successful bid on 2018-03-12 19:45:13', 0, 0, '2018-03-12 19:45:13'),
(503, 144, 3, 'Successful bid', 'Successful bid on 2018-03-13 11:20:13', 0, 0, '2018-03-13 11:20:13'),
(504, 59, 3, 'Successful bid', 'Successful bid on 2018-03-13 11:20:14', 0, 0, '2018-03-13 11:20:14'),
(505, 81, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-13 11:23:19', 0, 0, '2018-03-13 11:23:19'),
(506, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-13 11:23:19', 0, 0, '2018-03-13 11:23:19'),
(507, 128, 4, 'Selected for Auction : 128511777', 'Selected for Auction : 128511777', 0, 0, '2018-03-13 11:32:36'),
(508, 129, 4, 'Selected for Auction : 129519540', 'Selected for Auction : 129519540', 0, 0, '2018-03-13 11:32:37'),
(509, 26, 3, 'Successful send proforma invoice : 2622246', 'Successful send proforma invoice 2018-03-13 11:37:24 ', 0, 0, '2018-03-13 11:37:24'),
(510, 59, 3, 'One buyer buy your goods: Grapes', 'One buyer buy your goods: Grapes on 2018-03-13 11:37:26 ', 0, 0, '2018-03-13 11:37:26'),
(511, 145, 3, 'Successful bid', 'Successful bid on 2018-03-13 13:39:56', 0, 0, '2018-03-13 13:39:56'),
(512, 59, 3, 'Successful bid', 'Successful bid on 2018-03-13 13:39:58', 0, 0, '2018-03-13 13:39:58'),
(513, 146, 3, 'Successful bid', 'Successful bid on 2018-03-13 15:58:22', 0, 0, '2018-03-13 15:58:22'),
(514, 59, 3, 'Successful bid', 'Successful bid on 2018-03-13 15:58:24', 0, 0, '2018-03-13 15:58:24'),
(515, 1, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-13 18:08:07', 0, 0, '2018-03-13 18:08:07'),
(516, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-13 18:08:07', 0, 0, '2018-03-13 18:08:07'),
(517, 26, 3, 'Successful send proforma invoice : 2639329', 'Successful send proforma invoice 2018-03-14 11:16:00 ', 0, 0, '2018-03-14 11:16:00'),
(518, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-14 11:16:04 ', 0, 0, '2018-03-14 11:16:04'),
(519, 26, 4, 'Pay Slip Upload for : 2639329', 'Pay Slip Upload for : 2639329', 0, 0, '2018-03-14 11:19:17'),
(520, 26, 4, 'Payment success for ordernumber : 2639329', 'Payment success for ordernumber : 2639329', 0, 0, '2018-03-14 11:20:56'),
(521, 59, 4, 'Payment success for ordernumber : 2639329', 'Payment success for ordernumber : 2639329', 0, 0, '2018-03-14 11:20:57'),
(522, 26, 4, 'Pay Slip Upload for : 2622246', 'Pay Slip Upload for : 2622246', 0, 0, '2018-03-14 12:06:48'),
(523, 26, 4, 'Payment success for ordernumber : 2622246', 'Payment success for ordernumber : 2622246', 0, 0, '2018-03-14 12:08:46'),
(524, 59, 4, 'Payment success for ordernumber : 2622246', 'Payment success for ordernumber : 2622246', 0, 0, '2018-03-14 12:08:48'),
(525, 26, 4, 'Pay Slip Upload for : 2631404', 'Pay Slip Upload for : 2631404', 0, 0, '2018-03-14 12:09:29'),
(526, 26, 4, 'Payment success for ordernumber : 2631404', 'Payment success for ordernumber : 2631404', 0, 0, '2018-03-14 12:10:44'),
(527, 59, 4, 'Payment success for ordernumber : 2631404', 'Payment success for ordernumber : 2631404', 0, 0, '2018-03-14 12:10:46'),
(528, 26, 4, 'Pay Slip Upload for : 2658671', 'Pay Slip Upload for : 2658671', 0, 0, '2018-03-14 12:16:56'),
(529, 26, 3, 'Successful bid in auction of product named apple', 'Successful bid in auction of product named apple on 2018-03-14 12:54:52', 0, 0, '2018-03-14 12:54:52'),
(530, 58, 3, 'One user successfully bid in auction of product named apple', 'One user successfully bid in auction of product named apple on 2018-03-14 12:54:52', 0, 0, '2018-03-14 12:54:52'),
(531, 26, 3, 'Successful bid in auction of product named apple', 'Successful bid in auction of product named apple on 2018-03-14 12:55:14', 0, 0, '2018-03-14 12:55:14'),
(532, 58, 3, 'One user successfully bid in auction of product named apple', 'One user successfully bid in auction of product named apple on 2018-03-14 12:55:14', 0, 0, '2018-03-14 12:55:14'),
(533, 26, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-14 13:09:44', 0, 0, '2018-03-14 13:09:44'),
(534, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-14 13:09:44', 0, 0, '2018-03-14 13:09:44'),
(535, 84, 4, 'Selected for Auction : 84519589', 'Selected for Auction : 84519589', 0, 0, '2018-03-14 18:59:59'),
(536, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 14:50:22', 0, 0, '2018-03-15 14:50:22'),
(537, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 14:50:22', 0, 0, '2018-03-15 14:50:22'),
(538, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 14:53:19', 0, 0, '2018-03-15 14:53:19'),
(539, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 14:53:19', 0, 0, '2018-03-15 14:53:19'),
(540, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 14:56:59', 0, 0, '2018-03-15 14:56:59'),
(541, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 14:56:59', 0, 0, '2018-03-15 14:56:59'),
(542, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:06:20', 0, 0, '2018-03-15 15:06:20'),
(543, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:06:20', 0, 0, '2018-03-15 15:06:20'),
(544, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:07:56', 0, 0, '2018-03-15 15:07:56'),
(545, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:07:56', 0, 0, '2018-03-15 15:07:56'),
(546, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:13:29', 0, 0, '2018-03-15 15:13:29'),
(547, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:13:29', 0, 0, '2018-03-15 15:13:29'),
(548, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:15:08', 0, 0, '2018-03-15 15:15:08'),
(549, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:15:08', 0, 0, '2018-03-15 15:15:08'),
(550, 141, 3, 'Successful bid in auction of product named Chicken Sausage', 'Successful bid in auction of product named Chicken Sausage on 2018-03-15 15:17:00', 0, 0, '2018-03-15 15:17:00'),
(551, 26, 3, 'One user successfully bid in auction of product named Chicken Sausage', 'One user successfully bid in auction of product named Chicken Sausage on 2018-03-15 15:17:00', 0, 0, '2018-03-15 15:17:00'),
(552, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:18:24', 0, 0, '2018-03-15 15:18:24'),
(553, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:18:24', 0, 0, '2018-03-15 15:18:24'),
(554, 141, 3, 'Successful bid in auction of product named Orange', 'Successful bid in auction of product named Orange on 2018-03-15 15:18:34', 0, 0, '2018-03-15 15:18:34'),
(555, 133, 3, 'One user successfully bid in auction of product named Orange', 'One user successfully bid in auction of product named Orange on 2018-03-15 15:18:34', 0, 0, '2018-03-15 15:18:34'),
(556, 140, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-15 17:12:57', 0, 0, '2018-03-15 17:12:57'),
(557, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-15 17:12:57', 0, 0, '2018-03-15 17:12:57'),
(558, 140, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-15 17:14:23', 0, 0, '2018-03-15 17:14:23'),
(559, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-15 17:14:23', 0, 0, '2018-03-15 17:14:23'),
(560, 140, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-15 17:15:50', 0, 0, '2018-03-15 17:15:50'),
(561, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-15 17:15:50', 0, 0, '2018-03-15 17:15:50'),
(562, 140, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-15 18:27:57', 0, 0, '2018-03-15 18:27:57'),
(563, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-15 18:27:57', 0, 0, '2018-03-15 18:27:57'),
(564, 140, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-15 18:28:14', 0, 0, '2018-03-15 18:28:14'),
(565, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-15 18:28:14', 0, 0, '2018-03-15 18:28:14'),
(566, 140, 3, 'Successful bid in auction of product named litchi', 'Successful bid in auction of product named litchi on 2018-03-15 18:29:25', 0, 0, '2018-03-15 18:29:25'),
(567, 59, 3, 'One user successfully bid in auction of product named litchi', 'One user successfully bid in auction of product named litchi on 2018-03-15 18:29:25', 0, 0, '2018-03-15 18:29:25'),
(568, 140, 3, 'Successful send proforma invoice : 14033448', 'Successful send proforma invoice 2018-03-15 18:37:22 ', 0, 0, '2018-03-15 18:37:22'),
(569, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-15 18:37:25 ', 0, 0, '2018-03-15 18:37:25'),
(570, 140, 3, 'Successful send proforma invoice : 14024429', 'Successful send proforma invoice 2018-03-15 18:42:47 ', 0, 0, '2018-03-15 18:42:47'),
(571, 59, 3, 'One buyer buy your goods: Grapes', 'One buyer buy your goods: Grapes on 2018-03-15 18:42:49 ', 0, 0, '2018-03-15 18:42:49'),
(572, 140, 3, 'Successful bid in auction of product named Broccoli', 'Successful bid in auction of product named Broccoli on 2018-03-15 18:44:57', 0, 0, '2018-03-15 18:44:57'),
(573, 59, 3, 'One user successfully bid in auction of product named Broccoli', 'One user successfully bid in auction of product named Broccoli on 2018-03-15 18:44:57', 0, 0, '2018-03-15 18:44:57'),
(574, 140, 3, 'Successful send proforma invoice : 14031442', 'Successful send proforma invoice 2018-03-15 18:45:47 ', 0, 0, '2018-03-15 18:45:47'),
(575, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-15 18:45:49 ', 0, 0, '2018-03-15 18:45:49'),
(576, 26, 3, 'Successful bid in auction of product named Mango', 'Successful bid in auction of product named Mango on 2018-03-16 12:20:19', 0, 0, '2018-03-16 12:20:19'),
(577, 140, 3, 'One user successfully bid in auction of product named Mango', 'One user successfully bid in auction of product named Mango on 2018-03-16 12:20:19', 0, 0, '2018-03-16 12:20:19'),
(578, 26, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-16 13:31:08', 0, 0, '2018-03-16 13:31:08'),
(579, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-16 13:31:08', 0, 0, '2018-03-16 13:31:08'),
(580, 26, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-16 13:43:33', 0, 0, '2018-03-16 13:43:33'),
(581, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-16 13:43:33', 0, 0, '2018-03-16 13:43:33'),
(582, 26, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-16 13:55:32', 0, 0, '2018-03-16 13:55:32'),
(583, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-16 13:55:32', 0, 0, '2018-03-16 13:55:32'),
(584, 140, 3, 'Successful bid in auction of product named Blackberry', 'Successful bid in auction of product named Blackberry on 2018-03-16 18:23:54', 0, 0, '2018-03-16 18:23:54'),
(585, 58, 3, 'One user successfully bid in auction of product named Blackberry', 'One user successfully bid in auction of product named Blackberry on 2018-03-16 18:23:54', 0, 0, '2018-03-16 18:23:54'),
(586, 26, 4, 'Pay Slip Upload for : 2658808', 'Pay Slip Upload for : 2658808', 0, 0, '2018-03-16 19:27:49'),
(587, 26, 4, 'Pay Slip Upload for : 2658808', 'Pay Slip Upload for : 2658808', 0, 0, '2018-03-16 19:31:03'),
(588, 141, 4, 'Pay Slip Upload for : 14132029', 'Pay Slip Upload for : 14132029', 0, 0, '2018-03-16 19:35:34'),
(589, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-17 15:44:37', 0, 0, '2018-03-17 15:44:37'),
(590, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-17 15:57:00', 0, 0, '2018-03-17 15:57:00'),
(591, 26, 2, 'Thanks for posting the product', 'Product Successfully Posted on 2018-03-17 16:02:22', 0, 0, '2018-03-17 16:02:22'),
(592, 26, 4, 'Selected for Auction : 26587053', 'Selected for Auction : 26587053', 0, 0, '2018-03-17 18:14:10'),
(593, 141, 4, 'Your order #141586873 returned successfully', 'Your order #141586873 returned successfully', 0, 0, '2018-03-17 18:32:30'),
(594, 140, 4, 'Your order #141586873 returned successfully', 'Your order #141586873 returned successfully', 0, 0, '2018-03-17 18:32:31'),
(595, 141, 4, 'Payslip rejected for ordernumber : 14132029', 'Payslip rejected for ordernumber : 14132029', 0, 0, '2018-03-17 18:33:15'),
(596, 26, 4, 'Your order #2639329 has been successfully delivered', 'Your order #2639329 has been successfully delivered', 0, 0, '2018-03-17 18:34:39'),
(597, 59, 4, 'Your order #2639329 has been successfully delivered', 'Your order #2639329 has been successfully delivered', 0, 0, '2018-03-17 18:34:41'),
(598, 127, 2, 'Product Reject', 'dfgd', 0, 0, '2018-03-17 18:40:30'),
(599, 26, 2, 'Product not verified', 'Bank details not verified', 0, 0, '2018-03-17 18:45:50'),
(600, 127, 2, 'Product Reject', 'Bank details not verified', 0, 0, '2018-03-17 18:47:56'),
(601, 26, 5, 'Product returned Chicken Sausage', 'Product returned on 2018-03-17 18:50:17', 0, 0, '2018-03-17 18:50:17'),
(602, 140, 4, 'My requrement : #Vegetable ', 'I want Vegetable category product of unit 1 kg expected price per carton AED 20 with in  2018-03-27', 0, 0, '2018-03-19 12:19:02'),
(603, 26, 4, 'Buyer requrement : #Vegetable ', 'Buyer want Vegetable category product of unit 1 kg expected price per carton AED 20 with in  2018-03-27', 0, 0, '2018-03-19 12:19:03'),
(604, 140, 3, 'Successful send proforma invoice : 14035727', 'Successful send proforma invoice 2018-03-21 14:47:58 ', 0, 0, '2018-03-21 14:47:58'),
(605, 59, 3, 'One buyer buy your goods: Mango', 'One buyer buy your goods: Mango on 2018-03-21 14:48:00 ', 0, 0, '2018-03-21 14:48:00'),
(606, 140, 4, 'Pay Slip Upload for : 14035727', 'Pay Slip Upload for : 14035727', 0, 0, '2018-03-21 14:51:49'),
(607, 26, 3, 'Successful wish price #Mango', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :28/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :40</p>\r\n<p>Date :22/03/2018</p>\r\n\r\n', 0, 0, '2018-03-22 19:40:50'),
(608, 59, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :28/03/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :40</p>\r\n<p>Date :22/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-22 19:40:51'),
(609, 147, 1, 'Successfully Registered', '<p>Welcome to The Surplus Exchange!</p>\n\n<p>You have successfully registered with us now as a Seller. </p>\n\n<p>Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p>You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<br /><p>Happy buying/selling.</p>\n\n', 0, 0, '2018-03-23 10:58:07'),
(610, 141, 3, 'Successful wish price #Mango', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :28/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n', 0, 0, '2018-03-23 11:28:02'),
(611, 59, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :28/03/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 11:28:03'),
(612, 141, 4, 'My Requrement Vegetable', '@if(@$notification)\r\n@else\r\nHello User,<br/><br/>\r\n@endif\r\n<p>We inform to all of our seller</p>\r\n\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 11:31:04'),
(613, 26, 4, 'Buyer Requiremt Vegetable', '@if(@$notification)\r\n@else\r\nHello Puru Sahoo,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Vegetable category product of unit 1 kg expected price per carton AED 20 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 11:31:05'),
(614, 147, 2, 'Thanks for posting the product #Broccoli', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>India<br />\r\n<b>Expiry Date :</b>23/03/2018 <br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>50<br />\r\n\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b>warehouse1<br />\r\n<b>Address :</b>Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b>Al Ain<br />\r\n<b>Emirates :</b>Abu Dhabi<br />\r\n<b>Country :</b>UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 11:34:28'),
(615, 147, 2, 'Product status : Broccoli', '<p>Your request for posting of the following product is rejected due to  following discrepancies.</p>\r\n\r\n\r\n<p><b></b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>22/04/2018<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<p>We apologize for the inconvinience this time and look forward to your future participation in near future.</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 11:44:10'),
(616, 147, 2, 'Product status : Broccoli', '<p>Your request for posting of the following product is rejected due to  following discrepancies.</p>\r\n\r\n\r\n<p><b>Upload Bank details</b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>22/04/2018<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<p>We apologize for the inconvinience this time and look forward to your future participation in near future.</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 11:45:44'),
(617, 147, 2, 'Product status : Broccoli', '<p>Your request for posting of the following product is rejected due to  following discrepancies.</p>\r\n\r\n\r\n<p><b>Upload Bank details please ohterwise you unable to sell</b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>22/04/2018<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<p>We apologize for the inconvinience this time and look forward to your future participation in near future.</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 11:49:51'),
(618, 147, 1, 'Bank details rejected', '<p>\r\n    Greetings from the Surplus Store.\r\n</p>\r\n<p>\r\n    Your bank details rejected. Kindly provide correct bank information.\r\n</p>\r\n<p>\r\n    Eagerly waiting for your response.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 11:54:18'),
(619, 147, 1, 'Bank Details varified', '<p>\r\nYou bank profile details verification with us is complete now and we are happy to inform that you can use the features of our exchange.\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 11:58:46'),
(620, 147, 2, 'Pending documents to upload #Broccoli', '<p>Your request for posting of the following product is on hold due to following discrepancies.</p>\r\n<p><b>Upload Bank details please ohterwise you unable to sell please please</b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>22/04/2018<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<p>Please take remedial action or contact our support for help.</p><br />\r\n\r\n\r\n', 0, 0, '2018-03-23 12:50:00'),
(621, 141, 4, 'My Requrement Fruits', 'Hello User,<br/><br/>\r\n<p>We inform to all of our seller</p>\r\n\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n', 0, 0, '2018-03-23 12:53:53'),
(622, 26, 4, 'Buyer Requiremt Fruits', '@if(@$notification)\r\n@else\r\nHello Puru Sahoo,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Fruits category product of unit 1 kg expected price per carton AED 20 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 12:53:54'),
(623, 59, 4, 'Buyer Requiremt Fruits', '@if(@$notification)\r\n@else\r\nHello Purusottama Sahoo,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Fruits category product of unit 1 kg expected price per carton AED 20 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 12:53:56'),
(624, 128, 4, 'Buyer Requiremt Fruits', '@if(@$notification)\r\n@else\r\nHello Pramodini Das,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Fruits category product of unit 1 kg expected price per carton AED 20 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 12:53:57'),
(625, 140, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 12:55:19'),
(626, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 12:55:20'),
(627, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 12:55:21'),
(628, 147, 2, 'Successfully verified all documents #Broccoli', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>22/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n', 0, 0, '2018-03-23 12:55:24'),
(629, 141, 3, 'Successful wish price #Broccoli', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n', 0, 0, '2018-03-23 13:01:39'),
(630, 147, 3, 'Successful wish price #Broccoli', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:01:41'),
(631, 141, 3, 'Reject wish price #Broccoli', '<p>\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:12:29');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(632, 147, 3, 'Reject wish price #Broccoli', '<p>\r\nYour decision on the following offer has been communicated to the prospective buyer. </p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 22/04/2018 \r\n    </p>\r\n\r\n<p><b><u>Wish price details ( Rejected )</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:12:31'),
(633, 141, 3, 'Successful wish price #Broccoli', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n', 0, 0, '2018-03-23 13:16:46'),
(634, 147, 3, 'Successful wish price #Broccoli', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:16:47'),
(635, 141, 3, 'Successful accepect wish price #Broccoli', '<p>\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n</p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n<p>\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:18:03'),
(636, 147, 3, 'Successful accepect wish price #Broccoli', '<p>\r\nYour decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details ( Accepted )</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :20</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 13:18:04'),
(637, 147, 5, 'Product Broccoli is returned', 'Product returned on 2018-03-23 13:22:01', 0, 0, '2018-03-23 13:22:01'),
(638, 147, 2, 'Thanks for posting the product #Broccoli', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>India<br />\r\n<b>Expiry Date :</b>22/04/2018 <br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<br/>\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b>warehouse1<br />\r\n<b>Address :</b>Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b>Al Ain<br />\r\n<b>Emirates :</b>Abu Dhabi<br />\r\n<b>Country :</b>UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 13:32:33'),
(639, 140, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 15:36:08'),
(640, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 15:36:09'),
(641, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 15:36:10'),
(642, 147, 2, 'Successfully verified all documents #Broccoli', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>22/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n', 0, 0, '2018-03-23 15:36:13'),
(643, 141, 3, 'Successful wish price #Broccoli', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Wish Price / Carton :10</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n', 0, 0, '2018-03-23 15:49:49'),
(644, 147, 3, 'Successful wish price #Broccoli', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Wish Price / Carton :10</p>\r\n<p>Date :23/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 15:49:50'),
(645, 141, 3, 'Successfully Placed Order #141632193', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product Broccoli </p>\r\n\r\n\r\n<p>Product name :Broccoli</p>\r\n<p>Carton Purchased :1</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IjhZeHEwUDdkcitrUkVJSHVJS1NWc1E9PSIsInZhbHVlIjoiMDRZd2w5WHRuY0dGeEJDajc3aTJ5bTRtQmdxamd0TlBRNUl3Vk54Rmo5cz0iLCJtYWMiOiI4ZjBlYzg5NjQ2YmM1NDU5ODBmNjZlNTIyMjllOTdjZmNmODVjZTY3ZDEzMzZjNjMwMDVhMTNjNjlmYjE5ZjcxIn0=\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-23 15:57:32'),
(646, 147, 3, 'Purchase goods :Broccoli', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name :Broccoli</p>\r\n<p>Carton Purchased :1</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-23 15:57:33'),
(647, 141, 4, 'Payment slip uploaded agenest order number #141632193', '<p>\r\n    The payment slip against the order ( 141632193 ) has been received by us.\r\n</p>\r\n<p> \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n', 0, 0, '2018-03-23 17:42:00'),
(648, 141, 4, 'Payment slip rejected for order number # 141632193', '<p>\nYour transaction is on hold with the payslip against the recent purchase.\n</p>\n<p>\n<b>Product Name :</b>Broccoli<br />\n<b>Order Number :</b>141632193 <br />\n</p>\n<p>\nPlease upload a fresh document here or contact our support at <number> for additional details.\n</p>\n\nAdmin\nSurplus Store, Dubai\n\nDear User,\n<br/><br/>\nPayslip rejected for  #141632193.<br/>\n\n<br/><br/>\nRegards,<br/>\nAdmin', 0, 0, '2018-03-23 17:44:44'),
(649, 141, 4, 'Payment slip uploaded agenest order number #141632193', '<p>\r\n    The payment slip against the order ( 141632193 ) has been received by us.\r\n</p>\r\n<p> \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n', 0, 0, '2018-03-23 17:55:37'),
(650, 141, 4, 'Payment successfully varified for order number 141632193', '<br/><br/>\n<p>\n    Your payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n\n<b>Product Name :</b>Broccoli<br />\n<b>Order Number :</b>141632193 <br />\n\n\n\n<br/>\n', 0, 0, '2018-03-23 17:55:46'),
(651, 147, 4, 'Payment successfully varified for order number 141632193', '<br/><br/>\n<p>\n    Payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p>\n<b>Product Name :</b>Broccoli<br />\n<b>Order Number :</b>141632193 <br />\n</p>\n\n', 0, 0, '2018-03-23 17:55:47'),
(652, 141, 4, 'My Requrement Vegetable', 'Hello User,<br/><br/>\r\n<p>We inform to all of our seller</p>\r\n\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n', 0, 0, '2018-03-23 18:04:21'),
(653, 26, 4, 'Buyer Requiremt Vegetable', '@if(@$notification)\r\n@else\r\nHello Puru Sahoo,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Vegetable category product of unit 1 kg expected price per carton AED 1 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 18:04:22'),
(654, 141, 4, 'My Requrement Vegetable', 'Hello User,<br/><br/>\r\n<p>We inform to all of our seller</p>\r\n\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n', 0, 0, '2018-03-23 18:06:03'),
(655, 26, 4, 'Buyer Requiremt Vegetable', '@if(@$notification)\r\n@else\r\nHello Puru Sahoo,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Vegetable category product of unit 1 kg expected price per carton AED 1 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 18:06:04'),
(656, 147, 4, 'Buyer Requiremt Vegetable', '@if(@$notification)\r\n@else\r\nHello Kisan kumar,<br/><br/>\r\n@endif\r\n\r\n<p>Buyer want Vegetable category product of unit 1 kg expected price per carton AED 1 with in 30/03/2018 </p>\r\n@if(@$notification)\r\n@else\r\n<br/>\r\nThanks<br/><br/>\r\nAdmin<br/><br/>\r\nSurplus Store, Dubai\r\n@endif', 0, 0, '2018-03-23 18:06:06'),
(657, 141, 4, 'Your order #141632193 has been successfully delivered', 'Please find status of your order #141632193:<br />\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141632193</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45\r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 No             </td>\r\n            <td class=\"action\">\r\n                AED 45.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.25<br />\r\n                AED47.25</td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n', 0, 0, '2018-03-23 18:21:50'),
(658, 147, 4, 'Your order #141632193 has been successfully delivered', 'Please find status of your order #141632193:<br />\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141632193</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45\r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 No             </td>\r\n            <td class=\"action\">\r\n                AED 45.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.25<br />\r\n                AED47.25</td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n', 0, 0, '2018-03-23 18:21:51'),
(659, 141, 4, 'Your order #141632193 returned successfully', 'Please find status of your order #141632193:<br />\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141632193</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45\r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 No             </td>\r\n            <td class=\"action\">\r\n                AED 45.00\r\n            </td>\r\n            <td>\r\n                                Returned\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.25<br />\r\n                AED47.25</td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n', 0, 0, '2018-03-23 18:23:59'),
(660, 147, 4, 'Your order #141632193 returned successfully', 'Please find status of your order #141632193:<br />\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141632193</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45\r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 No             </td>\r\n            <td class=\"action\">\r\n                AED 45.00\r\n            </td>\r\n            <td>\r\n                                Returned\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.25<br />\r\n                AED47.25</td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n', 0, 0, '2018-03-23 18:24:00'),
(661, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b><br />\r\n\r\n\r\n', 0, 0, '2018-03-23 18:28:25'),
(662, 147, 2, 'Product Broccoli is available in auction mode', '<p>Your product in auction mode.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Auction Start Date :</b> 23/03/2018  <br />\r\n<b>Auction End  Date :</b>12/04/2018 <br />\r\n<b>Expiry Date :</b> 22/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED  40<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 18:28:28'),
(663, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Expiry Date :</b>2018-04-22 00:00:00<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b><br />\r\n\r\n\r\n', 0, 0, '2018-03-23 18:30:19'),
(664, 147, 2, 'Product Broccoli is available in auction mode', '<p>Your product in auction mode.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Broccoli<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Auction Start Date :</b> 23/03/2018  <br />\r\n<b>Auction End  Date :</b>12/04/2018 <br />\r\n<b>Expiry Date :</b> 22/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED  35<br />\r\n\r\n\r\n', 0, 0, '2018-03-23 18:30:21'),
(665, 141, 3, 'Successful bid #Broccoli', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :23/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 18:32:17'),
(666, 147, 3, 'Successful bid #Broccoli', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-04-22 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :2018-03-23</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 18:32:18'),
(667, 141, 4, 'Successfully select bid auction 141633258', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 22/04/2018</p>\r\n<p>Price /Carton : 45</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#141633258</p>\r\n<p>Carton Purchased :1</p>\r\n<p>Bid Price :AED 35.5</p>\r\n<p>Bid Date : 23/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-23 18:34:38'),
(668, 141, 3, 'Successful bid #Broccoli', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :23/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 18:38:09'),
(669, 147, 3, 'Successful bid #Broccoli', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-04-22 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :2018-03-23</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 18:38:11'),
(670, 141, 3, 'Successful bid #Broccoli', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :5</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :23/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-23 18:45:00'),
(671, 147, 3, 'Successful bid #Broccoli', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-04-22 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :5</p>\r\n<p>Bid Price / Carton :35.50</p>\r\n<p>Bid Date :2018-03-23</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-23 18:45:01'),
(672, 141, 4, 'Selected for Auction : 141633275', 'Selected for Auction : 141633275', 0, 0, '2018-03-23 18:47:46'),
(673, 141, 3, 'Successfully Placed Order #14128838', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product Grapes </p>\r\n\r\n\r\n<p>Product name :Grapes</p>\r\n<p>Carton Purchased :5</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImIwZnpTazR3QnUzN2tMQzRPZDRiXC93PT0iLCJ2YWx1ZSI6InZmbXcxbm9BNGw3d1E5UDFNU0VyXC9BPT0iLCJtYWMiOiIxMmY3MzFlOWQwMWRmYTkwNzJhNjAxMWJmOTI4N2M4MjY2ODNlMWJlNjVjMzU3NzhiMjY5NWU2MDBmNmQ3ZjA5In0=\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-26 11:03:55'),
(674, 59, 3, 'Purchase goods :Grapes', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name :Grapes</p>\r\n<p>Carton Purchased :5</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-26 11:03:56'),
(675, 141, 4, 'Payment slip uploaded agenest order number #14128838', '<p>\r\n    The payment slip against the order ( 14128838 ) has been received by us.\r\n</p>\r\n<p> \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n', 0, 0, '2018-03-26 11:05:11'),
(676, 141, 4, 'Payment successfully varified for order number 14128838', '<br/><br/>\n<p>\n    Your payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n\n<b>Product Name :</b>Grapes<br />\n<b>Order Number :</b>14128838 <br />\n\n\n\n<br/>\n', 0, 0, '2018-03-26 11:05:59'),
(677, 59, 4, 'Payment successfully varified for order number 14128838', '<br/><br/>\n<p>\n    Payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p>\n<b>Product Name :</b>Grapes<br />\n<b>Order Number :</b>14128838 <br />\n</p>\n\n', 0, 0, '2018-03-26 11:06:00'),
(678, 26, 3, 'Successful wish price #Green Banana', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :5</p>\r\n<p>Wish Price / Carton :10</p>\r\n<p>Date :26/03/2018</p>\r\n\r\n', 0, 0, '2018-03-26 17:20:44'),
(679, 58, 3, 'Successful wish price #Green Banana', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :5</p>\r\n<p>Wish Price / Carton :10</p>\r\n<p>Date :26/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:20:45'),
(680, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :181.00</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:26'),
(681, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :181.00</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:27'),
(682, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Bid Price / Carton :181.00</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:41'),
(683, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :2</p>\r\n<p>Bid Price / Carton :181.00</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:42'),
(684, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :183.00</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:58'),
(685, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :183.00</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:47:59'),
(686, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :500</p>\r\n<p>Bid Price / Carton :180.5</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 17:48:08'),
(687, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :500</p>\r\n<p>Bid Price / Carton :180.5</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:48:09'),
(688, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :181.50</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 17:48:23'),
(689, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :181.50</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 17:48:25'),
(690, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :500</p>\r\n<p>Bid Price / Carton :185</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 18:00:39'),
(691, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :500</p>\r\n<p>Bid Price / Carton :185</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 18:00:40'),
(692, 141, 3, 'Successful bid #Blackberry', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :186.00</p>\r\n<p>Bid Date :26/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-26 18:00:52'),
(693, 58, 3, 'Successful bid #Blackberry', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Blackberry</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :2018-03-31 \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Bid Price / Carton :186.00</p>\r\n<p>Bid Date :2018-03-26</p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 18:00:53'),
(694, 26, 3, 'Successful wish price #pomegranate', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : pomegranate</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :50</p>\r\n<p>Date :26/03/2018</p>\r\n\r\n', 0, 0, '2018-03-26 20:43:13'),
(695, 59, 3, 'Successful wish price #pomegranate', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : pomegranate</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :31/03/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :10</p>\r\n<p>Wish Price / Carton :50</p>\r\n<p>Date :26/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-26 20:43:14'),
(696, 137, 2, 'Product status : Mango', '<p>Your request for posting of the following product is rejected due to  following discrepancies.</p>\r\n\r\n\r\n<p><b>not upload</b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>07/04/2018<br />\r\n<b>Total Carton :</b>20<br />\r\n<b>Price / Carton :</b>45<br />\r\n\r\n<p>We apologize for the inconvinience this time and look forward to your future participation in near future.</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-27 11:50:28'),
(697, 129, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-05<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:55:46'),
(698, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-05<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:55:48'),
(699, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-05<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:55:49'),
(700, 133, 2, 'Successfully verified all documents #Orange', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>05/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 11:55:52'),
(701, 129, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-03-25<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:57:17'),
(702, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-03-25<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:57:18'),
(703, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-03-25<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:57:20'),
(704, 133, 2, 'Successfully verified all documents #Orange', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>25/03/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 11:57:22'),
(705, 129, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:59:10'),
(706, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:59:11'),
(707, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 11:59:12'),
(708, 133, 2, 'Successfully verified all documents #Orange', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>30/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 11:59:14'),
(709, 129, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:00:01'),
(710, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:00:02'),
(711, 141, 2, 'Product Orange is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-30<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:00:03'),
(712, 133, 2, 'Successfully verified all documents #Orange', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Orange<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>30/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:00:06'),
(713, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:02:26'),
(714, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:02:27'),
(715, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:02:28'),
(716, 140, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>09/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:02:31'),
(717, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:15'),
(718, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:16');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(719, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:17'),
(720, 140, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>09/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:07:19'),
(721, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:52'),
(722, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:53'),
(723, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>2018-04-09<br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:07:54'),
(724, 140, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Mango<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b>09/04/2018  <br /><br />\r\n<b>Total Carton :</b>1000<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:07:57'),
(725, 129, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:34:30'),
(726, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:34:32'),
(727, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:34:33'),
(728, 59, 2, 'Successfully verified all documents #Grapes', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>27/03/2018  <br /><br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:34:36'),
(729, 129, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-26<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:04'),
(730, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-26<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:05'),
(731, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-26<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:06'),
(732, 59, 2, 'Successfully verified all documents #Grapes', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>26/03/2018  <br /><br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:35:09'),
(733, 129, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:44'),
(734, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:45'),
(735, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n<b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>2018-03-27<br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 12:35:46'),
(736, 59, 2, 'Successfully verified all documents #Grapes', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Grapes<br />\r\n<b>Brand Name :</b>Indian<br />\r\n    <b>Country of origin :</b>3<br />\r\n<b>Expiry Date :</b>27/03/2018  <br /><br />\r\n<b>Total Carton :</b>100<br />\r\n<b>Price / Carton :</b>65<br />\r\n\r\n', 0, 0, '2018-03-27 12:35:48'),
(737, 147, 3, 'Successfully Placed Order #14734832', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p>Product name :Mango</p>\r\n<p>Carton Purchased :1</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImJuaUZWOXE4VEs0TFdaNjFBNTl5a2c9PSIsInZhbHVlIjoibVFLYlppUGh2N29XazZrbG0wajhxUT09IiwibWFjIjoiOTRiNDA3YjM5MWNiMmNjYTE2Yzc0NGYyYTkwYWE4ODFhMWEzM2E5MzY5ZTc2NWMyNzhhNGViNzQwNGJlZTYyOCJ9\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-27 12:37:06'),
(738, 59, 3, 'Purchase goods :Mango', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name :Mango</p>\r\n<p>Carton Purchased :1</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-27 12:37:07'),
(739, 147, 3, 'Successful wish price #Grapes', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :27/03/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Wish Price / Carton :1</p>\r\n<p>Date :27/03/2018</p>\r\n\r\n', 0, 0, '2018-03-27 12:38:16'),
(740, 59, 3, 'Successful wish price #Grapes', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :27/03/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  :1</p>\r\n<p>Wish Price / Carton :1</p>\r\n<p>Date :27/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-27 12:38:17'),
(741, 148, 1, 'Successfully Registered', '<p>Welcome to The Surplus Exchange!</p>\n\n<p>You have successfully registered with us now as a Seller. </p>\n\n<p>Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p>You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<br /><p>Happy buying/selling.</p>\n\n', 0, 0, '2018-03-27 15:19:14'),
(742, 148, 2, 'Thanks for posting the product #Dry Fruits', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b>Dry Fruits<br />\r\n<b>Brand Name :</b>Cookies<br />\r\n<b>Country of origin :</b>India<br />\r\n<b>Expiry Date :</b>30/04/2018 <br />\r\n<b>Total Carton :</b>1<br />\r\n<b>Price / Carton :</b>5<br />\r\n\r\n<br/>\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b>warehouse1<br />\r\n<b>Address :</b>Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b>Al Ain<br />\r\n<b>Emirates :</b>Abu Dhabi<br />\r\n<b>Country :</b>UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 15:26:06'),
(743, 148, 1, 'Bank Details varified', '<p>\r\nYou bank profile details verification with us is complete now and we are happy to inform that you can use the features of our exchange.\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-27 17:29:40'),
(744, 141, 2, 'Product Dry Fruits is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Dry Fruits<br />\r\n<b>Brand Name :</b>Cookies<br />\r\n<b>Expiry Date :</b>2018-04-30 00:00:00<br />\r\n<b>Total Carton :</b>1<br />\r\n<b>Price / Carton :</b>5<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 17:31:36'),
(745, 129, 2, 'Product Dry Fruits is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Dry Fruits<br />\r\n<b>Brand Name :</b>Cookies<br />\r\n<b>Expiry Date :</b>2018-04-30 00:00:00<br />\r\n<b>Total Carton :</b>1<br />\r\n<b>Price / Carton :</b>5<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 17:31:38'),
(746, 141, 2, 'Product Dry Fruits is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Dry Fruits<br />\r\n<b>Brand Name :</b>Cookies<br />\r\n<b>Expiry Date :</b>2018-04-30 00:00:00<br />\r\n<b>Total Carton :</b>1<br />\r\n<b>Price / Carton :</b>5<br />\r\n\r\n\r\n', 0, 0, '2018-03-27 17:31:39'),
(747, 148, 2, 'Successfully verified all documents #Dry Fruits', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b>Dry Fruits<br />\r\n<b>Brand Name :</b>Cookies<br />\r\n<b>Expiry Date :</b>30/04/2018  <br /><br />\r\n<b>Total Carton :</b>1<br />\r\n<b>Price / Carton :</b>5<br />\r\n\r\n', 0, 0, '2018-03-27 17:31:41'),
(748, 147, 3, 'Successfully Placed Order #147646852', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product Dry Fruits </p>\r\n\r\n\r\n<p>Product name :Dry Fruits</p>\r\n<p>Carton Purchased :1</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IitmaEZMVVJuOGFNYXN3UlF4ckRsMXc9PSIsInZhbHVlIjoiZk53b2xuV1hXbGRpK3ZGeXl5WkdXdXdTVlA5NDZ6M2NhdEY1UndxVis0MD0iLCJtYWMiOiI5NjgxYjNjOTAyZTc5MWNkODc3MzRlNDA4NjI5MjU1YTY0NDU1ZmEzNjY3ZGU2YjY1OGQzOWM0NmFmNzViY2E0In0=\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-27 19:58:58'),
(749, 148, 3, 'Purchase goods :Dry Fruits', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name :Dry Fruits</p>\r\n<p>Carton Purchased :1</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-27 19:58:59'),
(750, 141, 4, 'Successfully select bid auction 141512964', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Chicken Sausage</p>\r\n<p>Brand name : CountryGreens</p>\r\n<p>Expiry Date : 21/06/2018</p>\r\n<p>Price /Carton : 63</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#141512964</p>\r\n<p>Carton Purchased :1</p>\r\n<p>Bid Price :AED 64</p>\r\n<p>Bid Date : 15/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-28 11:16:24'),
(751, 129, 2, 'Product pomegranate is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> pomegranate<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:53:58'),
(752, 141, 2, 'Product pomegranate is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> pomegranate<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:53:59'),
(753, 141, 2, 'Product pomegranate is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> pomegranate<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:54:00'),
(754, 59, 2, 'Successfully verified all documents #pomegranate', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> pomegranate<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n', 0, 0, '2018-03-29 10:54:03'),
(755, 129, 2, 'Product litchi is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> litchi<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 75<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:56:58'),
(756, 141, 2, 'Product litchi is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> litchi<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 75<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:56:59'),
(757, 141, 2, 'Product litchi is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> litchi<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 75<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:57:00'),
(758, 59, 2, 'Successfully verified all documents #litchi', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> litchi<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 16/05/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 75<br />\r\n\r\n', 0, 0, '2018-03-29 10:57:03'),
(759, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Broccoli<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:57:39'),
(760, 141, 2, 'Product Broccoli is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Broccoli<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:57:40'),
(761, 59, 2, 'Successfully verified all documents #Broccoli', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Broccoli<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 68<br />\r\n\r\n', 0, 0, '2018-03-29 10:57:42'),
(762, 141, 2, 'Product bitter melon is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> bitter melon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:58:06'),
(763, 141, 2, 'Product bitter melon is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> bitter melon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:58:07'),
(764, 59, 2, 'Successfully verified all documents #bitter melon', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> bitter melon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n', 0, 0, '2018-03-29 10:58:09'),
(765, 129, 2, 'Product Watermelon is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Watermelon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 25<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:58:45'),
(766, 141, 2, 'Product Watermelon is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Watermelon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 25<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:58:46'),
(767, 141, 2, 'Product Watermelon is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Watermelon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 25<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:58:48'),
(768, 59, 2, 'Successfully verified all documents #Watermelon', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Watermelon<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 25<br />\r\n\r\n', 0, 0, '2018-03-29 10:58:55'),
(769, 141, 2, 'Product Redpeeper is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Redpeeper<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 300<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:59:29'),
(770, 141, 2, 'Product Redpeeper is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Redpeeper<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 300<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 10:59:31'),
(771, 59, 2, 'Successfully verified all documents #Redpeeper', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Redpeeper<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018  <br /><br />\r\n<b>Total Carton :</b> 300<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n', 0, 0, '2018-03-29 10:59:33'),
(772, 141, 2, 'Product Beans is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:00:22'),
(773, 141, 2, 'Product Beans is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:00:23'),
(774, 59, 2, 'Successfully verified all documents #Beans', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n', 0, 0, '2018-03-29 11:00:25'),
(775, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 29/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:00:58'),
(776, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 29/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:00:59'),
(777, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 29/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:00'),
(778, 59, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 29/05/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n', 0, 0, '2018-03-29 11:01:03'),
(779, 129, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Grapes<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:12'),
(780, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Grapes<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:14'),
(781, 141, 2, 'Product Grapes is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Grapes<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:15'),
(782, 59, 2, 'Successfully verified all documents #Grapes', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Grapes<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n', 0, 0, '2018-03-29 11:01:17'),
(783, 141, 2, 'Product Carrot is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:46'),
(784, 141, 2, 'Product Carrot is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:01:47'),
(785, 59, 2, 'Successfully verified all documents #Carrot', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n', 0, 0, '2018-03-29 11:01:50'),
(786, 129, 2, 'Product Blackberry is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Blackberry<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 500<br />\r\n<b>Price / Carton :</b> AED 190<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:08'),
(787, 141, 2, 'Product Blackberry is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Blackberry<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 500<br />\r\n<b>Price / Carton :</b> AED 190<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:09'),
(788, 141, 2, 'Product Blackberry is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Blackberry<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 500<br />\r\n<b>Price / Carton :</b> AED 190<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:10'),
(789, 58, 2, 'Successfully verified all documents #Blackberry', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Blackberry<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018  <br /><br />\r\n<b>Total Carton :</b> 500<br />\r\n<b>Price / Carton :</b> AED 190<br />\r\n\r\n', 0, 0, '2018-03-29 11:03:13'),
(790, 129, 2, 'Product Green Banana is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Green Banana<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 58<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:53'),
(791, 141, 2, 'Product Green Banana is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Green Banana<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 58<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:54'),
(792, 141, 2, 'Product Green Banana is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Green Banana<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 58<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:03:55'),
(793, 58, 2, 'Successfully verified all documents #Green Banana', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Green Banana<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 58<br />\r\n\r\n', 0, 0, '2018-03-29 11:03:57');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(794, 129, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 15/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 95<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:04:56'),
(795, 141, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 15/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 95<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:04:57'),
(796, 141, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 15/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 95<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:04:58'),
(797, 58, 2, 'Successfully verified all documents #apple', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 15/05/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 95<br />\r\n\r\n', 0, 0, '2018-03-29 11:05:01'),
(798, 141, 2, 'Product Peas is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Peas<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:05:55'),
(799, 141, 2, 'Product Peas is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Peas<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:05:56'),
(800, 58, 2, 'Successfully verified all documents #Peas', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Peas<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 30/04/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 85<br />\r\n\r\n', 0, 0, '2018-03-29 11:05:59'),
(801, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 24/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:07:03'),
(802, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 24/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:07:04'),
(803, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 24/04/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:07:06'),
(804, 140, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 4<br />\r\n<b>Expiry Date :</b> 24/04/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n', 0, 0, '2018-03-29 11:07:09'),
(805, 141, 2, 'Product Beans is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:10:52'),
(806, 141, 2, 'Product Beans is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018<br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:10:54'),
(807, 59, 2, 'Successfully verified all documents #Beans', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Beans<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 13/05/2018  <br /><br />\r\n<b>Total Carton :</b> 1000<br />\r\n<b>Price / Carton :</b> AED 65<br />\r\n\r\n', 0, 0, '2018-03-29 11:10:57'),
(808, 141, 2, 'Product Carrot is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:11:40'),
(809, 141, 2, 'Product Carrot is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018<br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 11:11:42'),
(810, 59, 2, 'Successfully verified all documents #Carrot', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Carrot<br />\r\n<b>Brand Name :</b> Indian<br />\r\n    <b>Country of origin :</b> 3<br />\r\n<b>Expiry Date :</b> 31/05/2018  <br /><br />\r\n<b>Total Carton :</b> 100<br />\r\n<b>Price / Carton :</b> AED 45<br />\r\n\r\n', 0, 0, '2018-03-29 11:11:44'),
(811, 59, 2, 'Thanks for posting the product #apple', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> india<br />\r\n<b>Country of origin :</b> India<br />\r\n<b>Expiry Date :</b> 30/04/2019 <br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> 25<br />\r\n\r\n<br/>\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b> warehouse1<br />\r\n<b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b> Al Ain<br />\r\n<b>Emirates :</b> Abu Dhabi<br />\r\n<b>Country :</b> UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 12:05:14'),
(812, 59, 2, 'Product status : apple', '<p>Your request for posting of the following product is rejected due to  following discrepancies.</p>\r\n\r\n\r\n<p><b>bad product</b></p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> india<br />\r\n<b>Country of origin :</b>4<br />\r\n<b>Expiry Date :</b> 30/04/2019<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 25<br />\r\n\r\n<p>We apologize for the inconvinience this time and look forward to your future participation very soon.</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 12:12:32'),
(813, 26, 3, 'Successful wish price #Grapes', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 10</p>\r\n<p>Wish Price / Carton : AED 60</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n', 0, 0, '2018-03-29 12:18:29'),
(814, 59, 3, 'Successful wish price #Grapes', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 10</p>\r\n<p>Wish Price / Carton : AED 60</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 12:18:30'),
(815, 150, 1, 'Successfully Registered', '<p>Welcome to The Surplus Exchange!</p>\n\n<p>You have successfully registered with us now as a Seller. </p>\n\n<p>Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p>You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<br /><p>Happy buying/selling.</p>\n\n', 0, 0, '2018-03-29 17:53:18'),
(816, 151, 1, 'Successfully Registered', '<p>Welcome to The Surplus Exchange!</p>\n\n<p>You have successfully registered with us now as a Seller. </p>\n\n<p>Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p>You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<br /><p>Happy buying/selling.</p>\n\n', 0, 0, '2018-03-29 18:40:42'),
(817, 151, 2, 'Thanks for posting the product #apple', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> indian<br />\r\n<b>Country of origin :</b> India<br />\r\n<b>Expiry Date :</b> 23/04/2019 <br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> 20<br />\r\n\r\n<br/>\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b> warehouse1<br />\r\n<b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b> Al Ain<br />\r\n<b>Emirates :</b> Abu Dhabi<br />\r\n<b>Country :</b> UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 18:45:24'),
(818, 151, 1, 'Bank Details varified', '<p>\r\nYou bank profile details verification with us is complete now and we are happy to inform that you can use the features of our exchange.\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 18:50:04'),
(819, 129, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> indian<br />\r\n<b>Expiry Date :</b> 23/04/2019<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 20<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 18:53:12'),
(820, 141, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> indian<br />\r\n<b>Expiry Date :</b> 23/04/2019<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 20<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 18:53:14'),
(821, 141, 2, 'Product apple is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> indian<br />\r\n<b>Expiry Date :</b> 23/04/2019<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 20<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 18:53:15'),
(822, 151, 2, 'Successfully verified all documents #apple', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> apple<br />\r\n<b>Brand Name :</b> indian<br />\r\n<b>Expiry Date :</b> 23/04/2019  <br /><br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 20<br />\r\n\r\n', 0, 0, '2018-03-29 18:53:23'),
(823, 141, 3, 'Successful wish price #Green Banana', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Wish Price / Carton : AED 1</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n', 0, 0, '2018-03-29 19:01:03'),
(824, 58, 3, 'Successful wish price #Green Banana', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Wish Price / Carton : AED 1</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:01:05'),
(825, 141, 3, 'Successful wish price #Green Banana', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Wish Price / Carton : AED 2</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n', 0, 0, '2018-03-29 19:01:31'),
(826, 58, 3, 'Successful wish price #Green Banana', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Green Banana</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Wish Price / Carton : AED 2</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:01:33'),
(827, 26, 3, 'Successfully Placed Order #26669304', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product apple </p>\r\n\r\n\r\n<p>Product name : apple</p>\r\n<p>Carton Purchased : 20</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InVvRDZaMkpaN25lUE0zZ3BzdThJblE9PSIsInZhbHVlIjoic05OZkIxY3o4WTZJWE5Wc2duUHpqUT09IiwibWFjIjoiMzUyNDVjNmZhNmYzNmQ1NGY2NWNhYjYxN2QyMTVmNjIyMmE0MTAyZDZkYTM2NzU5N2JjODU4ZWNmZDk1MjY2NyJ9\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-29 19:02:10'),
(828, 151, 3, 'Purchase goods :apple', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name : apple</p>\r\n<p>Carton Purchased : 20</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-29 19:02:11'),
(829, 26, 4, 'Payment slip uploaded agenest order number #26669304', '<p>\r\n    The payment slip against the order ( 26669304 ) has been received by us.\r\n</p>\r\n<p> \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n', 0, 0, '2018-03-29 19:06:02'),
(830, 26, 4, 'Payment successfully varified for order number 26669304', '<br/><br/>\n<p>\n    Your payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n\n<b>Product Name :</b> apple<br />\n<b>Order Number :</b> 26669304 <br />\n\n\n\n<br/>\n', 0, 0, '2018-03-29 19:07:08'),
(831, 151, 4, 'Payment successfully varified for order number 26669304', '<br/><br/>\n<p>\n    Payment against the following purchase has been accepted.\n</p>\n<p>\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p>\n<b>Product Name :</b> apple<br />\n<b>Order Number :</b> 26669304 <br />\n</p>\n\n', 0, 0, '2018-03-29 19:07:10'),
(832, 26, 3, 'Successful bid #Broccoli', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 22/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 994</p>\r\n<p>Bid Price / Carton : AED 35</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:12:09'),
(833, 147, 3, 'Successful bid #Broccoli', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Broccoli</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    22/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 994</p>\r\n<p>Bid Price / Carton : AED 35</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:12:11'),
(834, 84, 3, 'Successful wish price #Grapes', '<p>We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 12</p>\r\n<p>Wish Price / Carton : AED 60</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n', 0, 0, '2018-03-29 19:18:48'),
(835, 59, 3, 'Successful wish price #Grapes', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Grapes</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 12</p>\r\n<p>Wish Price / Carton : AED 60</p>\r\n<p>Date : 29/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:18:49'),
(836, 26, 3, 'Successful bid #Peas', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Peas</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 80</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:24:07'),
(837, 58, 3, 'Successful bid #Peas', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Peas</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 80</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:24:08'),
(838, 26, 4, 'Successfully select bid auction 26293953', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Peas</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018</p>\r\n<p>Price /Carton : AED 85</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#26293953</p>\r\n<p>Carton Purchased :100</p>\r\n<p>Bid Price :AED 80</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-29 19:26:41'),
(839, 151, 2, 'Thanks for posting the product #Mango', '<p>Your request for posting of the following product has reached us.</p>\r\n\r\n<p>Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p><b>Product Details:</b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Country of origin :</b> India<br />\r\n<b>Expiry Date :</b> 16/05/2018 <br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> 5<br />\r\n\r\n<br/>\r\n\r\n<p><b>Drop Location:</b></p>\r\n<b>Warehouse Name :</b> warehouse1<br />\r\n<b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India<br />\r\n<b>Locality :</b> Al Ain<br />\r\n<b>Emirates :</b> Abu Dhabi<br />\r\n<b>Country :</b> UAE<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 19:31:26'),
(840, 129, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 5<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 19:32:40'),
(841, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 5<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 19:32:41'),
(842, 141, 2, 'Product Mango is available in site', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Expiry Date :</b> 16/05/2018<br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 5<br />\r\n\r\n\r\n', 0, 0, '2018-03-29 19:32:42'),
(843, 151, 2, 'Successfully verified all documents #Mango', '<p>We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p>Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p>Meanwhile, you may log into our website and fine tune the product details or post more products for sell.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<b>Product Name :</b> Mango<br />\r\n<b>Brand Name :</b> Indian<br />\r\n<b>Expiry Date :</b> 16/05/2018  <br /><br />\r\n<b>Total Carton :</b> 200<br />\r\n<b>Price / Carton :</b> AED 5<br />\r\n\r\n', 0, 0, '2018-03-29 19:32:45'),
(844, 26, 3, 'Successful bid #Mango', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 24/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 20</p>\r\n<p>Bid Price / Carton : AED 61.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:38:22'),
(845, 140, 3, 'Successful bid #Mango', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    24/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 20</p>\r\n<p>Bid Price / Carton : AED 61.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:38:23'),
(846, 26, 3, 'Successful bid #Orange', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 50</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:39:59'),
(847, 133, 3, 'Successful bid #Orange', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 50</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:40:01'),
(848, 151, 3, 'Successful bid #Orange', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 500</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:41:08'),
(849, 133, 3, 'Successful bid #Orange', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 500</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:41:09'),
(850, 26, 3, 'Successful bid #Orange', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 30</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:41:37'),
(851, 133, 3, 'Successful bid #Orange', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 30</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:41:39'),
(852, 151, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 200</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:42:51'),
(853, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 200</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:42:52'),
(854, 26, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 10</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:43:12'),
(855, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 10</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:43:14'),
(856, 26, 4, 'Successfully select bid auction 26104533', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018</p>\r\n<p>Price /Carton : AED 45</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#26104533</p>\r\n<p>Carton Purchased :10</p>\r\n<p>Bid Price :AED 45.5</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-29 19:45:05'),
(857, 151, 4, 'Successfully select bid auction 151107586', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018</p>\r\n<p>Price /Carton : AED 45</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#151107586</p>\r\n<p>Carton Purchased :200</p>\r\n<p>Bid Price :AED 45.5</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-29 19:45:06'),
(858, 151, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:47:08'),
(859, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:47:10'),
(860, 26, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 20</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:47:16'),
(861, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 20</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:47:17'),
(862, 26, 4, 'Successfully select bid auction 26107691', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018</p>\r\n<p>Price /Carton : AED 45</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#26107691</p>\r\n<p>Carton Purchased :20</p>\r\n<p>Bid Price :AED 45.5</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-29 19:48:02'),
(863, 151, 4, 'Successfully select bid auction 151104648', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018</p>\r\n<p>Price /Carton : AED 45</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#151104648</p>\r\n<p>Carton Purchased :100</p>\r\n<p>Bid Price :AED 45.5</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-29 19:48:04'),
(864, 84, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.5</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:54:33'),
(865, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.5</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:54:34'),
(866, 84, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:55:36'),
(867, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:55:37'),
(868, 84, 3, 'Successful bid #bitter melon', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 500</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 19:56:01'),
(869, 59, 3, 'Successful bid #bitter melon', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : bitter melon</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 500</p>\r\n<p>Bid Price / Carton : AED 45.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 19:56:02'),
(870, 151, 3, 'Successful bid #Mango', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 24/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 61.50</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 20:34:03'),
(871, 140, 3, 'Successful bid #Mango', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    24/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 61.50</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 20:34:04'),
(872, 151, 3, 'Successful bid #Mango', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 24/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1000</p>\r\n<p>Bid Price / Carton : AED 61</p>\r\n<p>Bid Date : 29/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-29 20:35:13'),
(873, 140, 3, 'Successful bid #Mango', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    24/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1000</p>\r\n<p>Bid Price / Carton : AED 61</p>\r\n<p>Bid Date : 29/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-29 20:35:14'),
(874, 26, 3, 'Successful bid #Orange', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 30/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-30 13:40:25'),
(875, 133, 3, 'Successful bid #Orange', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Orange</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    30/04/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 66.50</p>\r\n<p>Bid Date : 30/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 13:40:26'),
(876, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 10\r\n    <br/>Wish Price / Carton : AED 40\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 14:22:05'),
(877, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 10</p>\r\n<p>Wish Price / Carton : AED 40</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 14:22:06'),
(878, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 100\r\n    <br/>Wish Price / Carton : AED 42.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 14:28:10'),
(879, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 100</p>\r\n<p>Wish Price / Carton : AED 42</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 14:28:12');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(880, 151, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 12\r\n    <br/>Wish Price / Carton : AED 12.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 15:57:59'),
(881, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 12</p>\r\n<p>Wish Price / Carton : AED 12</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 15:58:01'),
(882, 151, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 23\r\n    <br/>Wish Price / Carton : AED 23.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 16:36:14'),
(883, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 23</p>\r\n<p>Wish Price / Carton : AED 23</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 16:36:15'),
(884, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 20\r\n    <br/>Wish Price / Carton : AED 23.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 17:06:10'),
(885, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 20</p>\r\n<p>Wish Price / Carton : AED 23</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 17:06:11'),
(886, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 12\r\n    <br/>Wish Price / Carton : AED 12.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 17:07:30'),
(887, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 12</p>\r\n<p>Wish Price / Carton : AED 12</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 17:07:31'),
(888, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 23\r\n    <br/>Wish Price / Carton : AED 23.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 17:08:33'),
(889, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 23</p>\r\n<p>Wish Price / Carton : AED 23</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 17:08:34'),
(890, 151, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 23\r\n    <br/>Wish Price / Carton : AED 23.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 17:09:46'),
(891, 133, 3, 'Successful wish price #Mango', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 06/04/2018\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 23</p>\r\n<p>Wish Price / Carton : AED 23</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 17:09:47'),
(892, 141, 3, 'Successful wish price #apple', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : apple\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 23/04/2019\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 30/03/2018\r\n</p>\r\n\r\n', 0, 0, '2018-03-30 18:37:05'),
(893, 151, 3, 'Successful wish price #apple', '<p>\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a></p>\r\n\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : apple</p>\r\n<p>Brand name : indian</p>\r\n<p>Expiry Date : 23/04/2019\r\n    </p>\r\n\r\n<p><b><u>Wish price details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Wish Price / Carton : AED 1</p>\r\n<p>Date : 30/03/2018</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 18:37:07'),
(894, 141, 3, 'Successful bid #Mango', '<p>Your bid for the following product has been accepted.\r\nWe will keep you posted on the future development.</p>\r\n\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 16/05/2018\r\n\r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 5.5</p>\r\n<p>Bid Date : 30/03/2018</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-03-30 19:03:54'),
(895, 151, 3, 'Successful bid #Mango', '<p>Buyers are taking notice of your following product</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date :\r\n    16/05/2018\r\n    \r\n    </p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Carton  : 1</p>\r\n<p>Bid Price / Carton : AED 5.5</p>\r\n<p>Bid Date : 30/03/2018 </p>\r\n\r\n<p>You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n\r\n', 0, 0, '2018-03-30 19:03:55'),
(896, 141, 4, 'Successfully select bid auction 141677853', '<p>Congratulations!</p>\r\n<p>Your bid had been accepted by the seller earlier than expected!</p>\r\n<p><b><u>Product Details</u></b></p>\r\n<p>Product Name : Mango</p>\r\n<p>Brand name : Indian</p>\r\n<p>Expiry Date : 16/05/2018</p>\r\n<p>Price /Carton : AED 5</p>\r\n\r\n<p><b><u>Bid details</u></b></p>\r\n<p>Order#141677853</p>\r\n<p>Carton Purchased :1</p>\r\n<p>Bid Price :AED 5.5</p>\r\n<p>Bid Date : 30/03/2018</p>\r\n<p>Please make the payments at the earliest to expedite the delivery.</p>\r\n', 0, 0, '2018-03-30 19:05:31'),
(897, 141, 3, 'Successfully Placed Order #141677853', '<p>Congratulation on the latest win!</p>\r\n<p>Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p>Product name : Mango</p>\r\n<p>Carton Purchased : 1</p>\r\n<p><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IldkOWhCbWpHMHg4MWh1TjBUd2hVNXc9PSIsInZhbHVlIjoiOXUzQ3dhUExaWVArNWg2cm1XbWlBTHFsYjlxVEQxY0tRaElvWE1UbjA1dz0iLCJtYWMiOiI1MjhjZmQ5YzdjMjdjZmVkNjFjYWE0Nzc5NmU1YjliMjI5MzkwMGUzYWYyOWQ4OGExZjA0YzU0MDAyOTkxNTljIn0=\">Click here</a> to upload your payment slip / Status of order </p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-30 19:14:49'),
(898, 151, 3, 'Purchase goods :Mango', '<p>Congratulation on the latest sale!</p>\r\n<p>Product name : Mango</p>\r\n<p>Carton Purchased : 1</p>\r\n\r\n<p>Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-03-30 19:14:50'),
(899, 26, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 5.50\r\n    <br/>Bid Date : 31/03/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-03-31 17:04:42'),
(900, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 5.50\r\n\r\n    <br/> Bid Date : 31/03/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-03-31 17:04:43'),
(901, 26, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 3\r\n    <br/>Bid Price / Carton : AED 6.00\r\n    <br/>Bid Date : 31/03/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-03-31 17:05:06'),
(902, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 3\r\n    <br/>Bid Price / Carton : AED 6.00\r\n\r\n    <br/> Bid Date : 31/03/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-03-31 17:05:07'),
(903, 151, 2, 'Thanks for posting the product #kisan carrot', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan carrot\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 17:29:52'),
(904, 151, 2, 'Thanks for posting the product #prawn', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> prawn\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 250.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 17:35:00'),
(905, 151, 2, 'Thanks for posting the product #kisan jam', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan jam\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 16/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 40.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 17:38:37'),
(906, 151, 2, 'Thanks for posting the product #kisan fruti', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan fruti\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 30.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 17:42:27'),
(907, 151, 2, 'Thanks for posting the product #kisan patato', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan patato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 1000\r\n    <br />\r\n    <b>Price / Carton :</b> AED 8.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 17:50:07'),
(908, 151, 2, 'Thanks for posting the product #kisan tamato', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 5.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 18:01:00'),
(909, 129, 2, 'Product kisan fruti is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan fruti\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 30.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-03-31 18:01:02'),
(910, 141, 2, 'Product kisan fruti is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan fruti\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 30.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-03-31 18:01:03'),
(911, 151, 2, 'Successfully verified all documents #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Relax and sit back, when we go about selling your product.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Meanwhile, you may log into our website and fine tune the product details or post more products for sell.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan fruti\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 30.00 \r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 18:01:07'),
(912, 129, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 5.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-03-31 18:01:26'),
(913, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 5.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-03-31 18:01:27'),
(914, 151, 2, 'Successfully verified all documents #kisan tamato', '<p style=\"line-height: 1.4\">\r\n    We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Relax and sit back, when we go about selling your product.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Meanwhile, you may log into our website and fine tune the product details or post more products for sell.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 5.00 \r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-03-31 18:01:29'),
(915, 26, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : kisan tamato\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 4\r\n    <br/>Bid Price / Carton : AED 11.50\r\n    <br/>Bid Date : 31/03/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-03-31 18:07:34'),
(916, 151, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : kisan tamato\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 4\r\n    <br/>Bid Price / Carton : AED 11.50\r\n\r\n    <br/> Bid Date : 31/03/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-03-31 18:07:35'),
(917, 26, 3, 'Successfully Placed Order #26257629', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ijdqb1d0cm9VYzljYUNJVks2K0dvZHc9PSIsInZhbHVlIjoiQzRZdXNscFB4ZEQ1aDUzSDBZMXdVUT09IiwibWFjIjoiZTg3YTZkNWRiYzJkMDIzNTU0ZTBjNTUyYTc3MjRmZWI5YTgwYzhhNDE4ODVlMGJiZGY1M2U2NjYyZTVmMWFiMiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 00:25:11'),
(918, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 00:25:13'),
(919, 152, 3, 'Successful Order 152275524', 'Successful Order on 2018-04-02 01:04:16', 0, 0, '2018-04-02 01:04:16'),
(920, 58, 3, 'Successful bid', 'Successful bid on 2018-04-02 01:04:17', 0, 0, '2018-04-02 01:04:17'),
(921, 26, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : \r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 02/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-02 10:42:23'),
(922, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : \r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 02/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-02 10:42:24'),
(923, 59, 2, 'Thanks for posting the product #aoo', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> aoo\r\n    <br />\r\n    <b>Brand Name :</b> test\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 18/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 258\r\n    <br />\r\n    <b>Price / Carton :</b> AED 23.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-02 11:15:27'),
(924, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 02/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 11:55:45'),
(925, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018</p>\r\n\r\n', 0, 0, '2018-04-02 11:55:46'),
(926, 26, 3, 'Successfully Placed Order #26107691', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product bitter melon </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : bitter melon\r\n<br/>Carton Purchased : 20\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ilh1K2hSM0RMV1dvUThRd0szRHMxVFE9PSIsInZhbHVlIjoiNzEyOFdubEkyckl2MG8rKzh4eGdyQT09IiwibWFjIjoiOTI5MmRlMjQ2YWE5ZjVjZDVhZWYyYjVmZGRlZGFmY2RjOTZkZGY3ZTQzMmViMjg4MmQwMGUxMmUxNjVkYTZlOSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 13:22:41'),
(927, 59, 3, 'Purchase goods :bitter melon', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : bitter melon\r\n    <br/>\r\n    Carton Purchased : 20\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 13:22:42'),
(928, 26, 4, 'Payment slip uploaded agenest order number #26107691', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 26107691 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 13:26:06'),
(929, 153, 3, 'Successful Order 153567542', 'Successful Order on 2018-04-02 13:30:34', 0, 0, '2018-04-02 13:30:34'),
(930, 133, 3, 'Successful bid', 'Successful bid on 2018-04-02 13:30:36', 0, 0, '2018-04-02 13:30:36'),
(931, 154, 3, 'Successful Order 154562488', 'Successful Order on 2018-04-02 13:38:56', 0, 0, '2018-04-02 13:38:56'),
(932, 133, 3, 'Successful bid', 'Successful bid on 2018-04-02 13:38:57', 0, 0, '2018-04-02 13:38:57'),
(933, 26, 4, 'Payment successfully varified for order number 26107691', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> bitter melon<br />\n<b>Order Number :</b> 26107691 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-02 13:50:01'),
(934, 59, 4, 'Payment successfully varified for order number 26107691', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> bitter melon<br />\n    <b>Order Number :</b> 26107691 <br />\n</p>\n\n', 0, 0, '2018-04-02 13:50:02'),
(935, 26, 4, 'Payment slip rejected for order number # 2658671', '<p style=\"line-height: 1.4\" >\nYour transaction is on hold with the payslip against the recent purchase.\n</p>\n<p style=\"line-height: 1.4\" >\n<b>Product Name :</b> Yellow Pepper<br />\n<b>Order Number :</b> 2658671 <br />\n</p>\n<p style=\"line-height: 1.4\">\nPlease upload a fresh document here or contact our support at <number> for additional details.\n</p>\n\n', 0, 0, '2018-04-02 13:50:38'),
(936, 26, 4, 'Order #26107691 cancelled', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #26107691:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>26107691</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                20\r\n            </td>\r\n            <td>\r\n                AED 910.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED35.00             </td>\r\n            <td class=\"action\">\r\n                AED 945.00\r\n            </td>\r\n            <td>\r\n                                 Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 47.25<br />\r\n                AED 992.25\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-02 14:19:26'),
(937, 59, 4, 'order #26107691 cancelled', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #26107691:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>26107691</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                20\r\n            </td>\r\n            <td>\r\n                AED 910.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED35.00             </td>\r\n            <td class=\"action\">\r\n                AED 945.00\r\n            </td>\r\n            <td>\r\n                                 Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 47.25<br />\r\n                AED 992.25\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-02 14:19:28'),
(938, 26, 4, 'Your order #2639329 returned successfully', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #2639329:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>2639329</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED3.00             </td>\r\n            <td class=\"action\">\r\n                AED 48.00\r\n            </td>\r\n            <td>\r\n                                 Returned\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.40<br />\r\n                AED 50.40\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-02 14:55:37'),
(939, 59, 4, 'Your order #2639329 returned successfully', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #2639329:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>2639329</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 45.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED3.00             </td>\r\n            <td class=\"action\">\r\n                AED 48.00\r\n            </td>\r\n            <td>\r\n                                 Returned\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 2.40<br />\r\n                AED 50.40\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-02 14:55:38'),
(940, 26, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-02 15:08:56'),
(941, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:08:58'),
(942, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 02/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:09:37'),
(943, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018</p>\r\n\r\n', 0, 0, '2018-04-02 15:09:38'),
(944, 26, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:09:53'),
(945, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 02/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:09:54'),
(946, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 02/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:10:20'),
(947, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018</p>\r\n\r\n', 0, 0, '2018-04-02 15:10:21'),
(948, 26, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-02 15:10:47'),
(949, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 02/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-02 15:10:48'),
(950, 26, 3, 'Successfully Placed Order #26714301', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InZLdXhNUjhsXC9hb0xwclhKV2FsWjdRPT0iLCJ2YWx1ZSI6IkEzbDFhRnhkQmVMRWozcFlISzNYOUE9PSIsIm1hYyI6ImI2NWNlZjU2ZDc5NDMxODI5NWI0YWMyMzY5NmM5MjBiY2M0MmM4YzQ3OTk2MmQyZjA1YWUzNzcxMDk3YTAwMjQifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 15:19:12'),
(951, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-02 15:19:13');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(952, 26, 4, 'My Requrement Fruits', '<!doctype html>\r\n<html>\r\n    <body style=\"padding:0; margin:0; width: 97%;border: solid 1px #1f62b6;\">\r\n        <div style=\"height: auto;padding: 10px 0;background-color:#2a77d7;\">\r\n            <div style=\"width: 100%; margin:0 auto;color: #fff;\">               \r\n                   \r\n                        <img src=\"http://tsdemo.in/auctionportal/public/assets/logo_email.png\" alt=\"Surplus Store\" style=\"height:auto;padding-left: 10px;float:left;\">\r\n                    \r\n                <div style=\"clear:both;\"></div>\r\n            </div>\r\n        </div>\r\n        <div style=\"clear:both;\"></div>\r\n        <div style=\"width: 100%; margin:0 auto;\">\r\n            <div style=\"padding: 0 10px;\"><br/>\r\nDear Puru Sahoo,\r\n<br/>\r\n\r\n<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n\r\nThanks<br />\r\nAdmin<br />\r\nSurplus Store, Dubai <br/>\r\n<br/>\r\n</div>\r\n        </div>\r\n        <div style=\"padding:1px 0; background-color:#1f62b6;position:absolute;bottom:0;\">\r\n            <p style=\"font-size:10px; color:#fff; text-align:center;\">\r\n                 \r\n                        Contact : <a style=\"color: #fff;\">(+ 971) 48858378 / 8853930</a> \r\n                        <br/>\r\n                        Mail : <a style=\"color: #fff;\">info@akmfoods.com</a>\r\n                        <br/>\r\n                        <br/>\r\n                \r\n                Copyright 2018 Surplus store.  All rights reserved.\r\n            </p>\r\n\r\n        </div>\r\n    </body>\r\n</html>', 0, 0, '2018-04-02 15:40:14'),
(953, 59, 4, 'Buyer Requiremt Fruits', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fruits category product of unit 1 kg expected price per carton AED 30 with in 09/04/2018</p>\r\n\r\n', 0, 0, '2018-04-02 15:40:15'),
(954, 128, 4, 'Buyer Requiremt Fruits', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fruits category product of unit 1 kg expected price per carton AED 30 with in 09/04/2018</p>\r\n\r\n', 0, 0, '2018-04-02 15:40:17'),
(955, 26, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 03/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-03 20:40:05'),
(956, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 03/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-03 20:40:07'),
(957, 26, 2, 'Thanks for posting the product #test', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> test\r\n    <br />\r\n    <b>Brand Name :</b> testssgs\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 25/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 258\r\n    <br />\r\n    <b>Price / Carton :</b> AED 253.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-03 20:48:24'),
(958, 26, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 5\r\n    <br/>Wish Price / Carton : AED 55.00\r\n    <br/>\r\n    Date : 03/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-03 20:49:16'),
(959, 59, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 5\r\n<br/>Wish Price / Carton : AED 55.00\r\n<br/>Date : 03/04/2018</p>\r\n\r\n', 0, 0, '2018-04-03 20:49:18'),
(960, 26, 3, 'Successfully Placed Order #26255420', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 5\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InhqRE9GdkdvblwvXC83RmJWZ0VUNGg0UT09IiwidmFsdWUiOiJscEltVUZhd1RwRTRVVTN6Ym1YSUVBPT0iLCJtYWMiOiIyYTEyOGI5ZGRjYzJlYTYxYTA2ZjE5YWEwNTBmMTZmN2U0MzM4NDZkZDAxYjE0ZWVlZjEzYzdjN2MwZWJiMzE4In0=\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-03 20:49:37'),
(961, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-03 20:49:38'),
(962, 150, 2, 'Thanks for posting the product #chicken ', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> chicken \r\n    <br />\r\n    <b>Brand Name :</b> india(mk)\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 05/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 10\r\n    <br />\r\n    <b>Price / Carton :</b> AED 25.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 11:20:08'),
(963, 26, 2, 'Thanks for posting the product #test', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> test\r\n    <br />\r\n    <b>Brand Name :</b> rrrr\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 26/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 22\r\n    <br />\r\n    <b>Price / Carton :</b> AED 11.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 11:56:04'),
(964, 155, 1, 'Successfully Registered', '<p style=\"line-height: 1.4\" >Welcome to The Surplus Exchange!</p>\n\n<p style=\"line-height: 1.4\" >You have successfully registered with us now as a Seller. </p>\n\n<p style=\"line-height: 1.4\" >Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p style=\"line-height: 1.4\" >You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<p style=\"line-height: 1.4\">Happy buying/selling.</p>\n\n', 0, 0, '2018-04-04 12:22:46'),
(965, 155, 2, 'Thanks for posting the product #yellowfruit', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> yellowfruit\r\n    <br />\r\n    <b>Brand Name :</b> mk\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 12/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 10\r\n    <br />\r\n    <b>Price / Carton :</b> AED 30.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 12:28:47'),
(966, 155, 2, 'Thanks for posting the product #fruitm', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> fruitm\r\n    <br />\r\n    <b>Brand Name :</b> mk\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 19/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 10\r\n    <br />\r\n    <b>Price / Carton :</b> AED 50.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 12:40:02'),
(967, 156, 1, 'Successfully Registered', '<p style=\"line-height: 1.4\" >Welcome to The Surplus Exchange!</p>\n\n<p style=\"line-height: 1.4\" >You have successfully registered with us now as a Buyer. </p>\n\n<p style=\"line-height: 1.4\" >Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p style=\"line-height: 1.4\" >You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<p style=\"line-height: 1.4\">Happy buying/selling.</p>\n\n', 0, 0, '2018-04-04 13:05:21'),
(968, 155, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 45.00\r\n    <br/>\r\n    Date : 04/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-04 13:16:30'),
(969, 133, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 45.00\r\n<br/>Date : 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 13:16:31'),
(970, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 15:04:59'),
(971, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 15:05:00'),
(972, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 23.00\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 15:05:22'),
(973, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 23.00\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 15:05:23'),
(974, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 19,933.00\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 15:05:48'),
(975, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 19,933.00\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 15:05:49'),
(976, 26, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 16:17:48'),
(977, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 16:17:49'),
(978, 155, 3, 'Successful bid #Beans', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Beans\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 13/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 30.50\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 16:33:26'),
(979, 59, 3, 'Successful bid #Beans', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Beans\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    13/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 30.50\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 16:33:27'),
(980, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 16:40:37'),
(981, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 20.50\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 16:40:38'),
(982, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 100\r\n    <br/>Bid Price / Carton : AED 20.00\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 16:42:32'),
(983, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 100\r\n    <br/>Bid Price / Carton : AED 20.00\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 16:42:33'),
(984, 84, 2, 'Thanks for posting the product #rrr', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> rrr\r\n    <br />\r\n    <b>Brand Name :</b> rrrt\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 04/04/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 25\r\n    <br />\r\n    <b>Price / Carton :</b> AED 123.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 16:59:10'),
(985, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 17:02:19'),
(986, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 17:02:20'),
(987, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1000\r\n    <br/>Bid Price / Carton : AED 61.00\r\n    <br/>Bid Date : 04/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-04 17:54:08'),
(988, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1000\r\n    <br/>Bid Price / Carton : AED 61.00\r\n\r\n    <br/> Bid Date : 04/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-04 17:54:09'),
(989, 151, 3, 'Successfully Placed Order #15126283', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Grapes </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Grapes\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IlFTM2ZqcXZEU2JBQlRNT3pHMGx5dkE9PSIsInZhbHVlIjoiYVh2dEtWazFDb2tJbkcyc2pXQ2Ntdz09IiwibWFjIjoiYjBiZGIxZjJlYjRjZjhmMGQ1NzE1Y2IxZjYzM2JiZTYyN2VhNDFjZjZmNzJjMDYyNmMzNWRkOTE2OWMyYmViZiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 18:01:02'),
(990, 59, 3, 'Purchase goods :Grapes', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Grapes\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 18:01:03'),
(991, 26, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 8\r\n    <br/>Wish Price / Carton : AED 5.00\r\n    <br/>\r\n    Date : 04/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-04 19:56:07'),
(992, 133, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 06/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 8\r\n<br/>Wish Price / Carton : AED 5.00\r\n<br/>Date : 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 19:56:08'),
(993, 26, 3, 'Successful wish price #Grapes', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Grapes\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 5\r\n    <br/>Wish Price / Carton : AED 2.00\r\n    <br/>\r\n    Date : 04/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-04 20:01:56'),
(994, 59, 3, 'Successful wish price #Grapes', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Grapes\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 5\r\n<br/>Wish Price / Carton : AED 2.00\r\n<br/>Date : 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 20:01:57'),
(995, 26, 2, 'Thanks for posting the product #kisan jam', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan jam\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 08/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 20:37:47'),
(996, 26, 2, 'Thanks for posting the product #test', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> test\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 04/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 20:39:24'),
(997, 26, 2, 'Thanks for posting the product #test', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> test\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 04/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 500\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 20:42:11'),
(998, 26, 2, 'Thanks for posting the product #hdhhd', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> hdhhd\r\n    <br />\r\n    <b>Brand Name :</b> jduf\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 04/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 5\r\n    <br />\r\n    <b>Price / Carton :</b> AED 1.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 21:07:45'),
(999, 26, 2, 'Thanks for posting the product #hxhdu', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> hxhdu\r\n    <br />\r\n    <b>Brand Name :</b> gsyys\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 04/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 3\r\n    <br />\r\n    <b>Price / Carton :</b> AED 54.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-04 21:09:35'),
(1000, 26, 4, 'My Requrement Fish', '<!doctype html>\r\n<html>\r\n    <body style=\"padding:0; margin:0; width: 97%;border: solid 1px #1f62b6;\">\r\n        <div style=\"height: auto;padding: 10px 0;background-color:#2a77d7;\">\r\n            <div style=\"width: 100%; margin:0 auto;color: #fff;\">               \r\n                   \r\n                        <img src=\"http://tsdemo.in/auctionportal/public/assets/logo_email.png\" alt=\"Surplus Store\" style=\"height:auto;padding-left: 10px;float:left;\">\r\n                    \r\n                <div style=\"clear:both;\"></div>\r\n            </div>\r\n        </div>\r\n        <div style=\"clear:both;\"></div>\r\n        <div style=\"width: 100%; margin:0 auto;\">\r\n            <div style=\"padding: 0 10px;\"><br/>\r\nDear Puru Sahoo,\r\n<br/>\r\n\r\n<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n\r\nThanks<br />\r\nAdmin<br />\r\nSurplus Store, Dubai <br/>\r\n<br/>\r\n</div>\r\n        </div>\r\n        <div style=\"padding:1px 0; background-color:#1f62b6;position:absolute;bottom:0;\">\r\n            <p style=\"font-size:10px; color:#fff; text-align:center;\">\r\n                 \r\n                        Contact : <a style=\"color: #fff;\">(+ 971) 48858378 / 8853930</a> \r\n                        <br/>\r\n                        Mail : <a style=\"color: #fff;\">info@akmfoods.com</a>\r\n                        <br/>\r\n                        <br/>\r\n                \r\n                Copyright 2018 Surplus store.  All rights reserved.\r\n            </p>\r\n\r\n        </div>\r\n    </body>\r\n</html>', 0, 0, '2018-04-04 21:11:29'),
(1001, 82, 4, 'Buyer Requiremt Fish', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fish category product of unit KG expected price per carton AED 599 with in 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 21:11:30'),
(1002, 84, 4, 'Buyer Requiremt Fish', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fish category product of unit KG expected price per carton AED 599 with in 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 21:11:31'),
(1003, 26, 4, 'My Requrement Fruits', '<!doctype html>\r\n<html>\r\n    <body style=\"padding:0; margin:0; width: 97%;border: solid 1px #1f62b6;\">\r\n        <div style=\"height: auto;padding: 10px 0;background-color:#2a77d7;\">\r\n            <div style=\"width: 100%; margin:0 auto;color: #fff;\">               \r\n                   \r\n                        <img src=\"http://tsdemo.in/auctionportal/public/assets/logo_email.png\" alt=\"Surplus Store\" style=\"height:auto;padding-left: 10px;float:left;\">\r\n                    \r\n                <div style=\"clear:both;\"></div>\r\n            </div>\r\n        </div>\r\n        <div style=\"clear:both;\"></div>\r\n        <div style=\"width: 100%; margin:0 auto;\">\r\n            <div style=\"padding: 0 10px;\"><br/>\r\nDear Puru Sahoo,\r\n<br/>\r\n\r\n<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n\r\nThanks<br />\r\nAdmin<br />\r\nSurplus Store, Dubai <br/>\r\n<br/>\r\n</div>\r\n        </div>\r\n        <div style=\"padding:1px 0; background-color:#1f62b6;position:absolute;bottom:0;\">\r\n            <p style=\"font-size:10px; color:#fff; text-align:center;\">\r\n                 \r\n                        Contact : <a style=\"color: #fff;\">(+ 971) 48858378 / 8853930</a> \r\n                        <br/>\r\n                        Mail : <a style=\"color: #fff;\">info@akmfoods.com</a>\r\n                        <br/>\r\n                        <br/>\r\n                \r\n                Copyright 2018 Surplus store.  All rights reserved.\r\n            </p>\r\n\r\n        </div>\r\n    </body>\r\n</html>', 0, 0, '2018-04-04 21:12:34'),
(1004, 59, 4, 'Buyer Requiremt Fruits', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fruits category product of unit KG expected price per carton AED 8 with in 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 21:12:35'),
(1005, 128, 4, 'Buyer Requiremt Fruits', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fruits category product of unit KG expected price per carton AED 8 with in 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 21:12:36'),
(1006, 26, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 5.00\r\n    <br/>\r\n    Date : 04/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-04 21:13:32'),
(1007, 59, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 5.00\r\n<br/>Date : 04/04/2018</p>\r\n\r\n', 0, 0, '2018-04-04 21:13:33'),
(1008, 26, 3, 'Successfully Placed Order #26259597', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InVrVW1QK25KVnp0Vit0YURPaFB1bkE9PSIsInZhbHVlIjoiYVZqZVB2MTh1VG1yNzBwMVR5Uk9QQT09IiwibWFjIjoiYzhhYTdmYzFkOTFjMTg0Mjg5MDliMDJkNzQxZGRiZmFjMjk1MjYwMzY2N2YxMmM4NGMzZTZiZGQ0ZTJhNzcyMSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:13:55'),
(1009, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:13:56'),
(1010, 26, 3, 'Successfully Placed Order #2629386', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Grapes </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Grapes\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ik05N2dmM2JpUFVvUnBXZk1JMjV6ZEE9PSIsInZhbHVlIjoiZzNmamNVVWxBUDhXa3d0Rm1TaTB2Zz09IiwibWFjIjoiYTlhYzI5OWU0MzI2ZWE5N2VmNTRiNmJiZTdlNjFmYzBlMmNhZjFiMDU1NDIxZDVlZmQ2ZmZmNDU1MDY2NDQyZSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:14:40'),
(1011, 59, 3, 'Purchase goods :Grapes', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Grapes\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:14:41'),
(1012, 26, 3, 'Successfully Placed Order #26565085', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImJpQ2ZZeEJ2NWNoVVdyYXpuTWE4WlE9PSIsInZhbHVlIjoiOVRnYWRuSlFWSzNRaGJVNzV2XC9xanc9PSIsIm1hYyI6ImE4NGVjZjVhMGY4NGY5YTUyMzc3MTQ5ODMxZmFjZWNmNTZiNjk4ZWY3NTZjNmM3MWI4YzJmYTAyYTM3ZWI1MmUifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:17:08'),
(1013, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-04 21:17:09'),
(1014, 155, 2, 'Thanks for posting the product #mangotest', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> mangotest\r\n    <br />\r\n    <b>Brand Name :</b> mk\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 16/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 40\r\n    <br />\r\n    <b>Price / Carton :</b> AED 50.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-05 11:06:47'),
(1015, 141, 3, 'Successfully Placed Order #141565735', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InBzRHFrSk5ncXRBaVp3SWVUZVA1OHc9PSIsInZhbHVlIjoiT2ZvZzJteXRNWCtDTWd1SWtUaStCaUFuTFFqMVZRT1ZEU0Rkdk45bnJ3MD0iLCJtYWMiOiJlMjZhMzU5ZDUzZjBjYzQ4NWVlNGZlZGZjMzEyMjlmMTMwMDhhMGJmNDEwNWZkM2M5NjI4ZDE4YjNhNzA3NDkwIn0=\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 11:17:55'),
(1016, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 11:17:56'),
(1017, 141, 4, 'Payment slip uploaded agenest order number #141565735', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141565735 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:25:21'),
(1018, 141, 4, 'Payment successfully varified for order number 141565735', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> Mango<br />\n<b>Order Number :</b> 141565735 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 11:28:13'),
(1019, 133, 4, 'Payment successfully varified for order number 141565735', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> Mango<br />\n    <b>Order Number :</b> 141565735 <br />\n</p>\n\n', 0, 0, '2018-04-05 11:28:14');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(1020, 141, 4, 'Your order #141565735 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #141565735:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141565735</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 90.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED1.50             </td>\r\n            <td class=\"action\">\r\n                AED 91.50\r\n            </td>\r\n            <td>\r\n                                 Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 4.58<br />\r\n                AED 96.08\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-05 11:33:16'),
(1021, 133, 4, 'Your order #141565735 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #141565735:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141565735</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 90.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED1.50             </td>\r\n            <td class=\"action\">\r\n                AED 91.50\r\n            </td>\r\n            <td>\r\n                                 Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 4.58<br />\r\n                AED 96.08\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-05 11:33:17'),
(1022, 141, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 10.00\r\n    <br/>\r\n    Date : 05/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:40:54'),
(1023, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 10.00\r\n<br/>Date : 05/04/2018</p>\r\n\r\n', 0, 0, '2018-04-05 11:40:55'),
(1024, 141, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 10.00\r\n<br/>Date : 05/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-05 11:43:00'),
(1025, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 10.00\r\n<br/>Date : 05/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:43:01'),
(1026, 141, 3, 'Successfully Placed Order #141712453', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImM0dzRcL1RLNTBQelVOT1daemJnSnFnPT0iLCJ2YWx1ZSI6IkJLRlVnN0xlVm1STVU1bzlYMHlmUHdoaEFGSnJrbVM4ZFlldXF0UTNhOUk9IiwibWFjIjoiMjQ3NGE2MTFmMzEzNjlmZGZmMTNhOTU2ZTgxODVmNGRkYmE1YWNiY2RhNTc5YzVlOWJhZDhlMDVmNzZlN2ZkMiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 11:45:38'),
(1027, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 11:45:39'),
(1028, 141, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 05/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:49:11'),
(1029, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 05/04/2018</p>\r\n\r\n', 0, 0, '2018-04-05 11:49:13'),
(1030, 141, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 05/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:49:32'),
(1031, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 05/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 11:49:33'),
(1032, 141, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 5.50\r\n    <br/>Bid Date : 05/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-05 12:01:37'),
(1033, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 5.50\r\n\r\n    <br/> Bid Date : 05/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-05 12:01:39'),
(1034, 141, 4, 'Successfully select bid auction 141673325', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : Mango\r\n    <br/>Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n    <br/>Price /Carton : AED 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #141673325\r\n    <br/>Carton Purchased :1\r\n\r\n    <br/> Bid Price :AED 5.50 \r\n    <br/> Bid Date : 05/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 12:02:30'),
(1035, 141, 4, 'Payment slip uploaded agenest order number #141712453', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141712453 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 12:05:41'),
(1036, 141, 4, 'Payment successfully varified for order number 141712453', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> kisan fruti<br />\n<b>Order Number :</b> 141712453 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 12:05:59'),
(1037, 151, 4, 'Payment successfully varified for order number 141712453', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> kisan fruti<br />\n    <b>Order Number :</b> 141712453 <br />\n</p>\n\n', 0, 0, '2018-04-05 12:06:00'),
(1038, 141, 4, 'Order #141712453 cancelled', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #141712453:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141712453</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 20.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED1.50             </td>\r\n            <td class=\"action\">\r\n                AED 21.50\r\n            </td>\r\n            <td>\r\n                                 Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.08<br />\r\n                AED 22.57\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-05 12:06:06'),
(1039, 151, 4, 'order #141712453 cancelled', '<p style=\"line-height: 1.4\">\r\nPlease find status of your order #141712453:\r\n</p>\r\n\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141712453</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 20.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED1.50             </td>\r\n            <td class=\"action\">\r\n                AED 21.50\r\n            </td>\r\n            <td>\r\n                                 Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.08<br />\r\n                AED 22.57\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n\r\n', 0, 0, '2018-04-05 12:06:08'),
(1040, 129, 2, 'Product kisan jam is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan jam\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 16/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 40.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-05 12:10:19'),
(1041, 141, 2, 'Product kisan jam is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan jam\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 16/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 40.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-05 12:10:20'),
(1042, 151, 2, 'Successfully verified all documents #kisan jam', '<p style=\"line-height: 1.4\">\r\n    We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Relax and sit back, when we go about selling your product.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Meanwhile, you may log into our website and fine tune the product details or post more products for sell.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan jam\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 16/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 40.00 \r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-05 12:10:23'),
(1043, 141, 4, 'My Requrement Vegetable', '<!doctype html>\r\n<html>\r\n    <body style=\"padding:0; margin:0; width: 97%;border: solid 1px #1f62b6;\">\r\n        <div style=\"height: auto;padding: 10px 0;background-color:#2a77d7;\">\r\n            <div style=\"width: 100%; margin:0 auto;color: #fff;\">               \r\n                   \r\n                        <img src=\"http://tsdemo.in/auctionportal/public/assets/logo_email.png\" alt=\"Surplus Store\" style=\"height:auto;padding-left: 10px;float:left;\">\r\n                    \r\n                <div style=\"clear:both;\"></div>\r\n            </div>\r\n        </div>\r\n        <div style=\"clear:both;\"></div>\r\n        <div style=\"width: 100%; margin:0 auto;\">\r\n            <div style=\"padding: 0 10px;\"><br/>\r\nDear Kumar patra,\r\n<br/>\r\n\r\n<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n\r\nThanks<br />\r\nAdmin<br />\r\nSurplus Store, Dubai <br/>\r\n<br/>\r\n</div>\r\n        </div>\r\n        <div style=\"padding:1px 0; background-color:#1f62b6;position:absolute;bottom:0;\">\r\n            <p style=\"font-size:10px; color:#fff; text-align:center;\">\r\n                 \r\n                        Contact : <a style=\"color: #fff;\">(+ 971) 48858378 / 8853930</a> \r\n                        <br/>\r\n                        Mail : <a style=\"color: #fff;\">info@akmfoods.com</a>\r\n                        <br/>\r\n                        <br/>\r\n                \r\n                Copyright 2018 Surplus store.  All rights reserved.\r\n            </p>\r\n\r\n        </div>\r\n    </body>\r\n</html>', 0, 0, '2018-04-05 12:16:11'),
(1044, 26, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1kg expected price per carton AED 20 with in 13/04/2018</p>\r\n\r\n', 0, 0, '2018-04-05 12:16:12'),
(1045, 147, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1kg expected price per carton AED 20 with in 13/04/2018</p>\r\n\r\n', 0, 0, '2018-04-05 12:16:14'),
(1046, 151, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1kg expected price per carton AED 20 with in 13/04/2018</p>\r\n\r\n', 0, 0, '2018-04-05 12:16:15'),
(1047, 26, 3, 'Successfully Placed Order #26104533', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product bitter melon </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : bitter melon\r\n<br/>Carton Purchased : 10\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IkloTHd3clNzUHpQRFF5OFg5WFEzb3c9PSIsInZhbHVlIjoiZ3VKSUE3MWwyT2drZjVzZ1hxaW96QT09IiwibWFjIjoiZTU3NGIyMGQ5ODhmNzg1NWM3MDc0MGYzY2I5NjE3ZTM5NWY2M2I2NTRkODI5OTk4YTRhMWY2MzU2NzllYTE1MiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 12:48:35'),
(1048, 59, 3, 'Purchase goods :bitter melon', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : bitter melon\r\n    <br/>\r\n    Carton Purchased : 10\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 12:48:36'),
(1049, 141, 3, 'Successfully Placed Order #141719973', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ik93ZUJSTHNVRVwvSitFYlBad3VvSUhBPT0iLCJ2YWx1ZSI6Im1ZamRqTTVVdzJxUmtQRlFOREhYN3M1ZU8zVHZcLzZ6Q1R5aWZFd0lcL1dmQT0iLCJtYWMiOiI2NzBhMDAwYzhjMzgyOGEyZGFmZTcyOGVmOTBhNzE1OTY1OThmOTk2YTEzNTkzZTc5ZmJmOGQ3N2EwNTBjZGQyIn0=\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 15:51:59'),
(1050, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 15:52:00'),
(1051, 141, 4, 'Payment slip uploaded agenest order number #141719973', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141719973 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 15:52:13'),
(1052, 141, 4, 'Payment successfully varified for order number 141719973', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> kisan fruti<br />\n<b>Order Number :</b> 141719973 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 15:53:07'),
(1053, 151, 4, 'Payment successfully varified for order number 141719973', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> kisan fruti<br />\n    <b>Order Number :</b> 141719973 <br />\n</p>\n\n', 0, 0, '2018-04-05 15:53:08'),
(1054, 141, 4, 'Order #141719973 cancelled', '<p style=\"line-height: 1.4\">\r\n            We wanted to confirm that the following order has been cancelled.\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141719973:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141719973</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n    Looking forward to your next purchase. Our apologies for the inconvenience, if any.\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 15:53:14'),
(1055, 151, 4, 'order #141719973 cancelled', '<p style=\"line-height: 1.4\">\r\n            We wanted to confirm that the following order has been cancelled.\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141719973:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141719973</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Cancelled\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n    Looking forward to your next purchase. Our apologies for the inconvenience, if any.\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 15:53:15'),
(1056, 141, 3, 'Successfully Placed Order #141715736', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Imd1OGpUNXpKYUZzdHJReDl3N0NJR2c9PSIsInZhbHVlIjoiME5vSEFsQ0FCS1E0UmIrZ1c4TkdSU3l0VkVsb0pkT1hkS1BwWGZyKzliMD0iLCJtYWMiOiJhYzAyMDQ1OTI0OTdlMGI2MGU5MTYyNWM3ODc3ZjE1MzU2YzgzY2VkZjIwZTYwOTgzNDM0ZjZiYjJkNzA0M2MwIn0=\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 15:57:07'),
(1057, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 15:57:08'),
(1058, 141, 4, 'Payment slip uploaded agenest order number #141715736', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141715736 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 15:57:35'),
(1059, 141, 4, 'Payment successfully varified for order number 141715736', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> kisan fruti<br />\n<b>Order Number :</b> 141715736 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 15:57:56'),
(1060, 151, 4, 'Payment successfully varified for order number 141715736', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> kisan fruti<br />\n    <b>Order Number :</b> 141715736 <br />\n</p>\n\n', 0, 0, '2018-04-05 15:57:57'),
(1061, 141, 4, 'Your order #141715736 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141715736:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141715736</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 15:58:15'),
(1062, 151, 4, 'Your order #141715736 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141715736:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141715736</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 15:58:16'),
(1063, 141, 3, 'Successfully Placed Order #141569261', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IndmQXBTbzdrZUUxSTBCNG44c2pGVlE9PSIsInZhbHVlIjoiUVhHazlhSStieWZlR0c3NDE2cXBISGxGWlRRbklvRGxDNG1NXC9vOVBBalU9IiwibWFjIjoiMzJkZjM5ODkyMWZmN2RlNmM2YmViMzFkODdmZGVhOTkyZjQzMjU2MmIyZGE0Yzc4NGU3OGYwMDZmMmQ4ZGZjMSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 16:38:32'),
(1064, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 16:38:33'),
(1065, 141, 4, 'Payment slip uploaded agenest order number #141569261', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141569261 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 16:38:47'),
(1066, 141, 4, 'Payment successfully varified for order number 141569261', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> Mango<br />\n<b>Order Number :</b> 141569261 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 16:39:05'),
(1067, 133, 4, 'Payment successfully varified for order number 141569261', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> Mango<br />\n    <b>Order Number :</b> 141569261 <br />\n</p>\n\n', 0, 0, '2018-04-05 16:39:06'),
(1068, 141, 3, 'Successfully Placed Order #141719287', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IlI1XC94V1VWWlZBYzZrZ2h1TFJlQWR3PT0iLCJ2YWx1ZSI6ImRxSXRibmZDZGZTQVREa2tYdnIyVkc0RkowU0FrUUkwRzdUUGJ3SGtjWGM9IiwibWFjIjoiN2MzNTY3YjMwZDgzMTZlYjM5NzliNjZmNjg5MDI1YWIxMDYwNTJkYmVmMWM0NWEwOTY5Yjk4MDA4NTU0NzgwMiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 16:40:58'),
(1069, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-05 16:40:59'),
(1070, 141, 4, 'Payment slip uploaded agenest order number #141719287', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 141719287 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-05 16:41:13'),
(1071, 141, 4, 'Payment successfully varified for order number 141719287', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> kisan fruti<br />\n<b>Order Number :</b> 141719287 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-05 16:41:33'),
(1072, 151, 4, 'Payment successfully varified for order number 141719287', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> kisan fruti<br />\n    <b>Order Number :</b> 141719287 <br />\n</p>\n\n', 0, 0, '2018-04-05 16:41:34'),
(1073, 141, 4, 'Your order #141719287 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141719287:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141719287</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 16:41:49'),
(1074, 151, 4, 'Your order #141719287 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #141719287:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>141719287</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                1\r\n            </td>\r\n            <td>\r\n                AED 30.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 N/A             </td>\r\n            <td class=\"action\">\r\n                AED 30.00\r\n            </td>\r\n            <td>\r\n                                Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 1.50<br />\r\n                AED 31.50\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-05 16:41:50'),
(1075, 26, 2, 'Thanks for posting the product #sabya', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> sabya\r\n    <br />\r\n    <b>Brand Name :</b> sabya\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 50\r\n    <br />\r\n    <b>Price / Carton :</b> AED 22.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-06 19:34:21'),
(1076, 26, 3, 'Successfully Placed Order #2625850', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Grapes </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Grapes\r\n<br/>Carton Purchased : 45\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IjJsTlU4YzJFOEw2VHJpSlhpQlowV1E9PSIsInZhbHVlIjoiMnJlNE9aMVBvb3pzYXNVQ3pMZEZOQT09IiwibWFjIjoiMWJjYWU4YWJiYWRlMDRiZGNiYzgwNDk2OWRhOTM3NmI2OWE1ZmRiYWVkODkzY2Y1NGVkZDVjZjljOTNkYjEzMiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 19:39:06'),
(1077, 59, 3, 'Purchase goods :Grapes', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Grapes\r\n    <br/>\r\n    Carton Purchased : 45\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 19:39:07'),
(1078, 140, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 21:25:50'),
(1079, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 21:25:51'),
(1080, 140, 3, 'Successfully Placed Order #140569881', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IjFIQ1MwVDdGNjVpeUtsOTM0U09la3c9PSIsInZhbHVlIjoiODdVandcL1BkM2JudWNtR0M4b3BFQ3NwR0NrR0FZY2FLOEZNa28zRzBBUTA9IiwibWFjIjoiMmRmMGM5YTE5NDU4YWMzOTRkODY5MzI4ZTcwZGFhZWIxNzEzMWM4ZDYxNzBlNmU3NDBhZGY2MGNjMDMyMGM3NSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:26:51'),
(1081, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:26:52'),
(1082, 26, 3, 'Successfully Placed Order #26561400', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImM0VUNWU1VVcmg1XC8xRm5zWGRVNjZ3PT0iLCJ2YWx1ZSI6IndLTGFGeTNQVEZuaDBubFJwV0RCS1E9PSIsIm1hYyI6IjRkYjRiYWQzMmY4MDAwMjc4OTI2ODBkMjc2YWNmOTc2N2ZjOGNhMjYyNWVlMjliYjQ2OWFlYzQ0YmM0NjczMGQifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:28:18'),
(1083, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:28:20'),
(1084, 26, 3, 'Successfully Placed Order #26564800', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ik1JWmtqRVRHRHNRakJIVDQ3OUxYWWc9PSIsInZhbHVlIjoicENGODkyZG00MTRcL09ja3gzbXQrTlE9PSIsIm1hYyI6ImEwNTk1NGVhYjg1ODJhNDJiMmNlM2Y0ZmRjMzAxY2QyOGZhY2FmOWYzYmUxYjZlZTQzMWVjM2FmYjIwMmQ2YjQifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:35:56'),
(1085, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:35:57'),
(1086, 26, 3, 'Successfully Placed Order #26257479', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 8\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImJ3UUc5UlZOS0RFY2hVb0tkZ3lHN2c9PSIsInZhbHVlIjoib1hCY3RTZVZudVJLMXB0OVVITUUydz09IiwibWFjIjoiYzEyZmY0MTVkZDFhZGU5N2ZmOWE0MTdmN2FiODhkOTEzZWZjMmQyZDE5MWZiNmY4YTRjNDUyMGMzOGIxYTQxYSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:36:55'),
(1087, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 8\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:36:57');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(1088, 26, 3, 'Successfully Placed Order #26251559', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 8\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Iks3WElHZU01b2VvZjEwb25raUtNZGc9PSIsInZhbHVlIjoiQnEwbnlRa1RORlcwSFkxWU9meFlLdz09IiwibWFjIjoiYWFiMDk2YmViNDc5NWYzMTc2YmE3N2RmM2RkOTYwODFmNmZiZjRiYWVhMzQ0MWFkOTE2ZjVlNmY4NjU0NDgyNyJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:37:05'),
(1089, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 8\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:37:06'),
(1090, 26, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 21:39:59'),
(1091, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 21:40:01'),
(1092, 26, 4, 'Successfully select bid auction 26582307', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : Mango\r\n    <br/>Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n    <br/>Price /Carton : AED 65\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #26582307\r\n    <br/>Carton Purchased :1\r\n\r\n    <br/> Bid Price :AED 62.00 \r\n    <br/> Bid Date : 06/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 21:40:50'),
(1093, 84, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 198\r\n    <br/>Bid Price / Carton : AED 5.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 21:44:20'),
(1094, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    16/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 198\r\n    <br/>Bid Price / Carton : AED 5.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 21:44:22'),
(1095, 84, 4, 'Successfully select bid auction 84672893', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : Mango\r\n    <br/>Brand name : Indian\r\n    <br/>Expiry Date : 16/05/2018\r\n\r\n    <br/>Price /Carton : AED 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #84672893\r\n    <br/>Carton Purchased :198\r\n\r\n    <br/> Bid Price :AED 5.00 \r\n    <br/> Bid Date : 06/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 21:44:54'),
(1096, 84, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : kisan tamato\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 11.50\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 21:49:12'),
(1097, 151, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : kisan tamato\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 11.50\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 21:49:14'),
(1098, 84, 4, 'Successfully select bid auction 84732760', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan tamato\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n    <br/>Price /Carton : AED 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #84732760\r\n    <br/>Carton Purchased :1\r\n\r\n    <br/> Bid Price :AED 11.50 \r\n    <br/> Bid Date : 06/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 21:49:30'),
(1099, 26, 3, 'Successful wish price #Green Banana', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Green Banana\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 5\r\n    <br/>Wish Price / Carton : AED 55.00\r\n    <br/>\r\n    Date : 06/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 21:56:30'),
(1100, 58, 3, 'Successful wish price #Green Banana', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Green Banana\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 5\r\n<br/>Wish Price / Carton : AED 55.00\r\n<br/>Date : 06/04/2018</p>\r\n\r\n', 0, 0, '2018-04-06 21:56:32'),
(1101, 26, 3, 'Successfully Placed Order #26275111', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Green Banana </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Green Banana\r\n<br/>Carton Purchased : 5\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImJcL21vSjVadjVXcjBJazRGRFdLb1wvZz09IiwidmFsdWUiOiIrbVJUWjBXcmhuaGI5bW12K2NQVXl3PT0iLCJtYWMiOiIzMjMyZGQ4ODE1NzE5ZTQ1MDExNDU1ZjUxYTkyZWIyODI2OWM3NjRhNDY1MWRiMmE5MmVhMzkxYzIxODE5NTI3In0=\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:56:53'),
(1102, 58, 3, 'Purchase goods :Green Banana', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Green Banana\r\n    <br/>\r\n    Carton Purchased : 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 21:56:54'),
(1103, 151, 2, 'Thanks for posting the product #apple', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> apple\r\n    <br />\r\n    <b>Brand Name :</b> india\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 30/07/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-06 22:06:05'),
(1104, 151, 2, 'Thanks for posting the product #patato', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> patato\r\n    <br />\r\n    <b>Brand Name :</b> india\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 30/06/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 50\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-06 22:08:14'),
(1105, 151, 2, 'Thanks for posting the product #patato', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> patato\r\n    <br />\r\n    <b>Brand Name :</b> india\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 40.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-06 22:14:02'),
(1106, 26, 4, 'Successfully select bid auction 26739755', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan tamato\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n    <br/>Price /Carton : AED 5\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #26739755\r\n    <br/>Carton Purchased :4\r\n\r\n    <br/> Bid Price :AED 11.50 \r\n    <br/> Bid Date : 31/03/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 22:28:47'),
(1107, 26, 4, 'Successfully select bid auction 26668161', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : apple\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n    <br/>Price /Carton : AED 20\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #26668161\r\n    <br/>Carton Purchased :1\r\n\r\n    <br/> Bid Price :AED 18.50 \r\n    <br/> Bid Date : 03/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 22:29:34'),
(1108, 151, 4, 'Payment slip uploaded agenest order number #15126283', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 15126283 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-06 22:39:24'),
(1109, 151, 3, 'Successfully Placed Order #151104648', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product bitter melon </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : bitter melon\r\n<br/>Carton Purchased : 100\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Ink0OVZoQndNSFZ0eXhWakozWCtaV1E9PSIsInZhbHVlIjoiR3J6XC9SVWtJRVRXR2ZDcGVQb2VqdVE9PSIsIm1hYyI6IjM0Mzg1ZjQxYTFiN2Q5YzhlOTI5MzJiNDM2N2YyOGEzNGZkNmExN2M0MGE3MThjMWFjYzI2OGE1ODRjN2Y1Y2QifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 22:41:25'),
(1110, 59, 3, 'Purchase goods :bitter melon', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : bitter melon\r\n    <br/>\r\n    Carton Purchased : 100\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 22:41:26'),
(1111, 151, 3, 'Successfully Placed Order #151568485', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IlpPVVJVZ1NyN3FKb1lHdThQOWYyZ2c9PSIsInZhbHVlIjoibGZaTjg4bmt6enZJZzJsWUlmSzBYRVwvWE8rOStYa3BpM1FmQWNnZk9rQUk9IiwibWFjIjoiNTYzMmY2YjgyYTQ1ZjFkNWM1M2M5ZjZhMDljMmY4YTE3Nzk1OTI1OTE5OWEyZTUwYjBhZTYyZDUwMjZlZGE4YSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 22:50:54'),
(1112, 133, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-06 22:50:55'),
(1113, 151, 3, 'Successful bid #Beans', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Beans\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 13/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 30.50\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:51:32'),
(1114, 59, 3, 'Successful bid #Beans', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Beans\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    13/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 30.50\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:51:33'),
(1115, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:52:06'),
(1116, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:52:08'),
(1117, 151, 3, 'Successful bid #Watermelon', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Watermelon\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 25.50\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:54:08'),
(1118, 59, 3, 'Successful bid #Watermelon', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Watermelon\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 25.50\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:54:09'),
(1119, 151, 3, 'Successful bid #Blackberry', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Blackberry\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 181.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:54:18'),
(1120, 58, 3, 'Successful bid #Blackberry', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Blackberry\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 181.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:54:19'),
(1121, 151, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 21.00\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:54:46'),
(1122, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 21.00\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:54:47'),
(1123, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 65.50\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:55:01'),
(1124, 59, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 65.50\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:55:02'),
(1125, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 65.50\r\n    <br/>Bid Date : 06/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-06 22:55:14'),
(1126, 59, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 65.50\r\n\r\n    <br/> Bid Date : 06/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-06 22:55:15'),
(1127, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 11:19:14'),
(1128, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 11:19:22'),
(1129, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 11:38:40'),
(1130, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 11:38:41'),
(1131, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 11:44:57'),
(1132, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 36.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 11:44:59'),
(1133, 151, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 37.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 11:46:08'),
(1134, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 37.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 11:46:11'),
(1135, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 11:48:08'),
(1136, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 62.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 11:48:09'),
(1137, 141, 3, 'Successful accepect wish price #apple', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : apple\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 12:10:36'),
(1138, 151, 3, 'Successful accepect wish price #apple', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : apple\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 23/04/2019\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:10:38'),
(1139, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:11:29'),
(1140, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 12:11:30'),
(1141, 26, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 12:12:16'),
(1142, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:12:17'),
(1143, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 7.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:13:09'),
(1144, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 7.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 12:13:10'),
(1145, 26, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 7.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:13:29'),
(1146, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 7.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:13:31'),
(1147, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:14:22'),
(1148, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 12:14:24'),
(1149, 26, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:15:20'),
(1150, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:15:22'),
(1151, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:16:27'),
(1152, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 12:16:28'),
(1153, 26, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 12:16:38'),
(1154, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:16:40'),
(1155, 26, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 2.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:17:24'),
(1156, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 2.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 12:17:25'),
(1157, 26, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 2.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:18:00'),
(1158, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 2.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 12:18:02'),
(1159, 151, 3, 'Successfully Placed Order #15122677', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Grapes </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Grapes\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImZFR0VJeVNhXC83XC9oTUJlTVN6QU8wdz09IiwidmFsdWUiOiI2cjU3XC9uVTNKWjdVTXNhUGJSV1FFUT09IiwibWFjIjoiZWVhYWM0YmZmNWRlYWJhMTRhNWJiZGE0MzhjNzE1MDIzNTJjNDU4ZjAyNmJhMGI3ZDg2ZGVlN2IzZTQ5ZmIwMCJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-07 12:22:56'),
(1160, 59, 3, 'Purchase goods :Grapes', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Grapes\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-07 12:22:58'),
(1161, 26, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 37.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 13:39:07'),
(1162, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 37.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 13:39:08'),
(1163, 26, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 993\r\n    <br/>Bid Price / Carton : AED 37.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 13:42:25');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(1164, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 993\r\n    <br/>Bid Price / Carton : AED 37.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 13:42:26'),
(1165, 26, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 38.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 13:42:40'),
(1166, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 38.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 13:42:42'),
(1167, 26, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Broccoli\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 38.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 13:42:51'),
(1168, 147, 3, 'Successful bid #Broccoli', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Broccoli\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    22/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 38.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 13:42:53'),
(1169, 157, 1, 'Successfully Registered', '<p style=\"line-height: 1.4\" >Welcome to The Surplus Exchange!</p>\n\n<p style=\"line-height: 1.4\" >You have successfully registered with us now as a Buyer. </p>\n\n<p style=\"line-height: 1.4\" >Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p style=\"line-height: 1.4\" >You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<p style=\"line-height: 1.4\">Happy buying/selling.</p>\n\n', 0, 0, '2018-04-07 14:17:44'),
(1170, 151, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Mango\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 63.00\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 15:16:40'),
(1171, 140, 3, 'Successful bid #Mango', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Mango\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    24/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 63.00\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 15:16:41'),
(1172, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 15:44:13'),
(1173, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 30/11/-0001\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 15:44:14'),
(1174, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 15:45:46'),
(1175, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 30/11/-0001\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 15:45:48'),
(1176, 156, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 16:05:39'),
(1177, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 16:05:41'),
(1178, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:06:54'),
(1179, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 30/04/2018\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 16:06:56'),
(1180, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:10:10'),
(1181, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 16:10:12'),
(1182, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:10:46'),
(1183, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 16:10:48'),
(1184, 141, 2, 'Product kisan tamato is available in site', '<p style=\"line-height: 1.4\" >We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Relax and sit back, when we go about selling your product.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Meanwhile, you may log into our website and fine tune the product details or more products for buy.</p>\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n        <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED AED 0.00 \r\n    <br />\r\n    \r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:11:31'),
(1185, 151, 2, 'Product kisan tamato is available in auction mode', '<p style=\"line-height: 1.4\" >Your product in auction mode.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n        <br/>\r\n    <b>Product Name :</b> kisan tamato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    \r\n    <b>Auction Start Date :</b> 31/03/2018\r\n    <br />\r\n    <b>Auction End  Date :</b> 27/04/2018\r\n    <br />\r\n    <b>Expiry Date :</b> 31/05/2018\r\n    <br />\r\n    <b>Total Carton :</b> 100\r\n    <br />\r\n    <b>Price / Carton :</b> AED 15.00\r\n    <br />\r\n    \r\n        ', 0, 0, '2018-04-07 16:11:33'),
(1186, 156, 4, 'Successfully select bid auction 156663350', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : apple\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n    <br/>Price /Carton : AED 20\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #156663350\r\n    <br/>Carton Purchased :2\r\n\r\n    <br/> Bid Price :AED 18.50 \r\n    <br/> Bid Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:16:52'),
(1187, 156, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 16:20:58'),
(1188, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 16:21:00'),
(1189, 156, 4, 'Successfully select bid auction 156667955', '<p style=\"line-height: 1.4\" >Congratulations!</p>\r\n<p style=\"line-height: 1.4\" >Your bid had been accepted by the seller earlier than expected!</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : apple\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n    <br/>Price /Carton : AED 20\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Bid details</u></b>\r\n    <br/>Order number #156667955\r\n    <br/>Carton Purchased :1\r\n\r\n    <br/> Bid Price :AED 18.50 \r\n    <br/> Bid Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >\r\n    Please make the payments at the earliest to expedite the delivery.\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:21:20'),
(1190, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 20.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:28:40'),
(1191, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 20.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:28:42'),
(1192, 156, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 20.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 16:29:45'),
(1193, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 20.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:29:47'),
(1194, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:31:15'),
(1195, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:31:16'),
(1196, 156, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 16:31:47'),
(1197, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:31:48'),
(1198, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:15'),
(1199, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:16'),
(1200, 156, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:33'),
(1201, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:35'),
(1202, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:58'),
(1203, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:32:59'),
(1204, 156, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:07'),
(1205, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:08'),
(1206, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:27'),
(1207, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:29'),
(1208, 156, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:35'),
(1209, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:38'),
(1210, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:52'),
(1211, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:33:53'),
(1212, 156, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 16:34:00'),
(1213, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:34:02'),
(1214, 156, 4, 'My Requrement Fruit', '<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n', 0, 0, '2018-04-07 16:36:04'),
(1215, 133, 4, 'Buyer Requiremt Fruit', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Fruit category product of unit 1 KG expected price per carton AED 50 with in 07/05/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:36:06'),
(1216, 156, 4, 'My Requrement Vegetable', '<p style=\"line-height: 1.4\" >We inform to all of our seller</p>\r\n\r\n', 0, 0, '2018-04-07 16:37:00'),
(1217, 26, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1 KG expected price per carton AED 1 with in 07/05/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:37:01'),
(1218, 147, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1 KG expected price per carton AED 1 with in 07/05/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:37:03'),
(1219, 151, 4, 'Buyer Requiremt Vegetable', '<p style=\"line-height: 1.4\" >\r\n    Buyer want Vegetable category product of unit 1 KG expected price per carton AED 1 with in 07/05/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:37:10'),
(1220, 156, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:39:53'),
(1221, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 16:39:55'),
(1222, 156, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:40:04'),
(1223, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 16:40:06'),
(1224, 151, 4, 'Payment successfully varified for order number 15126283', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> Grapes<br />\n<b>Order Number :</b> 15126283 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-07 16:41:29'),
(1225, 59, 4, 'Payment successfully varified for order number 15126283', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> Grapes<br />\n    <b>Order Number :</b> 15126283 <br />\n</p>\n\n', 0, 0, '2018-04-07 16:41:32'),
(1226, 151, 4, 'Your order #15126283 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #15126283:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>15126283</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 130.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED5.50             </td>\r\n            <td class=\"action\">\r\n                AED 135.50\r\n            </td>\r\n            <td>\r\n                                    Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 6.78<br />\r\n                AED 142.28\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-07 16:43:25'),
(1227, 59, 4, 'Your order #15126283 has been successfully delivered', '<p style=\"line-height: 1.4\">\r\n            The following product has been delivered at your registered location(s).\r\n    </p>\r\n<p style=\"line-height: 1.4\">Please find status of your order #15126283:</p>\r\n<table border=\"1\">         \r\n    <thead>\r\n        <tr>\r\n            <th>Order Number</th>\r\n            <th>Quantity</th>\r\n            <th>Price</th>\r\n            <th>Delivery Price</th>\r\n            <th>Sub Total Price</th>\r\n            <th>Status</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n        <tr>\r\n            <td>\r\n                <b>15126283</b>                                           \r\n            </td>\r\n            <td class=\"title-1\">\r\n                2\r\n            </td>\r\n            <td>\r\n                AED 130.00 \r\n            </td>\r\n            <td class=\"total title-1\">\r\n                 AED5.50             </td>\r\n            <td class=\"action\">\r\n                AED 135.50\r\n            </td>\r\n            <td>\r\n                                    Successfully delivered\r\n                            </td>\r\n        </tr>\r\n    </tbody>\r\n\r\n    <tfoot>\r\n        <tr>\r\n            <td colspan=\"3\">&nbsp;</td>\r\n            <td>\r\n                Vat(5 %)<br />\r\n                Total Price\r\n            </td>\r\n            <td class=\"subtotal title-1\">\r\n                AED 6.78<br />\r\n                AED 142.28\r\n            </td>\r\n        </tr>\r\n    </tfoot>\r\n</table>\r\n<p style=\"line-height: 1.4\">\r\n</p>\r\n\r\n\r\n\r\n', 0, 0, '2018-04-07 16:43:27'),
(1228, 156, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 16:48:09'),
(1229, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 16:48:11'),
(1230, 151, 2, 'Thanks for posting the product #kisan patato', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan patato\r\n    <br />\r\n    <b>Brand Name :</b> indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 07/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 40\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-07 17:07:51'),
(1231, 156, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Wish Price / Carton : AED 68.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 17:08:19'),
(1232, 59, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 2\r\n<br/>Wish Price / Carton : AED 68.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 17:08:21'),
(1233, 151, 2, 'Thanks for posting the product #kisan carrot', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisan carrot\r\n    <br />\r\n    <b>Brand Name :</b> Indian\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 07/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 50\r\n    <br />\r\n    <b>Price / Carton :</b> AED 45.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-07 17:12:18'),
(1234, 155, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Carrot\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 21.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 17:13:59'),
(1235, 59, 3, 'Successful bid #Carrot', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Carrot\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 21.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 17:14:01'),
(1236, 155, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 20\r\n    <br/>Wish Price / Carton : AED 68.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 17:15:42'),
(1237, 59, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 20\r\n<br/>Wish Price / Carton : AED 68.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 17:15:48');
INSERT INTO `sps__notification` (`id`, `user_id`, `type`, `subject`, `description`, `is_read`, `is_delete`, `add_date`) VALUES
(1238, 155, 2, 'Thanks for posting the product #mangotestkisan', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> mangotestkisan\r\n    <br />\r\n    <b>Brand Name :</b> india\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 19/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 2\r\n    <br />\r\n    <b>Price / Carton :</b> AED 50.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-07 17:25:44'),
(1239, 155, 2, 'Thanks for posting the product #mangotestkisan', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> mangotestkisan\r\n    <br />\r\n    <b>Brand Name :</b> india\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 19/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 2\r\n    <br />\r\n    <b>Price / Carton :</b> AED 50.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-07 17:26:01'),
(1240, 155, 2, 'Thanks for posting the product #kisanmango', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> kisanmango\r\n    <br />\r\n    <b>Brand Name :</b> mk\r\n    <br />\r\n    <b>Country of origin :</b> India\r\n    <br />\r\n    <b>Expiry Date :</b> 19/05/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 52\r\n    <br />\r\n    <b>Price / Carton :</b> AED 50.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-07 17:46:31'),
(1241, 141, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 18:21:42'),
(1242, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 18:21:44'),
(1243, 141, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been accepted by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    For next steps or more shopping <a href=\"http://tsdemo.in/auctionportal/public/wishpricerequirment\" >please click here</a>\r\n</p>\r\n', 0, 0, '2018-04-07 18:23:44'),
(1244, 151, 3, 'Successful accepect wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer. </p>\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details ( Accepted )</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 18:23:45'),
(1245, 141, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>\r\n    Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 18:24:55'),
(1246, 151, 3, 'Successful wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : kisan fruti\r\n    <br/>\r\n    Brand name : indian\r\n\r\n    <br/>\r\n    Expiry Date : 31/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018</p>\r\n\r\n', 0, 0, '2018-04-07 18:24:57'),
(1247, 141, 3, 'Rejected wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\nThe following offer or yours has been rejected by seller.    \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n<br/>Product Name : kisan fruti\r\n<br/>Brand name : indian\r\n<br/>Expiry Date : 31/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Wish price details</u></b>\r\n<br/>Carton  : 1\r\n<br/>Wish Price / Carton : AED 1.00\r\n<br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 18:25:05'),
(1248, 151, 3, 'Reject wish price #kisan fruti', '<p style=\"line-height: 1.4\">\r\n    Your decision on the following offer has been communicated to the prospective buyer.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>Product Name : kisan fruti\r\n    <br/>Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018 \r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details ( Rejected )</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Wish Price / Carton : AED 1.00\r\n    <br/>Date : 07/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 18:25:06'),
(1249, 141, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : kisan tamato\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 15.50\r\n    <br/>Bid Date : 07/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-07 18:26:35'),
(1250, 151, 3, 'Successful bid #kisan tamato', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : kisan tamato\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    31/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 1\r\n    <br/>Bid Price / Carton : AED 15.50\r\n\r\n    <br/> Bid Date : 07/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-07 18:26:37'),
(1251, 26, 3, 'Successfully Placed Order #26258273', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 100\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImJhZFBmUWZZVnJoQXRIMkY1VHdnMGc9PSIsInZhbHVlIjoiZ2pUdUJjcXF1S1dIRkhOZkpFTEkxZz09IiwibWFjIjoiMmU5MWI4ODZiYWUxZmZmMzMyNWI1ODE1NGE2MDhmZWVjMjZiNmQwMDk0MDM1NTFkNjJhMDNjMTgzMjAxZDIzOSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-07 19:17:45'),
(1252, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 100\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-07 19:17:47'),
(1253, 26, 4, 'Payment slip uploaded agenest order number #26258273', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 26258273 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-07 19:18:27'),
(1254, 141, 3, 'Successfully Placed Order #141711186', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product kisan fruti </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : kisan fruti\r\n<br/>Carton Purchased : 1\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6Inp6UVF5eVZPR1QzV3hqeTZYYkhhZmc9PSIsInZhbHVlIjoiS3Q4Q1JoM2R5VU9PaWUxVHI0ckNDaDJFbmRsbFwvdm0rVk5lNExIZlpXb0k9IiwibWFjIjoiMzhjMTI1YmUxOWM3NWU4N2M2MWI2NjM3ODAyMzYyNDc4ZGUyZGFhNzJiMTkyMGRmNzc1ODEyZDE4ZjFlNDAxNSJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-11 20:15:23'),
(1255, 151, 3, 'Purchase goods :kisan fruti', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : kisan fruti\r\n    <br/>\r\n    Carton Purchased : 1\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-11 20:15:25'),
(1256, 26, 4, 'Balance info for the month:March,Year :2018', '<p style=\"line-height: 1.4\" >\r\n     Transaction   report for the month  March, 2018 is attached for your reference.\r\n</p>\r\n<p style=\"line-height: 1.4\" >\r\n    Please revert, in case you identify any issues and we will be happy to help.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-13 12:24:57'),
(1257, 58, 4, 'Balance info for the month:January,Year :2018', '<p style=\"line-height: 1.4\" >\r\n     Transaction   report for the month  January, 2018 is attached for your reference.\r\n</p>\r\n<p style=\"line-height: 1.4\" >\r\n    Please revert, in case you identify any issues and we will be happy to help.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-13 12:27:36'),
(1258, 58, 4, 'Balance info for the month:January,Year :2018', '<p style=\"line-height: 1.4\" >\r\n     Transaction   report for the month  January, 2018 is attached for your reference.\r\n</p>\r\n<p style=\"line-height: 1.4\" >\r\n    Please revert, in case you identify any issues and we will be happy to help.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-13 12:28:12'),
(1259, 58, 4, 'Balance info for the month:January,Year :2018', '<p style=\"line-height: 1.4\" >\r\n     Transaction   report for the month  January, 2018 is attached for your reference.\r\n</p>\r\n<p style=\"line-height: 1.4\" >\r\n    Please revert, in case you identify any issues and we will be happy to help.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-13 12:28:34'),
(1260, 58, 4, 'Balance info for the month:January,Year :2018', '<p style=\"line-height: 1.4\" >\r\n     Payment    report for the month  January, 2018 is attached for your reference.\r\n</p>\r\n<p style=\"line-height: 1.4\" >\r\n    Please revert, in case you identify any issues and we will be happy to help.\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-13 12:28:44'),
(1261, 26, 4, 'Payment successfully varified for order number 26258273', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> pomegranate<br />\n<b>Order Number :</b> 26258273 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-14 14:32:50'),
(1262, 59, 4, 'Payment successfully varified for order number 26258273', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> pomegranate<br />\n    <b>Order Number :</b> 26258273 <br />\n</p>\n\n', 0, 0, '2018-04-14 14:32:52'),
(1263, 158, 1, 'Successfully Registered', '<p style=\"line-height: 1.4\" >Welcome to The Surplus Exchange!</p>\n\n<p style=\"line-height: 1.4\" >You have successfully registered with us now as a Buyer. </p>\n\n<p style=\"line-height: 1.4\" >Don\'t forget to visit your profile page after activation and share additional details to help us serve better.</p>\n\n<p style=\"line-height: 1.4\" >You can contact our support center for additional help, any time between 10 AM to 6 PM on all working days.</p>\n\n<p style=\"line-height: 1.4\">Happy buying/selling.</p>\n\n', 0, 0, '2018-04-28 01:30:56'),
(1264, 58, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 50\r\n    <br/>Wish Price / Carton : AED 65.00\r\n    <br/>\r\n    Date : 29/04/2018\r\n</p>\r\n\r\n', 0, 0, '2018-04-29 12:18:03'),
(1265, 59, 3, 'Successful wish price #pomegranate', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : pomegranate\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 30/04/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 50\r\n<br/>Wish Price / Carton : AED 65.00\r\n<br/>Date : 29/04/2018</p>\r\n\r\n', 0, 0, '2018-04-29 12:18:04'),
(1266, 58, 3, 'Successfully Placed Order #58256423', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product pomegranate </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : pomegranate\r\n<br/>Carton Purchased : 60\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6InFiVlBkdmoyVmxnaUxIQTF2S21VRmc9PSIsInZhbHVlIjoiQk1zbStoYnV1QXpSdk1abzlWYTZKUT09IiwibWFjIjoiMzEwMWI3ZGQxNTA2YjBiYWE5Mzc5MDFiYzQ0YmVjZDEwOWI5YTM1ODE3NzMwOTgyZWQ3YmNiNmQyNGM0NGU3NiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-29 12:33:09'),
(1267, 59, 3, 'Purchase goods :pomegranate', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : pomegranate\r\n    <br/>\r\n    Carton Purchased : 60\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-04-29 12:33:11'),
(1268, 58, 4, 'Payment slip uploaded agenest order number #58256423', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 58256423 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-04-29 12:37:47'),
(1269, 58, 4, 'Payment successfully varified for order number 58256423', '<p  style=\"line-height: 1.4\" >\n    Your payment against the following purchase has been accepted.\n</p>\n<p  style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p  style=\"line-height: 1.4\" >\n<b>Product Name :</b> pomegranate<br />\n<b>Order Number :</b> 58256423 <br />\n\n</p>\n\n\n', 0, 0, '2018-04-29 12:41:47'),
(1270, 59, 4, 'Payment successfully varified for order number 58256423', '<p style=\"line-height: 1.4\" >\n    Payment against the following purchase has been accepted.\n</p>\n<p style=\"line-height: 1.4\" >\n    The delivery of the product will be arranged at the earliest.\n</p> \n<p style=\"line-height: 1.4\" >\n    <b>Product Name :</b> pomegranate<br />\n    <b>Order Number :</b> 58256423 <br />\n</p>\n\n', 0, 0, '2018-04-29 12:41:49'),
(1271, 26, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 15/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 10\r\n    <br/>Bid Price / Carton : AED 95.50\r\n    <br/>Bid Date : 29/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-29 13:04:32'),
(1272, 58, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    15/05/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 10\r\n    <br/>Bid Price / Carton : AED 95.50\r\n\r\n    <br/> Bid Date : 29/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-29 13:04:34'),
(1273, 26, 3, 'Successful bid #Orange', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : Orange\r\n    <br/> Brand name : Indian\r\n    <br/>Expiry Date : 30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 4\r\n    <br/>Bid Price / Carton : AED 66.50\r\n    <br/>Bid Date : 29/04/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-04-29 13:07:17'),
(1274, 133, 3, 'Successful bid #Orange', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : Orange\r\n\r\n    <br/> Brand name : Indian\r\n\r\n    <br/>Expiry Date :\r\n    30/04/2018\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 4\r\n    <br/>Bid Price / Carton : AED 66.50\r\n\r\n    <br/> Bid Date : 29/04/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-04-29 13:07:19'),
(1275, 26, 2, 'Thanks for posting the product #Apple Test', '<p style=\"line-height: 1.4\">Your request for posting of the following product has reached us.</p>\r\n\r\n<p style=\"line-height: 1.4\" >Please arrange to drop the consignment at our warehouse for quality check and posting of the product. We request you to send all the necessary documents along with the product. </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Product Details:</b>\r\n        <br/><b>Product Name :</b> Apple Test\r\n    <br />\r\n    <b>Brand Name :</b> country Green\r\n    <br />\r\n    <b>Country of origin :</b> Bangladesh\r\n    <br />\r\n    <b>Expiry Date :</b> 12/06/2018 \r\n    <br />\r\n    <b>Total Carton :</b> 40\r\n    <br />\r\n    <b>Price / Carton :</b> AED 6.00 \r\n    <br />\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\" ><b>Drop Location:</b>\r\n        <br/><b>Warehouse Name :</b> warehouse1\r\n    <br />\r\n    <b>Address :</b> Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India\r\n    <br />\r\n    <b>Locality :</b> Al Ain\r\n    <br />\r\n    <b>Emirates :</b> Abu Dhabi\r\n    <br />\r\n    <b>Country :</b> UAE\r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-29 13:25:38'),
(1276, 26, 2, 'Successfully verified all documents #Apple Test', '<p style=\"line-height: 1.4\">\r\n    We have received your products in good condition and will make necessary arrangement to post it for sale at the earliest.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Relax and sit back, when we go about selling your product.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\">\r\n    Meanwhile, you may log into our website and fine tune the product details or post more products for sell.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n        <br/><b>Product Name :</b> Apple Test\r\n    <br />\r\n    <b>Brand Name :</b> country Green\r\n    <br />\r\n        <b>Country of origin :</b> 7\r\n    <br />\r\n        <b>Expiry Date :</b> 12/06/2018\r\n    <br />\r\n    <b>Total Carton :</b> 40\r\n    <br />\r\n    <b>Price / Carton :</b> AED 6.00 \r\n    <br />\r\n    </p>\r\n\r\n', 0, 0, '2018-04-29 13:29:23'),
(1277, 1, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">We have shared your offer details with the product owner and will keep you posted on the development.</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n    <br/>\r\n    Expiry Date : 29/05/2018\r\n\r\n    </p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n    <br/>Carton  : 5\r\n    <br/>Wish Price / Carton : AED 5.00\r\n    <br/>\r\n    Date : 04/05/2018\r\n</p>\r\n\r\n', 0, 0, '2018-05-04 13:25:49'),
(1278, 59, 3, 'Successful wish price #Mango', '<p style=\"line-height: 1.4\">\r\n    We have received the following proposal from a prospective buyer. Please accept or reject the offer by <a href=\"http://tsdemo.in/auctionportal/public/buyerwishpriceproduct\">clicking here.</a>\r\n</p>\r\n\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Product Details</u></b>\r\n    <br/>\r\n    Product Name : Mango\r\n    <br/>\r\n    Brand name : Indian\r\n\r\n    <br/>\r\n    Expiry Date : 29/05/2018\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Wish price details</u></b>\r\n<br/>Carton  : 5\r\n<br/>Wish Price / Carton : AED 5.00\r\n<br/>Date : 04/05/2018</p>\r\n\r\n', 0, 0, '2018-05-04 13:25:51'),
(1279, 58, 3, 'Successfully Placed Order #5833317', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 60\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6ImpTSEE3T013alI5Nm9OWVwvM0x2Y2lBPT0iLCJ2YWx1ZSI6IlFrTmdOTnp2WVR3ZlVZRHZxMUZ2eEE9PSIsIm1hYyI6ImJiOGYxZGJlNTdmYTFhMzRlY2NhNzc5YzVkZTQxNjE4MTQ4NjhmOTFlZWM1NDcxOTRiNzkxY2NkNWMzNDRjYjYifQ==\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-05-04 13:34:56'),
(1280, 59, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 60\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-05-04 13:34:58'),
(1281, 26, 3, 'Successfully Placed Order #2638216', '<p style=\"line-height: 1.4\" >Congratulation on the latest win!</p>\r\n<p style=\"line-height: 1.4\" >Please find the proforma invoice(s) against your product Mango </p>\r\n\r\n\r\n<p style=\"line-height: 1.4\">Product name : Mango\r\n<br/>Carton Purchased : 2\r\n<br/><a href=\"http://tsdemo.in/auctionportal/public/orderdetails/eyJpdiI6IkdHQjBtVTcyM0tFc3hwXC9SNU5ZVVBRPT0iLCJ2YWx1ZSI6IjU5XC9JOFdrUWZQdHhybTdSeWVmXC9LUT09IiwibWFjIjoiZjc5MmY5OGRkYjJiY2QyY2RjNTBmMDkyNWM0ODU4OTBkNTVhM2ZhMzUzZDRhODRjYmYxM2ViMjUwZjllNDQ1ZiJ9\">Click here</a> to upload your payment slip / Status of order\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-05-09 16:40:28'),
(1282, 59, 3, 'Purchase goods :Mango', '<p style=\"line-height: 1.4\" >Congratulation on the latest sale!</p>\r\n<p style=\"line-height: 1.4\">\r\n    Product name : Mango\r\n    <br/>\r\n    Carton Purchased : 2\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >Let us know if you find any discrepancies.</p>\r\n\r\n', 0, 0, '2018-05-09 16:40:30'),
(1283, 26, 4, 'Payment slip uploaded agenest order number #2638216', '<p style=\"line-height: 1.4\">\r\n    The payment slip against the order ( 2638216 ) has been received by us.\r\n</p>\r\n<p style=\"line-height: 1.4\" > \r\n    We will verify the payslip and get back to you on the next step.\r\n</p>\r\n\r\n', 0, 0, '2018-05-09 16:41:27'),
(1284, 159, 1, 'Successfully Registered', 'Successfully Registered on 2018-06-11 15:04:54', 0, 0, '2018-06-11 15:04:54'),
(1285, 26, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\" >Your bid for the following product has been accepted.\r\n    We will keep you posted on the future development.\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" ><b><u>Product Details</u></b>\r\n    <br/> Product Name : apple\r\n    <br/> Brand name : indian\r\n    <br/>Expiry Date : 23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"><b><u>Bid details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Bid Price / Carton : AED 18.50\r\n    <br/>Bid Date : 30/07/2018\r\n</p>\r\n\r\n\r\n', 0, 0, '2018-07-30 20:53:19'),
(1286, 151, 3, 'Successful bid #apple', '<p style=\"line-height: 1.4\">\r\n    Buyers are taking notice of your following product\r\n</p>\r\n<p style=\"line-height: 1.4\"> <b><u>Product Details</u></b>\r\n\r\n    <br/> Product Name : apple\r\n\r\n    <br/> Brand name : indian\r\n\r\n    <br/>Expiry Date :\r\n    23/04/2019\r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\"> <b><u>Bid details</u></b>\r\n    <br/>Carton  : 2\r\n    <br/>Bid Price / Carton : AED 18.50\r\n\r\n    <br/> Bid Date : 30/07/2018 \r\n\r\n</p>\r\n\r\n<p style=\"line-height: 1.4\" >You may log in to our portal to view the details or accept the bids right away.</p>\r\n\r\n', 0, 0, '2018-07-30 20:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `sps__password_resets`
--

CREATE TABLE `sps__password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sps__password_resets`
--

INSERT INTO `sps__password_resets` (`email`, `token`, `created_at`) VALUES
('nagen@thoughtspheres.com', '9b8a405fba3a2fc87467a75180dc07f6addf1781', '2018-01-29 09:38:50'),
('test@gmail.com', 'ea97b75619f5cb2b9df9d184c4541aafe3b87484', '2018-03-13 11:50:21'),
('sahoo.purusottama@gmail.com', 'd0dae843cd3e78cb5d0b24823eaac1d68fb9ac00', '2018-03-19 11:21:36'),
('kisanpatra.92@gmail.com', 'a6d4fb049aa3762c6461493c0115a2aa0f9c8a35', '2018-03-31 15:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product`
--

CREATE TABLE `sps__product` (
  `id` int(11) NOT NULL,
  `user_id` int(111) NOT NULL,
  `category_id` int(12) NOT NULL,
  `subcategory_id` int(12) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `brand_name` varchar(222) DEFAULT NULL,
  `country_origin` int(11) DEFAULT NULL,
  `metakey` text,
  `metadescription` text,
  `skuid` varchar(22) DEFAULT NULL,
  `goods_capacity_carton` int(11) DEFAULT NULL COMMENT 'no. of goods per cartons',
  `total_quantity` int(11) DEFAULT NULL COMMENT 'total cartons ',
  `quantity_available` int(20) DEFAULT NULL COMMENT 'available cartons',
  `measurement` int(12) DEFAULT NULL COMMENT 'if pack size 500 grm then put 500 here',
  `goodsunit` varchar(20) DEFAULT NULL COMMENT 'Kgs / Lit / cartons',
  `baseprice` float(6,2) DEFAULT NULL COMMENT 'selling /offer price of goods selling price < rack price',
  `rackprice` float(10,2) DEFAULT NULL COMMENT 'real price of goods',
  `cbm_used` int(12) DEFAULT '0',
  `summary` text,
  `description` mediumtext,
  `status` smallint(1) DEFAULT NULL COMMENT '1-->seller req 2->1st approve 3-> 1st reject 4-> final aprove 5-> final reject,6->Return product',
  `status_comments` text,
  `expirydate` datetime DEFAULT NULL,
  `auto_auction` tinyint(1) DEFAULT NULL,
  `auctionstartdate` datetime DEFAULT NULL,
  `auctionenddate` datetime DEFAULT NULL,
  `auctionprice` float(10,2) DEFAULT '0.00',
  `verify_status` int(11) DEFAULT NULL,
  `verify_comments` text,
  `warehouse_id` int(12) DEFAULT NULL,
  `warehouse_slot_number` varchar(100) DEFAULT NULL,
  `warehouse_price_per_day` float(10,2) DEFAULT NULL,
  `view_count` int(11) DEFAULT '1',
  `request_date` timestamp NULL DEFAULT NULL,
  `return_amount` float(20,2) DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `IP` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product`
--

INSERT INTO `sps__product` (`id`, `user_id`, `category_id`, `subcategory_id`, `name`, `brand_name`, `country_origin`, `metakey`, `metadescription`, `skuid`, `goods_capacity_carton`, `total_quantity`, `quantity_available`, `measurement`, `goodsunit`, `baseprice`, `rackprice`, `cbm_used`, `summary`, `description`, `status`, `status_comments`, `expirydate`, `auto_auction`, `auctionstartdate`, `auctionenddate`, `auctionprice`, `verify_status`, `verify_comments`, `warehouse_id`, `warehouse_slot_number`, `warehouse_price_per_day`, `view_count`, `request_date`, `return_amount`, `return_date`, `IP`) VALUES
(1, 59, 10, NULL, 'Carrot', 'Indian', 3, NULL, NULL, '06-02591', 10, 100, 100, 1, 'KG', 50.00, 45.00, 1, NULL, '', 4, 'test', '2018-05-31 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-23 00:00:00', 20.00, 63, '', 2, NULL, NULL, 14, '2018-02-06 07:28:38', NULL, NULL, NULL),
(2, 59, 15, NULL, 'Grapes', 'Indian', 3, NULL, NULL, '06-02592', 10, 100, 49, 1, 'KG', 70.00, 65.00, 1, NULL, '<p>Demo</p>', 4, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 65.00, 63, '', 2, NULL, NULL, 41, '2018-02-06 07:48:02', NULL, NULL, NULL),
(3, 59, 15, NULL, 'Mango', 'Indian', 3, NULL, NULL, '06-02593', 50, 100, 39, 1, 'KG', 50.00, 45.00, 2, NULL, '', 4, NULL, '2018-05-29 00:00:00', 0, '2018-05-29 00:00:00', NULL, 45.00, 63, '', 2, NULL, NULL, 67, '2018-01-06 07:59:44', NULL, NULL, NULL),
(4, 59, 10, NULL, 'Beans', 'Indian', 3, NULL, NULL, '06-02594', 50, 1000, 1000, 1, 'KG', 70.00, 65.00, 3, NULL, '', 4, NULL, '2018-05-13 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-18 00:00:00', 30.00, 63, '', 2, NULL, NULL, 24, '2018-02-06 09:36:56', NULL, NULL, NULL),
(5, 58, 10, NULL, 'Yellow Pepper', 'Indian', 3, NULL, NULL, '06-02585', 20, 100, 6, 1, 'KG', 70.00, 68.00, 3, NULL, '', 4, NULL, '2018-03-08 00:00:00', 0, '2018-03-08 00:00:00', NULL, 68.00, 63, NULL, 2, NULL, NULL, 63, '2018-01-06 10:52:42', NULL, NULL, NULL),
(6, 58, 15, NULL, 'Strawberry', 'Indian', 3, NULL, NULL, '06-02586', 50, 100, 77, 1, 'KG', 200.00, 195.00, 3, NULL, '', 4, 'Change Health document', '2018-03-08 00:00:00', 0, '2018-03-08 00:00:00', NULL, 195.00, 63, NULL, 2, NULL, NULL, 32, '2018-02-06 10:54:54', NULL, NULL, NULL),
(7, 58, 15, NULL, 'Blackberry', 'Indian', 3, NULL, NULL, '06-02587', 60, 500, 500, 500, 'Grams', 200.00, 190.00, 3, NULL, '', 4, 'dvf', '2018-05-31 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-30 00:00:00', 180.00, 63, '', 2, NULL, NULL, 50, '2018-02-06 11:07:50', NULL, NULL, NULL),
(8, 59, 10, NULL, 'Redpeeper', 'Indian', 3, NULL, NULL, '06-02598', 60, 300, 300, 1, 'KG', 90.00, 85.00, 5, NULL, '', 4, NULL, '2018-05-31 00:00:00', 1, '2018-03-29 00:00:00', '2018-05-21 00:00:00', 85.00, 63, '', 2, NULL, NULL, 52, '2018-02-06 11:12:48', NULL, NULL, NULL),
(9, 59, 15, NULL, 'Watermelon', 'Indian', 3, NULL, NULL, '06-02599', 10, 100, 100, 2, 'KG', 30.00, 25.00, 5, NULL, '', 4, 'Change date', '2018-05-31 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-30 00:00:00', 25.00, 63, '', 2, NULL, NULL, 20, '2018-02-06 11:18:11', NULL, NULL, NULL),
(10, 59, 10, NULL, 'bitter melon', 'Indian', 3, NULL, NULL, '06-025910', 50, 1000, 690, 1, 'KG', 50.00, 45.00, 5, NULL, '', 4, 'Change date', '2018-04-30 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-20 00:00:00', 45.00, 63, '', 2, NULL, NULL, 35, '2018-02-06 11:24:31', NULL, NULL, NULL),
(12, 59, 10, NULL, 'Broccoli', 'Indian', 4, NULL, NULL, '06-025912', 20, 1000, 1000, 1, 'KG', 70.00, 68.00, 6, NULL, '', 4, 'Change date', '2018-04-30 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-20 00:00:00', 65.00, 63, '', 2, NULL, NULL, 20, '2018-02-06 12:42:56', NULL, NULL, NULL),
(13, 59, 15, NULL, 'litchi', 'Indian', 4, NULL, NULL, '06-025913', 60, 1000, 1000, 500, 'Grams', 80.00, 75.00, 5, NULL, '', 4, 'ghdf', '2018-05-16 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-30 00:00:00', 75.00, 63, '', 2, NULL, NULL, 115, '2018-02-06 12:50:24', NULL, NULL, NULL),
(15, 87, 15, NULL, 'Orange', 'abc', 4, NULL, NULL, '07-028715', 10, 10, 10, 1, 'KG', 500.00, 450.00, 1, NULL, '<p>test</p>', 2, NULL, '2018-03-09 00:00:00', 0, '2018-03-09 00:00:00', NULL, 450.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-07 07:33:47', NULL, NULL, NULL),
(18, 87, 11, NULL, 'applw', 'acv', 3, NULL, NULL, '07-028718', 25, 250, 250, 45, 'Grams', 2580.00, 2564.00, 555, NULL, '', 2, NULL, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', NULL, 2564.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-07 10:02:24', NULL, NULL, NULL),
(25, 59, 15, NULL, 'pomegranate', 'Indian', 4, NULL, NULL, '08-025925', 50, 1000, 817, 1, 'KG', 70.00, 68.00, 5, NULL, '', 4, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 68.00, 63, '', 2, NULL, NULL, 18, '2018-02-08 09:23:10', NULL, NULL, NULL),
(26, 58, 15, NULL, 'Orange', 'Indian', 4, NULL, NULL, '08-025826', 100, 1000, 995, 1, 'KG', 60.00, 58.00, 6, NULL, '', 4, NULL, '2018-03-11 00:00:00', 0, '2018-03-11 00:00:00', NULL, 58.00, 63, '', 2, NULL, NULL, 19, '2018-02-08 09:26:45', NULL, NULL, NULL),
(27, 58, 15, NULL, 'Green Banana', 'Indian', 4, NULL, NULL, '08-025827', 100, 1000, 994, 1, 'KG', 60.00, 58.00, 5, NULL, '', 4, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 58.00, 63, '', 2, NULL, NULL, 21, '2018-02-08 09:30:32', NULL, NULL, NULL),
(28, 58, 15, NULL, 'apple', 'Indian', 4, NULL, NULL, '08-025828', 60, 1000, 1000, 1, 'KG', 100.00, 95.00, 5, NULL, '', 4, 'Change date', '2018-05-15 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-19 00:00:00', 95.00, 63, '', 2, NULL, NULL, 28, '2018-02-08 09:33:25', NULL, NULL, NULL),
(29, 58, 10, NULL, 'Peas', 'Indian', 4, NULL, NULL, '08-025829', 50, 100, 0, 1, 'KG', 100.00, 85.00, 2, NULL, '', 4, 'Change date', '2018-04-30 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-06 00:00:00', 80.00, 63, '', 2, NULL, NULL, 16, '2018-02-08 09:36:42', NULL, NULL, NULL),
(33, 58, 3, NULL, 'test', 'abc', 4, NULL, NULL, '10-025833', 20, 100, 100, 1, 'KG', 1000.00, 950.00, 10, NULL, '', 2, NULL, '2018-03-12 00:00:00', 0, '2018-03-12 00:00:00', NULL, 950.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-10 15:36:30', NULL, NULL, NULL),
(34, 58, 13, NULL, 'test2', 'aaa', 4, NULL, NULL, '10-025834', 10, 1000, 1000, 1, 'KG', 2000.00, 1900.00, 50, NULL, '', 2, NULL, '2018-03-12 00:00:00', 0, '2018-03-12 00:00:00', NULL, 1900.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-10 15:37:41', NULL, NULL, NULL),
(38, 85, 10, NULL, 'Cucumber', 'Indian', 4, NULL, NULL, '10-028538', 50, 1000, 1000, 1, 'KG', 45.00, 40.00, 4, NULL, '', 2, NULL, '2018-03-12 00:00:00', 0, '2018-03-12 00:00:00', NULL, 40.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-10 16:25:55', NULL, NULL, NULL),
(39, 118, 10, NULL, 'Cucumber', 'Indian', 4, NULL, NULL, '10-0211839', 50, 1000, 1000, 1, 'KG', 45.00, 40.00, 5, NULL, '', 2, NULL, '2018-03-12 00:00:00', 0, '2018-03-12 00:00:00', NULL, 40.00, NULL, NULL, 2, NULL, NULL, 1, '2018-02-10 16:35:23', NULL, NULL, NULL),
(40, 59, 15, NULL, 'Mango', 'indian', 4, NULL, NULL, '01-035940', 20, 100, 100, 30, 'KG', 20.00, 15.00, 1, NULL, '<p>Apple test</p>', 2, NULL, '2018-04-30 00:00:00', 1, '2018-03-01 00:00:00', '2018-03-31 00:00:00', 15.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:53:13', NULL, NULL, NULL),
(41, 59, 15, NULL, 'apple', 'test', 4, NULL, NULL, '01-035941', 98, 200, 200, 20, 'KG', 100.00, 89.00, 3, NULL, '<p>test</p>', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 89.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:29:35', NULL, NULL, NULL),
(42, 26, 11, NULL, 'apple', 'chess', 4, NULL, NULL, '01-032642', 10, 5, 5, 50, 'KG', 25.00, 23.00, 250, NULL, '', 2, NULL, '2018-04-12 00:00:00', 0, '2018-04-12 00:00:00', NULL, 23.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 11:42:12', NULL, NULL, NULL),
(43, 59, 10, NULL, 'Carrot', 'Indian', 4, NULL, NULL, '01-035943', 100, 12, 12, 78, 'KG', 12.00, 10.00, 23, NULL, '', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 10.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:50:39', NULL, NULL, NULL),
(44, 59, 15, NULL, 'Green Banana', 'indian', 4, NULL, NULL, '01-035944', 10, 5, 5, 1, 'KG', 10.00, 8.00, 2, NULL, '', 2, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 8.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:49:20', NULL, NULL, NULL),
(45, 59, 13, 14, 'Blackberry', 'indian', 4, NULL, NULL, '01-035945', 23, 300, 300, 30, 'KG', 20.00, 18.00, 5, NULL, '<p>Test</p>', 2, NULL, '2018-03-31 00:00:00', 1, '2018-03-01 00:00:00', '2018-03-21 00:00:00', 15.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:52:00', NULL, NULL, NULL),
(46, 26, 13, 14, 'test', 'test', 7, NULL, NULL, '01-032646', 30, 200, 200, 19, 'KG', 100.00, 90.00, 40, NULL, '<p>test</p>', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 90.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:45:33', NULL, NULL, NULL),
(47, 59, 12, NULL, 'test', 'test', 7, NULL, NULL, '01-035947', 23, 300, 300, 23, 'KG', 20.00, 10.00, 4, NULL, '<p>test</p>', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 10.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:51:34', NULL, NULL, NULL),
(48, 59, 15, NULL, 'Strawberry', 'indian', 4, NULL, NULL, '01-035948', 20, 5, 5, 1, 'KG', 90.00, 85.00, 3, NULL, '', 2, NULL, '2018-03-31 00:00:00', 1, '2018-03-01 00:00:00', '2018-03-21 00:00:00', 85.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 10:55:05', NULL, NULL, NULL),
(49, 26, 1, NULL, 'chiese', 'test', 4, NULL, NULL, '01-032649', 25, 250, 250, 11, 'Grams', 26.00, 24.00, 24, NULL, '', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 24.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-01 11:41:33', NULL, NULL, NULL),
(50, 26, 10, NULL, 'sprouts', 'xyz', 4, NULL, NULL, '01-032650', 100, 200, 200, 1, 'KG', 50.00, 48.00, 2, NULL, '<p>test</p>', 2, NULL, '2018-03-31 00:00:00', 0, '2018-03-31 00:00:00', NULL, 48.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-31 12:50:13', NULL, NULL, NULL),
(51, 26, 8, NULL, 'Chicken Sausage', 'CountryGreens', 4, NULL, NULL, '01-032651', 40, 239, 6, 12, 'KG', 65.00, 63.00, 10, NULL, '<p>vvv</p>', 6, '', '2018-06-21 00:00:00', 1, '2018-03-01 00:00:00', '2018-03-23 00:00:00', 61.00, 63, NULL, 2, NULL, NULL, 21, '2018-03-01 12:38:16', 0.00, '2018-03-17 18:50:17', NULL),
(52, 26, 7, NULL, 'king fish', 'searock', 4, NULL, NULL, '01-032652', 1, 5, 5, 100, 'KG', 300.00, 285.00, 2, NULL, '', 2, NULL, '2018-04-28 00:00:00', 1, '2018-03-22 00:00:00', '2018-03-28 00:00:00', 245.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-14 19:09:20', NULL, NULL, NULL),
(53, 127, 15, NULL, 'test', 'kisan test', 4, NULL, NULL, '05-0312753', 20, 200, 200, 20, 'KG', 19.00, 18.00, 1, NULL, '<p>Test</p>', 3, NULL, '2018-04-11 00:00:00', 0, '2018-04-11 00:00:00', NULL, 18.00, NULL, 'Bank details not verified', 2, NULL, NULL, 1, '2018-03-05 19:21:23', NULL, NULL, NULL),
(54, 133, 15, NULL, 'Orange', 'Indian', 4, NULL, NULL, '06-0313354', 50, 1000, 1000, 2, 'KG', 60.00, 65.00, 0, NULL, '', 4, NULL, '2018-04-30 00:00:00', 1, '2018-03-27 00:00:00', '2018-04-19 00:00:00', 65.00, 63, '', 2, NULL, NULL, 33, '2018-03-06 16:31:37', NULL, NULL, NULL),
(56, 133, 15, NULL, 'Mango', 'Indian', 4, NULL, NULL, '07-0313356', 50, 1000, 986, 1, 'KG', 50.00, 45.00, 1, NULL, '', 4, NULL, '2018-04-06 00:00:00', 0, '2018-04-06 00:00:00', NULL, 45.00, 63, 'Good product', 2, NULL, NULL, 39, '2018-03-07 12:51:29', NULL, NULL, NULL),
(57, 137, 15, NULL, 'Mango', 'Indian', 4, NULL, NULL, '08-0313757', 60, 20, 20, 1, 'KG', 50.00, 45.00, 0, NULL, '', 3, NULL, '2018-04-07 00:00:00', 0, '2018-04-07 00:00:00', NULL, 45.00, NULL, 'not upload', 2, NULL, NULL, 1, '2018-03-08 19:57:52', NULL, NULL, NULL),
(58, 140, 15, NULL, 'Mango', 'Indian', 4, NULL, NULL, '10-0314058', 30, 1000, 999, 1, 'KG', 70.00, 65.00, 1, NULL, '', 4, NULL, '2018-04-24 00:00:00', 1, '2018-03-29 00:00:00', '2018-04-14 00:00:00', 61.00, 63, 'Not upload Bank details please upload', 2, NULL, NULL, 21, '2018-03-10 17:30:53', NULL, NULL, NULL),
(59, 26, 11, NULL, 'apple', 'Indian', 4, NULL, NULL, '17-032659', 50, 100, 100, 1, 'KG', 20.00, 19.00, 0, NULL, '', 2, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 19.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-17 15:44:37', NULL, NULL, NULL),
(60, 26, 11, NULL, 'apple', 'www', 4, NULL, NULL, '17-032660', 1, 8, 8, 5, 'KG', 25.00, 24.00, 0, NULL, '', 2, NULL, '2018-04-19 00:00:00', 0, '2018-04-19 00:00:00', NULL, 24.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-17 15:57:00', NULL, NULL, NULL),
(61, 26, 3, NULL, 'test', 'rrr', 6, NULL, NULL, '17-032661', 58, 698, 698, 5, 'Litre', 258.00, 253.00, 0, NULL, '', 2, NULL, '2018-04-27 00:00:00', 0, '2018-04-27 00:00:00', NULL, 253.00, NULL, 'Bank details not verified', 2, NULL, NULL, 1, '2018-03-17 16:02:22', NULL, NULL, NULL),
(62, 147, 10, NULL, 'Broccoli', 'Indian', 4, NULL, NULL, '23-0314762', 30, 1000, 990, 1, 'KG', 50.00, 45.00, 0, NULL, '', 6, '', '2018-04-22 00:00:00', 0, '2018-04-22 00:00:00', NULL, 45.00, 63, 'Upload Bank details please ohterwise you unable to sell please please', 2, NULL, NULL, 8, '2018-03-23 11:39:14', 0.00, '2018-03-23 13:22:00', NULL),
(63, 147, 10, NULL, 'Broccoli', 'Indian', 4, NULL, NULL, '23-0314763', 50, 1000, 994, 1, 'KG', 50.00, 45.00, 0, NULL, '', 4, NULL, '2018-04-22 00:00:00', 1, '2018-03-23 00:00:00', '2018-04-12 00:00:00', 35.00, 63, NULL, 2, NULL, NULL, 20, '2018-03-23 13:32:29', NULL, NULL, NULL),
(64, 148, 11, NULL, 'Dry Fruits', 'Cookies', 4, NULL, NULL, '27-0314864', 2, 1, 0, 2, 'KG', 5.00, 5.00, 0, NULL, '', 4, NULL, '2018-04-30 00:00:00', 0, '2018-04-30 00:00:00', NULL, 5.00, 63, NULL, 2, NULL, NULL, 3, '2018-03-27 16:59:03', NULL, NULL, NULL),
(65, 59, 15, NULL, 'apple', 'india', 4, NULL, NULL, '29-035965', 20, 200, 200, 39, 'KG', 29.00, 25.00, 0, NULL, '<p>test</p>', 5, NULL, '2019-04-30 00:00:00', 0, '2019-04-30 00:00:00', NULL, 25.00, NULL, 'bad product', 2, NULL, NULL, 1, '2018-03-29 12:05:09', NULL, NULL, NULL),
(66, 151, 15, NULL, 'apple', 'indian', 4, NULL, NULL, '29-0315166', 20, 200, 175, 2, 'KG', 25.00, 20.00, 0, NULL, '<p>test product</p>', 4, NULL, '2019-04-23 00:00:00', 1, '2018-04-01 00:00:00', '2018-04-10 00:00:00', 18.00, 63, NULL, 2, NULL, NULL, 19, '2018-03-29 18:45:20', NULL, NULL, NULL),
(67, 151, 15, NULL, 'Mango', 'Indian', 4, NULL, NULL, '29-0315167', 20, 200, 0, 1, 'KG', 10.00, 5.00, 0, NULL, '', 4, NULL, '2018-05-16 00:00:00', 1, '2018-03-29 00:00:00', '2018-05-06 00:00:00', 5.00, 63, NULL, 2, NULL, NULL, 9, '2018-03-29 19:31:22', NULL, NULL, NULL),
(68, 151, 10, NULL, 'kisan carrot', 'indian', 4, NULL, NULL, '31-0315168', 50, 100, 100, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-31 00:00:00', 0, '2018-05-31 00:00:00', NULL, 45.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-31 17:29:48', NULL, NULL, NULL),
(69, 151, 15, NULL, 'prawn', 'indian', 4, NULL, NULL, '31-0315169', 50, 100, 100, 5, 'KG', 300.00, 250.00, 0, NULL, '', 2, NULL, '2018-05-31 00:00:00', 0, '2018-05-31 00:00:00', NULL, 250.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-31 17:34:56', NULL, NULL, NULL),
(70, 151, 15, NULL, 'kisan jam', 'indian', 4, NULL, NULL, '31-0315170', 50, 100, 100, 5, 'KG', 50.00, 40.00, 0, NULL, '', 4, NULL, '2018-05-16 00:00:00', 0, '2018-05-16 00:00:00', NULL, 40.00, 63, NULL, 2, NULL, NULL, 1, '2018-03-31 17:38:33', NULL, NULL, NULL),
(71, 151, 15, NULL, 'kisan fruti', 'indian', 4, NULL, NULL, '31-0315171', 50, 100, 89, 3, 'KG', 40.00, 30.00, 0, NULL, '', 4, NULL, '2018-05-31 00:00:00', 0, '2018-05-31 00:00:00', NULL, 30.00, 63, NULL, 2, NULL, NULL, 28, '2018-03-31 17:42:23', NULL, NULL, NULL),
(72, 151, 15, NULL, 'kisan patato', 'indian', 4, NULL, NULL, '31-0315172', 100, 1000, 1000, 1, 'KG', 10.00, 8.00, 0, NULL, '', 2, NULL, '2018-05-31 00:00:00', 1, '2018-03-31 00:00:00', '2018-05-01 00:00:00', 15.00, NULL, NULL, 2, NULL, NULL, 1, '2018-03-31 17:50:04', NULL, NULL, NULL),
(73, 151, 15, NULL, 'kisan tamato', 'indian', 4, NULL, NULL, '31-0315173', 50, 100, 94, 1, 'KG', 10.00, 5.00, 0, NULL, '', 4, NULL, '2018-05-31 00:00:00', 1, '2018-03-31 00:00:00', '2018-04-27 00:00:00', 15.00, 63, NULL, 2, NULL, NULL, 5, '2018-03-31 18:00:57', NULL, NULL, NULL),
(74, 59, 18, NULL, 'aoo', 'test', 4, NULL, NULL, '02-045974', 25, 258, 258, 5, 'Litre', 25.00, 23.00, 0, NULL, '', 2, NULL, '2018-05-18 00:00:00', 0, '2018-05-18 00:00:00', NULL, 23.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-02 11:15:21', NULL, NULL, NULL),
(75, 26, 7, NULL, 'test', 'testssgs', 4, NULL, NULL, '03-042675', 25, 258, 258, 258, 'KG', 258.00, 253.00, 0, NULL, 'nice', 2, NULL, '2018-05-25 00:00:00', 1, '2018-04-13 00:00:00', '2018-04-27 00:00:00', 205.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-03 20:48:21', NULL, NULL, NULL),
(76, 150, 8, NULL, 'chicken ', 'india(mk)', 4, NULL, NULL, '04-0415076', 10, 10, 10, 1, 'KG', 100.00, 25.00, 0, NULL, 'product', 2, NULL, '2018-05-05 00:00:00', 0, '2018-05-05 00:00:00', NULL, 25.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 11:20:04', NULL, NULL, NULL),
(77, 26, 11, NULL, 'test', 'rrrr', 4, NULL, NULL, '04-042677', 25, 22, 22, 25, 'KG', 12.00, 11.00, 0, NULL, '', 2, NULL, '2018-05-26 00:00:00', 0, '2018-05-26 00:00:00', NULL, 11.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 11:56:01', NULL, NULL, NULL),
(78, 155, 11, NULL, 'yellowfruit', 'mk', 4, NULL, NULL, '04-0415578', 15, 10, 10, 1, 'KG', 150.00, 30.00, 0, NULL, 'sweet', 2, NULL, '2018-05-12 00:00:00', 0, '2018-05-12 00:00:00', NULL, 30.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 12:28:43', NULL, NULL, NULL),
(79, 155, 11, NULL, 'fruitm', 'mk', 4, NULL, NULL, '04-0415579', 10, 10, 10, 1, 'KG', 400.00, 50.00, 0, NULL, 'product', 2, NULL, '2018-05-19 00:00:00', 0, '2018-05-19 00:00:00', NULL, 50.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 12:39:58', NULL, NULL, NULL),
(80, 84, 18, NULL, 'rrr', 'rrrt', 4, NULL, NULL, '04-048480', 258, 25, 25, 222, 'KG', 145.00, 123.00, 0, NULL, '', 2, NULL, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', NULL, 111.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 16:59:05', NULL, NULL, NULL),
(81, 26, 15, NULL, 'kisan jam', 'indian', 4, NULL, NULL, '04-042681', 50, 100, 100, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-08 00:00:00', 0, '2018-05-08 00:00:00', NULL, 45.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 20:37:42', NULL, NULL, NULL),
(82, 26, 15, NULL, 'test', 'indian', 4, NULL, NULL, '04-042682', 28, 100, 100, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-04 00:00:00', 0, '2018-05-04 00:00:00', NULL, 45.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 20:39:20', NULL, NULL, NULL),
(83, 26, 15, NULL, 'test', 'indian', 4, NULL, NULL, '04-042683', 29, 500, 500, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-04 00:00:00', 0, '2018-05-04 00:00:00', NULL, 45.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 20:42:08', NULL, NULL, NULL),
(84, 26, 7, NULL, 'hdhhd', 'jduf', 4, NULL, NULL, '04-042684', 2, 5, 5, 6, 'KG', 2.00, 1.00, 0, NULL, '', 2, NULL, '2018-05-04 00:00:00', 1, '2018-04-04 00:00:00', '2018-04-30 00:00:00', 2.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 21:07:42', NULL, NULL, NULL),
(85, 26, 3, NULL, 'hxhdu', 'gsyys', 4, NULL, NULL, '04-042685', 2, 3, 3, 55, 'KG', 685.00, 54.00, 0, NULL, '', 2, NULL, '2018-05-04 00:00:00', 1, '2018-04-04 00:00:00', '2018-04-30 00:00:00', 2.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-04 21:09:31', NULL, NULL, NULL),
(86, 155, 11, NULL, 'mangotest', 'mk', 4, NULL, NULL, '05-0415586', 15, 40, 40, 1, 'KG', 80.00, 50.00, 0, NULL, 'sweetmango', 2, NULL, '2018-05-16 00:00:00', 0, '2018-05-16 00:00:00', NULL, 50.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-05 11:06:44', NULL, NULL, NULL),
(87, 26, 10, NULL, 'sabya', 'sabya', 4, NULL, NULL, '06-042687', 2, 50, 50, 1, 'KG', 25.00, 22.00, 0, NULL, '', 2, NULL, '2018-05-31 00:00:00', 1, '2018-04-09 00:00:00', '2018-04-25 00:00:00', 22.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-06 19:34:17', NULL, NULL, NULL),
(88, 151, 11, NULL, 'apple', 'india', 4, NULL, NULL, '06-0415188', 50, 100, 100, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-07-30 00:00:00', 1, '2018-04-06 00:00:00', '2018-04-24 00:00:00', 40.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-06 22:06:00', NULL, NULL, NULL),
(89, 151, 15, NULL, 'patato', 'india', 4, NULL, NULL, '06-0415189', 20, 50, 50, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-06-30 00:00:00', 1, '2018-04-06 00:00:00', '2018-04-30 00:00:00', 40.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-06 22:08:10', NULL, NULL, NULL),
(90, 151, 15, NULL, 'patato', 'india', 4, NULL, NULL, '06-0415190', 50, 100, 100, 1, 'KG', 50.00, 40.00, 0, NULL, '', 2, NULL, '2018-05-31 00:00:00', 1, '2018-04-06 00:00:00', '2018-04-30 00:00:00', 4.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-06 22:13:57', NULL, NULL, NULL),
(91, 151, 11, NULL, 'kisan patato', 'indian', 4, NULL, NULL, '07-0415191', 10, 40, 40, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-07 00:00:00', 1, '2018-04-07 00:00:00', '2018-04-07 00:00:00', 40.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-07 17:07:46', NULL, NULL, NULL),
(92, 151, 10, NULL, 'kisan carrot', 'Indian', 4, NULL, NULL, '07-0415192', 40, 50, 50, 1, 'KG', 50.00, 45.00, 0, NULL, '', 2, NULL, '2018-05-07 00:00:00', 1, '2018-04-07 00:00:00', '2018-04-27 00:00:00', 45.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-07 17:12:13', NULL, NULL, NULL),
(93, 155, 15, NULL, 'mangotestkisan', 'india', 4, NULL, NULL, '07-0415593', 1, 2, 2, 3, 'KG', 200.00, 50.00, 0, NULL, 'Niceproducts', 2, NULL, '2018-05-19 00:00:00', 1, '2018-04-08 00:00:00', '2018-04-18 00:00:00', 200.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-07 17:25:39', NULL, NULL, NULL),
(94, 155, 15, NULL, 'mangotestkisan', 'india', 4, NULL, NULL, '07-0415594', 1, 2, 2, 3, 'KG', 200.00, 50.00, 0, NULL, 'Niceproducts', 2, NULL, '2018-05-19 00:00:00', 1, '2018-04-08 00:00:00', '2018-04-18 00:00:00', 200.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-07 17:25:56', NULL, NULL, NULL),
(95, 155, 11, NULL, 'kisanmango', 'mk', 4, NULL, NULL, '07-0415595', 18, 52, 52, 1, 'KG', 250.00, 50.00, 0, NULL, 'Nice', 2, NULL, '2018-05-19 00:00:00', 1, '2018-04-14 00:00:00', '2018-04-17 00:00:00', 500.00, NULL, NULL, 2, NULL, NULL, 1, '2018-04-07 17:46:26', NULL, NULL, NULL),
(96, 26, 13, 14, 'Apple Test', 'country Green', 7, NULL, NULL, '29-042696', 3, 40, 40, 3, 'KG', 8.00, 6.00, 0, NULL, '', 4, NULL, '2018-06-12 00:00:00', 1, '2018-05-17 00:00:00', '2018-05-22 00:00:00', 6.00, 63, '', 2, NULL, NULL, 1, '2018-04-29 13:25:34', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sps__productdocuments`
--

CREATE TABLE `sps__productdocuments` (
  `id` int(122) NOT NULL,
  `product_id` int(122) NOT NULL,
  `name` varchar(222) NOT NULL,
  `description` text,
  `flag` int(11) DEFAULT NULL COMMENT '1->health,2->Municipality,3->certificate_origin',
  `status` tinyint(1) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__productdocuments`
--

INSERT INTO `sps__productdocuments` (`id`, `product_id`, `name`, `description`, `flag`, `status`, `add_date`) VALUES
(1, 1, 'doc_h_Untitled6.png', NULL, 1, 1, '2018-02-06 07:28:41'),
(2, 1, 'doc_m_Untitled7.png', NULL, 2, 1, '2018-02-06 07:28:41'),
(3, 1, 'doc_c_Untitled6.png', NULL, 3, 1, '2018-02-06 07:28:41'),
(4, 2, 'doc_h_Untitled.png', NULL, 1, 1, '2018-02-06 07:43:06'),
(5, 2, 'doc_m_Untitled4.png', NULL, 2, 1, '2018-02-06 07:43:06'),
(6, 2, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-06 07:43:06'),
(7, 3, 'doc_h_Untitled38.png', NULL, 1, 1, '2018-02-06 07:59:48'),
(8, 3, 'doc_m_deliverynote.pdf', NULL, 2, 1, '2018-02-08 10:14:49'),
(9, 3, 'doc_c_Untitled36.png', NULL, 3, 1, '2018-02-06 07:59:48'),
(10, 4, 'doc_h_Untitled7.png', NULL, 1, 1, '2018-02-06 09:37:03'),
(11, 4, 'doc_m_Untitled9.png', NULL, 2, 1, '2018-02-06 09:37:03'),
(12, 4, 'doc_c_Untitled39.png', NULL, 3, 1, '2018-02-06 09:37:03'),
(13, 5, 'doc_h_Untitled35.png', NULL, 1, 1, '2018-02-06 10:52:46'),
(14, 5, 'doc_m_Untitled28.png', NULL, 2, 1, '2018-02-06 10:52:46'),
(15, 5, 'doc_c_Untitled37.png', NULL, 3, 1, '2018-02-06 10:52:46'),
(16, 6, 'doc_h_report_02-06-2017.pdf', NULL, 1, 1, '2018-02-08 07:46:59'),
(17, 6, 'doc_m_deliverynote.pdf', NULL, 2, 1, '2018-02-08 10:14:49'),
(18, 6, 'doc_c_Untitled9.png', NULL, 3, 1, '2018-02-06 10:54:56'),
(19, 7, 'doc_h_Untitled12.png', NULL, 1, 1, '2018-02-06 11:06:22'),
(20, 7, 'doc_m_Untitled8.png', NULL, 2, 1, '2018-02-06 11:06:22'),
(21, 7, 'doc_c_Untitled9.png', NULL, 3, 1, '2018-02-06 11:06:22'),
(22, 8, 'doc_h_Untitled39.png', NULL, 1, 1, '2018-02-06 11:12:51'),
(23, 8, 'doc_m_Untitled40.png', NULL, 2, 1, '2018-02-06 11:12:51'),
(24, 8, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-06 11:12:51'),
(25, 9, 'doc_h_Untitled11.png', NULL, 1, 1, '2018-02-06 11:18:14'),
(26, 9, 'doc_m_Untitled5.png', NULL, 2, 1, '2018-02-06 11:18:14'),
(27, 9, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-06 11:18:14'),
(28, 10, 'doc_h_Untitled7.png', NULL, 1, 1, '2018-02-06 11:24:34'),
(29, 10, 'doc_m_Untitled11.png', NULL, 2, 1, '2018-02-06 11:24:34'),
(30, 10, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-06 11:24:34'),
(31, 11, 'doc_h_images.jpg', NULL, 1, 1, '2018-02-06 11:27:00'),
(32, 11, 'doc_m_images1_(2).jpg', NULL, 2, 1, '2018-02-06 11:27:00'),
(33, 11, 'doc_c_images_(2).jpg', NULL, 3, 1, '2018-02-06 11:27:00'),
(34, 12, 'doc_h_Untitled9.png', NULL, 1, 1, '2018-02-06 12:42:59'),
(35, 12, 'doc_m_Untitled8.png', NULL, 2, 1, '2018-02-06 12:42:59'),
(36, 12, 'doc_c_Untitled7.png', NULL, 3, 1, '2018-02-06 12:42:59'),
(37, 13, 'doc_h_Untitled11.png', NULL, 1, 1, '2018-02-06 12:50:28'),
(38, 13, 'doc_m_deliverynote.pdf', NULL, 2, 1, '2018-02-08 10:14:49'),
(39, 13, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-06 12:50:28'),
(43, 15, 'doc_h_orange.jpg', NULL, 1, 1, '2018-02-07 07:33:49'),
(44, 15, 'doc_m_orange.jpg', NULL, 2, 1, '2018-02-07 07:33:49'),
(45, 15, 'doc_c_orange.jpg', NULL, 3, 1, '2018-02-07 07:33:49'),
(52, 25, 'doc_h_Untitled7.png', NULL, 1, 1, '2018-02-08 09:23:16'),
(53, 25, 'doc_m_report_02-06-2017.pdf', NULL, 2, 1, '2018-02-08 09:23:16'),
(54, 25, 'doc_c_report_02-06-2017.pdf', NULL, 3, 1, '2018-02-08 09:23:16'),
(55, 26, 'doc_h_Untitled5.png', NULL, 1, 1, '2018-02-08 09:26:47'),
(56, 26, 'doc_m_deliverynote.pdf', NULL, 2, 1, '2018-02-08 10:14:49'),
(57, 26, 'doc_c_Untitled11.png', NULL, 3, 1, '2018-02-08 09:26:47'),
(58, 27, 'doc_h_Untitled3thguthguithg.png', NULL, 1, 1, '2018-02-08 09:30:35'),
(59, 27, 'doc_m_Untitled6.png', NULL, 2, 1, '2018-02-08 09:30:35'),
(60, 27, 'doc_c_Untitled9.png', NULL, 3, 1, '2018-02-08 09:30:35'),
(61, 28, 'doc_h_Untitled7.png', NULL, 1, 1, '2018-02-08 09:32:45'),
(62, 28, 'doc_m_Untitled8.png', NULL, 2, 1, '2018-02-08 09:32:46'),
(63, 28, 'doc_c_Untitled9.png', NULL, 3, 1, '2018-02-08 09:32:46'),
(64, 29, 'doc_h_Untitled7.png', NULL, 1, 1, '2018-02-08 09:36:46'),
(65, 29, 'doc_m_Untitled6.png', NULL, 2, 1, '2018-02-08 09:36:46'),
(66, 29, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-08 09:36:46'),
(70, 33, 'doc_h_salmonfillets.jpg', NULL, NULL, 1, '2018-02-10 15:36:30'),
(71, 33, 'doc_m_salmonfillets.jpg', NULL, NULL, 1, '2018-02-10 15:36:30'),
(72, 33, 'doc_c_salmonfillets.jpg', NULL, NULL, 1, '2018-02-10 15:36:30'),
(79, 38, 'doc_h_Untitled3thguthguithg.png', NULL, 1, 1, '2018-02-10 16:25:58'),
(80, 38, 'doc_m_Untitled7.png', NULL, 2, 1, '2018-02-10 16:25:58'),
(81, 38, 'doc_c_Untitled.png', NULL, 3, 1, '2018-02-10 16:25:58'),
(82, 39, 'doc_h_Untitled.png', NULL, 1, 1, '2018-02-10 16:35:26'),
(83, 39, 'doc_m_Untitled11.png', NULL, 2, 1, '2018-02-10 16:35:26'),
(84, 39, 'doc_c_Untitled12.png', NULL, 3, 1, '2018-02-10 16:35:26'),
(85, 41, 'doc_h_smallapple.jpg', NULL, NULL, 1, '2018-03-01 10:29:35'),
(86, 41, 'doc_m_smallapple.jpg', NULL, NULL, 1, '2018-03-01 10:29:35'),
(87, 41, 'doc_c_smallapple.jpg', NULL, NULL, 1, '2018-03-01 10:29:35'),
(88, 42, 'doc_h_cute_baby_boy_green_eyes-3840x2160.jpg', NULL, 1, 1, '2018-03-01 10:33:00'),
(89, 42, 'doc_m_images_(1).jpg', NULL, 2, 1, '2018-03-01 10:33:00'),
(90, 42, 'doc_c_blue_eyes.jpg', NULL, 3, 1, '2018-03-01 10:33:00'),
(91, 44, 'doc_h_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:49:20'),
(92, 44, 'doc_m_Hydrangeas.jpg', NULL, NULL, 1, '2018-03-01 10:49:20'),
(93, 44, 'doc_c_Jellyfish.jpg', NULL, NULL, 1, '2018-03-01 10:49:20'),
(94, 43, 'doc_h_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:50:39'),
(95, 43, 'doc_m_Hydrangeas.jpg', NULL, NULL, 1, '2018-03-01 10:50:39'),
(96, 43, 'doc_c_Hydrangeas.jpg', NULL, NULL, 1, '2018-03-01 10:50:39'),
(97, 47, 'doc_h_smallapple.jpg', NULL, 1, 1, '2018-03-01 10:51:34'),
(98, 47, 'doc_m_smallapple.jpg', NULL, 2, 1, '2018-03-01 10:51:34'),
(99, 47, 'doc_c_smallapple.jpg', NULL, 3, 1, '2018-03-01 10:51:34'),
(100, 45, 'doc_h_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:52:00'),
(101, 45, 'doc_m_Chrysanthemum.jpg', NULL, NULL, 1, '2018-03-01 10:52:00'),
(102, 45, 'doc_c_Hydrangeas.jpg', NULL, NULL, 1, '2018-03-01 10:52:00'),
(103, 40, 'doc_h_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:53:13'),
(104, 40, 'doc_m_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:53:13'),
(105, 40, 'doc_c_Desert.jpg', NULL, NULL, 1, '2018-03-01 10:53:13'),
(106, 48, 'doc_h_Desert.jpg', NULL, 1, 1, '2018-03-01 10:55:05'),
(107, 48, 'doc_m_Hydrangeas.jpg', NULL, 2, 1, '2018-03-01 10:55:05'),
(108, 48, 'doc_c_Jellyfish.jpg', NULL, 3, 1, '2018-03-01 10:55:05'),
(109, 49, 'doc_h_images_(1).jpg', NULL, 1, 1, '2018-03-01 10:55:46'),
(110, 49, 'doc_m_blue_eyes.jpg', NULL, 2, 1, '2018-03-01 10:55:46'),
(111, 49, 'doc_c_images_(1).jpg', NULL, 3, 1, '2018-03-01 10:55:46'),
(112, 50, 'doc_h_brusselsprouts.jpg', NULL, 1, 1, '2018-03-01 11:44:48'),
(113, 50, 'doc_m_brusselsprouts.jpg', NULL, 2, 1, '2018-03-01 11:44:48'),
(114, 50, 'doc_c_brusselsprouts.jpg', NULL, 3, 1, '2018-03-01 11:44:48'),
(115, 51, 'doc_h_sausage.jpg', NULL, 1, 1, '2018-03-01 12:38:16'),
(116, 51, 'doc_m_sausage.jpg', NULL, 2, 1, '2018-03-01 12:38:16'),
(117, 51, 'doc_c_sausage.jpg', NULL, 3, 1, '2018-03-01 12:38:16'),
(118, 52, 'doc_h_images_(1).jpg', NULL, 1, 1, '2018-03-01 16:07:24'),
(119, 52, 'doc_m_cute_baby_boy_green_eyes-3840x2160.jpg', NULL, 2, 1, '2018-03-01 16:07:24'),
(120, 52, 'doc_c_images_(1).jpg', NULL, 3, 1, '2018-03-01 16:07:24'),
(121, 53, 'doc_h_smallapple.jpg', NULL, 1, 1, '2018-03-05 19:21:27'),
(122, 53, 'doc_m_smallapple.jpg', NULL, 2, 1, '2018-03-05 19:21:27'),
(123, 53, 'doc_c_smallapple.jpg', NULL, 3, 1, '2018-03-05 19:21:27'),
(124, 54, 'doc_h_Desert.jpg', NULL, 1, 1, '2018-03-06 16:31:41'),
(125, 54, 'doc_m_Penguins.jpg', NULL, 2, 1, '2018-03-06 16:31:41'),
(126, 54, 'doc_c_Tulips.jpg', NULL, 3, 1, '2018-03-06 16:31:41'),
(130, 56, 'doc_h_blackberry2.jpg', NULL, 1, 1, '2018-03-07 12:51:33'),
(131, 56, 'doc_m_blackberry3.jpg', NULL, 2, 1, '2018-03-07 12:51:33'),
(132, 56, 'doc_c_blackberry3.jpg', NULL, 3, 1, '2018-03-07 12:51:33'),
(133, 57, 'doc_h_mango.jpg', NULL, 1, 1, '2018-03-08 19:57:56'),
(134, 57, 'doc_m_mango.jpg', NULL, 2, 1, '2018-03-08 19:57:56'),
(135, 57, 'doc_c_mango.jpg', NULL, 3, 1, '2018-03-08 19:57:56'),
(136, 58, 'doc_h_blackberry3.jpg', NULL, 1, 1, '2018-03-10 17:31:01'),
(137, 58, 'doc_m_blackberry3.jpg', NULL, 2, 1, '2018-03-10 17:31:01'),
(138, 58, 'doc_c_blackberry3.jpg', NULL, 3, 1, '2018-03-10 17:31:01'),
(139, 59, 'doc_h_IMG_20180317_153910045_HDR.jpg', NULL, 1, 1, '2018-03-17 15:44:37'),
(140, 59, 'doc_m_IMG_20180317_153910045_HDR.jpg', NULL, 2, 1, '2018-03-17 15:44:37'),
(141, 59, 'doc_c_IMG_20180317_151006037_HDR.jpg', NULL, 3, 1, '2018-03-17 15:44:37'),
(142, 60, 'doc_h_IMG_20180317_155600765_HDR.jpg', NULL, 1, 1, '2018-03-17 15:57:00'),
(143, 60, 'doc_m_IMG_20180317_135203627_HDR.jpg', NULL, 2, 1, '2018-03-17 15:57:00'),
(144, 60, 'doc_c_IMG-20180317-WA0002.jpg', NULL, 3, 1, '2018-03-17 15:57:00'),
(145, 61, 'doc_h_IMG_20180317_151006037_HDR.jpg', NULL, 1, 1, '2018-03-17 16:02:22'),
(146, 61, 'doc_m_IMG_20180317_153910045_HDR.jpg', NULL, 2, 1, '2018-03-17 16:02:22'),
(147, 61, 'doc_c_IMG-20180317-WA0002.jpg', NULL, 3, 1, '2018-03-17 16:02:22'),
(148, 62, 'doc_h_blackberry3.jpg', NULL, 1, 1, '2018-03-23 11:34:28'),
(149, 62, 'doc_m_blackberry250x250.jpg', NULL, 2, 1, '2018-03-23 11:34:28'),
(150, 62, 'doc_c_blackberry2.jpg', NULL, 3, 1, '2018-03-23 11:34:28'),
(151, 63, 'doc_h_blackberry2.jpg', NULL, 1, 1, '2018-03-23 13:32:33'),
(152, 63, 'doc_m_blackberry2.jpg', NULL, 2, 1, '2018-03-23 13:32:33'),
(153, 63, 'doc_c_blackberry2.jpg', NULL, 3, 1, '2018-03-23 13:32:33'),
(154, 64, 'doc_h_Chrysanthemum.jpg', NULL, 1, 1, '2018-03-27 15:26:06'),
(155, 64, 'doc_m_Desert.jpg', NULL, 2, 1, '2018-03-27 15:26:06'),
(156, 64, 'doc_c_Hydrangeas.jpg', NULL, 3, 1, '2018-03-27 15:26:06'),
(157, 65, 'doc_h_Chrysanthemum.jpg', NULL, 1, 1, '2018-03-29 12:05:14'),
(158, 65, 'doc_m_Chrysanthemum.jpg', NULL, 2, 1, '2018-03-29 12:05:14'),
(159, 65, 'doc_c_Chrysanthemum.jpg', NULL, 3, 1, '2018-03-29 12:05:14'),
(160, 66, 'doc_h_Chrysanthemum.jpg', NULL, 1, 1, '2018-03-29 18:45:24'),
(161, 66, 'doc_m_Chrysanthemum.jpg', NULL, 2, 1, '2018-03-29 18:45:24'),
(162, 66, 'doc_c_Chrysanthemum.jpg', NULL, 3, 1, '2018-03-29 18:45:24'),
(163, 67, 'doc_h_mangoes.jpg', NULL, 1, 1, '2018-03-29 19:31:26'),
(164, 67, 'doc_m_mangoes.jpg', NULL, 2, 1, '2018-03-29 19:31:26'),
(165, 67, 'doc_c_mangoes.jpg', NULL, 3, 1, '2018-03-29 19:31:26'),
(166, 68, 'doc_h_IMG-20180331-WA0022.jpg', NULL, 1, 1, '2018-03-31 17:29:48'),
(167, 68, 'doc_m_IMG-20180331-WA0017.jpg', NULL, 2, 1, '2018-03-31 17:29:48'),
(168, 68, 'doc_c_IMG-20180331-WA0021.jpg', NULL, 3, 1, '2018-03-31 17:29:48'),
(169, 69, 'doc_h_Screenshot_20180331-161913.png', NULL, 1, 1, '2018-03-31 17:34:56'),
(170, 69, 'doc_m_IMG-20180331-WA0022.jpg', NULL, 2, 1, '2018-03-31 17:34:56'),
(171, 69, 'doc_c_IMG-20180331-WA0021.jpg', NULL, 3, 1, '2018-03-31 17:34:56'),
(172, 70, 'doc_h_IMG-20180331-WA0021.jpg', NULL, 1, 1, '2018-03-31 17:38:33'),
(173, 70, 'doc_m_IMG-20180331-WA0022.jpg', NULL, 2, 1, '2018-03-31 17:38:33'),
(174, 70, 'doc_c_Screenshot_20180331-161913.png', NULL, 3, 1, '2018-03-31 17:38:33'),
(175, 71, 'doc_h_IMG-20180331-WA0017.jpg', NULL, 1, 1, '2018-03-31 17:42:23'),
(176, 71, 'doc_m_IMG-20180331-WA0016.jpg', NULL, 2, 1, '2018-03-31 17:42:23'),
(177, 71, 'doc_c_IMG-20180331-WA0019.jpg', NULL, 3, 1, '2018-03-31 17:42:23'),
(178, 72, 'doc_h_IMG-20180331-WA0017.jpg', NULL, 1, 1, '2018-03-31 17:50:04'),
(179, 72, 'doc_m_IMG_20180331_174005527_HDR.jpg', NULL, 2, 1, '2018-03-31 17:50:04'),
(180, 72, 'doc_c_IMG_20180331_174035749_HDR.jpg', NULL, 3, 1, '2018-03-31 17:50:04'),
(181, 73, 'doc_h_IMG_20180331_174005527_HDR.jpg', NULL, 1, 1, '2018-03-31 18:00:57'),
(182, 73, 'doc_m_IMG-20180331-WA0022.jpg', NULL, 2, 1, '2018-03-31 18:00:57'),
(183, 73, 'doc_c_Screenshot_20180331-161913.png', NULL, 3, 1, '2018-03-31 18:00:57'),
(184, 74, 'doc_h_IMG-20180402-WA0011.jpg', NULL, 1, 1, '2018-04-02 11:15:21'),
(185, 74, 'doc_m_IMG-20180402-WA0001.jpg', NULL, 2, 1, '2018-04-02 11:15:21'),
(186, 74, 'doc_c_IMG-20180330-WA0004.jpg', NULL, 3, 1, '2018-04-02 11:15:21'),
(187, 75, 'doc_h_IMG-20180331-WA0001.jpg', NULL, 1, 1, '2018-04-03 20:48:21'),
(188, 75, 'doc_m_IMG-20180403-WA0010.jpg', NULL, 2, 1, '2018-04-03 20:48:21'),
(189, 75, 'doc_c_IMG-20180320-WA0022.jpg', NULL, 3, 1, '2018-04-03 20:48:21'),
(190, 76, 'doc_h_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 1, 1, '2018-04-04 11:20:04'),
(191, 76, 'doc_m_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 2, 1, '2018-04-04 11:20:04'),
(192, 76, 'doc_c_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 3, 1, '2018-04-04 11:20:04'),
(193, 77, 'doc_h_IMG-20180403-WA0041.jpg', NULL, 1, 1, '2018-04-04 11:56:01'),
(194, 77, 'doc_m_IMG-20180404-WA0000.jpg', NULL, 2, 1, '2018-04-04 11:56:01'),
(195, 77, 'doc_c_IMG-20180404-WA0001.jpg', NULL, 3, 1, '2018-04-04 11:56:01'),
(196, 78, 'doc_h_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 1, 1, '2018-04-04 12:28:43'),
(197, 78, 'doc_m_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 2, 1, '2018-04-04 12:28:43'),
(198, 78, 'doc_c_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 3, 1, '2018-04-04 12:28:43'),
(199, 79, 'doc_h_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 1, 1, '2018-04-04 12:39:59'),
(200, 79, 'doc_m_Notification-OAVS-Principal-PGT-TGT-Other-Posts.pdf', NULL, 2, 1, '2018-04-04 12:39:59'),
(201, 79, 'doc_c_20180401_163717.jpg', NULL, 3, 1, '2018-04-04 12:39:59'),
(202, 80, 'doc_h_IMG-20180404-WA0004.jpg', NULL, 1, 1, '2018-04-04 16:59:05'),
(203, 80, 'doc_m_TS002_Salary_Slip_FEB_2018.pdf', NULL, 2, 1, '2018-04-04 16:59:05'),
(204, 80, 'doc_c_IMG-20180404-WA0004.jpg', NULL, 3, 1, '2018-04-04 16:59:05'),
(205, 81, 'doc_h_IMG-20180403-WA0005.jpg', NULL, 1, 1, '2018-04-04 20:37:42'),
(206, 81, 'doc_m_IMG-20180403-WA0007.jpg', NULL, 2, 1, '2018-04-04 20:37:42'),
(207, 81, 'doc_c_IMG-20180403-WA0005.jpg', NULL, 3, 1, '2018-04-04 20:37:42'),
(208, 82, 'doc_h_IMG-20180403-WA0005.jpg', NULL, 1, 1, '2018-04-04 20:39:20'),
(209, 82, 'doc_m_IMG-20180403-WA0005.jpg', NULL, 2, 1, '2018-04-04 20:39:20'),
(210, 82, 'doc_c_IMG-20180403-WA0005.jpg', NULL, 3, 1, '2018-04-04 20:39:20'),
(211, 83, 'doc_h_IMG-20180403-WA0007.jpg', NULL, 1, 1, '2018-04-04 20:42:08'),
(212, 83, 'doc_m_IMG-20180403-WA0005.jpg', NULL, 2, 1, '2018-04-04 20:42:08'),
(213, 83, 'doc_c_IMG-20180403-WA0005.jpg', NULL, 3, 1, '2018-04-04 20:42:08'),
(214, 84, 'doc_h_IMG-20180404-WA0025.jpg', NULL, 1, 1, '2018-04-04 21:07:42'),
(215, 84, 'doc_m_IMG-20180404-WA0021.jpg', NULL, 2, 1, '2018-04-04 21:07:42'),
(216, 84, 'doc_c_IMG-20180404-WA0021.jpg', NULL, 3, 1, '2018-04-04 21:07:42'),
(217, 85, 'doc_h_IMG-20180404-WA0025.jpg', NULL, 1, 1, '2018-04-04 21:09:31'),
(218, 85, 'doc_m_IMG-20180404-WA0025.jpg', NULL, 2, 1, '2018-04-04 21:09:31'),
(219, 85, 'doc_c_IMG-20180404-WA0025.jpg', NULL, 3, 1, '2018-04-04 21:09:31'),
(220, 86, 'doc_h_IMG-20180402-WA0000.jpg', NULL, 1, 1, '2018-04-05 11:06:44'),
(221, 86, 'doc_m_IMG-20180403-WA0001.jpg', NULL, 2, 1, '2018-04-05 11:06:44'),
(222, 86, 'doc_c_IMG-20180402-WA0000.jpg', NULL, 3, 1, '2018-04-05 11:06:44'),
(223, 87, 'doc_h_IMG-20180406-WA0063.jpg', NULL, 1, 1, '2018-04-06 19:34:18'),
(224, 87, 'doc_m_IMG-20180406-WA0055.jpg', NULL, 2, 1, '2018-04-06 19:34:18'),
(225, 87, 'doc_c_IMG-20180406-WA0046.jpg', NULL, 3, 1, '2018-04-06 19:34:18'),
(226, 88, 'doc_h_IMG-20180406-WA0000.jpg', NULL, 1, 1, '2018-04-06 22:06:00'),
(227, 88, 'doc_m_IMG-20180406-WA0000.jpg', NULL, 2, 1, '2018-04-06 22:06:00'),
(228, 88, 'doc_c_IMG-20180406-WA0000.jpg', NULL, 3, 1, '2018-04-06 22:06:00'),
(229, 89, 'doc_h_IMG-20180406-WA0000.jpg', NULL, 1, 1, '2018-04-06 22:08:10'),
(230, 89, 'doc_m_IMG-20180406-WA0000.jpg', NULL, 2, 1, '2018-04-06 22:08:10'),
(231, 89, 'doc_c_IMG-20180406-WA0000.jpg', NULL, 3, 1, '2018-04-06 22:08:10'),
(232, 90, 'doc_h_IMG-20180406-WA0000.jpg', NULL, 1, 1, '2018-04-06 22:13:57'),
(233, 90, 'doc_m_IMG-20180406-WA0000.jpg', NULL, 2, 1, '2018-04-06 22:13:57'),
(234, 90, 'doc_c_IMG-20180406-WA0000.jpg', NULL, 3, 1, '2018-04-06 22:13:57'),
(235, 91, 'doc_h_IMG-20180407-WA0009.jpg', NULL, 1, 1, '2018-04-07 17:07:46'),
(236, 91, 'doc_m_IMG-20180407-WA0009.jpg', NULL, 2, 1, '2018-04-07 17:07:46'),
(237, 91, 'doc_c_IMG-20180407-WA0009.jpg', NULL, 3, 1, '2018-04-07 17:07:46'),
(238, 92, 'doc_h_apple300x300.jpg', NULL, 1, 1, '2018-04-07 17:12:18'),
(239, 92, 'doc_m_bitter_melon640x640.jpg', NULL, 2, 1, '2018-04-07 17:12:18'),
(240, 92, 'doc_c_bitter_melon640x640.jpg', NULL, 3, 1, '2018-04-07 17:12:18'),
(241, 93, 'doc_h_IMG-20180406-WA0000.jpg', NULL, 1, 1, '2018-04-07 17:25:39'),
(242, 93, 'doc_m_IMG-20180407-WA0001.jpg', NULL, 2, 1, '2018-04-07 17:25:39'),
(243, 93, 'doc_c_IMG-20180407-WA0001.jpg', NULL, 3, 1, '2018-04-07 17:25:39'),
(244, 95, 'doc_h_IMG-20180407-WA0002.jpg', NULL, 1, 1, '2018-04-07 17:46:26'),
(245, 95, 'doc_m_Screenshot_2018-04-06-23-09-26.png', NULL, 2, 1, '2018-04-07 17:46:26'),
(246, 95, 'doc_c_IMG-20180407-WA0000.jpg', NULL, 3, 1, '2018-04-07 17:46:26'),
(247, 96, 'doc_h_DR_003.JPG', NULL, 1, 1, '2018-04-29 13:25:39'),
(248, 96, 'doc_m_DR_0005.1.JPG', NULL, 2, 1, '2018-04-29 13:25:39'),
(249, 96, 'doc_c_DR_003.JPG', NULL, 3, 1, '2018-04-29 13:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `sps__productimage`
--

CREATE TABLE `sps__productimage` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__productimage`
--

INSERT INTO `sps__productimage` (`id`, `product_id`, `name`, `status`, `add_date`) VALUES
(1, 1, 'carrot_400x400.jpeg', 1, '2018-02-06 12:58:41'),
(2, 1, 'carrot2.jpg', 1, '2018-02-06 12:58:41'),
(3, 1, 'carrot22.jpg', 1, '2018-02-06 12:58:41'),
(4, 1, 'carrot-500x500.jpg', 1, '2018-02-06 12:58:41'),
(5, 2, 'Grapes1.jpg', 1, '2018-02-06 13:13:06'),
(6, 2, 'Grapes2.jpg', 1, '2018-02-06 13:13:06'),
(7, 2, 'grapes-500x500.jpg', 1, '2018-02-06 13:13:06'),
(11, 4, 'green_beans-800x800.jpg', 1, '2018-02-06 15:07:03'),
(9, 3, 'mango.jpg', 1, '2018-02-06 13:29:48'),
(10, 3, 'mangoes.jpg', 1, '2018-02-06 13:29:48'),
(12, 4, 'greenbean2s.jpg', 1, '2018-02-06 15:07:03'),
(13, 5, 'yellow_peeper1.jpg', 1, '2018-02-06 16:22:46'),
(14, 5, 'Yellow_pepper.JPG', 1, '2018-02-06 16:22:46'),
(15, 5, 'yellowpepper2.jpg', 1, '2018-02-06 16:22:46'),
(16, 6, 'Strawberry.jpg', 1, '2018-02-06 16:24:56'),
(17, 6, 'Strawberry1.jpg', 1, '2018-02-06 16:24:56'),
(18, 7, 'blackberry2.jpg', 1, '2018-02-06 16:36:22'),
(19, 7, 'blackberry3.jpg', 1, '2018-02-06 16:36:22'),
(20, 7, 'blackberry250x250.jpg', 1, '2018-02-06 16:36:22'),
(21, 8, 'red-capsicum.jpg', 1, '2018-02-06 16:42:51'),
(22, 8, 'Redpeeper1.jpg', 1, '2018-02-06 16:42:51'),
(23, 8, 'Redpepper_540x540.jpg', 1, '2018-02-06 16:42:51'),
(24, 9, 'Watermelon1.jpg', 1, '2018-02-06 16:48:14'),
(25, 9, 'Watermelon2.png', 1, '2018-02-06 16:48:14'),
(26, 10, 'bitter_melon640x640.jpg', 1, '2018-02-06 16:54:34'),
(27, 10, 'Bitter-melon1.jpg', 1, '2018-02-06 16:54:34'),
(28, 11, 'Barbados_Cherry.jpg', 1, '2018-02-06 16:57:00'),
(29, 11, 'Barbados1.jpg', 1, '2018-02-06 16:57:00'),
(30, 12, 'Broccoli.jpg', 1, '2018-02-06 18:12:59'),
(31, 12, 'broccoli1.jpeg', 1, '2018-02-06 18:12:59'),
(32, 12, 'Broccoli2.jpg', 1, '2018-02-06 18:12:59'),
(33, 13, 'litchi.jpg', 1, '2018-02-06 18:20:28'),
(34, 13, 'litchi1.jpg', 1, '2018-02-06 18:20:28'),
(41, 25, 'pomegranate1.jpg', 1, '2018-02-08 14:53:16'),
(37, 15, 'orange.jpg', 1, '2018-02-07 13:03:50'),
(42, 25, 'pomegranate4.jpg', 1, '2018-02-08 14:53:16'),
(39, 18, 'applw0.jpg', 1, '2018-02-07 15:32:25'),
(43, 25, 'pomegranates-500x500.jpg', 1, '2018-02-08 14:53:16'),
(44, 26, 'Orange.jpg', 1, '2018-02-08 14:56:47'),
(45, 26, 'orange2_square-300x300.jpg', 1, '2018-02-08 14:56:47'),
(46, 27, 'greenbanana2-500x500.jpg', 1, '2018-02-08 15:00:35'),
(47, 27, 'Green-Banana-300x300.jpg', 1, '2018-02-08 15:00:35'),
(48, 28, 'apple300x300.jpg', 1, '2018-02-08 15:02:47'),
(49, 29, 'frozen-green-peas.png', 1, '2018-02-08 15:06:46'),
(50, 29, 'green-peas-228x228.jpg', 1, '2018-02-08 15:06:46'),
(51, 29, 'peas3.jpg', 1, '2018-02-08 15:06:46'),
(53, 33, 'salmonfillets.jpg', 1, '2018-02-10 15:36:30'),
(56, 38, 'download.jpg', 1, '2018-02-10 16:25:58'),
(57, 39, 'download.jpg', 1, '2018-02-10 16:35:26'),
(58, 41, 'smallapple.jpg', 1, '2018-03-01 10:29:35'),
(59, 44, 'greenbanana2-500x500.jpg', 1, '2018-03-01 10:49:20'),
(60, 44, 'Green-Banana-300x300.jpg', 1, '2018-03-01 10:49:20'),
(61, 43, 'carrot_400x400.jpeg', 1, '2018-03-01 10:50:39'),
(62, 43, 'carrot2.jpg', 1, '2018-03-01 10:50:39'),
(63, 43, 'carrot22.jpg', 1, '2018-03-01 10:50:39'),
(64, 43, 'carrot-500x500.jpg', 1, '2018-03-01 10:50:39'),
(65, 47, 'smallapple.jpg', 1, '2018-03-01 10:51:34'),
(66, 45, 'blackberry2.jpg', 1, '2018-03-01 10:52:00'),
(67, 45, 'blackberry3.jpg', 1, '2018-03-01 10:52:00'),
(68, 45, 'blackberry250x250.jpg', 1, '2018-03-01 10:52:00'),
(69, 40, 'mango.jpg', 1, '2018-03-01 10:53:13'),
(70, 40, 'mangoes.jpg', 1, '2018-03-01 10:53:13'),
(71, 48, 'Strawberry.jpg', 1, '2018-03-01 10:55:05'),
(72, 48, 'Strawberry1.jpg', 1, '2018-03-01 10:55:05'),
(73, 49, 'mozzarellaCheese.jpg', 1, '2018-03-01 11:41:33'),
(74, 42, 'apple.jpg', 1, '2018-03-01 11:42:12'),
(75, 50, 'brusselsprouts.jpg', 1, '2018-03-01 11:44:48'),
(76, 51, 'sausage.jpg', 1, '2018-03-01 12:38:16'),
(88, 52, 'apple300x300.jpg', 1, '2018-03-14 19:06:50'),
(78, 53, 'smallapple.jpg', 1, '2018-03-05 19:21:27'),
(79, 54, 'Orange.jpg', 1, '2018-03-06 16:31:41'),
(80, 54, 'orange2_square-300x300.jpg', 1, '2018-03-06 16:31:41'),
(92, 62, 'Broccoli.jpg', 1, '2018-03-23 11:34:28'),
(82, 56, 'mango.jpg', 1, '2018-03-07 12:51:33'),
(83, 56, 'mangoes.jpg', 1, '2018-03-07 12:51:33'),
(84, 57, 'mango.jpg', 1, '2018-03-08 19:57:56'),
(85, 57, 'mangoes.jpg', 1, '2018-03-08 19:57:56'),
(86, 58, 'mango.jpg', 1, '2018-03-10 17:31:01'),
(87, 58, 'mangoes.jpg', 1, '2018-03-10 17:31:01'),
(89, 60, 'apple0.jpg', 1, '2018-03-17 15:57:00'),
(90, 61, 'test0.jpg', 1, '2018-03-17 16:02:22'),
(91, 61, 'test1.jpg', 1, '2018-03-17 16:02:22'),
(93, 62, 'broccoli1.jpeg', 1, '2018-03-23 11:34:28'),
(94, 62, 'Broccoli2.jpg', 1, '2018-03-23 11:34:28'),
(95, 63, 'Broccoli.jpg', 1, '2018-03-23 13:32:33'),
(96, 63, 'broccoli1.jpeg', 1, '2018-03-23 13:32:33'),
(97, 63, 'Broccoli2.jpg', 1, '2018-03-23 13:32:33'),
(99, 64, 'Chrysanthemum.jpg', 1, '2018-03-27 16:58:23'),
(100, 64, 'Desert.jpg', 1, '2018-03-27 16:58:23'),
(101, 64, 'Hydrangeas.jpg', 1, '2018-03-27 16:58:23'),
(103, 65, 'Hydrangeas.jpg', 1, '2018-03-29 12:05:14'),
(104, 66, 'Desert.jpg', 1, '2018-03-29 18:45:24'),
(105, 67, 'mangoes.jpg', 1, '2018-03-29 19:31:26'),
(106, 69, 'prawn0.jpg', 1, '2018-03-31 17:34:56'),
(107, 69, 'prawn1.jpg', 1, '2018-03-31 17:34:56'),
(108, 69, 'prawn2.jpg', 1, '2018-03-31 17:34:56'),
(109, 70, 'kisan jam0.jpg', 1, '2018-03-31 17:38:33'),
(110, 70, 'kisan jam1.jpg', 1, '2018-03-31 17:38:33'),
(111, 71, 'kisan fruti0.jpg', 1, '2018-03-31 17:42:23'),
(112, 71, 'kisan fruti1.jpg', 1, '2018-03-31 17:42:23'),
(113, 71, 'kisan fruti2.jpg', 1, '2018-03-31 17:42:23'),
(114, 73, 'kisan tamato0.jpg', 1, '2018-03-31 18:00:57'),
(115, 73, 'kisan tamato1.jpg', 1, '2018-03-31 18:00:57'),
(116, 74, 'aoo0.jpg', 1, '2018-04-02 11:15:21'),
(117, 74, 'aoo1.jpg', 1, '2018-04-02 11:15:21'),
(118, 75, 'test0.jpg', 1, '2018-04-03 20:48:21'),
(119, 75, 'test1.jpg', 1, '2018-04-03 20:48:21'),
(120, 76, 'chicken 0.jpg', 1, '2018-04-04 11:20:04'),
(121, 77, 'test0.jpg', 1, '2018-04-04 11:56:01'),
(122, 77, 'test1.jpg', 1, '2018-04-04 11:56:01'),
(123, 78, 'yellowfruit0.jpg', 1, '2018-04-04 12:28:43'),
(124, 79, 'fruitm0.jpg', 1, '2018-04-04 12:39:59'),
(125, 80, 'rrr0.jpg', 1, '2018-04-04 16:59:05'),
(126, 80, 'rrr1.jpg', 1, '2018-04-04 16:59:05'),
(127, 83, 'test0.jpg', 1, '2018-04-04 20:42:08'),
(128, 85, 'hxhdu0.jpg', 1, '2018-04-04 21:09:31'),
(129, 86, 'mangotest0.jpg', 1, '2018-04-05 11:06:44'),
(130, 87, 'sabya0.jpg', 1, '2018-04-06 19:34:18'),
(131, 87, 'sabya1.jpg', 1, '2018-04-06 19:34:18'),
(132, 89, 'patato0.jpg', 1, '2018-04-06 22:08:10'),
(133, 93, 'mangotestkisan0.jpg', 1, '2018-04-07 17:25:39'),
(134, 94, 'mangotestkisan0.jpg', 1, '2018-04-07 17:25:56'),
(135, 95, 'kisanmango0.jpg', 1, '2018-04-07 17:46:26'),
(136, 96, 'DR_0004.1.JPG', 1, '2018-04-29 13:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_bid`
--

CREATE TABLE `sps__product_bid` (
  `id` int(11) NOT NULL,
  `product_id` int(111) NOT NULL,
  `user_id` int(111) NOT NULL COMMENT 'buyerId',
  `is_full` tinyint(1) DEFAULT '0' COMMENT '1->Full 0->Parcial',
  `quantity` int(5) NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` smallint(1) DEFAULT '0' COMMENT '0->Pending,1->Accept,2->Reject,3->Re-auction',
  `settlement_status` tinyint(1) DEFAULT '0' COMMENT 'after bid win status 1->done 2->cancel',
  `ordernumber` int(12) DEFAULT NULL,
  `request_quantity` int(10) DEFAULT '0',
  `add_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_bid`
--

INSERT INTO `sps__product_bid` (`id`, `product_id`, `user_id`, `is_full`, `quantity`, `price`, `status`, `settlement_status`, `ordernumber`, `request_quantity`, `add_date`) VALUES
(1, 4, 82, 0, 1, 60.50, 3, 0, 8241692, 0, '2018-02-06 15:11:29'),
(2, 4, 58, 0, 1, 60.50, 1, 0, 5843001, 0, '2018-02-06 15:12:52'),
(3, 4, 26, 1, 992, 32.00, 1, 0, 2645795, 1000, '2018-02-06 15:36:52'),
(4, 4, 82, 0, 7, 33.00, 1, 0, 8249317, 0, '2018-02-06 15:38:13'),
(5, 1, 82, 0, 40, 20.50, 1, 0, 8216844, 0, '2018-02-06 15:41:51'),
(6, 1, 58, 0, 40, 20.50, 1, 0, 5818710, 0, '2018-02-06 15:42:25'),
(7, 1, 117, 0, 20, 20.50, 1, 0, 11714206, 40, '2018-02-06 15:43:58'),
(8, 7, 59, 0, 1, 500.00, 3, 0, 5973218, 0, '2018-02-06 19:46:02'),
(9, 8, 58, 0, 50, 85.50, 3, 0, 5886250, 0, '2018-02-08 15:47:04'),
(10, 8, 58, 0, 25, 85.50, 1, 0, 5887540, 50, '2018-02-08 15:50:36'),
(11, 13, 26, 1, 1000, 78.00, 3, 0, 26135806, 0, '2018-02-08 19:24:25'),
(12, 8, 26, 1, 300, 85.00, 0, 0, 2681853, 0, '2018-02-08 19:28:17'),
(13, 28, 26, 0, 1, 95.50, 3, 0, 26286377, 0, '2018-02-09 12:43:24'),
(14, 28, 26, 0, 100, 95.50, 3, 0, 26289098, 0, '2018-02-09 12:43:45'),
(15, 28, 59, 0, 1, 95.50, 0, 0, 59287937, 0, '2018-02-12 11:07:02'),
(16, 8, 82, 0, 150, 86.00, 3, 0, 8285187, 0, '2018-02-12 18:02:11'),
(17, 13, 117, 0, 500, 79.00, 3, 0, 117137408, 0, '2018-02-12 18:58:07'),
(18, 13, 84, 0, 99, 79.00, 1, 0, 84137634, 500, '2018-02-12 18:58:29'),
(19, 13, 82, 0, 150, 79.50, 3, 0, 82139092, 0, '2018-02-12 18:59:19'),
(20, 13, 82, 1, 956, 78.50, 3, 0, 82138112, 0, '2018-02-12 18:59:47'),
(21, 13, 117, 0, 200, 79.50, 3, 0, 117135936, 0, '2018-02-12 19:01:15'),
(22, 13, 117, 0, 710, 79.50, 3, 0, 117133471, 0, '2018-02-12 19:01:44'),
(23, 13, 79, 0, 500, 80.00, 1, 0, 79136060, 0, '2018-02-12 19:15:37'),
(24, 13, 87, 0, 200, 80.50, 1, 0, 87137062, 0, '2018-02-12 19:16:43'),
(25, 13, 82, 0, 200, 81.00, 1, 0, 82133326, 0, '2018-02-12 19:17:49'),
(26, 8, 82, 0, 1, 200.00, 3, 0, 8284678, 0, '2018-02-21 12:33:12'),
(27, 8, 82, 0, 1, 86.00, 3, 0, 8286229, 0, '2018-02-21 12:33:48'),
(28, 8, 82, 0, 150, 220.00, 3, 0, 8287438, 0, '2018-02-21 12:34:24'),
(29, 8, 82, 0, 1, 86.00, 3, 0, 8284773, 0, '2018-02-21 12:34:47'),
(30, 8, 82, 0, 250, 86.00, 1, 0, 8288063, 0, '2018-02-21 12:40:28'),
(31, 13, 117, 0, 1, 81.50, 1, 0, 117134110, 0, '2018-02-21 15:12:41'),
(32, 9, 26, 0, 15, 25.50, 1, 0, 2698536, 0, '2018-02-26 15:12:55'),
(33, 10, 26, 0, 50, 45.50, 3, 0, 26104544, 0, '2018-02-26 15:13:29'),
(34, 10, 82, 0, 6, 45.50, 1, 1, 82101086, 0, '2018-02-27 11:22:00'),
(35, 7, 59, 0, 2, 180.50, 3, 0, 5971137, 0, '2018-02-27 17:23:08'),
(36, 7, 82, 0, 1, 180.50, 3, 0, 8271086, 0, '2018-02-28 18:22:41'),
(37, 7, 59, 0, 1, 180.50, 3, 0, 5976950, 0, '2018-03-01 10:41:01'),
(38, 7, 59, 0, 300, 180.50, 3, 0, 5975444, 0, '2018-03-01 10:41:53'),
(39, 7, 82, 0, 250, 180.50, 2, 0, 8276673, 0, '2018-03-01 10:42:52'),
(40, 7, 59, 1, 500, 180.00, 0, 0, 5976261, 0, '2018-03-01 10:53:47'),
(41, 13, 82, 0, 900, 79.00, 0, 0, 82137672, 0, '2018-03-01 12:27:06'),
(42, 51, 84, 0, 10, 61.50, 2, 0, 84519658, 0, '2018-03-01 12:51:37'),
(43, 51, 82, 1, 239, 62.00, 3, 0, 82516229, 0, '2018-03-01 12:52:38'),
(44, 51, 84, 0, 10, 63.00, 3, 0, 84518950, 0, '2018-03-01 12:53:20'),
(45, 51, 82, 1, 239, 62.50, 0, 0, 82513091, 0, '2018-03-01 12:53:50'),
(46, 51, 84, 0, 220, 63.50, 1, 1, 84511480, 0, '2018-03-01 13:00:27'),
(47, 51, 84, 0, 10, 63.50, 1, 0, 84519589, 0, '2018-03-01 13:03:49'),
(48, 51, 128, 0, 1, 65.50, 1, 0, 128511777, 0, '2018-03-06 11:49:11'),
(49, 13, 129, 0, 2, 79.00, 3, 0, 129138637, 0, '2018-03-06 15:33:41'),
(50, 54, 129, 0, 1, 65.50, 3, 0, 129546766, 0, '2018-03-06 20:32:21'),
(51, 54, 129, 0, 3, 65.50, 1, 1, 129541328, 0, '2018-03-07 13:20:38'),
(52, 10, 26, 0, 1, 45.50, 3, 0, 26105206, 0, '2018-03-07 18:02:52'),
(53, 9, 26, 0, 1, 25.50, 0, 0, 2699307, 0, '2018-03-07 19:05:45'),
(54, 13, 129, 0, 1, 79.00, 0, 0, 129137033, 0, '2018-03-08 13:54:18'),
(55, 13, 87, 0, 1, 79.00, 0, 0, 87137596, 0, '2018-03-08 20:53:59'),
(56, 51, 129, 0, 1, 63.50, 1, 0, 129519540, 0, '2018-03-10 11:24:44'),
(57, 13, 141, 0, 1, 79.00, 0, 0, 141131701, 0, '2018-03-12 17:37:50'),
(58, 54, 141, 0, 1, 65.50, 3, 0, 141545808, 0, '2018-03-12 17:42:00'),
(59, 58, 141, 0, 1, 60.50, 1, 1, 141586873, 0, '2018-03-12 17:53:08'),
(60, 13, 81, 0, 5, 79.00, 0, 0, 81138021, 0, '2018-03-13 11:23:19'),
(61, 13, 1, 0, 1, 79.00, 0, 0, 1131149, 0, '2018-03-13 18:08:07'),
(62, 28, 26, 0, 1, 95.50, 3, 0, 26287718, 0, '2018-03-14 12:54:52'),
(63, 28, 26, 0, 2, 95.50, 3, 0, 26282610, 0, '2018-03-14 12:55:14'),
(64, 13, 26, 0, 1, 79.00, 0, 0, 26133861, 0, '2018-03-14 13:09:44'),
(65, 54, 141, 1, 1000, 65.00, 3, 0, 141542857, 0, '2018-03-15 14:50:22'),
(66, 54, 141, 1, 1000, 65.50, 3, 0, 141546502, 0, '2018-03-15 14:53:19'),
(67, 54, 141, 1, 1000, 66.00, 3, 0, 141548863, 0, '2018-03-15 14:56:59'),
(68, 54, 141, 1, 1000, 66.50, 3, 0, 141546864, 0, '2018-03-15 15:06:20'),
(69, 54, 141, 0, 1, 68.00, 3, 0, 141542159, 0, '2018-03-15 15:07:56'),
(70, 54, 141, 0, 1, 65.50, 3, 0, 141547748, 0, '2018-03-15 15:13:29'),
(71, 54, 141, 0, 2, 65.50, 3, 0, 141544841, 0, '2018-03-15 15:15:08'),
(72, 51, 141, 0, 1, 64.00, 1, 0, 141512964, 0, '2018-03-15 15:17:00'),
(73, 54, 141, 1, 1000, 65.00, 3, 0, 141541578, 0, '2018-03-15 15:18:24'),
(74, 54, 141, 1, 1, 65.50, 0, 0, 141544178, 0, '2018-03-15 15:18:34'),
(75, 13, 140, 0, 1, 75.50, 3, 0, 140134459, 0, '2018-03-15 17:12:57'),
(76, 7, 140, 0, 1, 181.00, 3, 0, 14073606, 0, '2018-03-15 17:14:23'),
(77, 7, 140, 0, 1, 185.00, 3, 0, 14078092, 0, '2018-03-15 17:15:50'),
(78, 13, 140, 0, 1, 75.50, 3, 0, 140134643, 0, '2018-03-15 18:27:57'),
(79, 13, 140, 0, 1, 75.50, 3, 0, 140136102, 0, '2018-03-15 18:28:14'),
(80, 13, 140, 0, 1, 75.50, 0, 0, 140136434, 0, '2018-03-15 18:29:25'),
(81, 12, 140, 0, 1, 65.50, 0, 0, 140128489, 0, '2018-03-15 18:44:57'),
(82, 58, 26, 0, 58, 60.50, 1, 0, 26587053, 0, '2018-03-16 12:20:19'),
(83, 7, 26, 0, 2, 181.00, 3, 0, 2673254, 0, '2018-03-16 13:31:08'),
(84, 7, 26, 0, 4, 183.00, 3, 0, 2675948, 0, '2018-03-16 13:43:33'),
(85, 7, 26, 0, 1, 181.00, 2, 0, 2672939, 0, '2018-03-16 13:55:32'),
(86, 7, 140, 0, 1, 181.00, 2, 0, 14077639, 0, '2018-03-16 18:23:54'),
(87, 63, 141, 0, 1, 35.50, 1, 0, 141633258, 0, '2018-03-23 18:32:15'),
(88, 63, 141, 0, 2, 35.50, 3, 0, 141635040, 0, '2018-03-23 18:38:08'),
(89, 63, 141, 0, 5, 35.50, 1, 0, 141633275, 0, '2018-03-23 18:44:58'),
(90, 7, 141, 0, 1, 181.00, 3, 0, 14174405, 0, '2018-03-26 17:47:23'),
(91, 7, 141, 0, 2, 181.00, 3, 0, 14174645, 0, '2018-03-26 17:47:39'),
(92, 7, 141, 0, 1, 183.00, 3, 0, 14175621, 0, '2018-03-26 17:47:56'),
(93, 7, 141, 1, 500, 180.50, 3, 0, 14174111, 0, '2018-03-26 17:48:07'),
(94, 7, 141, 0, 1, 181.50, 3, 0, 14174168, 0, '2018-03-26 17:48:22'),
(95, 7, 141, 1, 500, 185.00, 3, 0, 14177944, 0, '2018-03-26 18:00:38'),
(96, 7, 141, 0, 1, 186.00, 0, 0, 14174261, 0, '2018-03-26 18:00:50'),
(97, 63, 26, 1, 994, 35.00, 3, 0, 26632196, 0, '2018-03-29 19:12:08'),
(98, 29, 26, 1, 100, 80.00, 1, 0, 26293953, 0, '2018-03-29 19:24:05'),
(99, 58, 26, 0, 20, 61.50, 3, 0, 26583794, 0, '2018-03-29 19:38:20'),
(100, 54, 26, 0, 50, 66.50, 3, 0, 26544576, 0, '2018-03-29 19:39:58'),
(101, 54, 151, 0, 500, 66.50, 0, 0, 151546953, 0, '2018-03-29 19:41:06'),
(102, 54, 26, 0, 30, 66.50, 3, 0, 26542168, 0, '2018-03-29 19:41:36'),
(103, 10, 151, 0, 200, 45.50, 1, 0, 151107586, 0, '2018-03-29 19:42:50'),
(104, 10, 26, 0, 10, 45.50, 1, 1, 26104533, 0, '2018-03-29 19:43:11'),
(105, 10, 151, 0, 100, 45.50, 1, 1, 151104648, 0, '2018-03-29 19:47:07'),
(106, 10, 26, 0, 20, 45.50, 1, 1, 26107691, 0, '2018-03-29 19:47:14'),
(107, 10, 84, 0, 100, 45.50, 3, 0, 84103737, 0, '2018-03-29 19:54:31'),
(108, 10, 84, 0, 100, 45.50, 3, 0, 84103559, 0, '2018-03-29 19:55:34'),
(109, 10, 84, 0, 500, 45.50, 0, 0, 84106725, 0, '2018-03-29 19:56:00'),
(110, 58, 151, 0, 1, 61.50, 3, 0, 151587094, 0, '2018-03-29 20:34:02'),
(111, 58, 151, 1, 1000, 61.00, 3, 0, 151589740, 0, '2018-03-29 20:35:11'),
(112, 54, 26, 0, 1, 66.50, 3, 0, 26542779, 0, '2018-03-30 13:40:23'),
(113, 67, 141, 0, 1, 5.50, 1, 1, 141677853, 0, '2018-03-30 19:03:53'),
(114, 67, 26, 0, 1, 5.50, 3, 0, 26676578, 0, '2018-03-31 17:04:40'),
(115, 67, 26, 0, 3, 6.00, 0, 0, 26676897, 0, '2018-03-31 17:05:05'),
(116, 73, 26, 0, 4, 11.50, 1, 0, 26739755, 0, '2018-03-31 18:07:32'),
(117, 66, 26, 0, 0, 18.50, 3, 0, 26663603, 0, '2018-04-02 10:42:22'),
(118, 66, 26, 0, 1, 18.50, 1, 0, 26668161, 0, '2018-04-03 20:40:03'),
(119, 1, 155, 0, 1, 20.50, 3, 0, 15514936, 0, '2018-04-04 15:04:57'),
(120, 1, 155, 0, 1, 23.00, 3, 0, 15513714, 0, '2018-04-04 15:05:21'),
(121, 1, 155, 0, 1, 19933.00, 3, 0, 15519924, 0, '2018-04-04 15:05:47'),
(122, 1, 26, 0, 1, 20.50, 0, 0, 2613344, 0, '2018-04-04 16:17:47'),
(123, 4, 155, 0, 1, 30.50, 0, 0, 15545386, 0, '2018-04-04 16:33:25'),
(124, 1, 155, 0, 1, 20.50, 3, 0, 15519571, 0, '2018-04-04 16:40:35'),
(125, 1, 155, 1, 100, 20.00, 3, 0, 15516935, 0, '2018-04-04 16:42:30'),
(126, 58, 151, 0, 1, 62.00, 3, 0, 151584651, 0, '2018-04-04 17:02:17'),
(127, 58, 151, 1, 1000, 61.00, 3, 0, 151588139, 0, '2018-04-04 17:54:07'),
(128, 67, 141, 0, 1, 5.50, 1, 0, 141673325, 0, '2018-04-05 12:01:36'),
(129, 63, 140, 0, 1, 36.00, 0, 0, 140631410, 0, '2018-04-06 21:25:43'),
(130, 58, 26, 0, 1, 62.00, 1, 0, 26582307, 0, '2018-04-06 21:39:57'),
(131, 67, 84, 1, 198, 5.00, 1, 0, 84672893, 0, '2018-04-06 21:44:18'),
(132, 73, 84, 0, 1, 11.50, 1, 0, 84732760, 0, '2018-04-06 21:49:09'),
(133, 4, 151, 0, 1, 30.50, 0, 0, 15144630, 0, '2018-04-06 22:51:30'),
(134, 63, 151, 0, 1, 36.00, 3, 0, 151637069, 0, '2018-04-06 22:52:05'),
(135, 9, 151, 0, 1, 25.50, 0, 0, 15194442, 0, '2018-04-06 22:54:06'),
(136, 7, 151, 0, 1, 181.00, 0, 0, 15176178, 0, '2018-04-06 22:54:16'),
(137, 1, 151, 0, 1, 21.00, 0, 0, 15118384, 0, '2018-04-06 22:54:45'),
(138, 12, 151, 0, 1, 65.50, 3, 0, 151128254, 0, '2018-04-06 22:55:00'),
(139, 12, 151, 0, 1, 65.50, 0, 0, 151127822, 0, '2018-04-06 22:55:13'),
(140, 63, 151, 0, 1, 36.00, 3, 0, 151638679, 0, '2018-04-07 11:19:12'),
(141, 58, 151, 0, 1, 62.00, 3, 0, 151585557, 0, '2018-04-07 11:38:38'),
(142, 63, 151, 0, 1, 36.50, 3, 0, 151637406, 0, '2018-04-07 11:44:56'),
(143, 63, 151, 0, 1, 37.00, 0, 0, 151632545, 0, '2018-04-07 11:46:06'),
(144, 58, 151, 0, 1, 62.50, 3, 0, 151581072, 0, '2018-04-07 11:48:06'),
(145, 63, 26, 0, 1, 37.00, 3, 0, 26636465, 0, '2018-04-07 13:39:05'),
(146, 63, 26, 0, 993, 37.50, 3, 0, 26635440, 0, '2018-04-07 13:42:23'),
(147, 63, 26, 0, 1, 38.00, 3, 0, 26636192, 0, '2018-04-07 13:42:39'),
(148, 63, 26, 0, 1, 38.00, 0, 0, 26636220, 0, '2018-04-07 13:42:50'),
(149, 58, 151, 0, 1, 63.00, 0, 0, 151587116, 0, '2018-04-07 15:16:38'),
(150, 66, 156, 0, 2, 18.50, 1, 0, 156663350, 0, '2018-04-07 16:05:37'),
(151, 66, 156, 0, 1, 18.50, 1, 0, 156667955, 0, '2018-04-07 16:20:56'),
(152, 66, 156, 0, 1, 18.50, 0, 0, 156661380, 0, '2018-04-07 16:48:08'),
(153, 1, 155, 0, 1, 21.50, 0, 0, 15511445, 0, '2018-04-07 17:13:57'),
(154, 73, 141, 0, 1, 15.50, 1, 0, 141734010, 0, '2018-04-07 18:26:33'),
(155, 28, 26, 0, 10, 95.50, 0, 0, 26284112, 0, '2018-04-29 13:04:30'),
(156, 54, 26, 0, 4, 66.50, 0, 0, 26545357, 0, '2018-04-29 13:07:15'),
(157, 66, 26, 0, 2, 18.50, 0, 0, 26664428, 0, '2018-07-30 20:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_buyer_wishprice`
--

CREATE TABLE `sps__product_buyer_wishprice` (
  `id` int(123) NOT NULL,
  `product_id` int(12) DEFAULT NULL,
  `buyer_id` int(12) DEFAULT NULL,
  `seller_id` int(12) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `wish_price` float(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0->pending 1->accept 2->reject',
  `ordernumber` int(13) DEFAULT NULL,
  `settlement_status` tinyint(1) DEFAULT '0' COMMENT 'if settlement done 1 else 0',
  `add_date` datetime DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_buyer_wishprice`
--

INSERT INTO `sps__product_buyer_wishprice` (`id`, `product_id`, `buyer_id`, `seller_id`, `quantity`, `wish_price`, `status`, `ordernumber`, `settlement_status`, `add_date`, `approve_date`) VALUES
(1, 3, 26, 59, 10, 40.00, 0, NULL, 0, '2018-03-22 19:40:48', '2018-03-22 19:40:48'),
(2, 3, 141, 59, 10, 20.00, 0, NULL, 0, '2018-03-23 11:28:00', '2018-03-23 11:28:00'),
(3, 62, 141, 147, 10, 20.00, 2, 141622627, 0, '2018-03-23 13:01:38', '2018-03-23 13:12:28'),
(4, 62, 141, 147, 10, 20.00, 1, 141621381, 0, '2018-03-23 13:16:45', '2018-03-23 13:18:02'),
(5, 63, 141, 147, 2, 10.00, 0, NULL, 0, '2018-03-23 15:49:47', '2018-03-23 15:49:47'),
(6, 27, 26, 58, 5, 10.00, 0, NULL, 0, '2018-03-26 17:20:43', '2018-03-26 17:20:43'),
(7, 25, 26, 59, 10, 50.00, 0, NULL, 0, '2018-03-26 20:43:12', '2018-03-26 20:43:12'),
(8, 2, 147, 59, 1, 1.00, 0, NULL, 0, '2018-03-27 12:38:14', '2018-03-27 12:38:14'),
(9, 2, 26, 59, 10, 60.00, 0, NULL, 0, '2018-03-29 12:18:27', '2018-03-29 12:18:27'),
(10, 27, 141, 58, 1, 1.00, 0, NULL, 0, '2018-03-29 19:01:01', '2018-03-29 19:01:01'),
(11, 27, 141, 58, 1, 2.00, 0, NULL, 0, '2018-03-29 19:01:30', '2018-03-29 19:01:30'),
(12, 2, 84, 59, 12, 60.00, 0, NULL, 0, '2018-03-29 19:18:41', '2018-03-29 19:18:41'),
(13, 56, 26, 133, 10, 40.00, 0, NULL, 0, '2018-03-30 14:22:03', '2018-03-30 14:22:03'),
(14, 56, 26, 133, 100, 42.00, 0, NULL, 0, '2018-03-30 14:28:09', '2018-03-30 14:28:09'),
(15, 56, 151, 133, 12, 12.00, 0, NULL, 0, '2018-03-30 15:57:58', '2018-03-30 15:57:58'),
(16, 56, 151, 133, 23, 23.00, 0, NULL, 0, '2018-03-30 16:36:12', '2018-03-30 16:36:12'),
(17, 56, 26, 133, 20, 23.00, 0, NULL, 0, '2018-03-30 17:06:09', '2018-03-30 17:06:09'),
(18, 56, 26, 133, 12, 12.00, 0, NULL, 0, '2018-03-30 17:07:28', '2018-03-30 17:07:28'),
(19, 56, 26, 133, 23, 23.00, 0, NULL, 0, '2018-03-30 17:08:31', '2018-03-30 17:08:31'),
(20, 56, 151, 133, 23, 23.00, 0, NULL, 0, '2018-03-30 17:09:44', '2018-03-30 17:09:44'),
(21, 66, 141, 151, 1, 1.00, 1, 141669734, 0, '2018-03-30 18:37:03', '2018-04-07 12:10:34'),
(22, 71, 26, 151, 2, 1.00, 1, 26714301, 1, '2018-04-02 11:55:43', '2018-04-02 15:08:55'),
(23, 71, 26, 151, 1, 1.00, 2, 26711605, 0, '2018-04-02 15:09:36', '2018-04-02 15:09:52'),
(24, 71, 26, 151, 1, 1.00, 1, 26715508, 0, '2018-04-02 15:10:18', '2018-04-02 15:10:45'),
(25, 25, 26, 59, 5, 55.00, 0, NULL, 0, '2018-04-03 20:49:15', '2018-04-03 20:49:15'),
(26, 56, 155, 133, 2, 45.00, 0, NULL, 0, '2018-04-04 13:16:29', '2018-04-04 13:16:29'),
(27, 56, 26, 133, 8, 5.00, 0, NULL, 0, '2018-04-04 19:56:06', '2018-04-04 19:56:06'),
(28, 2, 26, 59, 5, 2.00, 0, NULL, 0, '2018-04-04 20:01:54', '2018-04-04 20:01:54'),
(29, 25, 26, 59, 2, 5.00, 0, NULL, 0, '2018-04-04 21:13:31', '2018-04-04 21:13:31'),
(30, 71, 141, 151, 2, 10.00, 1, 141712453, 1, '2018-04-05 11:40:52', '2018-04-05 11:42:58'),
(31, 71, 141, 151, 1, 1.00, 2, 141718344, 0, '2018-04-05 11:49:10', '2018-04-05 11:49:31'),
(32, 27, 26, 58, 5, 55.00, 0, NULL, 0, '2018-04-06 21:56:28', '2018-04-06 21:56:28'),
(33, 71, 26, 151, 1, 1.00, 1, 26717587, 0, '2018-04-07 12:11:27', '2018-04-07 12:12:14'),
(34, 71, 26, 151, 1, 7.00, 2, 26713432, 0, '2018-04-07 12:13:07', '2018-04-07 12:13:27'),
(35, 71, 26, 151, 1, 1.00, 2, 26713956, 0, '2018-04-07 12:14:21', '2018-04-07 12:15:19'),
(36, 71, 26, 151, 1, 1.00, 1, 26714619, 0, '2018-04-07 12:16:25', '2018-04-07 12:16:36'),
(37, 71, 26, 151, 2, 2.00, 2, 26712209, 0, '2018-04-07 12:17:22', '2018-04-07 12:17:58'),
(38, 71, 156, 151, 1, 20.00, 1, 156719625, 0, '2018-04-07 16:28:38', '2018-04-07 16:29:44'),
(39, 71, 156, 151, 1, 1.00, 1, 156717128, 0, '2018-04-07 16:31:13', '2018-04-07 16:31:45'),
(40, 71, 156, 151, 1, 1.00, 2, 156716860, 0, '2018-04-07 16:32:13', '2018-04-07 16:32:31'),
(41, 71, 156, 151, 1, 1.00, 2, 156718010, 0, '2018-04-07 16:32:56', '2018-04-07 16:33:05'),
(42, 71, 156, 151, 1, 1.00, 2, 156713147, 0, '2018-04-07 16:33:25', '2018-04-07 16:33:33'),
(43, 71, 156, 151, 1, 1.00, 1, 156717082, 0, '2018-04-07 16:33:50', '2018-04-07 16:33:58'),
(44, 71, 156, 151, 1, 1.00, 2, 156719311, 0, '2018-04-07 16:39:46', '2018-04-07 16:40:02'),
(45, 25, 156, 59, 2, 68.00, 0, NULL, 0, '2018-04-07 17:08:18', '2018-04-07 17:08:18'),
(46, 25, 155, 59, 20, 68.00, 0, NULL, 0, '2018-04-07 17:15:40', '2018-04-07 17:15:40'),
(47, 71, 141, 151, 1, 1.00, 1, 141711186, 1, '2018-04-07 18:21:40', '2018-04-07 18:23:41'),
(48, 71, 141, 151, 1, 1.00, 2, 141712055, 0, '2018-04-07 18:24:53', '2018-04-07 18:25:03'),
(49, 25, 58, 59, 50, 65.00, 0, NULL, 0, '2018-04-29 12:18:01', '2018-04-29 12:18:01'),
(50, 3, 1, 59, 5, 5.00, 0, NULL, 0, '2018-05-04 13:25:47', '2018-05-04 13:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_exchange_price`
--

CREATE TABLE `sps__product_exchange_price` (
  `id` int(11) NOT NULL,
  `seller_id` int(15) DEFAULT NULL,
  `product_id` int(12) NOT NULL,
  `cvm_used` int(12) NOT NULL,
  `price_per_day` float(10,2) NOT NULL COMMENT 'price per unit per day ',
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `comments` text,
  `add_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_exchange_price`
--

INSERT INTO `sps__product_exchange_price` (`id`, `seller_id`, `product_id`, `cvm_used`, `price_per_day`, `start_date`, `end_date`, `comments`, `add_date`) VALUES
(1, 59, 1, 1, 2.00, '2018-02-06', NULL, '', '2018-02-06 13:04:05'),
(2, 59, 4, 3, 2.00, '2018-02-06', NULL, '', '2018-02-06 15:10:20'),
(3, 58, 6, 3, 2.00, '2018-02-06', NULL, '', '2018-02-06 16:26:37'),
(4, 58, 5, 3, 2.00, '2018-01-06', '2018-01-08', '', '2018-01-06 16:27:41'),
(5, 58, 7, 3, 2.00, '2018-02-06', NULL, '', '2018-02-06 17:00:13'),
(6, 59, 8, 5, 2.00, '2018-02-06', NULL, '', '2018-02-06 17:00:33'),
(7, 59, 9, 5, 2.00, '2018-02-06', NULL, '', '2018-02-06 17:00:49'),
(8, 59, 10, 5, 2.00, '2018-02-06', NULL, '', '2018-02-06 17:01:13'),
(9, 59, 11, 6, 2.00, '2018-02-06', NULL, '', '2018-02-06 17:01:30'),
(10, 59, 13, 5, 2.00, '2018-02-06', NULL, '', '2018-02-06 18:21:14'),
(11, 58, 5, 1, 2.00, '2018-01-08', NULL, 'cbm used updated after product delivered', '2018-01-08 11:54:22'),
(12, 58, 29, 2, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:09:54'),
(13, 58, 28, 5, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:10:11'),
(14, 58, 27, 5, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:10:28'),
(15, 58, 26, 6, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:10:43'),
(16, 59, 25, 5, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:10:58'),
(17, 59, 12, 6, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:11:40'),
(18, 59, 3, 2, 2.00, '2018-01-08', '2018-01-09', 'cbm used updated after product delivered', '2018-01-08 15:13:01'),
(19, 59, 2, 1, 2.00, '2018-02-08', NULL, '', '2018-02-08 15:13:47'),
(20, 59, 3, 2, 2.00, '2018-01-09', NULL, 'cbm used updated after product delivered', '2018-01-09 11:13:24'),
(21, 26, 51, 10, 2.00, '2018-03-01', NULL, '', '2018-03-01 07:12:08'),
(22, 133, 54, 0, 2.00, '2018-03-06', NULL, '', '2018-03-06 14:48:29'),
(23, 133, 56, 1, 2.00, '2018-03-07', NULL, '', '2018-03-07 07:32:43'),
(24, 140, 58, 1, 2.00, '2018-03-10', NULL, '', '2018-03-10 12:12:34'),
(25, 147, 62, 0, 2.00, '2018-03-23', NULL, '', '2018-03-23 06:34:30'),
(26, 147, 63, 0, 2.00, '2018-03-23', NULL, '', '2018-03-23 10:06:07'),
(27, 148, 64, 0, 2.00, '2018-03-27', NULL, '', '2018-03-27 12:01:35'),
(28, 151, 66, 20, 2.00, '2018-03-29', NULL, 'store no 23 a,b', '2018-03-29 13:23:11'),
(29, 151, 67, 0, 2.00, '2018-03-29', NULL, '', '2018-03-29 14:02:38'),
(30, 151, 71, 0, 2.00, '2018-03-31', NULL, '', '2018-03-31 12:31:01'),
(31, 151, 73, 0, 2.00, '2018-03-31', NULL, '', '2018-03-31 12:31:25'),
(32, 151, 70, 0, 2.00, '2018-04-05', NULL, '', '2018-04-05 06:40:17'),
(33, 26, 96, 0, 2.00, '2018-04-29', NULL, '', '2018-04-29 07:59:20');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_exchange_price_bc`
--

CREATE TABLE `sps__product_exchange_price_bc` (
  `id` int(11) NOT NULL,
  `seller_id` int(15) DEFAULT NULL,
  `product_id` int(12) NOT NULL,
  `cvm_used` int(12) NOT NULL,
  `price_per_day` float(10,2) NOT NULL COMMENT 'price per unit per day ',
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `comments` text,
  `add_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_exchange_price_bc`
--

INSERT INTO `sps__product_exchange_price_bc` (`id`, `seller_id`, `product_id`, `cvm_used`, `price_per_day`, `start_date`, `end_date`, `comments`, `add_date`) VALUES
(1, NULL, 19, 1, 2.00, '2018-01-02', NULL, 'test', '2018-01-02 15:29:15'),
(2, NULL, 18, 2, 2.00, '2018-01-02', NULL, 'test1', '2018-01-02 15:30:01'),
(3, NULL, 17, 1, 2.00, '2018-01-02', NULL, 'test2', '2018-01-02 15:30:35'),
(4, NULL, 16, 1, 2.00, '2018-01-02', NULL, 'test3', '2018-01-02 15:31:03'),
(5, NULL, 15, 2, 2.00, '2018-01-02', NULL, 'test4', '2018-01-02 15:31:28'),
(6, NULL, 14, 1, 2.00, '2018-01-02', NULL, 'test5', '2018-01-02 15:31:56'),
(7, NULL, 13, 1, 2.00, '2018-01-02', NULL, 'test6', '2018-01-02 15:32:31'),
(8, NULL, 12, 1, 2.00, '2018-01-02', NULL, 'test7', '2018-01-02 15:32:53'),
(9, NULL, 11, 1, 2.00, '2018-01-02', NULL, 'test8', '2018-01-02 15:58:32'),
(10, NULL, 10, 1, 2.00, '2018-01-02', NULL, 'test9', '2018-01-02 15:58:56'),
(11, NULL, 9, 1, 2.00, '2018-01-02', NULL, 'test10', '2018-01-02 15:59:19'),
(12, NULL, 8, 1, 2.00, '2018-01-02', NULL, 'test11', '2018-01-02 15:59:40'),
(13, NULL, 7, 1, 2.00, '2018-01-02', NULL, 'test12', '2018-01-02 16:00:07'),
(14, NULL, 5, 1, 2.00, '2018-01-02', NULL, 'test13', '2018-01-02 16:00:32'),
(15, NULL, 4, 1, 2.00, '2018-01-02', NULL, 'test14', '2018-01-02 16:00:57'),
(16, NULL, 3, 1, 2.00, '2018-01-02', NULL, 'test15', '2018-01-02 16:01:19'),
(17, NULL, 2, 1, 2.00, '2018-01-02', NULL, 'teat16', '2018-01-02 16:01:52'),
(18, NULL, 1, 1, 2.00, '2018-01-02', NULL, 'test17', '2018-01-02 16:02:17'),
(19, NULL, 35, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:18:06'),
(20, NULL, 36, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:19:30'),
(21, NULL, 37, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:19:59'),
(22, NULL, 38, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:20:25'),
(23, NULL, 39, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:20:43'),
(24, NULL, 40, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:20:58'),
(25, NULL, 41, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:21:17'),
(26, NULL, 42, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:21:34'),
(27, NULL, 52, 2, 2.00, '2018-01-19', NULL, 'store', '2018-01-19 19:24:37'),
(28, NULL, 51, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:25:01'),
(29, NULL, 50, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:25:50'),
(30, NULL, 49, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:26:07'),
(31, NULL, 48, 0, 2.00, '2018-01-19', NULL, '', '2018-01-19 19:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_fixed_sell`
--

CREATE TABLE `sps__product_fixed_sell` (
  `id` int(200) NOT NULL,
  `buyer_id` int(100) NOT NULL,
  `seller_id` int(123) DEFAULT NULL,
  `product_id` int(100) NOT NULL,
  `selling_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->fixed 2->auction',
  `ordernumber` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `total_goods_cost` float(10,2) DEFAULT '0.00',
  `vat` float(10,2) DEFAULT NULL,
  `transport_cost` float(10,2) DEFAULT '0.00',
  `total_price` float(10,2) DEFAULT NULL,
  `total_price_vat` float(20,2) DEFAULT NULL,
  `offline_flg` tinyint(4) DEFAULT NULL COMMENT '1->Online,2->Offline',
  `transaction_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0->pending,1->Payment success,2->payment rejected',
  `transaction_id` varchar(100) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `is_delivered` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0->pending,1->Delivered,2->Canceled,3->Returned',
  `deliver_date` datetime DEFAULT NULL,
  `delivery_note_file` varchar(222) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL COMMENT 'both cancel date and return date ',
  `comment` text,
  `cheque` varchar(222) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `add_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_fixed_sell`
--

INSERT INTO `sps__product_fixed_sell` (`id`, `buyer_id`, `seller_id`, `product_id`, `selling_type`, `ordernumber`, `quantity`, `total_goods_cost`, `vat`, `transport_cost`, `total_price`, `total_price_vat`, `offline_flg`, `transaction_flg`, `transaction_id`, `transaction_date`, `is_delivered`, `deliver_date`, `delivery_note_file`, `cancel_date`, `comment`, `cheque`, `status`, `add_date`) VALUES
(1, 59, 58, 5, 1, 5959162, 10, 680.00, 5.00, 35.00, 715.00, 750.75, 2, 1, '9', '2018-02-07 00:00:00', 2, NULL, NULL, '2018-02-07 17:13:22', NULL, 'orange.jpg', 0, '2018-02-06 18:40:59'),
(2, 26, 58, 5, 1, 2653620, 10, 680.00, 5.00, 27.50, 707.50, 742.88, 2, 1, '10', '2018-01-08 00:00:00', 1, '2018-01-08 11:54:22', NULL, NULL, NULL, 'cucumbers.jpg', 0, '2018-01-08 11:45:30'),
(3, 58, 59, 3, 1, 5836612, 6, 270.00, 5.00, 0.00, 270.00, 283.50, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:05', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-08 15:48:39'),
(4, 58, 59, 3, 1, 5831822, 10, 450.00, 5.00, 0.00, 450.00, 472.50, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:06', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-08 15:51:01'),
(5, 82, 58, 6, 1, 8269159, 5, 975.00, 5.00, 0.00, 975.00, 1023.75, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:08', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-08 15:58:20'),
(6, 84, 59, 3, 1, 8436733, 5, 225.00, 5.00, 0.00, 225.00, 236.25, 2, 2, '14', '2018-02-10 00:00:00', 0, NULL, NULL, '2018-02-23 14:50:09', 'Order Cancelled due to payment delay.', 'Untitled7.png', 0, '2018-02-09 10:50:02'),
(7, 117, 59, 3, 1, 11737741, 10, 450.00, 5.00, 0.00, 450.00, 472.50, 2, 1, '11', '2018-02-09 00:00:00', 0, NULL, NULL, NULL, NULL, 'Untitled12.png', 0, '2018-02-09 10:50:29'),
(8, 82, 59, 3, 1, 8232184, 7, 315.00, 5.00, 0.00, 315.00, 330.75, 2, 1, '12', '2018-01-09 00:00:00', 1, '2018-01-09 11:13:24', NULL, NULL, NULL, 'Untitled1.png', 0, '2018-01-09 10:50:51'),
(10, 26, 58, 5, 1, 2655134, 10, 680.00, 5.00, 27.50, 707.50, 742.88, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:10', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-09 18:48:17'),
(11, 26, 58, 5, 1, 2652188, 10, 680.00, 5.00, 27.50, 707.50, 742.88, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:11', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-09 18:55:07'),
(12, 26, 58, 5, 1, 2653563, 10, 680.00, 5.00, 27.50, 707.50, 742.88, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:12', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-09 18:55:41'),
(13, 26, 58, 5, 1, 2658808, 3, 204.00, 5.00, 6.00, 210.00, 220.50, 2, 0, '39', '2018-03-16 00:00:00', 0, NULL, NULL, '2018-02-23 14:50:13', 'Order Cancelled due to payment delay.', 'IMG-20180316-WA0006.jpg', 0, '2018-02-09 20:08:02'),
(14, 26, 58, 5, 1, 2658671, 3, 204.00, 5.00, 6.00, 210.00, 220.50, 2, 2, '37', '2018-03-14 00:00:00', 0, NULL, NULL, '2018-02-23 14:50:15', 'Order Cancelled due to payment delay.', 'Bitter-melon1.jpg', 0, '2018-02-09 20:11:06'),
(15, 26, 59, 3, 1, 2631404, 4, 180.00, 5.00, 12.00, 192.00, 201.60, 2, 1, '36', '2018-03-14 00:00:00', 0, NULL, NULL, '2018-02-23 14:50:16', 'Order Cancelled due to payment delay.', 'Bitter-melon1.jpg', 0, '2018-02-09 20:16:25'),
(16, 82, 59, 13, 1, 82139799, 44, 3300.00, 5.00, 132.00, 3432.00, 3603.60, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:17', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-10 18:47:32'),
(17, 59, 58, 27, 1, 59277491, 5, 290.00, 5.00, 0.00, 290.00, 304.50, 2, 0, NULL, NULL, 0, NULL, NULL, '2018-02-23 14:50:18', 'Order Cancelled due to payment delay.', NULL, 0, '2018-02-12 11:32:18'),
(18, 82, 59, 8, 1, 8289931, 25, 2125.00, 5.00, 75.00, 2200.00, 2310.00, 2, 1, '15', '2018-02-21 00:00:00', 2, NULL, NULL, '2018-02-21 17:06:14', NULL, 'Desert.jpg', 0, '2018-02-13 11:29:56'),
(19, 82, 58, 5, 1, 8252728, 2, 136.00, 5.00, 0.00, 136.00, 142.80, 2, 0, NULL, NULL, 2, NULL, NULL, '2018-02-21 17:04:16', NULL, NULL, 0, '2018-02-21 11:47:56'),
(20, 82, 58, 5, 1, 8254858, 2, 136.00, 5.00, 0.00, 136.00, 142.80, 2, 0, NULL, NULL, 2, NULL, NULL, '2018-02-21 17:09:34', NULL, NULL, 0, '2018-02-21 17:08:55'),
(21, 82, 58, 5, 1, 8259918, 2, 136.00, 5.00, 0.00, 136.00, 142.80, 2, 1, '17', '2018-02-21 00:00:00', 2, NULL, NULL, '2018-02-21 17:24:52', NULL, 'Tulips.jpg', 0, '2018-02-21 17:10:51'),
(22, 82, 58, 5, 1, 8259896, 1, 68.00, 5.00, 1.50, 69.50, 72.97, 2, 1, '18', '2018-02-21 00:00:00', 2, NULL, NULL, '2018-02-21 17:31:37', NULL, 'Tulips.jpg', 0, '2018-02-21 17:28:46'),
(23, 82, 58, 5, 1, 8252566, 1, 68.00, 5.00, 1.50, 69.50, 72.97, 2, 1, '19', '2018-02-21 00:00:00', 3, '2018-02-21 17:33:47', NULL, '2018-02-21 17:35:53', NULL, 'Tulips.jpg', 0, '2018-02-21 17:32:40'),
(24, 82, 58, 5, 1, 8254447, 3, 204.00, 5.00, 0.00, 204.00, 214.20, 2, 0, NULL, NULL, 2, NULL, NULL, '2018-02-26 17:41:27', NULL, NULL, 0, '2018-02-26 17:40:25'),
(25, 82, 58, 5, 1, 8258087, 2, 136.00, 5.00, 0.00, 136.00, 142.80, 2, 1, '21', '2018-02-26 00:00:00', 2, NULL, NULL, '2018-02-26 18:42:31', NULL, 'Tulips.jpg', 0, '2018-02-26 18:34:15'),
(26, 82, 58, 5, 1, 8259773, 2, 136.00, 5.00, 0.00, 136.00, 142.80, 2, 1, '23', '2018-02-26 00:00:00', 3, '2018-02-26 18:58:59', NULL, '2018-02-27 17:04:11', NULL, 'Tulips.jpg', 0, '2018-02-26 18:43:52'),
(27, 82, 59, 10, 2, 82101086, 6, 273.00, 5.00, 0.00, 273.00, 286.65, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-02-27 17:12:46'),
(28, 59, 58, 6, 1, 5968363, 20, 3900.00, 5.00, 70.00, 3970.00, 4168.50, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-01 10:53:22'),
(29, 82, 58, 5, 1, 8254582, 90, 6120.00, 5.00, 15.00, 6135.00, 6441.75, 2, 1, '25', '2018-03-01 00:00:00', 1, '2018-03-01 12:15:04', NULL, NULL, NULL, 'apple1.jpg', 0, '2018-03-01 11:40:31'),
(30, 84, 26, 51, 2, 84511480, 220, 13970.00, 5.00, 0.00, 13970.00, 14668.50, 2, 1, '26', '2018-03-01 00:00:00', 1, '2018-03-01 13:12:14', NULL, NULL, NULL, 'Koala.jpg', 0, '2018-03-01 13:07:40'),
(31, 127, 58, 5, 1, 12753902, 1, 68.00, 5.00, 2.50, 70.50, 74.03, 2, 1, '30', '2018-03-08 00:00:00', 1, '2018-03-08 16:33:41', NULL, NULL, NULL, 'apple300x300.jpg', 0, '2018-03-05 19:25:22'),
(32, 129, 58, 6, 1, 12965297, 2, 390.00, 5.00, 0.00, 390.00, 409.50, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-06 13:33:05'),
(33, 130, 58, 5, 1, 13055799, 1, 68.00, 5.00, NULL, 68.00, 71.40, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-06 14:13:47'),
(34, 131, 58, 5, 1, 13153051, 1, 68.00, 5.00, NULL, 68.00, 71.40, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-06 14:23:59'),
(35, 132, 58, 6, 1, 13263654, 1, 195.00, 5.00, NULL, 195.00, 204.75, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-06 15:29:53'),
(36, 129, 58, 6, 1, 12962202, 1, 195.00, 5.00, 0.00, 195.00, 204.75, 2, 1, '29', '2018-03-07 00:00:00', 3, '2018-03-07 17:59:04', NULL, '2018-03-07 18:00:28', NULL, 'blackberry3.jpg', 0, '2018-03-06 20:37:59'),
(37, 129, 133, 56, 1, 129563501, 2, 90.00, 5.00, 0.00, 90.00, 94.50, 2, 1, '28', '2018-03-07 00:00:00', 2, NULL, NULL, '2018-03-07 15:29:31', NULL, 'blackberry250x250.jpg', 0, '2018-03-07 13:12:15'),
(38, 134, 58, 6, 1, 13464277, 1, 195.00, 5.00, NULL, 195.00, 204.75, 2, 0, NULL, NULL, 2, NULL, NULL, '2018-03-08 17:13:18', NULL, NULL, 0, '2018-03-07 15:40:46'),
(39, 129, 133, 54, 2, 129541328, 3, 196.50, 5.00, 0.00, 196.50, 206.32, 2, 1, '31', '2018-03-10 00:00:00', 3, '2018-03-10 13:01:36', NULL, '2018-03-10 13:02:23', NULL, 'apple300x300.jpg', 0, '2018-03-10 13:00:26'),
(40, 139, 59, 2, 1, 13921551, 1, 65.00, 5.00, NULL, 65.00, 68.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-10 16:42:46'),
(41, 129, 59, 3, 1, 12933846, 5, 225.00, 5.00, 0.00, 225.00, 236.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-10 16:53:14'),
(42, 142, 58, 26, 1, 142266742, 5, 290.00, 5.00, NULL, 290.00, 304.50, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-10 17:18:18'),
(43, 141, 59, 3, 1, 14132029, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 2, '40', '2018-03-16 00:00:00', 0, NULL, NULL, NULL, NULL, 'IMG_20180316_002032429_HDR.jpg', 0, '2018-03-12 14:57:11'),
(44, 141, 140, 58, 1, 141588503, 1, 65.00, 5.00, 0.00, 65.00, 68.25, 2, 1, '33', '2018-03-12 00:00:00', 1, '2018-03-12 19:12:04', NULL, NULL, NULL, 'bitter_melon640x640.jpg', 0, '2018-03-12 17:49:26'),
(45, 141, 140, 58, 2, 141586873, 1, 60.50, 5.00, 0.00, 60.50, 63.52, 2, 1, '32', '2018-03-12 00:00:00', 3, '2018-03-12 19:05:45', NULL, '2018-03-17 18:32:30', NULL, 'apple300x300.jpg', 0, '2018-03-12 18:52:39'),
(46, 143, 59, 3, 1, 14332408, 1, 45.00, 5.00, NULL, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-12 19:45:12'),
(47, 144, 59, 2, 1, 14426825, 3, 195.00, 5.00, NULL, 195.00, 204.75, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-13 11:20:13'),
(48, 26, 59, 2, 1, 2622246, 3, 195.00, 5.00, 8.50, 203.50, 213.68, 2, 1, '35', '2018-03-14 00:00:00', 0, NULL, NULL, NULL, NULL, 'bitter_melon640x640.jpg', 0, '2018-03-13 11:37:24'),
(49, 145, 59, 2, 1, 14524731, 1, 65.00, 5.00, NULL, 65.00, 68.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-13 13:39:56'),
(50, 146, 59, 3, 1, 14639935, 1, 45.00, 5.00, NULL, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-13 15:58:22'),
(51, 26, 59, 3, 1, 2639329, 1, 45.00, 5.00, 3.00, 48.00, 50.40, 2, 1, '34', '2018-03-14 00:00:00', 3, '2018-03-17 18:34:39', '2639329_blackberry2.jpg', '2018-04-02 14:55:35', NULL, '8259918_Lighthouse.jpg', 0, '2018-03-14 11:16:00'),
(52, 140, 59, 3, 1, 14033448, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-15 18:37:22'),
(53, 140, 59, 2, 1, 14024429, 1, 65.00, 5.00, 0.00, 65.00, 68.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-15 18:42:47'),
(54, 140, 59, 3, 1, 14031442, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-15 18:45:47'),
(55, 140, 59, 3, 1, 14035727, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 0, '41', '2018-03-21 00:00:00', 0, NULL, NULL, NULL, NULL, 'blackberry2.jpg', 0, '2018-03-21 14:47:58'),
(56, 141, 147, 63, 1, 141632193, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 1, '43', '2018-03-23 00:00:00', 3, '2018-03-23 18:21:48', '141632193_blackberry2.jpg', '2018-03-23 18:23:57', NULL, 'blackberry3.jpg', 0, '2018-03-23 15:57:30'),
(57, 141, 59, 2, 1, 14128838, 5, 325.00, 5.00, 0.00, 325.00, 341.25, 2, 1, '44', '2018-03-26 00:00:00', 0, NULL, NULL, NULL, NULL, 'blackberry2.jpg', 0, '2018-03-26 11:03:53'),
(58, 147, 59, 3, 1, 14734832, 1, 45.00, 5.00, 0.00, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-27 12:37:05'),
(59, 147, 148, 64, 1, 147646852, 1, 5.00, 5.00, 0.00, 5.00, 5.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-27 19:58:56'),
(60, 26, 151, 66, 1, 26669304, 20, 400.00, 5.00, 17.50, 417.50, 438.38, 2, 1, '45', '2018-03-29 00:00:00', 0, NULL, NULL, NULL, NULL, 'Koala.jpg', 0, '2018-03-29 19:02:08'),
(61, 141, 151, 67, 2, 141677853, 1, 5.50, 5.00, 0.00, 5.50, 5.78, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-30 19:14:46'),
(62, 26, 59, 25, 1, 26257629, 1, 68.00, 5.00, 0.00, 68.00, 71.40, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-02 00:25:09'),
(63, 152, 58, 27, 1, 152275524, 1, 58.00, 5.00, NULL, 58.00, 60.90, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-02 01:04:16'),
(64, 26, 59, 10, 2, 26107691, 20, 910.00, 5.00, 35.00, 945.00, 992.25, 2, 1, '46', '2018-04-02 00:00:00', 2, NULL, NULL, '2018-04-02 14:19:24', NULL, 'carrot_400x400.jpeg', 0, '2018-04-02 13:22:40'),
(65, 153, 133, 56, 1, 153567542, 1, 45.00, 5.00, NULL, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-02 13:30:34'),
(66, 154, 133, 56, 1, 154562488, 1, 45.00, 5.00, NULL, 45.00, 47.25, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-02 13:38:56'),
(67, 26, 151, 71, 3, 26714301, 2, 2.00, 5.00, 3.50, 5.50, 5.78, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-02 15:19:10'),
(68, 26, 59, 25, 1, 26255420, 5, 340.00, 5.00, 17.50, 357.50, 375.38, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-03 20:49:36'),
(69, 151, 59, 2, 1, 15126283, 2, 130.00, 5.00, 5.50, 135.50, 142.28, 2, 1, '53', '2018-04-06 00:00:00', 1, '2018-04-07 16:43:18', '15126283_deliverynote.pdf', '2018-04-07 16:43:18', NULL, 'IMG-20180406-WA0003.jpg', 0, '2018-04-04 18:00:59'),
(70, 26, 59, 25, 1, 26259597, 1, 68.00, 5.00, 3.50, 71.50, 75.08, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-04 21:13:54'),
(71, 26, 59, 2, 1, 2629386, 2, 130.00, 5.00, 7.00, 137.00, 143.85, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-04 21:14:38'),
(72, 26, 133, 56, 1, 26565085, 1, 45.00, 5.00, 3.50, 48.50, 50.92, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-04 21:17:06'),
(73, 141, 133, 56, 1, 141565735, 2, 90.00, 5.00, 1.50, 91.50, 96.08, 2, 1, '47', '2018-04-05 00:00:00', 1, '2018-04-05 11:33:14', '141565735_deliverynote_(5).pdf', '2018-04-05 11:33:14', NULL, 'Broccoli2.jpg', 0, '2018-04-05 11:17:53'),
(74, 141, 151, 71, 3, 141712453, 2, 20.00, 5.00, 1.50, 21.50, 22.57, 2, 1, '48', '2018-04-05 00:00:00', 2, NULL, NULL, '2018-04-05 12:06:05', NULL, 'bitter_melon640x640.jpg', 0, '2018-04-05 11:45:35'),
(75, 26, 59, 10, 2, 26104533, 10, 455.00, 5.00, 35.00, 490.00, 514.50, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-05 12:48:33'),
(76, 141, 151, 71, 1, 141719973, 1, 30.00, 5.00, 0.00, 30.00, 31.50, 2, 1, '49', '2018-04-05 00:00:00', 2, NULL, NULL, '2018-04-05 15:53:13', NULL, 'Bitter-melon1.jpg', 0, '2018-04-05 15:51:57'),
(77, 141, 151, 71, 1, 141715736, 1, 30.00, 5.00, 0.00, 30.00, 31.50, 2, 1, '50', '2018-04-05 00:00:00', 1, '2018-04-05 15:58:13', '141715736_deliverynote.pdf', '2018-04-05 15:58:13', NULL, 'carrot_400x400.jpeg', 0, '2018-04-05 15:57:05'),
(78, 141, 133, 56, 1, 141569261, 1, 45.00, 5.00, 1.50, 46.50, 48.83, 2, 1, '51', '2018-04-05 00:00:00', 0, NULL, NULL, NULL, NULL, 'carrot_400x400.jpeg', 0, '2018-04-05 16:38:30'),
(79, 141, 151, 71, 1, 141719287, 1, 30.00, 5.00, 0.00, 30.00, 31.50, 2, 1, '52', '2018-04-05 00:00:00', 1, '2018-04-05 16:41:47', '141719287_deliverynote.pdf', '2018-04-05 16:41:47', NULL, 'carrot2.jpg', 0, '2018-04-05 16:40:56'),
(80, 26, 59, 2, 1, 2625850, 45, 2925.00, 5.00, 67.50, 2992.50, 3142.12, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 19:39:04'),
(81, 140, 133, 56, 1, 140569881, 2, 90.00, 5.00, 0.00, 90.00, 94.50, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:26:49'),
(82, 26, 133, 56, 1, 26561400, 2, 90.00, 5.00, 7.00, 97.00, 101.85, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:28:16'),
(83, 26, 133, 56, 1, 26564800, 2, 90.00, 5.00, 7.00, 97.00, 101.85, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:35:39'),
(84, 26, 59, 25, 1, 26257479, 8, 544.00, 5.00, 28.00, 572.00, 600.60, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:36:53'),
(85, 26, 59, 25, 1, 26251559, 8, 544.00, 5.00, 28.00, 572.00, 600.60, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:37:03'),
(86, 26, 58, 27, 1, 26275111, 5, 290.00, 5.00, 17.50, 307.50, 322.88, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 21:56:51'),
(87, 151, 59, 10, 2, 151104648, 100, 4550.00, 5.00, 350.00, 4900.00, 5145.00, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 22:41:23'),
(88, 151, 133, 56, 1, 151568485, 2, 90.00, 5.00, 7.00, 97.00, 101.85, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 22:50:52'),
(89, 151, 59, 2, 1, 15122677, 2, 130.00, 5.00, 7.00, 137.00, 143.85, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-07 12:22:55'),
(90, 26, 59, 25, 1, 26258273, 100, 6800.00, 5.00, 212.50, 7012.50, 7363.12, 2, 1, '54', '2018-04-07 00:00:00', 0, NULL, NULL, NULL, NULL, 'IMG-20180407-WA0042.jpg', 0, '2018-04-07 19:17:43'),
(91, 141, 151, 71, 3, 141711186, 1, 1.00, 5.00, 0.00, 1.00, 1.05, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-04-11 20:15:14'),
(92, 58, 59, 25, 1, 58256423, 60, 4080.00, 5.00, 90.00, 4170.00, 4378.50, 2, 1, '55', '2018-04-29 00:00:00', 0, NULL, NULL, NULL, NULL, 'DR_0001.2.JPG', 0, '2018-04-29 12:33:07'),
(93, 58, 59, 3, 1, 5833317, 60, 2700.00, 5.00, 180.00, 2880.00, 3024.00, 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '2018-05-04 13:34:54'),
(94, 26, 59, 3, 1, 2638216, 2, 90.00, 5.00, 7.00, 97.00, 101.85, 2, 0, '56', '2018-05-09 00:00:00', 0, NULL, NULL, NULL, NULL, 'DR_0001.1.JPG', 0, '2018-05-09 16:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `sps__product_verifytrack`
--

CREATE TABLE `sps__product_verifytrack` (
  `id` int(222) NOT NULL,
  `product_id` int(222) DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL,
  `description` text,
  `add_date` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__product_verifytrack`
--

INSERT INTO `sps__product_verifytrack` (`id`, `product_id`, `status`, `description`, `add_date`, `added_by`) VALUES
(1, 1, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 12:58:38', 59),
(2, 2, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 13:13:03', 59),
(3, 3, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 13:29:44', 59),
(4, 4, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 15:06:56', 59),
(5, 5, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:22:42', 58),
(6, 6, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:24:54', 58),
(7, 7, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:36:19', 58),
(8, 8, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:42:48', 59),
(9, 9, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:48:11', 59),
(10, 10, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:54:31', 59),
(11, 11, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 16:56:57', 59),
(12, 12, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 18:12:56', 59),
(13, 13, 1, 'One goods request send to warehouse for primary verification.', '2018-02-06 18:20:24', 59),
(14, 14, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 12:07:56', 59),
(15, 15, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 13:03:47', 87),
(16, 16, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 15:12:05', 1),
(17, 17, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 15:15:34', 59),
(18, 18, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 15:32:25', 87),
(19, 19, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 15:39:17', 1),
(20, 20, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 16:04:05', 26),
(21, 21, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 16:05:35', 26),
(22, 22, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 16:26:55', 26),
(23, 23, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 16:31:56', 59),
(24, 24, 1, 'One goods request send to warehouse for primary verification.', '2018-02-07 16:47:26', 59),
(25, 25, 1, 'One goods request send to warehouse for primary verification.', '2018-02-08 14:53:10', 59),
(26, 26, 1, 'One goods request send to warehouse for primary verification.', '2018-02-08 14:56:45', 58),
(27, 27, 1, 'One goods request send to warehouse for primary verification.', '2018-02-08 15:00:32', 58),
(28, 28, 1, 'One goods request send to warehouse for primary verification.', '2018-02-08 15:02:40', 58),
(29, 29, 1, 'One goods request send to warehouse for primary verification.', '2018-02-08 15:06:42', 58),
(30, 30, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:02:13', 58),
(31, 31, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:33:00', 117),
(32, 32, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:33:25', 117),
(33, 33, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:34:51', 58),
(34, 34, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:37:41', 58),
(35, 35, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 15:57:40', 117),
(36, 36, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 16:07:05', 117),
(37, 37, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 16:10:17', 117),
(38, 38, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 16:25:55', 85),
(39, 39, 1, 'One goods request send to warehouse for primary verification.', '2018-02-10 16:35:23', 118),
(40, 40, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:20:42', 59),
(41, 41, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:23:11', 59),
(42, 42, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:33:00', 26),
(43, 43, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:33:24', 59),
(44, 44, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:36:13', 59),
(45, 45, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:42:21', 59),
(46, 46, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:45:33', 26),
(47, 47, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:51:34', 59),
(48, 48, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:55:05', 59),
(49, 49, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 10:55:46', 26),
(50, 50, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 11:44:48', 26),
(51, 51, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 12:38:16', 26),
(52, 51, 4, 'ok seems good', '2018-03-01 12:42:12', 1),
(53, 52, 1, 'One goods request send to warehouse for primary verification.', '2018-03-01 16:07:24', 26),
(54, 53, 1, 'One goods request send to warehouse for primary verification.', '2018-03-05 19:21:23', 127),
(55, 54, 1, 'One goods request send to warehouse for primary verification.', '2018-03-06 16:31:37', 133),
(56, 55, 1, 'One goods request send to warehouse for primary verification.', '2018-03-06 20:28:31', 133),
(57, 55, 5, '2345', '2018-03-06 20:29:18', 1),
(58, 56, 1, 'One goods request send to warehouse for primary verification.', '2018-03-07 12:51:29', 133),
(59, 56, 3, 'Please give proper document with proper product', '2018-03-07 12:58:59', 1),
(60, 56, 2, 'Please give proper document with proper product', '2018-03-07 13:00:16', 1),
(61, 56, 5, 'Please give proper document with proper product please please', '2018-03-07 13:01:08', 1),
(62, 56, 4, 'Good product', '2018-03-07 13:02:47', 1),
(63, 57, 1, 'One goods request send to warehouse for primary verification.', '2018-03-08 19:57:52', 137),
(64, 58, 1, 'One goods request send to warehouse for primary verification.', '2018-03-10 17:30:53', 140),
(65, 58, 3, 'Not upload Bank details', '2018-03-10 17:33:37', 1),
(66, 58, 2, 'Not upload Bank details', '2018-03-10 17:35:44', 1),
(67, 58, 5, 'Not upload Bank details please upload', '2018-03-10 17:36:12', 1),
(68, 59, 1, 'One goods request send to warehouse for primary verification.', '2018-03-17 15:44:37', 26),
(69, 60, 1, 'One goods request send to warehouse for primary verification.', '2018-03-17 15:57:00', 26),
(70, 61, 1, 'One goods request send to warehouse for primary verification.', '2018-03-17 16:02:22', 26),
(71, 53, 3, 'dfgd', '2018-03-17 18:40:30', 1),
(72, 62, 1, 'One goods request send to warehouse for primary verification.', '2018-03-23 11:34:23', 147),
(73, 62, 3, '', '2018-03-23 11:44:08', 1),
(74, 62, 2, 'Upload Bank details', '2018-03-23 11:49:18', 1),
(75, 62, 5, 'Upload Bank details please ohterwise you unable to sell', '2018-03-23 11:49:48', 1),
(76, 63, 1, 'One goods request send to warehouse for primary verification.', '2018-03-23 13:32:29', 147),
(77, 57, 3, 'not upload', '2018-03-27 11:50:25', 1),
(78, 64, 1, 'One goods request send to warehouse for primary verification.', '2018-03-27 15:26:02', 148),
(79, 65, 1, 'One goods request send to warehouse for primary verification.', '2018-03-29 12:05:09', 59),
(80, 65, 5, 'bad product', '2018-03-29 12:12:29', 1),
(81, 66, 1, 'One goods request send to warehouse for primary verification.', '2018-03-29 18:45:20', 151),
(82, 67, 1, 'One goods request send to warehouse for primary verification.', '2018-03-29 19:31:22', 151),
(83, 68, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 17:29:48', 151),
(84, 69, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 17:34:56', 151),
(85, 70, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 17:38:33', 151),
(86, 71, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 17:42:23', 151),
(87, 72, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 17:50:04', 151),
(88, 73, 1, 'One goods request send to warehouse for primary verification.', '2018-03-31 18:00:57', 151),
(89, 74, 1, 'One goods request send to warehouse for primary verification.', '2018-04-02 11:15:21', 59),
(90, 75, 1, 'One goods request send to warehouse for primary verification.', '2018-04-03 20:48:21', 26),
(91, 76, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 11:20:04', 150),
(92, 77, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 11:56:01', 26),
(93, 78, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 12:28:43', 155),
(94, 79, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 12:39:59', 155),
(95, 80, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 16:59:05', 84),
(96, 81, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 20:37:42', 26),
(97, 82, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 20:39:20', 26),
(98, 83, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 20:42:08', 26),
(99, 84, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 21:07:42', 26),
(100, 85, 1, 'One goods request send to warehouse for primary verification.', '2018-04-04 21:09:31', 26),
(101, 86, 1, 'One goods request send to warehouse for primary verification.', '2018-04-05 11:06:44', 155),
(102, 87, 1, 'One goods request send to warehouse for primary verification.', '2018-04-06 19:34:18', 26),
(103, 88, 1, 'One goods request send to warehouse for primary verification.', '2018-04-06 22:06:00', 151),
(104, 89, 1, 'One goods request send to warehouse for primary verification.', '2018-04-06 22:08:10', 151),
(105, 90, 1, 'One goods request send to warehouse for primary verification.', '2018-04-06 22:13:57', 151),
(106, 91, 1, 'One goods request send to warehouse for primary verification.', '2018-04-07 17:07:46', 151),
(107, 92, 1, 'One goods request send to warehouse for primary verification.', '2018-04-07 17:12:13', 151),
(108, 93, 1, 'One goods request send to warehouse for primary verification.', '2018-04-07 17:25:39', 155),
(109, 94, 1, 'One goods request send to warehouse for primary verification.', '2018-04-07 17:25:56', 155),
(110, 95, 1, 'One goods request send to warehouse for primary verification.', '2018-04-07 17:46:26', 155),
(111, 96, 1, 'One goods request send to warehouse for primary verification.', '2018-04-29 13:25:34', 26),
(112, 96, 4, '', '2018-04-29 13:29:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sps__request_product`
--

CREATE TABLE `sps__request_product` (
  `id` int(123) NOT NULL,
  `user_id` int(123) NOT NULL,
  `category_id` int(123) NOT NULL,
  `subcategory_id` int(123) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `add_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__revenue`
--

CREATE TABLE `sps__revenue` (
  `id` int(123) NOT NULL,
  `user_id` int(123) NOT NULL,
  `product_id` int(123) NOT NULL,
  `warehouse_id` int(123) NOT NULL,
  `price` float(10,2) NOT NULL,
  `date_range` varchar(222) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `totalprice` float(10,2) NOT NULL,
  `settlement_price` float(10,2) NOT NULL,
  `add_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__seller_settlement`
--

CREATE TABLE `sps__seller_settlement` (
  `id` int(50) NOT NULL,
  `seller_id` int(15) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `purchase_amount` float(30,2) DEFAULT NULL,
  `vat` float(10,2) DEFAULT NULL,
  `commision` float(10,2) DEFAULT NULL COMMENT 'warehouse comission',
  `warehouse_charge` float(30,2) DEFAULT NULL,
  `extra_charge` float(30,2) DEFAULT NULL,
  `extra_charge_vat` float(10,2) DEFAULT NULL,
  `extra_charge_comments` text,
  `total_amount` float(30,2) DEFAULT NULL,
  `status` smallint(1) DEFAULT '0' COMMENT '0->pending,1->success,2->failure',
  `refer_number` varchar(222) DEFAULT NULL COMMENT 'transaction refer number',
  `refer_comments` text COMMENT 'transaction comments',
  `add_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__seller_settlement`
--

INSERT INTO `sps__seller_settlement` (`id`, `seller_id`, `month`, `year`, `purchase_amount`, `vat`, `commision`, `warehouse_charge`, `extra_charge`, `extra_charge_vat`, `extra_charge_comments`, `total_amount`, `status`, `refer_number`, `refer_comments`, `add_date`) VALUES
(1, 59, 1, 2018, 315.00, 15.75, 6.30, 100.00, 50.00, NULL, 'test', 158.70, 1, NULL, NULL, '2018-03-01 13:16:40'),
(2, 26, 3, 2018, 13970.00, 698.50, 1466.85, 90.00, 90.00, 9.00, 'test ', 14478.50, 0, NULL, NULL, '2018-04-13 12:24:54'),
(3, 58, 1, 2018, 680.00, 34.00, 71.40, 90.00, 90.00, 9.00, 'tesr', 453.60, 1, NULL, NULL, '2018-04-13 12:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `sps__state`
--

CREATE TABLE `sps__state` (
  `id` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__state`
--

INSERT INTO `sps__state` (`id`, `id_country`, `name`, `add_date`) VALUES
(3, 1, 'Abu Dhabi', '2017-09-08 11:28:47'),
(4, 1, 'Sharjah', '2017-09-08 11:28:47'),
(6, 1, 'Dubai', '2017-10-24 10:54:00'),
(8, 1, 'Fujairah', '2017-12-19 06:58:59'),
(9, 1, 'Ajman', '2017-12-19 06:59:20'),
(10, 1, 'Umm al-Quwain', '2017-12-19 06:59:38'),
(11, 1, 'Ras Al Khaimah', '2017-12-19 06:59:52'),
(12, 1, 'Abudabi', '2018-03-08 16:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `sps__testimonial`
--

CREATE TABLE `sps__testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(222) DEFAULT NULL,
  `image` text,
  `description` text,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1->Active,2->inactive',
  `add_date` date DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__testimonial`
--

INSERT INTO `sps__testimonial` (`id`, `title`, `image`, `description`, `status`, `add_date`, `ip`) VALUES
(1, 'Md. Mohamod Aziz', 'img_1.png', 'I wonder, how siloed my business used to be before! SFE opened the world to me.', 1, '2017-12-16', '2017-12-16 13:30:46'),
(3, 'Md. Mohamod Aziz3', 'img_3.png', 'The exchange connected me to 100s of new buyers! Now business is fun.', 1, '2017-12-16', '2017-12-16 15:51:28'),
(4, 'Md. Mohamod Aziz4', 'img_4.png', 'I wonder, how siloed my business used to be before! SFE opened the world to me', 1, '2017-12-16', '2017-12-16 15:52:39'),
(5, 'Md. Mohamod Aziz2', 'img_2.png', 'I wonder, how siloed my business used to be before! SFE opened the world to me.', 1, '2017-12-16', '2017-12-16 15:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `sps__transaction`
--

CREATE TABLE `sps__transaction` (
  `id` int(11) NOT NULL,
  `ordernumber` int(11) DEFAULT NULL,
  `amount` float(50,2) DEFAULT NULL COMMENT '1->Auction,2->fixedsell',
  `teller_number` varchar(222) DEFAULT NULL,
  `bank_name` varchar(222) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0->pending,1->Accept,2->Reject',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__transaction`
--

INSERT INTO `sps__transaction` (`id`, `ordernumber`, `amount`, `teller_number`, `bank_name`, `status`, `add_date`, `ip`) VALUES
(1, 26104073, 18600.00, '123456', 'sbi', 0, '2018-01-11 00:00:00', '192.168.1.38'),
(3, 5818548, 871.50, '12345', 'SBI', 0, '2018-01-15 00:00:00', '192.168.1.38'),
(4, 2612656, 8715.00, '54678910112', 'sbi', 0, '2018-01-17 00:00:00', '192.168.1.38'),
(5, 2615449, 8715.00, '456123123', 'obc', 0, '2018-01-17 00:00:00', '192.168.1.38'),
(6, 2612195, 840.00, '121212', 'sbi', 0, '2018-01-22 00:00:00', '192.168.1.108'),
(7, 2612434, 871.50, '453534', 'sbi', 0, '2018-01-22 00:00:00', '192.168.1.108'),
(8, 2617402, 871.50, '12121', 'sbi', 0, '2018-01-22 00:00:00', '192.168.1.108'),
(9, 5959162, 750.75, '123456', 'sbi', 0, '2018-02-07 00:00:00', '192.168.1.108'),
(10, 2653620, 742.88, '12345', 'sbi', 0, '2018-02-08 00:00:00', '192.168.1.108'),
(11, 11737741, 472.50, '475745', 'SBI', 0, '2018-02-09 00:00:00', '192.168.1.111'),
(12, 8232184, 330.75, '4757457654', 'SBI', 0, '2018-02-09 00:00:00', '192.168.1.111'),
(13, 8436733, 236.25, '376546745', 'SBI', 0, '2018-02-09 00:00:00', '192.168.1.111'),
(14, 8436733, 236.25, '456757547', 'SBI', 0, '2018-02-10 00:00:00', '223.31.228.226'),
(15, 8289931, 2310.00, '653765756', '76465656567', 0, '2018-02-21 00:00:00', '45.251.36.143'),
(16, 8259918, 142.80, '5643456', '76465656567', 0, '2018-02-21 00:00:00', '45.251.36.143'),
(17, 8259918, 142.80, '5643456', '76465656567', 0, '2018-02-21 00:00:00', '223.31.228.226'),
(18, 8259896, 72.97, '5643456', '76465656567', 0, '2018-02-21 00:00:00', '223.31.228.226'),
(19, 8252566, 72.97, '5643456', '76465656567', 0, '2018-02-21 00:00:00', '223.31.228.226'),
(20, 8258087, 100.00, '5643456', '76465656567', 0, '2018-02-26 00:00:00', '45.251.37.3'),
(21, 8258087, 142.80, '5643456', '76465656567', 0, '2018-02-26 00:00:00', '223.31.228.226'),
(22, 8259773, 142.80, '5643456', '76465656567', 0, '2018-02-26 00:00:00', '223.31.228.226'),
(23, 8259773, 142.80, '5643456', '76465656567', 0, '2018-02-26 00:00:00', '223.31.228.226'),
(24, 8254582, 6441.75, '546783993', 'sbi', 0, '2018-03-01 00:00:00', '157.41.224.45'),
(25, 8254582, 6441.75, '09090900', 'sbi', 0, '2018-03-01 00:00:00', '157.41.170.201'),
(26, 84511480, 14668.50, '1122334455', 'Test Bank', 0, '2018-03-01 00:00:00', '157.41.224.45'),
(27, 129563501, 94.50, '121212', 'sbi', 0, '2018-03-07 00:00:00', '223.31.228.226'),
(28, 129563501, 94.50, '121212', 'sbi', 0, '2018-03-07 00:00:00', '223.31.228.226'),
(29, 12962202, 204.75, '121212', 'sbi', 0, '2018-03-07 00:00:00', '223.31.228.226'),
(30, 12753902, 74.03, 'fvdv', 'eer', 0, '2018-03-08 00:00:00', '223.31.228.226'),
(31, 129541328, 206.32, '121212', 'sbi', 0, '2018-03-10 00:00:00', '223.31.228.226'),
(32, 141586873, 63.52, '435235', 'SBI', 0, '2018-03-12 00:00:00', '223.31.228.226'),
(33, 141588503, 68.25, '435235', 'SBI', 0, '2018-03-12 00:00:00', '223.31.228.226'),
(34, 2639329, 50.40, '6546546', 'SBI', 0, '2018-03-14 00:00:00', '223.31.228.226'),
(35, 2622246, 213.68, '121212', 'sbi', 0, '2018-03-14 00:00:00', '223.31.228.226'),
(36, 2631404, 201.60, 'df', 'dfv', 0, '2018-03-14 00:00:00', '223.31.228.226'),
(37, 2658671, 220.50, '453534', 'sbi', 0, '2018-03-14 00:00:00', '223.31.228.226'),
(38, 2658808, 220.50, '234667\r\n', 'sbi\r\n', 0, '2018-03-16 00:00:00', '157.41.174.62'),
(39, 2658808, 220.50, '234556\r\n', 'sbi\r\n', 0, '2018-03-16 00:00:00', '157.41.174.62'),
(40, 14132029, 47.25, 'hdjr\r\n', 'hs\r\n', 0, '2018-03-16 00:00:00', '223.31.228.226'),
(41, 14035727, 47.25, '12121', 'sbi', 0, '2018-03-21 00:00:00', '223.31.228.226'),
(42, 141632193, 47.25, '435235', 'SBI', 0, '2018-03-23 00:00:00', '223.31.228.226'),
(43, 141632193, 47.25, '435235', 'SBI', 0, '2018-03-23 00:00:00', '223.31.228.226'),
(44, 14128838, 341.25, '435235', 'SBI', 0, '2018-03-26 00:00:00', '223.31.228.226'),
(45, 26669304, 438.38, '98767', 'sbi', 0, '2018-03-29 00:00:00', '157.41.237.152'),
(46, 26107691, 992.25, '435235', 'SBI', 0, '2018-04-02 00:00:00', '223.31.228.226'),
(47, 141565735, 96.08, '121212', 'sbi', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(48, 141712453, 22.57, '453534', 'sbi', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(49, 141719973, 31.50, '453534', 'sbi', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(50, 141715736, 31.50, '121212', 'sbi', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(51, 141569261, 48.83, '121212', 'sbi', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(52, 141719287, 31.50, '121212', 'acfs', 0, '2018-04-05 00:00:00', '223.31.228.226'),
(53, 15126283, 142.28, '233t\r\n', 'SBI\r\n', 0, '2018-04-06 00:00:00', NULL),
(54, 26258273, 7363.12, '564\r\n', 'Sbi\r\n', 0, '2018-04-07 00:00:00', NULL),
(55, 58256423, 4378.50, '34543', 'SBI', 0, '2018-04-29 00:00:00', '122.167.175.171'),
(56, 2638216, 101.85, '12321', 'Emirate Bank', 0, '2018-05-09 00:00:00', '122.172.42.28');

-- --------------------------------------------------------

--
-- Table structure for table `sps__transport`
--

CREATE TABLE `sps__transport` (
  `id` int(12) NOT NULL,
  `name` varchar(100) NOT NULL,
  `registration_number` varchar(50) NOT NULL,
  `description` text,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `alt_number` varchar(50) DEFAULT NULL,
  `address` text,
  `city` varchar(100) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip` varchar(12) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__transport`
--

INSERT INTO `sps__transport` (`id`, `name`, `registration_number`, `description`, `email`, `phone`, `alt_number`, `address`, `city`, `state`, `country`, `zip`, `status`, `add_date`, `ip`, `added_by`) VALUES
(1, 'Transport1', 'Transport1', 'Transport1', 'transport1@gmdd.nmn', '0789789456', '', 'Jayadev Vihar,Bhubaneswar', '1', 1, 1, '751013', 1, '2017-10-11 16:14:33', '192.168.1.108', 1),
(2, 'zdgdfg', 'fgdfg', 'zfgdfg', 'zdgdfg@dfgb.zfg', '56537', '', 'dfgbcfgb', '6', 6, 1, '5467', 1, '2018-03-08 17:07:55', '223.31.228.226', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sps__users`
--

CREATE TABLE `sps__users` (
  `id` int(123) NOT NULL,
  `name` varchar(222) DEFAULT NULL,
  `company_name` varchar(222) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `uniqueID` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `profile_img` varchar(50) DEFAULT NULL,
  `email` varchar(111) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `countrycode` varchar(7) DEFAULT NULL,
  `alt_number` varchar(50) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `transport_used` tinyint(1) DEFAULT '0',
  `usertype` smallint(1) NOT NULL DEFAULT '0' COMMENT '1->Buyer,2->Seller,3->Both',
  `is_admin` tinyint(1) DEFAULT '0',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lastlogin` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1->Active,2->Pending 3->In-active',
  `email_verify` varchar(100) DEFAULT NULL,
  `transact_verify` int(2) DEFAULT '1' COMMENT '0->pending 1->address, 2->Bank, 4->Both (binary concept)',
  `phone_verify` int(8) DEFAULT NULL COMMENT '1-->varifyed',
  `IP` varchar(20) NOT NULL,
  `device_id` varchar(222) DEFAULT NULL,
  `fcm_device_token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__users`
--

INSERT INTO `sps__users` (`id`, `name`, `company_name`, `reg_no`, `uniqueID`, `username`, `profile_img`, `email`, `phone`, `countrycode`, `alt_number`, `password`, `remember_token`, `transport_used`, `usertype`, `is_admin`, `reg_date`, `updated_at`, `lastlogin`, `status`, `email_verify`, `transact_verify`, `phone_verify`, `IP`, `device_id`, `fcm_device_token`) VALUES
(1, 'Admin P', NULL, NULL, NULL, 'spsadmin@thoughtspheres.com', NULL, 'spsadmin@thoughtspheres.com', '9861601016', NULL, NULL, '$2y$10$8OgJjUJQvBnz8wAyE70j5eJpRMGLjAYp4W2RMRUr.Er/i0eCh/FjC', '3LYzHo2lujqE1MbYgcsCdtPeKA4LsJmh7w9VJ2Hp1COR11cxq7h45xACPKvt', 0, 0, 1, '2019-06-20 13:54:12', '2019-06-20 19:24:12', '2017-09-05 18:30:00', 1, NULL, 1, NULL, '', NULL, NULL),
(2, 'Store Manager', NULL, NULL, NULL, 'surplusstore@thoughtspheres.com', NULL, 'surplusstore@thoughtspheres.com', '9861601016', NULL, NULL, '$2y$10$8OgJjUJQvBnz8wAyE70j5eJpRMGLjAYp4W2RMRUr.Er/i0eCh/FjC', 'GWVE9unMGZvH3XefnGEqmPCoCvcdUkgq56rVnaODxYd2TZ5plXJ2LbBNsOlq', 0, 0, 2, '2018-06-11 09:53:21', '2018-03-29 17:53:56', '2017-09-05 18:30:00', 1, NULL, 1, NULL, '', NULL, NULL),
(3, 'Finance Manager', NULL, NULL, NULL, 'surplusfinance@thoughtspheres.com', NULL, 'surplusfinance@thoughtspheres.com', '986166666', NULL, NULL, '$2y$10$8OgJjUJQvBnz8wAyE70j5eJpRMGLjAYp4W2RMRUr.Er/i0eCh/FjC', '7EivErPv85QsJFiqnp8yeYZkaJ8kIrSvUs9AS4tboPr1Ou1JcDWpEeyP2b6Z', 0, 0, 3, '2018-06-11 09:53:11', '2018-03-29 17:49:42', '2017-09-05 18:30:00', 1, NULL, 1, NULL, '', NULL, NULL),
(4, 'testing auction', NULL, '', NULL, 'test@gmail.com', NULL, 'test@gmail.com', '7897894561', NULL, '', '$2y$10$QNLn6TsIBeBesIkIbFbcGO/cc/fE6Dal4aydvnkJjnfo.sSYnCriy', 'yl7HX2SX7Qd7ZbHHY7BLnYKwd4sLovGD64Ei2GWmtePebeBBob5c7VPP5X44', 0, 2, 0, '2018-02-10 11:57:13', '2017-09-19 10:22:27', NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(19, 'Ranjita Infotech', NULL, 'RAN201598746', NULL, 'run091.cet@gmail.com', 'docs0001(1).jpg', 'run091.cet@gmail.com', '7873387488', NULL, '', '$2y$10$yEQbeZD4LOTEm8s60A.vpu7qwsusx4giXYH7IQ6Me6Tpse1SrRUXy', 'IsnjpheL5O03FijYxnroAO2NCS4iLsR5uUjpUFfc', 0, 3, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(26, 'Puru Sahoo', 'abc pvt ltd', 'yut768', '26YUTLTD', 'sahoo.purusottama@gmail.com', 'pomogranateKernels.jpg', 'sahoo.purusottama@gmail.com', '7873387482', NULL, '989898989823', '$2y$10$lKbcXoY7vpEBnD1K1b/xnegPTkFRQbRuzah5JAiKZZJoYG0HOnZHq', 'q3M5nShqMclOBBlVK6ClVZLfwBQTHQWvE0tpluHKPl7F0zlhRno0JaF7Mhkk', 0, 3, 0, '2018-07-30 15:30:06', '2018-07-30 21:00:06', NULL, 1, '1', 4, 4358, '192.168.1.38', NULL, 'dLYk1lbfk1E:APA91bGH5x_EXbdXJmML2j4ibX7Dx5uLRNBB-q0jDEUSyVvKCnAP-vX9xkgSkHCUM2kxLVKBRtQcLzgvV6bmbGQdXlnnhw7t3b9x6TRYUcABaxzypGB5rbzZov-crVE1_LaiCzh3LAQB'),
(27, 'sd', NULL, 'asd', '27ASDSD', 'asd@gmail.com', NULL, 'asd@gmail.com', '123123', NULL, '123', '', '9b2tBYi7O4C62HCIqZcdAiC9GSGD5QlESAOCnR9r', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 3, '1', 1, 1, '192.168.1.38', NULL, NULL),
(40, 'ranjita behera', 'ranjita pvt ltd', '908787', '40908LTD', 'run09.cet@gmail.com', 'ranjita.jpg', 'run09.cet@gmail.com', '7845784545', NULL, '8945612345', '$2y$10$U72oGfP21GoeNacNw.ebmONahRXXH0dku2s9OJo0nmgjSoxipT7eu', '9fSZj3PyEJS3H5A6I6q3VdbC5e9Cj6Fo90jcy6Nf', 0, 2, 0, '2017-11-22 13:53:22', NULL, NULL, 1, '1', 4, 6405, '192.168.1.38', NULL, NULL),
(41, 'ajit mishra', NULL, 'xxxx', '41XXXHRA', 'ajit@akmfoods.com', NULL, 'ajit@akmfoods.com', '0097150985', NULL, 'cccc', '$2y$10$JaYoZ9EUm1kVDpSIoiSDXuIdC4pPCK1K.on8I7Q5O51ClToNYefoO', 'mhzoVm9lmomHEj6CLzWo7lfCiq8Pk4AeEGy6tCxOefvE9H9pDadNQDCBdhtR', 0, 2, 0, '2018-02-10 11:57:13', '2017-10-30 08:53:45', NULL, 1, '1', 1, 5501, '192.168.1.97', NULL, NULL),
(58, 'Ranjita', 'ranjita pvt ltd', 'ranjita003', '58RANLTD', 'ranjitamayeesahoo@gmail.com', 'melanie-magdalena-318289.jpg', 'ranjitamayeesahoo@gmail.com', '3434534534', NULL, '6767676767', '$2y$10$3yX6TZxUxgI2VtCAA0xhseoq7i4ERlHFCR6oRGTvopZnlJMjKyZjG', '6zCM4I80hjkO7Dbo3DDA4yMQYeGj5JzhxLnMfbsveOvQkyHxE55SNM8uXvMV', 0, 3, 0, '2018-04-29 07:09:00', '2018-04-29 12:39:00', NULL, 1, '1', 4, 1, '192.168.1.38', NULL, NULL),
(59, 'Purusottama Sahoo', 'Developer Pvt Ltd', '561043', '59561LTD', 'purusottama@thoughtspheres.com', NULL, 'purusottama@thoughtspheres.com', '9776414862', NULL, '9861601016', '$2y$10$1lg9StDL5cLPDB8b3.b0NuAvz33OHruleIIaO9LiWbbK2eksDBUIO', '9POffnwN9qqNcZ6iBJ8I7MimjNGh2HYquvC3BPGJMVhVebO8ZW0RKNxkgHAD', 0, 2, 0, '2018-04-02 06:47:14', '2018-04-02 12:17:14', NULL, 1, '1', 4, 6102, '192.168.1.38', NULL, NULL),
(60, 'Dass', '', 'DP81234', '60DP8', 'das@gmail.com', NULL, 'das@gmail.com', '5646456456', NULL, '', '$2y$10$tPYaFEpB34lSBTzn3zSmYeW9FUibOpU0Z1PlD1/dWcSZXFF7BOoge', 'v41gA6tdAGiKLQjlZuROz8P8Nr7sniEA6w5LKnW1', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.111', NULL, NULL),
(66, 'abc', 'abc tech', 'AB0001C', '66AB0ECH', 'ranjita.sahoo@thoughtspheres.com', 'bahadur_shah.png', 'ranjita.sahoo@thoughtspheres.com', '7873387489', NULL, '7873387489', '$2y$10$Tj/wmOgyuw1iRMOUQc2NfOgMN5pALVzuul0f/OE/IK/vgpYSk0l46', 'BAkHzgmGsBOwWupwNFJa01HitZblSPueoLfJRG4f', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 2582, '192.168.1.108', NULL, NULL),
(67, 'abc', 'abc tech', 'abc0001', '67ABCECH', 'pramodini.das342578@thoughtspheres.com', NULL, 'pramodini.das342578@thoughtspheres.com', '7873387486', NULL, '7873387489', '$2y$10$UmR61fvzU4cegi41DmxWruvZYuy0UHmh4yCXmb3niSXGE7T0/7NW.', '0N46wQeR3suT4RM2SyhSPSQuF7uVVukA0M4zqoZs', 0, 1, 0, '2018-03-08 06:53:49', '2017-11-15 10:17:31', NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(69, 'kisan', 'Developer Pvt Ltd', 'PU0725', '69PU0LTD', 'kisan23@thoughtspheres.com', NULL, 'kisan23@thoughtspheres.com', '7873387481', NULL, NULL, '$2y$10$6PbLOtjVQOJKUa4kx.ICQ.s5NMV6U4HeF6DajVJ.9C59bvsrEM4d2', 's7yhOxoMC79IEfce8QcbIkRcD7iVhhOCora8vlkG', 0, 1, 0, '2018-03-06 06:41:33', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(70, 'kisan', 'Developer Pvt Ltd', 'PU0725', NULL, 'kisan1@thoughtspheres.com', NULL, 'kisan1@thoughtspheres.com', '7873387474', NULL, '', '$2y$10$8fSCg3FuHu9kcy62IO9iouUqWI0z.Uaq1U.R1XAtuQViZrwnZZXLa', 's7yhOxoMC79IEfce8QcbIkRcD7iVhhOCora8vlkG', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(71, 'kisan', 'Developer Pvt Ltd', 'PU0725', '71PU0LTD', 'kisan12@thoughtspheres.com', NULL, 'kisan12@thoughtspheres.com', '7873387435', NULL, NULL, '$2y$10$E3GK5Vkh4JsoBzfhsd.B4ewDFFY7mF7LrlB9aa8i8wf4ORcf.1V8q', 's7yhOxoMC79IEfce8QcbIkRcD7iVhhOCora8vlkG', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(72, 'kisan', 'Developer Pvt Ltd', 'PU0725', '72PU0LTD', 'kisan112@thoughtspheres.com', NULL, 'kisan112@thoughtspheres.com', '7873387425', NULL, NULL, '$2y$10$0HaJCYpFEZXKlAJuGtYUf.PnZlmv14vdIES2sg5YWo7pReHt11zb.', 's7yhOxoMC79IEfce8QcbIkRcD7iVhhOCora8vlkG', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(73, 'suparijita', 'suparijita Pvt Ltd', 'SU0001R', '73SU0LTD', 'suparijita@gmail.com', NULL, 'suparijita@gmail.com', '7845896587', NULL, '', '$2y$10$pn7cTVvKMtsMnXivTzsMbebR7/3X9LEow0l23umohRP.B.mv3DwcG', 'x7n7S33ETcf1lHsZTycogquQlyjogE7IvSsucUgZ', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(74, 'Demo', 'Demotest', '123456', '74123EST', 'demotest@gmail.com', NULL, 'demotest@gmail.com', '1234567890', NULL, '324781623478624783678', '$2y$10$O7KXLE/L7eBUdm2kXitk9uZKjgY8vnvFTT5iP.TeCMFmdt.RgS92K', 'LthGOMeJwcWS67fr25OgLvDoJxLDCcSgdUmzeERC', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 2, '1d5ee46fa3f8e201af524be26d6e1906', 1, 5152, '192.168.1.111', NULL, NULL),
(79, 'Kisan', 'kisan pvt. ltd.', 'kisan1234', '79KISTD.', 'kisanpatra.9232@gmail.com', NULL, 'kisanpatra.9232@gmail.com', '8984071718', NULL, '45623235325', '$2y$10$SgdYOpw5.1.Nuk/N/C3GfO9BD/RW..ylgsNKWaA5tZnQaeGkR8C4y', 'Dl2U27kqclqfTuBowTUFDNgAO50DRI8yQiNGSkgDhXcqOW8TyytE3XtdaaAK', 0, 1, 0, '2019-06-20 13:33:21', '2019-06-20 19:03:21', NULL, 1, '1', 1, 6367, '192.168.1.111', NULL, NULL),
(81, 'Rabindra Behera', 'RKB', 'RB0001', '81RB0RKB', 'rabindra.behera2004@gmail.com', NULL, 'rabindra.behera2004@gmail.com', '7875598748', NULL, '7875598748', '$2y$10$GUqsdPAPfUCLqpdXXqNC5uON6W3yxCmc.Xi9H4E1/O54Hh.JHx7hy', '5OgujVzmkDqwNS2W7MJ4steLlQnJBg2IsMd1rSo6v4zKSGdNKrnm40xdU4cg', 0, 1, 0, '2018-03-13 05:57:16', '2018-03-13 11:27:16', NULL, 1, '1', 1, 3064, '192.168.1.108', NULL, NULL),
(82, 'Kisan Patra', 'Raj Enterprises', '123456', '1512626332', 'patrakisa23n.92@gmail.com', 'download_(6).jpg', 'patraki23san.92@gmail.com', '7540931293', NULL, '74568273485', '$2y$10$odLRA5l.3NP.jr79LIXYl.uoSRvS74UhWJ//qj7bjeMIQMXR0LCNi', 'eGyzzg5lgxdZNYv9r35cKPUl3nhi0Bz5MCwC4XHKqC0ifz93CLbokh8tB815', 0, 1, 0, '2018-03-06 06:41:55', '2018-03-05 13:25:12', NULL, 1, '1', 1, 7553, '192.168.1.111', NULL, NULL),
(83, 'Rajib', 'kurkuri', '`123123', '1512631228', 'rajib@gmail.com', NULL, 'rajib@gmail.com', '9834248234', NULL, '234241321123', '$2y$10$cVl1CBZW6cNC.GzuJcSlHeswbLvaA0XuOW6AdWYnWHWHztyxhfaWe', 'S3arbSbhkvOwlNgvurt5PTqyiX78aNEvNKI2xWcL', 0, 2, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1986, '192.168.1.111', NULL, NULL),
(84, 'Nagen Biswal', 'Nagen Company', 'REG5546564', '1512631825', 'nagen@thoughtspheres.com', NULL, 'nagen@thoughtspheres.com', '9938118270', NULL, '9938118270', '$2y$10$l7llIqp2P3kfM0hh981oSOtZsgmPemJgVTZDl3KMHPq.LtYTljHYa', 'dRdgbdrssyUsGTSY96wQhIJTzdcbHjS20BOdeMwcP6i5Bq9waC6N4x43JL3c', 0, 3, 0, '2018-04-06 16:13:31', '2018-04-06 21:43:31', NULL, 1, '1', 1, 9885, '192.168.1.105', NULL, 'cW1MVlDSJ4U:APA91bH1uKXnmgETnFxDXKoTkodRBmMFPThr_iYnyIyRYLmpz_FiD7mG8FPa9GIEJmtycpkvsbLFdpnr4j4UhPBnJgVPcdvgHZxJqom1rA3vd_-ksPH4Aj8_JZwRHTXJvKnurbmjkWPW'),
(85, 'Rai', 'Rabi', '43295789', '1512714451', 'rabi@gmail.com', NULL, 'rabi@gmail.com', '7428357432', NULL, '', '$2y$10$VPooEaQj.ccTnEuQKNmSuuexVFnopiWeFtMuQ2bvZ38llI9bahIU6', 'Ykbwslp8QuWafqfAa0P98Bxf3P2vN4za1PxaWlqo', 0, 3, 0, '2018-02-10 11:01:17', NULL, NULL, 1, '1', 2, 7485, '192.168.1.111', NULL, NULL),
(86, 'Raju', 'Rajendra', '3241234', '1512718472', 'raju@gmail.com', NULL, 'raju@gmail.com', '578432758', NULL, '', '$2y$10$7yY0agg.XOYquaj0R.pT9efCzJ.B2/U/cEDeQYt.L6U6uN4HTJ/T6', 'fg8x2Ld5ODX2Naf0nrl0uZruyQhLMlXzxFYH534T', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 7371, '192.168.1.111', NULL, NULL),
(87, 'Kisan', 'kisan pvt. ltd.', '123456', '1513841054', 'kisan@gmail.com', 'snow-puru.png', 'kisan@gmail.com', '7540931299', NULL, '9776414862', '$2y$10$h5WuKXTXRJsbvUY1qVyJr.10HqjrGWlqq1cK.BaL8pg3su1tW5zz.', 'Dvj4TGbrpDTKPi7tYP5MeRNpjh2kbdLuWKbjS7ZzzlDWR7oySVX20qkOpogm', 0, 3, 0, '2018-02-07 08:01:59', '2018-01-02 11:13:35', NULL, 1, '1', 4, 6755, '192.168.1.111', NULL, NULL),
(91, 'ran123', 'ran123', 'ran123', '1514466377', 'suparijita.mohanty@thoughtspheres.com', NULL, 'suparijita.mohanty@thoughtspheres.com', '7897894559', NULL, NULL, '$2y$10$chVEKIY4f0v03aDhHCV6OeEVm1i93PktjlvcHnJGiMar7gyIaGZN2', 'RkMZMCDgq8CvUcvqHB7lp8IqvovVEw39flHKFJw8', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(92, 'puru', 'puru', 'asd', '1516099288', 'sahoo.purusottama23@gmail.com', NULL, 'sahoo.purusottama23@gmail.com', '89898989', NULL, NULL, '$2y$10$rWm5CMZlce3ZSQZHklEQ.uM.GZ3sHhN3/9J0yw9TZ7csZYSoVuEkS', 'nIUkWvehmDXjqYw0ybwvRAQxKxhBXerqIzYK939F', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(93, 'test', 'test ', '123', '1516104089', 'sahoo.purusottama232@gmail.com', NULL, 'sahoo.purusottama232@gmail.com', '45454545', NULL, NULL, '$2y$10$RWK2p8vfpBHDpu9tqXcRgOZTXl5a6UJEQ2VSHPcIVz0GyqfOQqCR6', 'nIUkWvehmDXjqYw0ybwvRAQxKxhBXerqIzYK939F', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(94, 'test', 'test', '456123789', '1516175629', 'sahoo.purusottama67@gmail.com', NULL, 'sahoo.purusottama67@gmail.com', '65656565', NULL, NULL, '$2y$10$hQQ7j9z4naLeGTKstCnEU.I6daFRtj0Iv8XHXDbkKQtRcLvdeMRVm', 'vygJoQrzLlUer7OaviU3x5OlMHLd1tTJkEaJtn4v', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.38', NULL, NULL),
(95, 'test', 'test', 'test123', '1516185349', 'puru@test.com', NULL, 'puru@test.com', '9861601019', NULL, NULL, '$2y$10$G9OEePJYjdtSxi1e6fB4kOPVAhDbyDz/gVfPQRJeh/yzckZXkOA5m', 'VNm6z6mmJrzI5n7NXKJVf5gtsl2HwJiO4UqCdKtc', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.82', NULL, NULL),
(96, 'Ranjita Infotech1', 'thoughtsphere', 'RAN201598746', '1516195090', 'ranjitamayeesahoo1@gmail.com', NULL, 'ranjitamayeesahoo1@gmail.com', '7873387454', NULL, NULL, '$2y$10$sa.uogfHRdbqkQnOFt229OaXsnnvY3b78e9Z5lutr0QoC53ohI1cG', 'RVwhJeKb3DhhfeqaREAXzLq3tPIwsPH4AeoA7o2i', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(97, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516197199', 'ranjitamayeesahoo123@gmail.com', NULL, 'ranjitamayeesahoo123@gmail.com', '7897894523', NULL, NULL, '$2y$10$VL7h6OdYD0qzHTQWlwIkzOegEYoIdd7mVrvsWXgwZrn399dKQ2dF6', 'RVwhJeKb3DhhfeqaREAXzLq3tPIwsPH4AeoA7o2i', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(98, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516197319', 'ranjitamayeesahoo11@gmail.com', NULL, 'ranjitamayeesahoo11@gmail.com', '7873387422', NULL, NULL, '$2y$10$/o4nW/7TBoyNXlDBZe6oE.t6yVnuHaBipD6OyXqAvftxG4G/xaHse', 'RVwhJeKb3DhhfeqaREAXzLq3tPIwsPH4AeoA7o2i', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(99, 'test', 'test', 'test123', '1516261651', 'test@test.com', NULL, 'test@test.com', '0987675', NULL, NULL, '$2y$10$2qKRIA4YQS7aGItQ7oWJhOdXpP6RI/FWmWREt1sVF1u8k3f7JBK0y', 'n1O6ZgcahIIuBVnZsNYZ45dQk6zDtsKJYDgX2yLx', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.82', NULL, NULL),
(104, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516270880', 'ranjitamayeesahoo11234@gmail.com', NULL, 'ranjitamayeesahoo11234@gmail.com', '7873387411', NULL, NULL, '$2y$10$danyUgx6WrBGKu3v484UrecBwDwThOizJVlpEXga8tgK8r/GNQcB.', 'q7edbzwQ2PtinVlq8GNllC3EDjG0r9UeWO0dFuXg', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(105, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516271116', 'ranjitamayeesaho11o@gmail.com', NULL, 'ranjitamayeesaho11o@gmail.com', '7895689711', NULL, NULL, '$2y$10$8aNTBSCZy4okmoNLSvrrnub9jrossGO0gAsIsMododEF7RN/Y2Cma', 'pt27LZSOXGXEscCTv1PfpHw3dvjpgq7uK21AaOWu', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(106, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516271345', 'df@dfgg1.hgj', NULL, 'df@dfgg1.hgj', '1173387489', NULL, NULL, '$2y$10$0p3lv8gnJwDRfMdaNnJg1Ooy6bmiyL8PjD22mKtzz/wNbrV.9oYcy', 'pt27LZSOXGXEscCTv1PfpHw3dvjpgq7uK21AaOWu', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(107, 'Ranbir', 'Ranbir raj groups', '12345', '1516271713', 'ranbir@gmail.com', NULL, 'ranbir@gmail.com', '5375874385', NULL, '', '$2y$10$PpowVdXwhamyOl11uNtaOeJpop8/t7GzVsxGBgGluio.sy.kjVoni', 'JkvLZJbzkCRyisSzHIlDlZkSQMbofLJOMxdqAa6c', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '6b4e8c3701b244b7df53ef7351d6f35e', 1, 9898, '192.168.1.111', NULL, NULL),
(108, 'test', 'test', 'test123', '1516286591', 'utd@gg.com', NULL, 'utd@gg.com', '7676767676', NULL, NULL, '$2y$10$6Fav8VR2GU1ICBvKzC7EaOzByAEYr7nOx56aCd8z1joyUNb67wFQG', 'kDp5KFKCl7oXvjPqZkOW4dqCFBQLJhd353S7znem', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(109, 'Ranjita Infotech', 'thoughtsphere', 'RAN201598746', '1516344415', 'ranjitamayeesaho6o@gmail.com', NULL, 'ranjitamayeesaho6o@gmail.com', '7873387689', NULL, NULL, '$2y$10$4mnewrxrzhX/1Wr6ZTDbDeYEDtS.99YaXgfxB/oxVn0oDebgHcEZu', 'PISeocx36MheQJVztOuab09G7OdEUdqhs5Yfo8rs', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(110, 'Ranjita Infotech', 'ranjita pvt ltd', 'RAN201598746', '1516346936', 'ranjitamayeesahoo12@gmail.com', NULL, 'ranjitamayeesahoo12@gmail.com', '7873387389', NULL, NULL, '$2y$10$eQQGUR4cFaVpKaMq5UJeY.zYMh.oK5HEhKntvDs4Z9S9trpmoy5hi', 'XBKImY1Bnzwn2ZW2AOLu5ql60zbioCkc6areoUbk', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(111, 'test', 'test', 'test', '1516359833', 'sahoo.purusottama12@gmail.com', NULL, 'sahoo.purusottama12@gmail.com', '90818181', NULL, NULL, '$2y$10$DtsMQlgt9HbZS32ElF155uQTYIt3wNz5MnO3CHSuEcvx9yh8rwfPO', 'cdu4XF0755916nNLaspLE5WIuRGkKPPVfRjcrNOj', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(112, 'test', 'test', '123456', '1516361282', 'sahoo.purusottama34@gmail.com', NULL, 'sahoo.purusottama34@gmail.com', '989898', NULL, NULL, '$2y$10$rS84g2BMvVUfCNW7DPuL5OY4t0c5aaYYxUlNVO73QNeO0KL3xO6EG', 'cdu4XF0755916nNLaspLE5WIuRGkKPPVfRjcrNOj', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(113, 'tesr', 'test', '12345', '1516361420', 'asa@test.com', NULL, 'asa@test.com', '547899', NULL, NULL, '$2y$10$ccXNfS2HwQdXJPxY4st9XOCTzUbnPLwbtNr7.NPMoxyHXwSBAMfgu', 'cdu4XF0755916nNLaspLE5WIuRGkKPPVfRjcrNOj', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(114, 'test', 'res', 'asd', '1516361756', 'ranjitamayeesaho3o@gmail.com', NULL, 'ranjitamayeesaho3o@gmail.com', '78945667', NULL, NULL, '$2y$10$V214S2QC40wo/nCW0VfkeuPa/tITYI5vWh1d7Zatb12bZmb9AP2hO', 'cdu4XF0755916nNLaspLE5WIuRGkKPPVfRjcrNOj', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '1', 1, 1, '192.168.1.108', NULL, NULL),
(115, 'nagen                                         ', 'CIRCUIT', '123456', '1516687089', 'nagenpresearch@gmail.com', NULL, 'nagenpresearch@gmail.com', '8270951636', NULL, '', '$2y$10$BCQDueA6h99ocClFcnJXSuOZ03geR5fiCFzK1r9aA4LWZ1PGrTxvy', 'd6KrvaEIJmfIFSTL1z2jkqMO6L9ZeJQPiZBa0OzV', 0, 2, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '3c6a7bc8546406816516c47b8cfa1ee5', 1, 9616, '192.168.1.105', NULL, NULL),
(116, 'rewfewr', 'wefderf', 'ewrfrewgf', '1516687336', 'rfwe@fbh.fngjn', NULL, 'rfwe@fbh.fngjn', '4532523453', NULL, '', '$2y$10$6LVdN.b41f/41JeXrhg/VOpCnz36sUDRTYLi1t8UzGURF2tBsN8EO', 'gvO3tAQdkRQSuKr3dWagOsleemkUojqekynxmf3Y', 0, 1, 0, '2018-02-10 11:57:13', NULL, NULL, 1, '7a644e4672f250b3da3729a30515385c', 1, 6693, '192.168.1.111', NULL, NULL),
(117, 'Nitu Gupta', 'Nitu Pvt. Ltd.', 'Nitu1234', '1517233746', 'Nitu@thoughtspheres.com', NULL, 'Nitu@thoughtspheres.com', '7978583731', NULL, '', '$2y$10$KKmzFwZr6o4sCGlLZ4MkJuQc6IMDll28qilRmI.5CIZ/RJ.hh3tWC', 'R5VaPRSrlgSF8jo6up0Ue7zKMxgjxHdrle7acX55HoNO3Hd3sAAhaD5h8tZn', 0, 2, 0, '2018-04-01 11:12:16', '2018-04-01 16:42:16', NULL, 1, '1', 2, 6253, '192.168.1.111', NULL, NULL),
(118, 'Promodini', 'Promodin PVT LTD', '3241234453', '1518260606', 'promo@gmail.com', NULL, 'promo@gmail.com', '987589327458', NULL, '', '$2y$10$HJGzesEhgmQky1qRF53a/uJWldyDMhVVEMpvu06OdgJV2vqiKnAqC', 'uY96kovpBWbQiGJHNKbk2dvvhrAo5ozHdCeNwd6v', 0, 2, 0, '2018-02-10 11:07:24', NULL, NULL, 1, '1', 2, 6019, '223.31.228.226', NULL, NULL),
(120, 'ts', 'ts', 'ts', '1519883661', 'kisantest.patra@thoughtspheres.com', NULL, 'kisantest.patra@thoughtspheres.com', '8984071719', NULL, '9861601016', '$2y$10$4oaOI8/oSIISg193f71MKurN.z2RZB3VY.sE9A485ZiTstYYddbK2', 'MU8XtbllSc3qmhFQ1esUGlvS4YavqUci0i1uTn8O', 0, 1, 0, '2018-03-05 13:20:22', NULL, NULL, 1, 'e80359ce2b78e594a6465e39b0dd80fc', 1, 7853, '223.31.228.226', NULL, NULL),
(121, 'ts', 'ts', 'ts', '1519883730', 'nisikantkar@gmail.com', NULL, 'nisikantkar@gmail.com', '787890976', NULL, '9861601016', '$2y$10$WSXXTikGaHtge7saiFMxJ..J2dtVTgia1wviKLSN1bB.T59YsaHE2', 'MU8XtbllSc3qmhFQ1esUGlvS4YavqUci0i1uTn8O', 0, 1, 0, '2018-03-01 11:25:31', NULL, NULL, 1, 'e80359ce2b78e594a6465e39b0dd80fc', 1, 4980, '223.31.228.226', NULL, NULL),
(122, 'rgreg', 'ghrjeg', 'rgg', '1519883989', 'fd@gbg.dv', NULL, 'fd@gbg.dv', '5464364565436', NULL, '', '$2y$10$NfrmtRPdSeLs5zreM3jvOuQg8aTdqPSnqycfTk9OTamFJi7wp6jYK', 'Kqk58Lwhg9XTatUQY0E6UnJaL0JfQH8LdliaxWKX', 0, 1, 0, '2018-03-01 11:29:50', NULL, NULL, 1, '88fc5ee37a387e52e5d1a80ea5e8489f', 1, 9540, '157.41.170.201', NULL, NULL),
(123, 'Prasant', 'Parsu Pvt. LTd.', 'Pas123456', '1520057120', 'prasant@gmail.com', NULL, 'prasant@gmail.com', '898465655454718', NULL, '', '$2y$10$sNYtNrQAfsGAU2iJa2hQH.9GS331j1C8KwY.hIBaf.RvcpdQSnDVG', '0uV9W0wDxeSgwQLUG4cTmcv4XKFKGbpik5ZK6mb8', 0, 1, 0, '2018-03-03 11:35:21', NULL, NULL, 1, 'a0d9205fb7648d86160d383e86a10f18', 1, 3685, '223.31.228.226', NULL, NULL),
(124, 'fdvfds', 'vbhfdv', 'fvsd', '1520057516', 'afvsd@vbgb.dfgh', NULL, 'afvsd@vbgb.dfgh', '52134523', NULL, 'dfvdfsv', '$2y$10$y5Kw0d3NJOrdCa.Hl2cP5.OUO5dfjjAAXP2OHX31aG8jJ6CXG8h9e', '0uV9W0wDxeSgwQLUG4cTmcv4XKFKGbpik5ZK6mb8', 0, 2, 0, '2018-03-03 11:41:56', NULL, NULL, 1, 'a0d9205fb7648d86160d383e86a10f18', 1, 3876, '223.31.228.226', NULL, NULL),
(125, 'Pramodini Das', 'Thoughtspheres', 'PD1234', '1520059895', 'pramodinidas102@gmail.com', NULL, 'pramodinidas102@gmail.com', '9658256449', NULL, '', '$2y$10$xIhF3eNPoRZto3kpW4sk/eIkgRrEyJ92tVrwxMlmqD6onFWb8CiOS', 'pNos1Q4pHDTB5rPMuErOAxxNCOTtUHTMTGPHfFn2gWRElgsLdGWdqgti1pUy', 0, 3, 0, '2018-03-06 05:29:54', '2018-03-03 16:27:39', NULL, 1, '5c159a01ea77a2d290fca8f6d9fa2c3e', 1, 9881, '223.31.228.226', NULL, NULL),
(126, 'test', 'ts', 'test1234', '1520253578', 'test23@gmail.com', NULL, 'test23@gmail.com', '98989898', NULL, '', '$2y$10$7Ds3aHg9pkg098A3etvFT.yVLBakhqju/EZ0.nMdzdavqzLwCHT3q', 'MMQEsWUnbBgPzD3HCgSrn047VfvQ8ZPsM49kdChh', 0, 1, 0, '2018-03-06 14:01:08', NULL, NULL, 1, '1', 1, 1579, '223.31.228.226', NULL, NULL),
(127, 'patra', 'kisan', 'test123', '1520257327', 'kisan222.patra@thoughtspheres.com', NULL, 'kisan222.patra@thoughtspheres.com', '9876567898', NULL, '', '$2y$10$4d0VU9iFj3YyE6gFK.rfEuh/8OQfXynwD3HpKb1cUm/ndOt85Qt3y', 'KgN6Ew44IiwkTbpALsoiEYI9liebyvjUc2leC6GABCyp5MxeLgr7GGuIeVqE', 0, 2, 0, '2018-03-08 12:31:49', '2018-03-08 18:01:49', NULL, 1, '1', 1, 6631, '223.31.228.226', NULL, NULL),
(128, 'Pramodini Das', 'TS Pvt. Ltd.', 'TS1234', '1520315774', 'pramodinidas100@gmail.com', NULL, 'pramodinidas100@gmail.com', '8249833854', NULL, '', '$2y$10$0ho5a.EPjPTlzK6bcgEkCuCdrodk/GnWT2zIpnlj7TCti1P4jpqP.', 'nuFE7PjE5366OFQRmOhMmUM2aJ8Kl2aH2ufX7eFJqSbiXMDQzHws2bqPc1SF', 0, 2, 0, '2018-03-06 07:37:19', '2018-03-06 13:07:19', NULL, 1, '1', 1, 3318, '223.31.228.226', NULL, NULL),
(129, 'Kisan Patra', 'Patra Pvt Ltd.', 'kisan12345', '1520319229', 'kisanpatra.92345@gmail.com', 'apple300x300.jpg', 'kisanpatra.92345@gmail.com', '84965345', NULL, '423534252345', '$2y$10$86wCBZD6GwXptju7hf40FOajX8RyC2WC7PzUTXwQcRI5YcbCgmgxG', 'v3bZICORQJDU7orWWoq2SFZ2NOa8eZIN2IssZBmmuPWBq0SaihrawWP0pcG0', 0, 1, 0, '2018-03-10 11:25:45', '2018-03-10 16:55:45', NULL, 1, '1', 1, 2810, '223.31.228.226', NULL, NULL),
(130, 'Rabindra', 'Rabina Pvt ltd.', 'Ra3123', '1520325797', 'rabina123@gmail.com', NULL, 'rabina123@gmail.com', '234576345', NULL, NULL, '$2y$10$aauUpMaIzvSF3LPwh8xPtO5KTU4/40yaPZEaqsPdZYtrH1il9KIQi', '4hnckTZsDwVDeIgsxOKBFcsVC4TrJrPeVRivFNTj562dWJgYBq1PoEsh75Jl', 0, 1, 0, '2018-03-06 08:53:26', '2018-03-06 14:23:26', NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(131, 'dfs', 'zsdg', 'zsdfg', '1520326437', 'sdg@dfgr.fcgth', NULL, 'sdg@dfgr.fcgth', 'asdg', NULL, NULL, '$2y$10$ao5HU1sw3oE8AbLb3eSjh.c8eWu/GcDefu/92MZ6PjgFIssPb8Fya', 'hSYbJQ7Rw5TRiSDC2zFyNf5iide0FnoLycQ1VVdU', 0, 1, 0, '2018-03-06 14:23:57', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(132, 'dfh', 'dfhd', 'dfh', '1520330388', 'dfth@fgbfg.fdgyj', NULL, 'dfth@fgbfg.fdgyj', 'dfh', NULL, NULL, '$2y$10$TwTJuRhGiWvKIt3CvIyJfupsuqBLw0/QXcB189wpqsSTJS01ZjgY6', 'mOn4jT27z9rEAJJ03A0eOZWd0Slde5G7rPYqPRS8', 0, 1, 0, '2018-03-06 15:29:49', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(133, 'Patra Kisan', 'RajEnterprises', 'Pa16352', '1520333451', 'patrakisan.92345@gmail.com', NULL, 'patrakisan.92345@gmail.com', '7653734576', NULL, '', '$2y$10$42E26Up0ndYmTQR4MoW41.8j4eqLLjiPnEuuTIxTGEkzLLMKZfG8O', 'SGkazkQ6yhmK0DdKPzacrMDxFzMtYfJllil5dH32ndLX8OdqKOnzXbFjhGXy', 0, 2, 0, '2018-03-10 11:26:37', '2018-03-10 13:07:04', NULL, 1, '1', 4, 1, '223.31.228.226', NULL, NULL),
(134, 'fj', 'j', 'dghjg', '1520417442', 'kisan.patra12234@orimarktechnologies.com', NULL, 'kisan.patra12234@orimarktechnologies.com', '5423523', NULL, '', '$2y$10$Ld6cF3VxIp2OyD7DQsiVWej63zqyLk.nsfAYYnyw7XzznzGExHLgi', 'Or3i3LGR06Bh0NjvEvmgg8SV53KoQ2Fdq6Aye2Jm', 0, 1, 0, '2018-03-08 06:58:04', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(135, 'Kisan', 'Orimark job', '1234231', '1520492318', 'kisan.patra@orimarktechnologies.com', NULL, 'kisan.patra@orimarktechnologies.com', '47855687', NULL, '', '$2y$10$SlbqDINZXw9WSaH21VO2vOQMGRsKwCMMX/Pyikz1StEtuWbycyAz6', 'TpR0Szgrsn2wLsN3pD5ANWC9eaUlRksWlbEDJGs98uPCdzdVNjeRAJ5zS1F1', 0, 2, 0, '2018-03-08 10:47:58', '2018-03-08 16:17:58', NULL, 1, '1', 1, 4445, '223.31.228.226', NULL, NULL),
(136, 'zfgbxf', 'xfcbdfb', 'xcfbf', '1520507838', 'xcfbcf@cbxb.dfxgdx', NULL, 'xcfbcf@cbxb.dfxgdx', '5465434654', NULL, '', '$2y$10$kv8omlRBwEdcRKIodZlytOdQkphqfRf2exr3J4UMstIbNW5bjR56W', '26tQrYNznirpJ9VnjMZELDLiq63rkLJwGZgyExhL', 0, 2, 0, '2018-03-08 16:47:18', NULL, NULL, 1, '1', 1, 4570, '223.31.228.226', NULL, NULL),
(137, 'vvu', 'xyz', 'vhhj', '1520513167', 'nituguptaaan@gmail.com', NULL, 'nituguptaaan@gmail.com', '1234', NULL, '', '$2y$10$elARfgrUiY8/pb/OH49dzeP0/Sqb65ljwlPoWpWz8rV9z6MBXgcc6', 'umjgZ0ejoP2OzEAAJTAonS2Q1SAEMiIHQJVrdW5xDdgIZEocENpiryEXBeoV', 0, 2, 0, '2018-03-08 13:54:23', '2018-03-08 19:24:23', NULL, 1, '1', 1, 9195, '223.31.228.226', NULL, NULL),
(138, '', '', '', '1520513635', '', NULL, '', '', NULL, '', '$2y$10$X53WKLVCdJBaBdas89Kt9udq7NIwFZkZ50wJx8GiF72WQyCg1LJqq', 'dQQ6xZZ1W19SCFkUIEvxYTzCKXDe7hX9js0e1I9W', 0, 0, 0, '2018-03-08 18:23:55', NULL, NULL, 1, 'eeee864ace2c1c4c2212694c80ec48ac', 1, 6421, '223.31.228.226', NULL, NULL),
(139, 'gh', 'Dfgsd', 'sdgsd', '1520680361', 'guruajayakumar@gmail.com', NULL, 'guruajayakumar@gmail.com', 'sdgsd', NULL, NULL, '$2y$10$g7gwqVrR.iaR/HaQaQ//ouKSDXJT6VER4smaCt3zJJotRr.FaU2NW', 'K030yoR19tR93CCdVuxesQjF6Ip32Dg60ZxgqLkb', 0, 1, 0, '2018-03-10 16:42:41', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(140, 'Kisan kumar', 'Bhagabati enter', 'kisan1232', '1520681421', 'kisanpatra.92856@gmail.com', NULL, 'kisanpatra.92856@gmail.com', '2345234235', NULL, '', '$2y$10$rhOh7vK4PdZv5OfteaX35e3mryS3qxTOYVvt/4T4nkVhXoQZG07jW', 'QjBWVywML14qpQNDmFuFZJWKLlhHvqZdFDMud44gdkQXbsAA516zbCCVQm7G', 0, 2, 0, '2018-03-23 05:16:37', '2018-03-23 10:45:07', NULL, 1, '1', 4, 1128, '223.31.228.226', NULL, NULL),
(141, 'Kumar patra', 'Orimark job', '1234231', '1520681750', 'patrakisan.92@gmail.com', NULL, 'patrakisan.92@gmail.com', '1236471212', NULL, '', '$2y$10$w4tRHG9u43sNDu.tYDIOMuJMRsBXOTQuAqdrdFpWhokm3kyAOGusu', 'DNcfU9TpGe0ECYpIOpV5kWUzHYfVxRbShscTZCI8fcQsi8nSmCMorASKozFH', 0, 2, 0, '2018-03-30 13:15:57', '2018-03-30 18:45:48', NULL, 1, '1', 1, 8848, '223.31.228.226', NULL, NULL),
(142, 'sdfgfdg', 'fgdg', 'sdfgsd', '1520682493', 'chjghj@cfgbf.srtgrs', NULL, 'chjghj@cfgbf.srtgrs', 'gjh', NULL, NULL, '$2y$10$4fVyDqxTCyGSOwjffx90oeCl/OY13k1cISjZAriMbxRBnDl6tu27e', 'TeOHEWQxC15ECgudY0GEYzSPOpnr4Bp7uePylD76LSzPWskdqqDGMzibPOYP', 0, 1, 0, '2018-03-10 11:51:01', '2018-03-10 17:21:01', NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(143, 'test', 'test', 'test', '1520864103', 'testtest@gmail.com', NULL, 'testtest@gmail.com', '1236789', NULL, NULL, '$2y$10$SWi3/2kkrEO2khU02wkQH.JbA/TMIfkX6n4o5fGjLcepDYeH3CrSm', 'eXKYf0ZbPUWo8D5IyB04xOswBuCsjpCuRHw7x173', 0, 1, 0, '2018-03-12 14:15:51', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(144, 'test1', 'test1', '897979879', '1520920175', 'ww@gmail.com', NULL, 'ww@gmail.com', '87988696', NULL, NULL, '$2y$10$B0OSOtMbMgp5oFG4ksSSPOOF8lvV8lbNZ1rd2amV5O0xW9KFVW80i', 'fmANewZBx4DrzkGrokZpPzw8fmZ39ejQZ5c8bOiC', 0, 1, 0, '2018-03-13 11:19:36', NULL, NULL, 1, '1', 1, 1, '171.61.115.115', NULL, NULL),
(145, 'asdfds', 'asdfas', 'asdf', '1520928592', 'fvbdvbjb@dfvbdfh.fvrhg', NULL, 'fvbdvbjb@dfvbdfh.fvrhg', 'fdgvsgvsg', NULL, NULL, '$2y$10$b6u6AxMmwEW3y5V3c9MyyuCROG.SJwAGD.2gNaEvMECjvnaR9s6KO', 'qvNtpRfLnJQ6NtPLzAoRf5txjRMLYnDTh6KR7pm4', 0, 1, 0, '2018-03-13 13:39:52', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(146, 'erfeg', 'sergserg', 'sfgserg', '1520936897', 'fgsdrgqw@gdrgh.drtgt', NULL, 'fgsdrgqw@gdrgh.drtgt', '342623456', NULL, '', '$2y$10$16KBS8bJTH6s6lL7DV2tG.8GXswQ6egCPI8RqkExuyN1t83L..vF6', 'WN0SvEXX9caMBgHv8IDfp4s5oqz8cbzSHfGjZ1k2', 0, 1, 0, '2018-03-17 12:31:13', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(147, 'Kisan kumar', 'kisan pvt ltd', '78354', '1521782884', 'kisanpatra.92345678@gmail.com', NULL, 'kisanpatra.92345678@gmail.com', '7823627856', NULL, '', '$2y$10$UDnkVXBd8YhzwefsqT/u3u5y3/few2W15q0XL59faoFB20RM4AlBW', 'rSCQO0CNxl8x9qgfkSgT2ueWMW2kXShcFKNGtuNnRBs9EOhN5dbPV6jNNB7L', 0, 2, 0, '2018-03-29 13:08:52', '2018-03-29 17:52:02', NULL, 1, '1', 4, 1044, '223.31.228.226', NULL, NULL),
(148, 'soumya', 'tsutr', '1234567ABC', '1522144151', 'soumyakanta@thoughtspheres.com', NULL, 'soumyakanta@thoughtspheres.com', '9668664828', NULL, '', '$2y$10$AGvlX7rr9xfJIQIHn2JQgOJPWS9JwfbKSoQXuZA8FrKUHqicMqNZW', 'e5usxXa0wC5Meev7Jsc14orh3lL13ciRMiD2sjboAAs8EMOpIdVM3RHuNEeX', 0, 2, 0, '2018-03-27 14:43:53', '2018-03-27 20:13:53', NULL, 1, '1', 4, 4958, '157.41.155.125', NULL, NULL),
(150, 'Kisan', 'OrimaRK', 'kisan2347', '1522326194', 'kisan@fhj.snv', NULL, 'kisan@fhj.snv', '345465', NULL, '', '$2y$10$2ghAfC86JIY2xDzDYFn3r.iLGCjrBEuasjj2vHjx6cAeyA7cFPY9u', 'rEB0hVYpMSQ6GeMz6gmNjmXtTbgJZjsHEfbdaTik', 0, 2, 0, '2018-03-29 17:53:14', NULL, NULL, 1, '2b046ede01c8f942fa456ff88db41672', 1, 5761, '223.31.228.226', NULL, NULL),
(151, 'puru', 'test company', 'test001', '1522329039', 'kisanpatra.92@gmail.com', NULL, 'kisanpatra.92@gmail.com', '876452311', NULL, '', '$2y$10$WLv6v2RBUdN/fIpV8VjL8u6qyUhsENtSyLACpB8EXE36M8x1KKJ8W', '1lVJ8PWPVeRj77a1YgEBsuTUHATYWJcmOSlfuFPEnnPR6Ng5Q0m3ucAGJZeT', 0, 2, 0, '2018-04-11 14:44:00', '2018-04-11 20:14:00', NULL, 1, '1', 4, 9743, '157.41.237.152', NULL, 'dL_FS_DYd0g:APA91bGwcDwPmFooPYxvnhTIhcCwN_r5URC1EfCzXyDyh_Ned2DmUtFpSiY_zs-4FKA0AJQgMSKoNY_nj0YmroUHTbIrPeqy5aMLHtSrDCl8V2-8ktbojsP10w4FAv8gWEAVtR329bUU'),
(152, 'vxkfxcvfvcxv', 'cxvv', 'cxvcxv', '1522611001', 'cxvcxE@bfb.jfdbvj', NULL, 'cxvcxE@bfb.jfdbvj', 'rwecvc', NULL, NULL, '$2y$10$rEy6uDb1if84Q49T3nTX9e9qZ31fF8jXEqPnCbSsYcMEx0iQeZAgq', 'k85XDwZSk4cOAQI6wOUOaprQOFVNLbzl6qF23ywQ', 0, 1, 0, '2018-04-02 01:00:01', NULL, NULL, 1, '1', 1, 1, '157.41.223.225', NULL, NULL),
(153, 'vbcxcb', 'fbxc', 'zxfg', '1522655950', 'fgbdcg@dfg.dft', NULL, 'fgbdcg@dfg.dft', 'fgb', NULL, NULL, '$2y$10$pZafb31wN8NfINl8L4pRgu6q0j3mAnFnSI4ooWc6Ud0zaFjUnEiGC', '6vCpcSEBIoJvWjYmzXumge50T9DPs0VbXohZxo01', 0, 1, 0, '2018-04-02 13:29:10', NULL, NULL, 1, '1', 1, 1, '223.31.228.226', NULL, NULL),
(154, 'zdfv', 'xcfv', 'zxfv', '1522656532', 'zxf@fgv.dft', NULL, 'zxf@fgv.dft', 'zxf', NULL, NULL, '$2y$10$mUq9sUJuRkEvJq7467HB8eBm9P6QtagPlFGxANGayPRM8umE4Xkg6', '6vCpcSEBIoJvWjYmzXumge50T9DPs0VbXohZxo01', 0, 1, 0, '2018-04-02 08:09:08', NULL, NULL, 1, 'e42b003231041b473a7228323b0ac838', 1, 1, '223.31.228.226', NULL, NULL),
(155, 'Minaketan', 'Thoughtspheres', '11111111', '1522824764', 'siluganthia@gmail.com', NULL, 'siluganthia@gmail.com', '8249245811', NULL, '', '$2y$10$jTJhyQka1.kICXkpeu80cuK80jSuSuB8ajciSL1y43oMzGbewrOOO', '61rvkUmdXyPgZvP0CunDjOQmgeMVE166PwviZQ1dsDF7XukSU0U4WXdbHROV', 0, 2, 0, '2018-04-07 11:43:16', '2018-04-07 17:13:16', NULL, 1, '1', 1, 2807, '223.31.228.226', NULL, 'dr2e5aMU10M:APA91bH3SNYRvQdhhKCSEzOQghLVYPzuvbwDaR9P2uo-_WexLBlxD2sh3CwocOIKQR-WGbw_z58t5aOsfDHszjJrqyjQZnzKy5W-fldp_2B871iCZKGm8IQzCQE37IIL8YSIpdT66sGS'),
(156, 'Minaketan', 'Thoughtspheres', '22222222', '1522827318', 'silu1ganthia@gmail.com', NULL, 'silu1ganthia@gmail.com', '9206771121', NULL, '', '$2y$10$s/1NCCDxaolj2H596o7Z9u3G6URFaeLgl6tFMLPFySW0G/Inb7FdW', 'O61rz4yw8xwrMWrvw47tOEQ5VqTvZpKKqLruHVCk', 0, 1, 0, '2018-04-07 11:17:39', '2018-04-07 16:47:39', NULL, 1, '1', 1, 1080, '223.31.228.226', NULL, 'dr2e5aMU10M:APA91bH3SNYRvQdhhKCSEzOQghLVYPzuvbwDaR9P2uo-_WexLBlxD2sh3CwocOIKQR-WGbw_z58t5aOsfDHszjJrqyjQZnzKy5W-fldp_2B871iCZKGm8IQzCQE37IIL8YSIpdT66sGS'),
(157, 'Javed', 'Javed LLC', '12345', '1523090861', 'aquibjaved1688@gmail.com', NULL, 'aquibjaved1688@gmail.com', '564089456', NULL, '', '$2y$10$ipMKTbBlTGJfpA.XvdkKeeO86LivY4Ovo1kewahgtps.H/JaSRYTi', 'RKTG9HUJSpf9ZZ5NAmMBmBX8JKuQoapjlX2jy7n7', 0, 1, 0, '2018-04-07 14:17:41', NULL, NULL, 1, 'df1da4c58562267518961e642e72481a', 1, 7747, '83.110.151.60', NULL, NULL),
(158, 'Rakhee', 'Green Land', '123456', '1524859253', 'Rakheedas75@gmail.com', NULL, 'Rakheedas75@gmail.com', '9538225755', NULL, '', '$2y$10$LeCSZ.YRkMG9aWuRHXRmXuD.1pRfP730ktg0ziCVCuuWGB3F1CSPq', 'wWXjd8EthuytB4mC2iVcuOQF336PNbOBam4VfMcU', 0, 1, 0, '2018-04-28 01:30:53', NULL, NULL, 1, 'bcec3d6d265c7128f46d9f07cd5023d7', 1, 5875, '122.167.175.171', NULL, NULL),
(159, 'admin', 'tgi', '123651', '1528709690', 'rohan.savara@clicktgi.net', NULL, 'rohan.savara@clicktgi.net', '1233565', NULL, '12132', '$2y$10$cjqbS2ddguen48D3gzFzMeQf6ycM8ro3LKp3aCiaNc/utTFSUv7HG', 'buH9XMrc1uiRZx3lbT2P1wsOG78supdmtGMOjgGS', 0, 3, 0, '2018-06-11 09:48:01', NULL, NULL, 1, '1', 1, 1, '88.98.208.100', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sps__users_address`
--

CREATE TABLE `sps__users_address` (
  `id` int(122) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `user_id` int(12) NOT NULL,
  `city` varchar(50) NOT NULL,
  `address` text,
  `state` int(4) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `is_verify` tinyint(1) DEFAULT '0' COMMENT '1->Accept,2->Reject',
  `verify_date` datetime DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__users_address`
--

INSERT INTO `sps__users_address` (`id`, `name`, `user_id`, `city`, `address`, `state`, `country`, `zip`, `latitude`, `longitude`, `status`, `is_primary`, `is_verify`, `verify_date`, `add_date`) VALUES
(20, 'Head office', 4, '14', 'Vill-Brahmanput,PO-Routpada\r\nVia-Begunia\r\nKhurda', 3, '1', '751064', NULL, NULL, 1, 0, 0, '2017-10-16 16:46:59', '2017-12-19 11:50:46'),
(22, 'Bhubaneswar branch', 58, '14', 'Jayadev Vihar,Bhubaneswar', 3, '1', '751013', NULL, NULL, 1, 0, 1, '2017-10-30 11:30:51', '2017-12-19 11:50:46'),
(23, 'test', 58, '14', 'tets adreesss ', 3, '1', '789456', NULL, NULL, 1, 0, 1, '2017-11-06 19:12:39', '2017-12-19 11:50:46'),
(25, 'Main branch', 40, '14', 'Rasulgarh', 3, '1', '751024', NULL, NULL, 1, 1, 1, '2017-11-13 12:39:22', '2017-12-19 11:50:46'),
(26, 'Jayadev vihar', 60, '15', 'BBSR', 3, '1', '435345', NULL, NULL, 1, 0, 1, '2017-11-13 16:11:15', '2017-12-19 11:51:14'),
(27, 'Main office', 68, '14', '12345', 3, '1', '123456', NULL, NULL, 1, 0, 1, '2017-11-16 12:34:33', '2017-12-19 11:50:46'),
(28, 'Main office', 69, '14', '12345', 3, '1', '123123', NULL, NULL, 1, 0, 1, '2017-11-16 12:46:51', '2017-12-19 11:50:46'),
(29, 'Main office', 71, '14', '12345', 3, '1', '123123', NULL, NULL, 1, 0, 1, '2017-11-16 12:48:17', '2017-12-19 11:50:46'),
(30, 'Main office', 72, '14', '12345', 3, '1', '123123', NULL, NULL, 1, 0, 1, '2017-11-16 12:53:42', '2017-12-19 11:50:46'),
(31, 'Main office', 73, '14', 'jayadev vihar', 3, '1', '751013', NULL, NULL, 1, 0, 1, '2017-11-16 16:04:13', '2017-12-19 11:50:46'),
(32, 'new User', 79, '15', 'jayadev bihar', 3, '1', 'xfgfdgfg', NULL, NULL, 1, 0, 1, '2017-11-16 17:59:27', '2017-12-19 11:51:14'),
(33, 'Kanas', 81, '14', 'Puri Lane', 3, '1', '752064', NULL, NULL, 1, 0, 1, '2017-12-01 15:33:21', '2017-12-19 11:50:46'),
(37, 'edfe', 86, '14', 'asef', 3, '1', '23414124', NULL, NULL, 1, 0, 1, '2017-12-08 13:11:49', '2017-12-19 11:50:46'),
(44, 'Main branch', 91, '16', 'Abu dhabi', 3, '1', '', NULL, NULL, 1, 0, 1, '2017-12-28 18:36:17', '2017-12-28 13:06:17'),
(45, 'khandagiri', 59, '16', 'aiginia', 3, '1', '787990', NULL, NULL, 1, 1, 1, '2018-01-02 15:35:09', '2018-01-02 10:05:09'),
(47, 'test', 92, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-16 16:11:28', '2018-01-16 10:41:28'),
(48, 'test', 93, '14', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-16 17:31:29', '2018-01-16 12:01:29'),
(49, 'addr', 94, '16', '4564', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-17 13:23:49', '2018-01-17 07:53:49'),
(50, 'test', 95, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-17 16:05:50', '2018-01-17 10:35:50'),
(51, 'zone1', 96, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-17 18:48:10', '2018-01-17 13:18:10'),
(52, 'Bhubaneswar branch', 97, '16', 'Jayadev Vihar,Bhubaneswar', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-17 19:23:20', '2018-01-17 13:53:20'),
(53, 'sdf sdf', 98, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-17 19:25:19', '2018-01-17 13:55:19'),
(54, 'testpuru', 99, '5', 'test', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 13:17:31', '2018-01-18 07:47:31'),
(55, 'Bhubaneswar branch', 100, '16', 'Jayadev Vihar,Bhubaneswar', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 15:26:06', '2018-01-18 09:56:06'),
(56, 'Bhubaneswar branch', 104, '16', 'Jayadev Vihar,Bhubaneswar', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 15:51:20', '2018-01-18 10:21:20'),
(57, 'zone1', 105, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 15:55:16', '2018-01-18 10:25:16'),
(58, 'sd', 106, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 15:59:05', '2018-01-18 10:29:05'),
(59, 'test', 108, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-18 20:13:11', '2018-01-18 14:43:11'),
(60, 'zone1', 109, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 12:16:55', '2018-01-19 06:46:55'),
(61, 'Bhubaneswar branch', 110, '16', 'Jayadev Vihar,Bhubaneswar', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 12:58:56', '2018-01-19 07:28:56'),
(63, '2345', 111, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 16:33:53', '2018-01-19 11:03:53'),
(64, 'ter', 112, '5', 'eee', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 16:58:02', '2018-01-19 11:28:02'),
(65, 'asd', 113, '16', 'asd', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 17:00:20', '2018-01-19 11:30:20'),
(66, 'zone1', 114, '5', 'bbsr', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-01-19 17:05:56', '2018-01-19 11:35:56'),
(69, 'kisan', 87, '6', 'HIG 45,Jayadev vihar', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-02-05 13:50:06'),
(70, 'Kisan office 2', 87, '6', 'Acharya bihar', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-02-05 13:52:35'),
(73, 'bbsr', 117, '16', 'bbsr', 3, '1', '', NULL, NULL, 1, 1, 1, '2018-02-06 16:34:41', '2018-02-06 11:04:41'),
(74, 'test 1', 59, '5', 'tes address', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-02-06 18:37:46', '2018-02-06 13:07:46'),
(75, 'nagen', 84, '6', 'BBSR', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-02-09 10:49:56', '2018-02-09 05:19:56'),
(76, 'Rai', 85, '6', 'Bhubaneswar, Odisha, India', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-02-10 16:29:28'),
(77, 'Promo', 118, '6', 'Bhubaneswar, Odisha, India', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-02-10 16:36:34'),
(78, 'kisan', 82, '10', 'BBSR', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-02-20 18:00:08'),
(79, 'Nagen Biswal', 84, '9', 'my office address', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-03-01 11:33:27'),
(80, 'test2', 82, '16', 'test test ', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-01 11:39:29', '2018-03-01 11:39:29'),
(81, 'patra', 127, '5', 'adress', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-05 19:25:07', '2018-03-05 19:25:07'),
(82, 'Kisan', 129, '6', 'Banapur', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-06 13:30:38', '2018-03-06 13:30:38'),
(83, 'Kisan', 130, '9', 'Khurda', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-06 14:13:17', '2018-03-06 14:13:17'),
(84, 'xftgh', 131, '12', 'xtfh', 4, '1', '', NULL, NULL, 1, 0, 1, '2018-03-06 14:23:57', '2018-03-06 14:23:57'),
(85, 'xfth', 132, '9', 'sdfth', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-06 15:29:49', '2018-03-06 15:29:49'),
(86, 'kisan', 134, '10', 'dfgs', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-07 15:40:42', '2018-03-07 15:40:42'),
(89, 'gbxfb', 134, '6', 'xfbdfb', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-08 16:44:50', '2018-03-08 16:44:50'),
(90, 'fgbcfbcg', 136, '6', 'fgcbfgcbnfg', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-08 16:47:47', '2018-03-08 16:47:47'),
(91, 'gsdfg', 139, '6', '1', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-10 16:42:41', '2018-03-10 16:42:41'),
(92, 'xfh', 141, '6', 'xh', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-10 17:16:21', '2018-03-10 17:16:21'),
(93, 'zdrtg', 142, '6', '4drgd', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-10 17:18:13', '2018-03-10 17:18:13'),
(94, 'test', 143, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-12 19:45:03', '2018-03-12 19:45:03'),
(95, 'dubai', 144, '10', 'reitor ', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-13 11:19:36', '2018-03-13 11:19:36'),
(96, 'dfgb', 145, '6', 'sdgbsd', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-13 13:39:52', '2018-03-13 13:39:52'),
(98, 'rgrtg', 140, '6', 'sergr', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-15 18:36:36', '2018-03-15 18:36:36'),
(99, 'kisan', 141, '10', 'bbsr', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-03-26 11:00:30'),
(100, 'patra', 141, '10', 'bbsr', 6, '1', '', NULL, NULL, 1, 0, 1, NULL, '2018-03-26 11:02:00'),
(101, 'kisan', 147, '6', 'bbsr', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-27 12:36:40', '2018-03-27 12:36:40'),
(102, 'test', 151, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-29 18:50:35', '2018-03-29 18:50:35'),
(103, 'test', 26, '16', 'test', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-29 19:00:34', '2018-03-29 19:00:34'),
(104, 'test2', 26, '16', 'test2', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-29 19:01:03', '2018-03-29 19:01:03'),
(105, 'test', 151, '15', 'gfgdf', 3, '1', '', NULL, NULL, 1, 0, 1, '2018-03-29 20:36:46', '2018-03-29 20:36:46'),
(106, 'wefr', 141, '9', 'fer', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-03-30 19:14:35', '2018-03-30 19:14:35'),
(107, 'vfd', 152, '6', 'fgvd', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-02 01:00:01', '2018-04-02 01:00:01'),
(108, 'fgs', 26, '6', 'sdf', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-02 13:12:03', '2018-04-02 13:12:03'),
(109, 'xfgh', 153, '6', 'xfgh', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-02 13:29:10', '2018-04-02 13:29:10'),
(110, 'x', 154, '6', 'thoughtsphere.com,SPECIMENS Building,HIG-45,II Floor,Jayadev Vihar,Bhubaneswar', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-02 13:38:52', '2018-04-02 13:38:52'),
(111, 'fgxdg', 26, '10', 'zxfg', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-02 15:18:35', '2018-04-02 15:18:35'),
(112, 'bbsr', 151, '8', 'jayadev Bihar', 6, '1', '', NULL, NULL, 1, 0, 1, '2018-04-04 17:53:34', '2018-04-04 17:53:34');

-- --------------------------------------------------------

--
-- Table structure for table `sps__users_bank_details`
--

CREATE TABLE `sps__users_bank_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(150) NOT NULL,
  `account_holder_name` varchar(100) DEFAULT NULL,
  `account_no` varchar(150) NOT NULL,
  `ifsc_code` varchar(100) NOT NULL,
  `cancel_cheque` varchar(100) DEFAULT NULL COMMENT 'cancel check image',
  `is_verify` tinyint(4) DEFAULT '0' COMMENT '1->verify 2->reject',
  `add_date` timestamp NULL DEFAULT NULL,
  `verify_date` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__users_bank_details`
--

INSERT INTO `sps__users_bank_details` (`id`, `user_id`, `bank_name`, `account_holder_name`, `account_no`, `ifsc_code`, `cancel_cheque`, `is_verify`, `add_date`, `verify_date`) VALUES
(1, 18, 'SBI', NULL, '12121212121212', '121234', 'mangal.png', 2, '2017-10-05 11:02:38', '2017-10-06 11:24:44'),
(3, 18, 'SBI', NULL, '34534343434', 'asd', NULL, 0, '2017-10-09 11:26:45', NULL),
(5, 19, 'SBI', NULL, '200158854480', 'SBIN0000116', NULL, 0, '2017-10-16 06:25:36', NULL),
(6, 26, 'SBI', 'Purusottama Sahoo', '1234567890', '45Tyus', 'Screenshot_from_2017-09-21_15-11-50.png', 1, '2017-10-21 06:53:03', '2017-10-21 10:42:22'),
(7, 26, 'SBI', 'purusottama sahoo', '8989898989', 'tetetet', NULL, 0, '2017-10-23 12:56:06', NULL),
(8, 35, 'SBI', 'TS ', '200158854470', 'SBIN0000116', NULL, 1, '2017-10-30 06:00:14', '2017-10-30 06:01:11'),
(9, 59, 'SBI', 'Rajesh', '3482754823657842', 'raj123456', NULL, 1, '2017-11-13 10:45:43', '2018-01-02 10:07:09'),
(10, 40, 'SBI', 'ranjita', '2011555663434', 'SBIN0000116', NULL, 1, '2017-11-22 13:48:12', '2017-11-22 13:48:44'),
(11, 87, 'SBI', 'kisan', '898502234', 'sbi12134', NULL, 1, '2017-12-21 07:33:36', '2018-02-07 08:01:59'),
(12, 59, 'SBI', 'purusottama', '2013429987', 'Alla2245246', NULL, 0, '2018-01-02 10:05:38', NULL),
(13, 58, 'SBI', 'kisan', '17358971358973489', 'Sbi3453', NULL, 1, '2018-01-19 13:53:10', '2018-01-19 13:54:04'),
(14, 85, 'SBI', 'Rai', '543643563456', 'SBI02343243', NULL, 1, '2018-02-10 16:27:47', '2018-02-10 16:31:17'),
(15, 118, 'SBI', 'Promo', '5498324723454', 'SBI0234324235', NULL, 1, '2018-02-10 16:36:11', '2018-02-10 16:37:24'),
(16, 117, 'SBI', 'Raju1346', '435326457345', 'SBI423633', NULL, 0, '2018-02-21 12:54:01', NULL),
(17, 84, 'SBI', 'Nagen Biswal', '11223344550', 'SBIN09779', NULL, 0, '2018-03-01 11:31:25', NULL),
(18, 133, 'SBI', 'kisan', '23467824356', '2545434', NULL, 1, '2018-03-06 20:04:07', '2018-03-06 20:13:21'),
(22, 140, 'SBI', 'kisan', '7463767345', '58234678562347856', NULL, 1, '2018-03-10 17:38:01', '2018-03-10 17:40:07'),
(24, 140, 'SBI', 'fghdf', '5423565436', 'dfghbf', NULL, 0, '2018-03-16 12:29:48', NULL),
(26, 147, 'SBI', 'kisan', '7463767345', 'SBIN0000116', NULL, 1, '2018-03-23 11:52:54', '2018-03-23 11:58:45'),
(27, 148, 'SBI', 'Soumya Mohanty', '2234324543545', 'IFSC234', NULL, 1, '2018-03-27 17:00:36', '2018-03-27 17:29:38'),
(28, 151, 'SBI', 'purusottama', '8989897654345678', '098766', NULL, 1, '2018-03-29 18:48:52', '2018-03-29 18:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `sps__users_category_like`
--

CREATE TABLE `sps__users_category_like` (
  `user_id` int(123) NOT NULL,
  `category_id` int(123) NOT NULL,
  `subcategory_id` int(123) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1->buying wish,2->selling wish',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__users_category_like`
--

INSERT INTO `sps__users_category_like` (`user_id`, `category_id`, `subcategory_id`, `status`, `add_date`) VALUES
(40, 1, NULL, 2, '2017-11-22 13:32:38'),
(40, 3, NULL, 2, '2017-11-22 13:32:38'),
(79, 1, NULL, 1, '2017-11-22 13:43:33'),
(79, 3, NULL, 1, '2017-11-22 13:43:33'),
(26, 1, NULL, 1, '2017-11-29 06:41:56'),
(26, 3, NULL, 1, '2017-11-29 06:41:56'),
(26, 6, NULL, 1, '2017-11-29 06:41:56'),
(26, 7, NULL, 1, '2017-11-29 06:41:56'),
(82, 6, NULL, 2, '2017-12-07 11:23:56'),
(82, 7, NULL, 2, '2017-12-07 11:23:56'),
(86, 3, NULL, 1, '2017-12-08 07:36:23'),
(87, 1, NULL, 2, '2018-01-05 14:27:43'),
(87, 3, NULL, 2, '2018-01-05 14:27:43'),
(87, 6, NULL, 2, '2018-01-05 14:27:43'),
(26, 1, NULL, 2, '2018-01-18 07:39:38'),
(26, 3, NULL, 2, '2018-01-18 07:39:38'),
(26, 7, NULL, 2, '2018-01-18 07:39:38'),
(26, 8, NULL, 2, '2018-01-18 07:39:38'),
(26, 9, NULL, 2, '2018-01-18 07:39:38'),
(26, 10, NULL, 2, '2018-01-18 07:39:38'),
(26, 11, NULL, 2, '2018-01-18 07:39:38'),
(117, 6, NULL, 2, '2018-02-21 12:54:42'),
(59, 11, NULL, 2, '2018-02-26 13:31:52'),
(84, 1, NULL, 1, '2018-03-01 11:34:18'),
(84, 3, NULL, 1, '2018-03-01 11:34:18'),
(84, 6, NULL, 1, '2018-03-01 11:34:18'),
(84, 8, NULL, 1, '2018-03-01 11:34:18'),
(84, 9, NULL, 1, '2018-03-01 11:34:18'),
(84, 7, NULL, 2, '2018-03-01 14:03:40'),
(58, 6, NULL, 2, '2018-03-05 17:05:39'),
(58, 9, NULL, 2, '2018-03-05 17:05:39'),
(82, 3, NULL, 1, '2018-03-05 18:43:35'),
(82, 7, NULL, 1, '2018-03-05 18:43:35'),
(128, 11, NULL, 2, '2018-03-06 12:12:50'),
(128, 9, NULL, 1, '2018-03-06 12:15:48'),
(133, 15, NULL, 2, '2018-03-06 17:06:23'),
(129, 11, NULL, 1, '2018-03-10 12:51:26'),
(141, 10, NULL, 1, '2018-03-23 11:33:31'),
(141, 11, NULL, 1, '2018-03-23 11:33:31'),
(141, 15, NULL, 1, '2018-03-23 11:33:31'),
(147, 10, NULL, 2, '2018-03-27 19:19:51'),
(151, 10, NULL, 2, '2018-04-05 12:14:38'),
(151, 11, NULL, 2, '2018-04-05 12:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `sps__user_request_product`
--

CREATE TABLE `sps__user_request_product` (
  `id` int(123) NOT NULL,
  `request_product_id` int(123) NOT NULL,
  `user_id` int(123) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__user_settlement_address`
--

CREATE TABLE `sps__user_settlement_address` (
  `id` int(11) NOT NULL,
  `bid_id` int(11) NOT NULL,
  `ordernumber` int(11) NOT NULL,
  `selling_type` int(11) NOT NULL COMMENT '1-> fixed 2-> auction',
  `user_id` int(11) NOT NULL COMMENT 'buyerId',
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bidding_price` float(10,2) NOT NULL,
  `delivery_price` float(10,2) DEFAULT NULL,
  `total_price` float(10,2) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `address` text,
  `status` smallint(1) DEFAULT NULL,
  `is_payment` tinyint(4) DEFAULT NULL COMMENT '0->not paid,1->payment done',
  `comment` text,
  `add_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__user_settlement_address`
--

INSERT INTO `sps__user_settlement_address` (`id`, `bid_id`, `ordernumber`, `selling_type`, `user_id`, `product_id`, `quantity`, `bidding_price`, `delivery_price`, `total_price`, `address_id`, `address`, `status`, `is_payment`, `comment`, `add_date`, `update_date`) VALUES
(1, 1, 5959162, 1, 59, 5, 10, 68.00, 35.00, 715.00, 45, '<b>khandagiri</b><br/>aiginia<br/>Al Ain<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-06 18:40:59', '2018-02-06 18:40:59'),
(2, 2, 2653620, 1, 26, 5, 5, 68.00, 15.00, 355.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-08 11:45:30', '2018-02-08 11:45:30'),
(3, 2, 2653620, 1, 26, 5, 5, 68.00, 12.50, 352.50, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah', 1, 0, 'buy product', '2018-02-08 11:45:30', '2018-02-08 11:45:30'),
(4, 3, 5836612, 1, 58, 3, 6, 45.00, 0.00, 270.00, 22, '<b>Bhubaneswar branch</b><br/>Jayadev Vihar,Bhubaneswar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-08 15:48:39', '2018-02-08 15:48:39'),
(5, 4, 5831822, 1, 58, 3, 10, 45.00, 0.00, 450.00, 22, '<b>Bhubaneswar branch</b><br/>Jayadev Vihar,Bhubaneswar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-08 15:51:01', '2018-02-08 15:51:01'),
(6, 5, 8269159, 1, 82, 6, 5, 195.00, 0.00, 975.00, 35, '<b>Jayadev bihar</b><br/>Jayadev bihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-08 15:58:20', '2018-02-08 15:58:20'),
(7, 6, 8436733, 1, 84, 3, 5, 45.00, 0.00, 225.00, 75, '<b>nagen</b><br/>BBSR<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-02-09 10:50:02', '2018-02-09 10:50:02'),
(8, 7, 11737741, 1, 117, 3, 10, 45.00, 0.00, 450.00, 73, '<b>bbsr</b><br/>bbsr<br/>Al Ain<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-09 10:50:29', '2018-02-09 10:50:29'),
(9, 8, 8232184, 1, 82, 3, 7, 45.00, 0.00, 315.00, 35, '<b>Jayadev bihar</b><br/>Jayadev bihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-09 10:50:51', '2018-02-09 10:50:51'),
(12, 10, 2655134, 1, 26, 5, 5, 68.00, 15.00, 355.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:48:17', '2018-02-09 18:48:17'),
(13, 10, 2655134, 1, 26, 5, 5, 68.00, 12.50, 352.50, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:48:17', '2018-02-09 18:48:17'),
(14, 11, 2652188, 1, 26, 5, 5, 68.00, 15.00, 355.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:55:07', '2018-02-09 18:55:07'),
(15, 11, 2652188, 1, 26, 5, 5, 68.00, 12.50, 352.50, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:55:07', '2018-02-09 18:55:07'),
(16, 12, 2653563, 1, 26, 5, 5, 68.00, 15.00, 355.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:55:41', '2018-02-09 18:55:41'),
(17, 12, 2653563, 1, 26, 5, 5, 68.00, 12.50, 352.50, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah<br/>UAE', 1, 0, 'buy product', '2018-02-09 18:55:41', '2018-02-09 18:55:41'),
(18, 13, 2658808, 1, 26, 5, 2, 68.00, 6.00, 142.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-09 20:08:02', '2018-02-09 20:08:02'),
(19, 14, 2658671, 1, 26, 5, 2, 68.00, 6.00, 142.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-09 20:11:06', '2018-02-09 20:11:06'),
(20, 14, 2658671, 1, 26, 5, 1, 68.00, 0.00, 68.00, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah', 1, 0, 'buy product', '2018-02-09 20:11:06', '2018-02-09 20:11:06'),
(21, 15, 2631404, 1, 26, 3, 4, 45.00, 12.00, 192.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-09 20:16:25', '2018-02-09 20:16:25'),
(22, 16, 82139799, 1, 82, 13, 44, 75.00, 132.00, 3432.00, 35, '<b>Jayadev bihar</b><br/>Jayadev bihar<br/>Central Capital District<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-02-10 18:47:32', '2018-02-10 18:47:32'),
(23, 17, 59277491, 1, 59, 27, 5, 58.00, 0.00, 290.00, 45, '<b>khandagiri</b><br/>aiginia<br/>Al Ain<br/>Abu Dhabi', 1, 0, 'buy product', '2018-02-12 11:32:18', '2018-02-12 11:32:18'),
(24, 18, 8289931, 1, 82, 8, 25, 85.00, 75.00, 2200.00, 35, '<b>Jayadev bihar</b><br/>Jayadev bihar<br/>Central Capital District<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-02-13 11:29:56', '2018-02-13 11:29:56'),
(25, 19, 8252728, 1, 82, 5, 2, 68.00, 0.00, 136.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-21 11:47:56', '2018-02-21 11:47:56'),
(26, 20, 8254858, 1, 82, 5, 2, 68.00, 0.00, 136.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-21 17:08:55', '2018-02-21 17:08:55'),
(27, 21, 8259918, 1, 82, 5, 2, 68.00, 0.00, 136.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-21 17:10:51', '2018-02-21 17:10:51'),
(28, 22, 8259896, 1, 82, 5, 1, 68.00, 1.50, 69.50, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-21 17:28:46', '2018-02-21 17:28:46'),
(29, 23, 8252566, 1, 82, 5, 1, 68.00, 1.50, 69.50, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-21 17:32:40', '2018-02-21 17:32:40'),
(30, 24, 8254447, 1, 82, 5, 3, 68.00, 0.00, 204.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-26 17:40:25', '2018-02-26 17:40:25'),
(31, 25, 8258087, 1, 82, 5, 2, 68.00, 0.00, 136.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-26 18:34:15', '2018-02-26 18:34:15'),
(32, 26, 8259773, 1, 82, 5, 2, 68.00, 0.00, 136.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-26 18:43:52', '2018-02-26 18:43:52'),
(33, 27, 82101086, 2, 82, 10, 6, 45.50, 0.00, 273.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-02-27 17:12:46', '2018-02-27 17:12:46'),
(34, 28, 5968363, 1, 59, 6, 20, 195.00, 70.00, 3970.00, 45, '<b>khandagiri</b><br/>aiginia<br/>Al Ain<br/>Abu Dhabi', 1, 0, 'buy product', '2018-03-01 10:53:22', '2018-03-01 10:53:22'),
(35, 29, 8254582, 1, 82, 5, 10, 68.00, 15.00, 695.00, 78, '<b>kisan</b><br/>BBSR<br/>Dubai Marina<br/>Dubai', 1, 0, 'buy product', '2018-03-01 11:40:31', '2018-03-01 11:40:31'),
(36, 29, 8254582, 1, 82, 5, 80, 68.00, 0.00, 5440.00, 80, '<b>test2</b><br/>test test <br/>Al Ain<br/>Abu Dhabi', 1, 0, 'buy product', '2018-03-01 11:40:31', '2018-03-01 11:40:31'),
(37, 30, 84511480, 2, 84, 51, 120, 63.50, 0.00, 7620.00, 75, '<b>nagen</b><br/>BBSR<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-01 13:07:40', '2018-03-01 13:07:40'),
(38, 30, 84511480, 2, 84, 51, 100, 63.50, 0.00, 6350.00, 79, '<b>Nagen Biswal</b><br/>my office address<br/>Burj al Arab<br/>Dubai', 1, 0, 'buy product', '2018-03-01 13:07:40', '2018-03-01 13:07:40'),
(39, 31, 12753902, 1, 127, 5, 1, 68.00, 2.50, 70.50, 81, '<b>patra</b><br/>adress<br/>Bur Dubai<br/>Dubai', 1, 0, 'buy product', '2018-03-05 19:25:22', '2018-03-05 19:25:22'),
(40, 32, 12965297, 1, 129, 6, 2, 195.00, 0.00, 390.00, 82, '<b>Kisan</b><br/>Banapur<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-06 13:33:05', '2018-03-06 13:33:05'),
(41, 33, 13055799, 1, 130, 5, 1, 68.00, NULL, 68.00, 83, '<b>Kisan</b><br/>Khurda<br/>Burj al Arab<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-06 14:13:47', '2018-03-06 14:13:47'),
(42, 34, 13153051, 1, 131, 5, 1, 68.00, NULL, 68.00, 84, '<b>xftgh</b><br/>xtfh<br/>Sharjah<br/>Sharjah<br/>UAE', 1, 0, 'buy product', '2018-03-06 14:23:59', '2018-03-06 14:23:59'),
(43, 35, 13263654, 1, 132, 6, 1, 195.00, NULL, 195.00, 85, '<b>xfth</b><br/>sdfth<br/>Burj al Arab<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-06 15:29:53', '2018-03-06 15:29:53'),
(44, 36, 12962202, 1, 129, 6, 1, 195.00, 0.00, 195.00, 82, '<b>Kisan</b><br/>Banapur<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-06 20:37:59', '2018-03-06 20:37:59'),
(45, 37, 129563501, 1, 129, 56, 2, 45.00, 0.00, 90.00, 82, '<b>Kisan</b><br/>Banapur<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-07 13:12:15', '2018-03-07 13:12:15'),
(46, 38, 13464277, 1, 134, 6, 1, 195.00, NULL, 195.00, 86, '<b>kisan</b><br/>dfgs<br/>Dubai Marina<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-07 15:40:46', '2018-03-07 15:40:46'),
(47, 39, 129541328, 2, 129, 54, 3, 65.50, 0.00, 196.50, 82, '<b>Kisan</b><br/>Banapur<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-10 13:00:26', '2018-03-10 13:00:26'),
(48, 40, 13921551, 1, 139, 2, 1, 65.00, NULL, 65.00, 91, '<b>gsdfg</b><br/>1<br/>Deira<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-10 16:42:46', '2018-03-10 16:42:46'),
(49, 41, 12933846, 1, 129, 3, 5, 45.00, 0.00, 225.00, 82, '<b>Kisan</b><br/>Banapur<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-10 16:53:14', '2018-03-10 16:53:14'),
(50, 42, 142266742, 1, 142, 26, 5, 58.00, NULL, 290.00, 93, '<b>zdrtg</b><br/>4drgd<br/>Deira<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-10 17:18:18', '2018-03-10 17:18:18'),
(51, 43, 14132029, 1, 141, 3, 1, 45.00, 0.00, 45.00, 92, '<b>xfh</b><br/>xh<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-12 14:57:11', '2018-03-12 14:57:11'),
(52, 44, 141588503, 1, 141, 58, 1, 65.00, 0.00, 65.00, 92, '<b>xfh</b><br/>xh<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-12 17:49:26', '2018-03-12 17:49:26'),
(53, 45, 141586873, 2, 141, 58, 1, 60.50, 0.00, 60.50, 92, '<b>xfh</b><br/>xh<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-12 18:52:39', '2018-03-12 18:52:39'),
(54, 46, 14332408, 1, 143, 3, 1, 45.00, NULL, 45.00, 94, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-03-12 19:45:12', '2018-03-12 19:45:12'),
(55, 47, 14426825, 1, 144, 2, 3, 65.00, NULL, 195.00, 95, '<b>dubai</b><br/>reitor <br/>Dubai Marina<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-13 11:20:13', '2018-03-13 11:20:13'),
(56, 48, 2622246, 1, 26, 2, 1, 65.00, 3.00, 68.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-03-13 11:37:24', '2018-03-13 11:37:24'),
(57, 48, 2622246, 1, 26, 2, 1, 65.00, 2.50, 67.50, 19, '<b>Head office1</b><br/>Jayadev Vihar,Bhubaneswar done changes<br/>Sharjah<br/>Sharjah', 1, 0, 'buy product', '2018-03-13 11:37:24', '2018-03-13 11:37:24'),
(58, 48, 2622246, 1, 26, 2, 1, 65.00, 3.00, 68.00, 46, '<b>testpuru</b><br/>address one<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-03-13 11:37:24', '2018-03-13 11:37:24'),
(59, 49, 14524731, 1, 145, 2, 1, 65.00, NULL, 65.00, 96, '<b>dfgb</b><br/>sdgbsd<br/>Deira<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-13 13:39:56', '2018-03-13 13:39:56'),
(60, 50, 14639935, 1, 146, 3, 1, 45.00, NULL, 45.00, 97, '<b>gdrg</b><br/>drgd<br/>Dubai Marina<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-03-13 15:58:22', '2018-03-13 15:58:22'),
(61, 51, 2639329, 1, 26, 3, 1, 45.00, 3.00, 48.00, 18, '<b>branch</b><br/>Acharya Vihar<br/>Central Capital District<br/>Abu Dhabi', 1, 0, 'buy product', '2018-03-14 11:16:00', '2018-03-14 11:16:00'),
(62, 52, 14033448, 1, 140, 3, 1, 45.00, 0.00, 45.00, 98, '<b>rgrtg</b><br/>sergr<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-15 18:37:22', '2018-03-15 18:37:22'),
(63, 53, 14024429, 1, 140, 2, 1, 65.00, 0.00, 65.00, 98, '<b>rgrtg</b><br/>sergr<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-15 18:42:47', '2018-03-15 18:42:47'),
(64, 54, 14031442, 1, 140, 3, 1, 45.00, 0.00, 45.00, 98, '<b>rgrtg</b><br/>sergr<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-15 18:45:47', '2018-03-15 18:45:47'),
(65, 55, 14035727, 1, 140, 3, 1, 45.00, 0.00, 45.00, 98, '<b>rgrtg</b><br/>sergr<br/>Deira<br/>Dubai', 1, 0, 'buy product', '2018-03-21 14:47:58', '2018-03-21 14:47:58'),
(66, 56, 141632193, 1, 141, 63, 1, 45.00, 0.00, 45.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-23 15:57:30', '2018-03-23 15:57:30'),
(67, 57, 14128838, 1, 141, 2, 2, 65.00, 0.00, 130.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-26 11:03:53', '2018-03-26 11:03:53'),
(68, 57, 14128838, 1, 141, 2, 2, 65.00, 0.00, 130.00, 99, '<b>kisan</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-26 11:03:53', '2018-03-26 11:03:53'),
(69, 57, 14128838, 1, 141, 2, 1, 65.00, 0.00, 65.00, 100, '<b>patra</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-26 11:03:53', '2018-03-26 11:03:53'),
(70, 58, 14734832, 1, 147, 3, 1, 45.00, 0.00, 45.00, 101, '<b>kisan</b><p>bbsr</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-27 12:37:05', '2018-03-27 12:37:05'),
(71, 59, 147646852, 1, 147, 64, 1, 5.00, 0.00, 5.00, 101, '<b>kisan</b><p>bbsr</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-27 19:58:56', '2018-03-27 19:58:56'),
(72, 60, 26669304, 1, 26, 66, 5, 20.00, 17.50, 117.50, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-03-29 19:02:08', '2018-03-29 19:02:08'),
(73, 60, 26669304, 1, 26, 66, 15, 20.00, 0.00, 300.00, 104, '<b>test2</b><p>test2</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-03-29 19:02:08', '2018-03-29 19:02:08'),
(74, 61, 141677853, 2, 141, 67, 1, 5.50, 0.00, 5.50, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-03-30 19:14:46', '2018-03-30 19:14:46'),
(75, 62, 26257629, 1, 26, 25, 1, 68.00, 0.00, 68.00, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-02 00:25:09', '2018-04-02 00:25:09'),
(76, 63, 152275524, 1, 152, 27, 1, 58.00, NULL, 58.00, 107, '<b>vfd</b><p>fgvd</p><p>Deira</p><p>Dubai</p><p>UAE</p>', 1, 0, 'buy product', '2018-04-02 01:04:16', '2018-04-02 01:04:16'),
(77, 64, 26107691, 2, 26, 10, 10, 45.50, 0.00, 455.00, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-02 13:22:40', '2018-04-02 13:22:40'),
(78, 64, 26107691, 2, 26, 10, 10, 45.50, 35.00, 490.00, 104, '<b>test2</b><p>test2</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-02 13:22:40', '2018-04-02 13:22:40'),
(79, 65, 153567542, 1, 153, 56, 1, 45.00, NULL, 45.00, 109, '<b>xfgh</b><p>xfgh</p><p>Deira</p><p>Dubai</p><p>UAE</p>', 1, 0, 'buy product', '2018-04-02 13:30:34', '2018-04-02 13:30:34'),
(80, 66, 154562488, 1, 154, 56, 1, 45.00, NULL, 45.00, 110, '<b>x</b><p>thoughtsphere.com,SPECIMENS Building,HIG-45,II Floor,Jayadev Vihar,Bhubaneswar</p><p>Deira</p><p>Dubai</p><p>UAE</p>', 1, 0, 'buy product', '2018-04-02 13:38:56', '2018-04-02 13:38:56'),
(81, 67, 26714301, 2, 26, 71, 1, 1.00, 3.50, 4.50, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-02 15:19:10', '2018-04-02 15:19:10'),
(82, 67, 26714301, 2, 26, 71, 1, 1.00, 0.00, 1.00, 108, '<b>fgs</b><p>sdf</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-02 15:19:10', '2018-04-02 15:19:10'),
(83, 68, 26255420, 1, 26, 25, 5, 68.00, 17.50, 357.50, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-03 20:49:36', '2018-04-03 20:49:36'),
(84, 69, 15126283, 1, 151, 2, 1, 65.00, 3.50, 68.50, 102, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-04 18:00:59', '2018-04-04 18:00:59'),
(85, 69, 15126283, 1, 151, 2, 0, 65.00, 0.00, 0.00, 105, '<b>test</b><br/>gfgdf<br/>Al Gharbia<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-04 18:00:59', '2018-04-04 18:00:59'),
(86, 69, 15126283, 1, 151, 2, 1, 65.00, 2.00, 67.00, 112, '<b>bbsr</b><br/>jayadev Bihar<br/>Jumeirah<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-04-04 18:00:59', '2018-04-04 18:00:59'),
(87, 70, 26259597, 1, 26, 25, 1, 68.00, 3.50, 71.50, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-04 21:13:54', '2018-04-04 21:13:54'),
(88, 71, 2629386, 1, 26, 2, 2, 65.00, 7.00, 137.00, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-04 21:14:38', '2018-04-04 21:14:38'),
(89, 72, 26565085, 1, 26, 56, 1, 45.00, 3.50, 48.50, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-04 21:17:06', '2018-04-04 21:17:06'),
(90, 73, 141565735, 1, 141, 56, 1, 45.00, 0.00, 45.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 11:17:53', '2018-04-05 11:17:53'),
(91, 73, 141565735, 1, 141, 56, 1, 45.00, 1.50, 46.50, 99, '<b>kisan</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 11:17:53', '2018-04-05 11:17:53'),
(92, 74, 141712453, 2, 141, 71, 1, 10.00, 0.00, 10.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 11:45:35', '2018-04-05 11:45:35'),
(93, 74, 141712453, 2, 141, 71, 1, 10.00, 1.50, 11.50, 99, '<b>kisan</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 11:45:35', '2018-04-05 11:45:35'),
(94, 75, 26104533, 2, 26, 10, 10, 45.50, 35.00, 490.00, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p><p>UAE</p>', 1, 0, 'Buy Auctiom product', '2018-04-05 12:48:33', '2018-04-05 12:48:33'),
(95, 76, 141719973, 1, 141, 71, 1, 30.00, 0.00, 30.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 15:51:57', '2018-04-05 15:51:57'),
(96, 77, 141715736, 1, 141, 71, 1, 30.00, 0.00, 30.00, 99, '<b>kisan</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 15:57:05', '2018-04-05 15:57:05'),
(97, 78, 141569261, 1, 141, 56, 1, 45.00, 1.50, 46.50, 99, '<b>kisan</b><p>bbsr</p><p>Dubai Marina</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 16:38:30', '2018-04-05 16:38:30'),
(98, 79, 141719287, 1, 141, 71, 1, 30.00, 0.00, 30.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-05 16:40:56', '2018-04-05 16:40:56'),
(99, 80, 2625850, 1, 26, 2, 45, 65.00, 67.50, 2992.50, 111, '<b>fgxdg</b><br/>zxfg<br/>Dubai Marina<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-04-06 19:39:04', '2018-04-06 19:39:04'),
(100, 81, 140569881, 1, 140, 56, 2, 45.00, 0.00, 90.00, 98, '<b>rgrtg</b><br/>sergr<br/>Deira<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:26:49', '2018-04-06 21:26:49'),
(101, 82, 26561400, 1, 26, 56, 2, 45.00, 7.00, 97.00, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:28:16', '2018-04-06 21:28:16'),
(102, 82, 26561400, 1, 26, 56, 0, 45.00, 0.00, 0.00, 104, '<b>test2</b><br/>test2<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:28:16', '2018-04-06 21:28:16'),
(103, 83, 26564800, 1, 26, 56, 2, 45.00, 7.00, 97.00, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:35:39', '2018-04-06 21:35:39'),
(104, 84, 26257479, 1, 26, 25, 8, 68.00, 28.00, 572.00, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:36:53', '2018-04-06 21:36:53'),
(105, 85, 26251559, 1, 26, 25, 8, 68.00, 28.00, 572.00, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:37:03', '2018-04-06 21:37:03'),
(106, 86, 26275111, 1, 26, 27, 5, 58.00, 17.50, 307.50, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 21:56:51', '2018-04-06 21:56:51'),
(107, 87, 151104648, 2, 151, 10, 100, 45.50, 350.00, 4900.00, 102, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p><p>UAE</p>', 1, 0, 'Buy Auctiom product', '2018-04-06 22:41:23', '2018-04-06 22:41:23'),
(108, 88, 151568485, 1, 151, 56, 2, 45.00, 7.00, 97.00, 102, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-06 22:50:52', '2018-04-06 22:50:52'),
(109, 89, 15122677, 1, 151, 2, 2, 65.00, 7.00, 137.00, 102, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-07 12:22:55', '2018-04-07 12:22:55'),
(110, 89, 15122677, 1, 151, 2, 0, 65.00, 0.00, 0.00, 105, '<b>test</b><br/>gfgdf<br/>Al Gharbia<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-07 12:22:55', '2018-04-07 12:22:55'),
(111, 90, 26258273, 1, 26, 25, 25, 68.00, 87.50, 1787.50, 103, '<b>test</b><br/>test<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-07 19:17:43', '2018-04-07 19:17:43'),
(112, 90, 26258273, 1, 26, 25, 25, 68.00, 87.50, 1787.50, 104, '<b>test2</b><br/>test2<br/>Al Ain<br/>Abu Dhabi<br/>UAE', 1, 0, 'buy product', '2018-04-07 19:17:43', '2018-04-07 19:17:43'),
(113, 90, 26258273, 1, 26, 25, 25, 68.00, 0.00, 1700.00, 108, '<b>fgs</b><br/>sdf<br/>Deira<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-04-07 19:17:43', '2018-04-07 19:17:43'),
(114, 90, 26258273, 1, 26, 25, 25, 68.00, 37.50, 1737.50, 111, '<b>fgxdg</b><br/>zxfg<br/>Dubai Marina<br/>Dubai<br/>UAE', 1, 0, 'buy product', '2018-04-07 19:17:43', '2018-04-07 19:17:43'),
(115, 91, 141711186, 2, 141, 71, 1, 1.00, 0.00, 1.00, 92, '<b>xfh</b><p>xh</p><p>Deira</p><p>Dubai</p>', 1, 0, 'buy product', '2018-04-11 20:15:14', '2018-04-11 20:15:14'),
(116, 92, 58256423, 1, 58, 25, 30, 68.00, 90.00, 2130.00, 22, '<b>Bhubaneswar branch</b><p>Jayadev Vihar,Bhubaneswar</p><p>Central Capital District</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-29 12:33:07', '2018-04-29 12:33:07'),
(117, 92, 58256423, 1, 58, 25, 30, 68.00, 0.00, 2040.00, 23, '<b>test</b><p>tets adreesss </p><p>Central Capital District</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-04-29 12:33:07', '2018-04-29 12:33:07'),
(118, 93, 5833317, 1, 58, 3, 30, 45.00, 90.00, 1440.00, 22, '<b>Bhubaneswar branch</b><p>Jayadev Vihar,Bhubaneswar</p><p>Central Capital District</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-05-04 13:34:54', '2018-05-04 13:34:54'),
(119, 93, 5833317, 1, 58, 3, 30, 45.00, 90.00, 1440.00, 23, '<b>test</b><p>tets adreesss </p><p>Central Capital District</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-05-04 13:34:54', '2018-05-04 13:34:54'),
(120, 94, 2638216, 1, 26, 3, 1, 45.00, 3.50, 48.50, 103, '<b>test</b><p>test</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-05-09 16:40:26', '2018-05-09 16:40:26'),
(121, 94, 2638216, 1, 26, 3, 1, 45.00, 3.50, 48.50, 104, '<b>test2</b><p>test2</p><p>Al Ain</p><p>Abu Dhabi</p>', 1, 0, 'buy product', '2018-05-09 16:40:26', '2018-05-09 16:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `sps__view_notification`
--

CREATE TABLE `sps__view_notification` (
  `id` int(123) NOT NULL,
  `user_id` int(123) DEFAULT NULL,
  `notification_type` int(12) DEFAULT NULL COMMENT '1->notification 2->user 3->orders 4->product request',
  `lastseen` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__view_notification`
--

INSERT INTO `sps__view_notification` (`id`, `user_id`, `notification_type`, `lastseen`, `status`) VALUES
(4, 1, 3, '2018-05-12 17:10:41', 1),
(5, 1, 2, '2018-07-30 21:01:26', 1),
(6, 1, 4, '2018-07-30 21:03:03', 1),
(7, 26, 1, '2018-04-29 12:46:37', 1),
(8, 140, 1, '2018-03-28 15:18:06', 1),
(9, 1, 1, '2018-03-28 20:15:05', 1),
(10, 59, 1, '2018-03-29 15:16:43', 1),
(11, 2, 3, '2018-03-29 10:41:11', 1),
(12, 2, 2, '2018-03-28 11:46:27', 1),
(13, 2, 4, '2018-03-28 12:00:13', 1),
(14, 3, 3, '2018-03-28 12:11:20', 1),
(15, 3, 4, '2018-03-28 11:46:51', 1),
(16, 3, 2, '2018-03-29 10:40:18', 1),
(17, 59, 3, '2018-03-28 12:24:09', 1),
(18, 138, 1, '2018-03-28 19:51:40', 1),
(19, 144, 1, '2018-03-28 20:12:50', 1),
(20, 58, 1, '2018-03-28 20:15:35', 1),
(21, 147, 1, '2018-03-29 10:44:04', 1),
(22, 84, 1, '2018-03-29 19:58:44', 1),
(23, 117, 1, '2018-03-29 13:31:58', 1),
(24, 151, 1, '2018-04-07 18:22:08', 1),
(25, 141, 1, '2018-04-07 18:23:52', 1),
(26, 155, 1, '2018-04-04 12:54:49', 1),
(27, 156, 1, '2018-04-04 13:09:59', 1),
(28, 79, 1, '2019-06-20 19:03:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sps__warehouse`
--

CREATE TABLE `sps__warehouse` (
  `id` int(121) NOT NULL,
  `id_exchange` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook_link` varchar(200) DEFAULT NULL,
  `linkedin_link` varchar(200) DEFAULT NULL,
  `twitter_link` varchar(200) DEFAULT NULL,
  `address` text,
  `city` varchar(50) DEFAULT NULL,
  `state` int(12) DEFAULT NULL,
  `country` int(12) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `latitude` varchar(200) DEFAULT NULL,
  `longitude` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL COMMENT 'Id user who add this warehouse / exchange'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__warehouse`
--

INSERT INTO `sps__warehouse` (`id`, `id_exchange`, `name`, `description`, `phone`, `email`, `facebook_link`, `linkedin_link`, `twitter_link`, `address`, `city`, `state`, `country`, `zip`, `latitude`, `longitude`, `status`, `add_date`, `ip`, `added_by`) VALUES
(2, 1, 'warehouse1', 'warehouse1', '7895689745', 'warehouse1@gmail.com', 'www.facebook.com/warehouse1', '', '', 'Rd No 1, Rasulgarh, Bhubaneswar, Odisha 751007, India', '16', 3, 1, '751013', '20.2935299925281', '85.85823464550776', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sps__warehouse_slot`
--

CREATE TABLE `sps__warehouse_slot` (
  `id` int(123) NOT NULL,
  `warehouse_id` int(80) NOT NULL,
  `rack_number` varchar(123) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sps__zone`
--

CREATE TABLE `sps__zone` (
  `id` int(11) NOT NULL,
  `warehouse_id` int(1) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(80) DEFAULT NULL,
  `description` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `distance` float(10,2) DEFAULT NULL,
  `carton_price` float(10,2) DEFAULT NULL,
  `cbm_price` decimal(10,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1->Active,2->In-active',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sps__zone`
--

INSERT INTO `sps__zone` (`id`, `warehouse_id`, `name`, `code`, `description`, `city`, `state`, `country`, `distance`, `carton_price`, `cbm_price`, `status`, `add_date`) VALUES
(1, 2, 'Zone1', 'Z001', 'Serves Bur Dubai Area', '5', '6', '1', 45.00, 2.50, '44.64', 1, '2017-12-21 07:05:54'),
(2, 2, 'Zone2', 'Z002', 'Serves Deira and vicinities', '6', '6', '1', 55.00, 2.50, '44.64', 2, '2017-12-27 09:58:02'),
(3, 2, 'Zone3', 'Z003', 'Serves Sheikh Zayed Road and Downtown Dubai', '7', '6', '1', 40.00, 2.00, '35.71', 1, '2017-12-21 07:06:04'),
(4, 2, 'Zone4', 'Z004', 'Serves Jumeirah Area', '8', '6', '1', 40.00, 2.00, '35.71', 1, '2017-12-21 07:06:04'),
(5, 2, 'Zone5', 'Z005', 'Serves Burj al Arab and vicinities', '9', '6', '1', 35.00, 2.00, '35.71', 1, '2017-12-21 07:06:04'),
(6, 2, 'Zone6', 'Z006', 'Serves The Palm Jumeirah and Dubai Marina', '10', '6', '1', 25.00, 1.50, '26.79', 1, '2017-12-27 09:53:01'),
(7, 2, 'Zone7', 'Z007', 'serves most of the Inner Suburb', '11', '6', '1', 20.00, 1.50, '26.79', 1, '2017-12-27 10:14:42'),
(8, 2, 'Zone8', 'Z008', 'Serves Sharjah City', '12', '4', '1', 65.00, 2.50, '44.64', 1, '2017-12-27 10:14:42'),
(9, 2, 'Zone9', 'Z009', 'Serves most of the other towns', '13', '4', '1', 80.00, 4.00, '71.43', 1, '2017-12-27 10:14:42'),
(10, 2, 'Zone10', 'Z010', 'Serves Central Capital District- Abu Dhabi', '14', '3', '1', 100.00, 3.00, '53.57', 1, '2017-12-27 10:14:42'),
(11, 2, 'Zone11', 'Z011', 'Serves Al Gharbia & Western Region- Abu Dhabi', '15', '3', '1', 110.00, 4.00, '71.43', 1, '2017-12-27 10:14:42'),
(12, 2, 'Zone12', 'Z012', 'Serves Al Ain & Eastern Region- Abu Dhabi', '16', '3', '1', 120.00, 3.50, '62.50', 1, '2018-01-11 11:29:49'),
(13, NULL, 'gfchn', 'cvgnfgc', 'cfgbn', '6', '6', '1', 546.00, 54.00, '5.00', 1, '2018-03-08 11:18:52'),
(14, NULL, 'dfgdfx', 'xcfgbc', 'xfhbf', '6', '6', '1', 540.00, 57.00, '54.00', 1, '2018-03-08 11:19:28'),
(15, NULL, 'Sfasdzffv', 'zxdfvz', 'zfvdv', '6', '6', '1', 423.00, 14.00, '23.00', 1, '2018-03-17 18:05:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sps__bank`
--
ALTER TABLE `sps__bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__buyer_product_requirement`
--
ALTER TABLE `sps__buyer_product_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__categories`
--
ALTER TABLE `sps__categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__city`
--
ALTER TABLE `sps__city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__configuration`
--
ALTER TABLE `sps__configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__country`
--
ALTER TABLE `sps__country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__exchange`
--
ALTER TABLE `sps__exchange`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__gst`
--
ALTER TABLE `sps__gst`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__logintrack`
--
ALTER TABLE `sps__logintrack`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__logs`
--
ALTER TABLE `sps__logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__migrations`
--
ALTER TABLE `sps__migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__notification`
--
ALTER TABLE `sps__notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__password_resets`
--
ALTER TABLE `sps__password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `sps__product`
--
ALTER TABLE `sps__product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__productdocuments`
--
ALTER TABLE `sps__productdocuments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__productimage`
--
ALTER TABLE `sps__productimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_bid`
--
ALTER TABLE `sps__product_bid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_buyer_wishprice`
--
ALTER TABLE `sps__product_buyer_wishprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_exchange_price`
--
ALTER TABLE `sps__product_exchange_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_exchange_price_bc`
--
ALTER TABLE `sps__product_exchange_price_bc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_fixed_sell`
--
ALTER TABLE `sps__product_fixed_sell`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__product_verifytrack`
--
ALTER TABLE `sps__product_verifytrack`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__request_product`
--
ALTER TABLE `sps__request_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__revenue`
--
ALTER TABLE `sps__revenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__seller_settlement`
--
ALTER TABLE `sps__seller_settlement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__state`
--
ALTER TABLE `sps__state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__testimonial`
--
ALTER TABLE `sps__testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__transaction`
--
ALTER TABLE `sps__transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__transport`
--
ALTER TABLE `sps__transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__users`
--
ALTER TABLE `sps__users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sps__users_address`
--
ALTER TABLE `sps__users_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__users_bank_details`
--
ALTER TABLE `sps__users_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__user_request_product`
--
ALTER TABLE `sps__user_request_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__user_settlement_address`
--
ALTER TABLE `sps__user_settlement_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__view_notification`
--
ALTER TABLE `sps__view_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__warehouse`
--
ALTER TABLE `sps__warehouse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__warehouse_slot`
--
ALTER TABLE `sps__warehouse_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sps__zone`
--
ALTER TABLE `sps__zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sps__bank`
--
ALTER TABLE `sps__bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sps__buyer_product_requirement`
--
ALTER TABLE `sps__buyer_product_requirement`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sps__categories`
--
ALTER TABLE `sps__categories`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sps__city`
--
ALTER TABLE `sps__city`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sps__configuration`
--
ALTER TABLE `sps__configuration`
  MODIFY `id` int(122) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__country`
--
ALTER TABLE `sps__country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sps__exchange`
--
ALTER TABLE `sps__exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sps__gst`
--
ALTER TABLE `sps__gst`
  MODIFY `id` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sps__logintrack`
--
ALTER TABLE `sps__logintrack`
  MODIFY `id` int(222) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__logs`
--
ALTER TABLE `sps__logs`
  MODIFY `id` int(222) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__migrations`
--
ALTER TABLE `sps__migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sps__notification`
--
ALTER TABLE `sps__notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1287;

--
-- AUTO_INCREMENT for table `sps__product`
--
ALTER TABLE `sps__product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `sps__productdocuments`
--
ALTER TABLE `sps__productdocuments`
  MODIFY `id` int(122) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `sps__productimage`
--
ALTER TABLE `sps__productimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `sps__product_bid`
--
ALTER TABLE `sps__product_bid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `sps__product_buyer_wishprice`
--
ALTER TABLE `sps__product_buyer_wishprice`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `sps__product_exchange_price`
--
ALTER TABLE `sps__product_exchange_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `sps__product_exchange_price_bc`
--
ALTER TABLE `sps__product_exchange_price_bc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sps__product_fixed_sell`
--
ALTER TABLE `sps__product_fixed_sell`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `sps__product_verifytrack`
--
ALTER TABLE `sps__product_verifytrack`
  MODIFY `id` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `sps__request_product`
--
ALTER TABLE `sps__request_product`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__revenue`
--
ALTER TABLE `sps__revenue`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__seller_settlement`
--
ALTER TABLE `sps__seller_settlement`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sps__state`
--
ALTER TABLE `sps__state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sps__testimonial`
--
ALTER TABLE `sps__testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sps__transaction`
--
ALTER TABLE `sps__transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `sps__transport`
--
ALTER TABLE `sps__transport`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sps__users`
--
ALTER TABLE `sps__users`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `sps__users_address`
--
ALTER TABLE `sps__users_address`
  MODIFY `id` int(122) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `sps__users_bank_details`
--
ALTER TABLE `sps__users_bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `sps__user_request_product`
--
ALTER TABLE `sps__user_request_product`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__user_settlement_address`
--
ALTER TABLE `sps__user_settlement_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `sps__view_notification`
--
ALTER TABLE `sps__view_notification`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `sps__warehouse`
--
ALTER TABLE `sps__warehouse`
  MODIFY `id` int(121) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sps__warehouse_slot`
--
ALTER TABLE `sps__warehouse_slot`
  MODIFY `id` int(123) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sps__zone`
--
ALTER TABLE `sps__zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
